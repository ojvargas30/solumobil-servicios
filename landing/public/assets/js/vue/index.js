// Aqui se requieren la logica de los componentes
import FormSearch from "./components/form-search.js"

window.env = {
    "API_PRODUCTS_URL": "http://solumobil.me/landing/products",
    "API_PRODUCTS_URL": "http://solumobil.me/landing/products"
}

const init = () => {

    Vue.component("form-search", FormSearch) // Aqui se requieren la vista de los componentes

    new Vue({ el: "#root-vue" })
}

document.addEventListener("DOMContentLoaded", init)