export default {

    template: "#form-search",

    data() {
        return {
            products: [],
            viewSearcher: false,
            API_PRODUCTS_URL: window.env.API_PRODUCTS_URL
        }
    },

    mounted() {
        console.log("Vue Ready")
    },

    methods: {
        getProducts(e) {

            fetch(this.API_PRODUCTS_URL)

                .then(res => res.json())

                .then(({ data }) => {

                    console.log(data);

                    this.products = data

                    this.viewSearcher = true
                })
        }
    }
}