let formMsg = document.getElementById('formWhatsapp');
let btnWhatsapp = document.getElementById('btnWhatsapp');
let btnSend = document.querySelector('#send');

if (btnWhatsapp) btnWhatsapp.addEventListener('click', () => formMsg.classList.toggle('d-none'));
if (btnSend) btnSend.addEventListener('click', sendMessage);

function sendMessage() {
  let name = document.getElementById('name').value;
  let msg = document.getElementById('msg').value;
  let url = // %0A es un salto de linea de whatsapp
    "https://api.whatsapp.com/send?phone=573208557457&text=Nombre: %0A" + name + "%0A%0AMensaje: %0A" + msg + "%0A";
  window.open(url);
  btnWhatsapp.click();
}


let bttop = document.getElementById('back-to-top');
window.onscroll = function () { scrollFunction() };

function scrollFunction()
{
  if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
    // bttop.style.display = 'none';
    bttop.classList.remove('d-none');
    bttop.classList.add('d-block');
  } else {
    // bttop.style.display = 'block';
    bttop.classList.remove('d-block');
    bttop.classList.add('d-none');
  }
}

$(document).ready(function () {
  $('.btn-mobile').on('click', function () {
    var menu = $('.navigation-list');
    if (menu.hasClass('navigation-list-show')) {
      menu.removeClass('navigation-list-show');
    } else {
      menu.addClass('navigation-list-show');
    }
  });

  $('#btnSend').click(function () {

    let errores = '';

    // Validado Nombre ==============================
    if ($('#names').val() == '') {
      errores += '<p>Escriba un nombre</p>';
      $('#names').css("border-bottom-color", "#F14B4B")
    } else {
      $('#names').css("border-bottom-color", "#d1d1d1")
    }

    // Validado Correo ==============================
    if ($('#email').val() == '') {
      errores += '<p>Ingrese un correo</p>';
      $('#email').css("border-bottom-color", "#F14B4B")
    } else {
      $('#email').css("border-bottom-color", "#d1d1d1")
    }

    // Validado Mensaje ==============================
    if ($('#mensaje').val() == '') {
      errores += '<p>Escriba un mensaje</p>';
      $('#mensaje').css("border-bottom-color", "#F14B4B")
    } else {
      $('#mensaje').css("border-bottom-color", "#d1d1d1")
    }

    // Validado Mensaje ==============================
    if ($('#phone').val() == '') {
      errores += '<p>Escriba un mensaje</p>';
      $('#phone').css("border-bottom-color", "#F14B4B")
    } else {
      $('#phone').css("border-bottom-color", "#d1d1d1")
    }
  });
});


// COOL PLANTILLA
$(function () {

  // Get the form.
  var form = $('#ajax-contact');

  // Get the messages div.
  var formMessages = $('#form-messages');

  // Set up an event listener for the contact form.
  $(form).submit(function (e) {
    // Stop the browser from submitting the form.
    e.preventDefault();

    // Serialize the form data.
    var formData = $(form).serialize();

    // Submit the form using AJAX.
    $.ajax({
      type: 'POST',
      url: $(form).attr('action'),
      data: formData
    })
      .done(function (response) {
        // Make sure that the formMessages div has the 'success' class.
        $(formMessages).removeClass('error');
        $(formMessages).addClass('success');

        // Set the message text.
        $(formMessages).text(response);

        // Clear the form.
        $('#name').val('');
        $('#email').val('');
        $('#subject').val('');
        $('#message').val('');
      })
      .fail(function (data) {
        // Make sure that the formMessages div has the 'error' class.
        $(formMessages).removeClass('success');
        $(formMessages).addClass('error');

        // Set the message text.
        if (data.responseText !== '') {
          $(formMessages).text(data.responseText);
        } else {
          $(formMessages).text('Oops! An error occured and your message could not be sent.');
        }
      });

  });

});


// function ilumina(where) {
//   where.addEventListener('mouseover', function (e) {
//     e.target.style.backgroundColor = "#EfEfEf";
//   }, false);
// }

// // Iluminacion de inputs
// let icorreo = document.getElementById('correo');
// let clave1 = document.getElementById('clave');
// let clave2 = document.getElementById('passConfirm');

// ilumina(icorreo,clave1,clave2);
// ilumina(icorreo);
// ilumina(clave1);
// ilumina(clave2);



// EVITAR RECARGO
$(function () {
  var form = $('#f1');
  form.submit(function (e) {
    $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize(),
      success: function () {
      },
      timeout: 20000
    });
    e.preventDefault();
  });
});


function swal(msg = '', type = '', text = '', timer = 3000, showConfirmButton = false, position = '', background = '', padding = '', width = null) {
  Swal.fire({
    icon: type,
    title: `<span class="pad">${msg}</span>`,
    imageUrl: 'https://unsplash.it/400/200',
    imageWidth: 300,
    imageHeight: 100,
    imageAlt: '¡Splash!',
    text: text,
    width: width,
    position: position,
    showConfirmButton: showConfirmButton,
    timer: timer,
    padding: padding,
    backdrop: background,
    footer: '<a href="mailto:oscarjaviervargas@hotmail.com">Reportar problema</a>'
  })
}


// DROPDOWN CLASSIC
// $("document").ready(function () {
//   $("#tableColumns-filter").kendoSortable({
//     filter: ".sortable",
//     hint: function (element) {
//       return element.clone().addClass("hint");
//     },
//     placeholder: function (element) {
//       return element.clone().addClass("placeholder");
//     }
//   });

//   $("ul.dropdown-menu li a").click(function () {
//     var item = $(this);
//     var itemStatus = item.find("span.glyphicon");
//     if (typeof itemStatus !== "undefined") {
//       if (
//         itemStatus.hasClass("glyphicon-pencil") &&
//         (itemStatus.hasClass("text-danger") ||
//           itemStatus.hasClass("text-success"))
//       ) {
//         if (itemStatus.hasClass("text-danger")) {
//           itemStatus.removeClass("text-danger").addClass("text-success");
//         } else {
//           itemStatus.removeClass("text-success").addClass("text-danger");
//         }
//       } else if (
//         itemStatus.hasClass("glyphicon-remove") ||
//         itemStatus.hasClass("glyphicon-ok")
//       ) {
//         if (itemStatus.hasClass("glyphicon-remove")) {
//           itemStatus
//             .removeClass("glyphicon-remove text-danger")
//             .addClass("glyphicon-ok text-success");
//         } else {
//           itemStatus
//             .removeClass("glyphicon-ok text-success")
//             .addClass("glyphicon-remove text-danger");
//         }
//       }
//     }
//   });

//   $(".dropdown-toggle").on("click", function (event) {
//     $(this).parent().find(".dropdown-menu").slideToggle();
//     //event.stopPropagation();
//   });

//   $(".dropdown-toggle").focusout(function (event) {
//     $(this).parent().find(".dropdown-menu").slideToggle();
//     event.stopPropagation();
//   });

//   $(window).on("click", function () {
//     $(".dropdown-menu").slideUp();
//   });

//   $(".dropdown-menu").on("click", function (event) {
//     event.stopPropagation();
//   });
// });








