function sendDataAjaxPost(arrCampos = []) {

    callLoader(true);

    $.post(url, sendParams, function (response) {
        // console.log(response.resp.errorMessage);
        // console.log(response.resp);
        console.log(response);

        callLoader(false);

    }, 'json').fail(function (jqXHR, textStatus, errorThrown) { // por el bucle

        console.log(errorThrown.responseText);

        callLoader(false);

        if (jqXHR.status === 0)

            alert('Not connect: Verify Network.');

        else if (jqXHR.status == 404)

            alert('Requested page not found [404]');

        else if (jqXHR.status == 500)

            alert('Internal Server Error [500].');

        else if (textStatus === 'parsererror')

            if (jqXHR.responseText.includes('SQLSTATE[23000]')) {

                // swalN("Ya existe un usuario con ese correo electrónico", 'error', 'Intenta de nuevo con otro correo');
                toastE("Ya existe un usuario con ese correo electrónico");
            } else

                alert('Requested JSON parse failed.' + jqXHR.responseText); // varios envios y duplicidad

        else if (textStatus === 'timeout')

            alert('Time out error.');

        else if (textStatus === 'abort')

            alert('Ajax request aborted.');

        else

            alert('Uncaught Error: ' + jqXHR.responseText);

        alert(msgJsonFail);// al terminar la app se comenat
        alert("Algo salio mal");

    });
}