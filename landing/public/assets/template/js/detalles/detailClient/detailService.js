
function getDetail() {

    var arrDetails = []

    let idDetSer = $("#id_revision_servicio_PK").val();
    let dCliente = $("#descripcion_cliente").val();
    let dirLugar = $("#direccion_lugar").val();

    if (dirLugar != '' && dCliente != '') {
        arrDetails.push({
            'id_revision_servicio_PK': idDetSer,
            'descripcion_cliente': dCliente,
            'direccion_lugar': dirLugar,
        })
    } else {
        swalError("Debes llenar todos los campos");
    }
    return arrDetails
}

