function ele(element = '') {
    let ele = document.getElementById(element);
    return ele;
}

function eve(element = {}, func, eventListener = 'click') {
    element.addEventListener(eventListener, func);
}

function eveFull(element = {}, func) {
    element.addEventListener('click', func);
    element.addEventListener('change', func);
    element.addEventListener('keyup', func);
    element.addEventListener('keydown', func);
    element.addEventListener('keypress', func);
    element.addEventListener('input', func);
}
