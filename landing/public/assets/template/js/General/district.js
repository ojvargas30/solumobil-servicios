
function getDistrict() {

    var arrDistrict = []

    let idBarrio = $("#id_barrio_PK").val();
    let nombreBarrio = $("#nombre_barrio").val();

    if (nombreBarrio != '') {
        arrDistrict.push({
            'id_barrio_PK': idBarrio,
            'nombre_barrio': nombreBarrio
        })
    } else {
        swal('Diligencie el campo de barrio');
    }
    return arrDistrict
}

// enviar elementos al back
$("#submitGhost-2").click(function (e) {
    e.preventDefault()

    let url = "?controller=district&method=save"
    let params = {
        districts: getDistrict()
    }

    // metodo POST USANDO AJAX PARA ENVIAR LA INFO AL BACKEND
    $.post(url, params, function (response) {
        if (typeof response.error !== 'undefined') {
            swal('Oops...', 'error', '¡Algo salio mal con el guardado del barrio!');
        } else {
            // recargo del select, poner lo que se inserta
            let { id_barrio_PK, nombre_barrio } = response.registerInsert;
            let select = document.getElementById("id_barrio_FK"),
                option = document.createElement("option");
            option.value = id_barrio_PK;
            option.textContent = nombre_barrio;
            select.appendChild(option);

            // Seleccion
            document.getElementById("seleccione").removeAttribute("selected");
            option.setAttribute("selected", "");

            console.log(response)

            swal('Barrio generado', 'success', 'y seleccionado :D');

            //Auto clic
            let clicBtnNew = document.getElementById('ghost-child-2');
            clicBtnNew.click();

        }
    }, 'json').fail(function (error) {
        if (error.responseText.includes('SQLSTATE[23000]')) {
            alert('Ya existe un barrio con ese nombre', 'error');
        }
        // swal('<i class="fas fa-flushed"></i>  Caramba...', 'error', '¡Algo salio mal!');
        // console.log("Inserción fallida (" + error.responseText + ")")
    });
});

