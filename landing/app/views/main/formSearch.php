<!-- Este componente se usa en layouts -> navbar -->
<div class="wrap-search-form">
    <form action="#" id="form-search-top" name="form-search-top">

        <input type="text" @input="getProducts($event)" name="search" placeholder="Buscar en Solumobil" autocomplete="off" />

        <button form="form-search-top" type="button">
            <i class="fa fa-search" aria-hidden="true"></i>
        </button>
        <div class="wrap-list-cate">
            <input type="hidden" name="product-cate" value="0" id="product-cate" />
            <a href="#" class="link-control txt-sm">Todas las categorías</a>
            <!-- Aqui debe ir un for de las categorías -->
            <ul class="list-cate">
                <li class="level-0">Todas las categorías</li>
                <li class="level-1">-Electronics</li>
                <li class="level-2">Batteries & Chargens</li>
                <li class="level-2">Headphone & Headsets</li>
                <li class="level-2">Mp3 Player & Acessories</li>
                <li class="level-1">-Smartphone & Table</li>
                <li class="level-2">Batteries & Chargens</li>
                <li class="level-2">Mp3 Player & Headphones</li>
                <li class="level-2">Table & Accessories</li>
                <li class="level-1">-Electronics</li>
                <li class="level-2">Batteries & Chargens</li>
                <li class="level-2">Headphone & Headsets</li>
                <li class="level-2">Mp3 Player & Acessories</li>
                <li class="level-1">-Smartphone & Table</li>
                <li class="level-2">Batteries & Chargens</li>
                <li class="level-2">Mp3 Player & Headphones</li>
                <li class="level-2">Table & Accessories</li>
            </ul>
        </div>
    </form>
</div>