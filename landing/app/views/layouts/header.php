<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Solumobil</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,700italic,900,900italic&amp;subset=latin,latin-ext" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open%20Sans:300,400,400italic,600,600italic,700,700italic&amp;subset=latin,latin-ext" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= URL_PROJECT ?>/public/assets/template/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= URL_PROJECT ?>/public/assets/template/css/font-awesome.min.css" />
    <!-- <link rel="stylesheet" type="text/css" href="<?= URL_PROJECT ?>/public/assets/template/css/bootstrap.min.css" /> -->
    <link rel="stylesheet" type="text/css" href="<?= URL_PROJECT ?>/public/assets/template/css/owl.carousel.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= URL_PROJECT ?>/public/assets/template/css/chosen.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= URL_PROJECT ?>/public/assets/template/css/color-01.css" />
    <link rel="stylesheet" type="text/css" href="<?= URL_PROJECT ?>/public/assets/template/css/style.css" />

    <!-- Bootstarp5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"></script>
    <!-- use vue js -->
    <script src="<?= URL_PROJECT ?>/public/libs/vue/vue.js"></script>
    <script type="module" src="<?= URL_PROJECT ?>/public/assets/js/vue/index.js"></script>
</head>

<body>
    <div id="root">