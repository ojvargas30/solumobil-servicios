<?php

$env = file_get_contents("./.env");
foreach (explode("\n", $env) as $dotenven) {
    if (strlen($dotenven) > 3) {
        $key = explode("=", $dotenven)[0];
        $value = explode("=", $dotenven)[1];
        $_ENV[$key] = $value;
    }
}

define("DB_HOST", trim($_ENV["DB_HOST"]));
define("DB_NAME", trim($_ENV["DB_NAME"]));
define("DB_USER", trim($_ENV["DB_USER"]));
define("DB_PASSWORD", trim($_ENV["DB_PASSWORD"]));
define("DB_DRIVER",  trim($_ENV["DB_DRIVER"]));
define("DB_CHARSET", trim($_ENV["DB_CHARSET"]));

// datos del servidor
define("PROTOCOL", $_SERVER["REQUEST_SCHEME"]);

define("SEPARATOR", "\\");
define("APP_PATH", dirname(dirname(__FILE__)) . "\\");
define("SYS_PATH", dirname(dirname(dirname(__FILE__))) . "\\core\\");
$url = PROTOCOL . '://' . $_SERVER["HTTP_HOST"] . '/' . str_replace(basename($_SERVER["PHP_SELF"]), "", $_SERVER["PHP_SELF"]);
define("URL_PROJECT", substr($url, 0, strlen($url) - 1));
define("URL_APP", PROTOCOL . '://' . $_SERVER["HTTP_HOST"] . '/' . dirname(str_replace(basename($_SERVER["PHP_SELF"]), "", $_SERVER["PHP_SELF"])) . "/app/");
