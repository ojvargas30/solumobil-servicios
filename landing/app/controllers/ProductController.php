<?php
class ProductController extends Controller
{
    public function __construct()
    {

    }

    public function all()
    {
        $products = Product::all(true, 10); // Get all products

        return httpResponse($products)->json();
    }
}
