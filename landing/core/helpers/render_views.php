<?php

function view($view, $data = [], $all = true) // $all true si quieres mostrar el navbar
{
    extract($data);

    $view = str_replace('.', '\\', $view);

    if (file_exists(APP_PATH . "views" . SEPARATOR . "{$view}.php")) {

        require_once APP_PATH . "views" . SEPARATOR . "layouts" . SEPARATOR . "header.php";

        if ($all) { // $all true si quieres mostrar el navbar
            require_once APP_PATH . "views" . SEPARATOR . "layouts" . SEPARATOR . "navbar.php";
        }

        require_once APP_PATH . "views" . SEPARATOR . "{$view}.php"; // Archivo que se recibe por parametro
        require_once APP_PATH . "views" . SEPARATOR . "layouts" . SEPARATOR . "footer.php";

    } else {
        exit("View not found");
    }
}

function include_view($view, $data = []) // Únicamente una vista
{
    extract($data);
    $view = str_replace('.', '\\', $view);
    require_once APP_PATH . "views" . SEPARATOR . "{$view}.php"; // Archivo que se recibe por parametro
}
