drop database if exists solumobil;
create database if not exists solumobil;
use solumobil;

create table records(
id_record_PK   int not null auto_increment,
username       varchar(75) null,
userrole       int null,
useridentify   varchar(20) null,
accessdate 	   datetime,
countaccess    int,
device         varchar(550) null,
primary key (id_record_PK)
);

create view vw_records 
as select r.*,count(countaccess) as totalaccess 
from records r 
group by useridentify;
create procedure sp_records() select * from vw_records;

create table comentario(
id_comentario_PK     			int not null auto_increment,
nombre varchar(35),
fecha date,
descripcion text,
primary key (id_comentario_PK)
);

insert into comentario values(null, "Oscar", "2002-12-02", "ExcelenteExcelenteExcelenteExcelenteExcelente"),
(null, "Luna", "2002-12-21", "dfdfsdfsdfsgfgsd"),
(null, "Claudelby", "2002-12-12", "ExcelenteExcsdfsdelenteExcelenteExcelenteExcelente");

create table estado(
id_estado_PK     			int not null auto_increment,
nombre_estado               varchar(50),
primary key (id_estado_PK)
);

create table rol(
id_rol_PK                 int not null auto_increment,
estado_rol	              bit not null,
nombre_rol                varchar(100) not null,
primary key (id_rol_PK)
);

create table usuario
(
id_usuario_PK             int not null auto_increment,
id_rol_FK                 int not null,
estado_usu                int(2) not null,
correo        			  varchar(200) unique not null,
clave	                  varchar(200) not null, -- usado tambien como id de google
fecha_creacion            datetime default current_timestamp null,
keyreg                    varchar(150) null,
intentos			      int(7) default 0,
foto	                  text null,
date_birth	              date null,
sexo		              char(1) null,
foreign key (id_rol_FK)   references    rol     (id_rol_PK),
primary key (id_usuario_PK)
);

create procedure sp_get_users()
SELECT u.*,r.* FROM usuario u
inner join rol r on r.id_rol_PK = u.id_rol_FK;

create procedure sp_users(IN pcorreo VARCHAR(200))
select intentos from usuario where correo = pcorreo;

create table requisiciones(
id_requisicion_PK     			int not null auto_increment,
id_usuario_FK 					int null,
titulo 							varchar(35) not null,
fecha							date null,
desc_material					text null,
cantidad						MEDIUMINT default 1 not null,
costo_unitario					decimal(8,0) null,
costo_total					    decimal(10,0) null,
primary key (id_requisicion_PK),
foreign key (id_usuario_FK)   references    usuario     (id_usuario_PK)
);

create view vw_requision as select r.*, u.* from requisiciones r
inner join usuario u on u.id_usuario_PK = r.id_usuario_FK;

create table barrio_cliente(
id_barrio_PK              int not null auto_increment,
estado_bc             	  bit not null,
nombre_barrio             varchar(300) unique not null,
primary key (id_barrio_PK)
);

create table cliente(
id_cliente_PK             int not null auto_increment,
id_barrio_FK              int default null,
id_usuario_FK             int unique not null,
tipo_doc_cli              varchar(40) default null,
nombre_cliente            varchar(100) null,
apellido_cliente          varchar(100) null,
num_id_cli                varchar(50) unique default null,
telefono                  varchar(15) unique default null,
direccion_residencia      varchar(130) default null,
primary key (id_cliente_PK),
foreign key (id_barrio_FK)   references    barrio_cliente     (id_barrio_PK),
foreign key (id_usuario_FK)   references    usuario     (id_usuario_PK)
);

create table tecnico(
id_tecnico_PK             int not null auto_increment,
tipo_doc_tec              varchar(40) not null,
id_usuario_FK             int unique not null,
nombre_tecnico            varchar(100) not null,
apellido_tecnico          varchar(100) not null,
telefono                  varchar(20) unique null,
num_id_tec         		  varchar(20) unique not null,
desc_tecnico              varchar(300) null,
direccion_residencia      varchar(130) null,
foreign key (id_usuario_FK)   references    usuario     (id_usuario_PK),
primary key (id_tecnico_PK)
);

create table marca_artefacto(
id_marca_artefacto_PK     int not null auto_increment,
estado_ma                 bit not null,
nombre_marca              varchar(150) unique not null,
primary key (id_marca_artefacto_PK)
);

create table categoria_artefacto(
id_categoria_artefacto_PK   int not null auto_increment,
estado_ca            	    bit not null,
nombre_categoria_artefacto  varchar(100) unique not null,
primary key (id_categoria_artefacto_PK)
);

create table artefacto(
id_artefacto_PK           int not null auto_increment,
id_marca_artefacto_FK     int not null,
id_categoria_artefacto_FK int not null,
estado_artefacto          bit not null,
modelo                    varchar(250) unique not null,
caracteristicas           varchar(1000) null,
imei                      varchar(16) null,
serie                     varchar(15) null,
foto_artefacto            varchar(200) null,
foreign key (id_marca_artefacto_FK)     references  marca_artefacto      (id_marca_artefacto_PK),
foreign key (id_categoria_artefacto_FK)     references  categoria_artefacto      (id_categoria_artefacto_PK),
primary key (id_artefacto_PK)
);

create table categoria_servicio(
id_categoria_servicio_PK     int not null auto_increment,
estado_cs                    bit not null,
nombre_cs    				 varchar(200) not null unique,
precio_cs				     float null,
primary key (id_categoria_servicio_PK)
);

create table servicio(
id_servicio_PK            int not null auto_increment,
id_cliente_FK		      int not null,
id_estado_servicio_FK     int not null,
fecha_peticion            date null,
hora_peticion             time null,
foreign key (id_estado_servicio_FK)       references  estado      (id_estado_PK),
foreign key (id_cliente_FK)       references  cliente    (id_cliente_PK),
primary key (id_servicio_PK)
);

create table artefacto_servicio(
id_artefacto_servicio_PK   int not null auto_increment,
id_artefacto_FK 		  int not null,
id_servicio_FK 		      int not null,
foreign key (id_artefacto_FK)       references  artefacto      (id_artefacto_PK),
foreign key (id_servicio_FK)       references  servicio      (id_servicio_PK),
primary key (id_artefacto_servicio_PK)
);

create table categorias_servicios(
id_categorias_servicios_PK   int not null auto_increment,
id_servicio_FK 		      int not null,
id_categoria_servicio_FK 		      int not null,
foreign key (id_servicio_FK)       references  servicio      (id_servicio_PK),
foreign key (id_categoria_servicio_FK)       references  categoria_servicio (id_categoria_servicio_PK),
primary key (id_categorias_servicios_PK)
);

create table evidencia_servicio(
id_evidencia_PK   int not null auto_increment,
id_servicio_FK 		      int not null,
url_foto_evidencia 		  text not null,
foreign key (id_servicio_FK)       references  servicio      (id_servicio_PK),
primary key (id_evidencia_PK)
);

create table revision_servicio(
id_revision_servicio_PK   int not null auto_increment,
id_servicio_FK 		      int not null,
is_software               bit null,
is_hardware               bit null,
descripcion_cliente       varchar(700) null,
diagnostico               varchar(650) not null,
abono					  float null,
saldo					  float null,
fecha_hora_cita_virtual   datetime null,
link_cita_virtual		  text null,
costo_revision            float null,
costo_domicilio           float null,
costo_total               float not null,
pagado					  int(2) default 1,
id_tecnico_FK	     	  int null,
fecha_inicio              datetime default null,
fecha_fin                 datetime default null,
fecha_encuentro           date null,
hora_encuentro            time null,
direccion_lugar           varchar(300) null,
-- evidencia                 varchar(300) null,
foreign key (id_servicio_FK)       references  servicio      (id_servicio_PK),
foreign key (id_tecnico_FK)       references  tecnico      (id_tecnico_PK),
primary key (id_revision_servicio_PK)
);

create table tecnico_servicio(
id_tecnico_servicio_PK   int not null auto_increment,
id_servicio_FK 		      int not null,
id_tecnico_asignado_FK    int null,
foreign key (id_servicio_FK)       references  servicio      (id_servicio_PK),
foreign key (id_tecnico_asignado_FK)       references  tecnico              (id_tecnico_PK),
primary key (id_tecnico_servicio_PK)
);

insert into estado values
(null,"En proceso"),
(null,"En espera"),
(null,"Finalizado"),
(null,"Cancelado");

insert into rol values
(null,1,'tecnico administrador'),
(null,1,'tecnico empleado'),
(null,1,'cliente'),
(null,0,'cliente Externo');

insert into barrio_cliente values(null,1,'Villa Teresita - Engativa'),
									 (null,1,'Villa Gladys - Engativa'),
									 (null,1,'Linterama -Engativa');


insert into marca_artefacto values(null,1,'Samsung'),
									 (null,1,'Huawei'),
									 (null,1,'Motorola'),
									 (null,1,'Asus'),
									 (null,1,'Lg'),
                                     (null,1,'Avvio'),
									 (null,1,'Blu'),
									 (null,1,'Xiaomi'),
									 (null,1,'Nokia'),
                                     (null,1,'Alcatel'),
                                     (null,1,'Iphone'),
                                     (null,1,'HTC'),
                                     (null,1,'Sony'),
                                     (null,1,'HP'),
                                     (null,1,'Dell'),
                                     (null,1,'Krono'),
                                     (null,1,'MyMmobile'),
                                     (null,1,'Zte'),
                                     (null,1,'apple'),
                                     (null,1,'lanix'),
                                     (null,1,'microsoft'),
                                     (null,1,'vodafone'),
                                     (null,1,'Azumi'),
                                     (null,1,'Bt-Speaker'),
                                     (null,1,'Philips'),
									 (null,1,'Lenovo');

insert into categoria_artefacto values(null,1,'Celular'),
									 (null,1,'Tablet'),
									 (null,1,'Computador'),
									 (null,1,'Portatil'),
									 (null,1,'Reproductor de sonido'),
									 (null,1,'Otro');

insert into artefacto values
(null,'1','1',1,'Samsung Galaxy J5 Prime (SM-G570M)',
'El Samsung Galaxy J5 Prime es una variante del Galaxy J5 con una pantalla HD de
 5 pulgadas, procesador quad-core a 1.4GHz, 2GB de RAM, 16GB de almacenamiento
 interno expandible, cámara principal de 13 megapixels con flash LED, cámara
 frontal de 5 MP, lector de huellas dactilares, chasis metálico y Android 6.0 ...',
 '1234567891234567','123456789123456',''),
(null,'2','2',1,'Tablet Krono 7031 Model Network',
'Especificaciones de MODEL-7021.
PANTALLA --- LCD 7'' 1024X600 Multi Touch (Capacitive 5 puntos)
PROCESADOR --- CPU MTK 8312 a 1.3 GHz GPU Mall 400MP.
MEMORIA --- 8GB flash memory, 512MB RAM.
SISTEMA OPERATIVO --- Android Jelly Bean 4.2.2.
CONECTIVIDAD --- Wi-Fi IEEE 802.11b/g/n, Bluetooth 3.0/Conexion 2G.',
'1234567891234567','123456789123456',''),
(null,'3','3',0,'Tablet Krono 7032 Model Network',
'Especificaciones de MODEL-7021.
PANTALLA --- LCD 7'' 1024X600 Multi Touch (Capacitive 5 puntos)
PROCESADOR --- CPU MTK 8312 a 1.3 GHz GPU Mall 400MP.
MEMORIA --- 8GB flash memory, 512MB RAM.
SISTEMA OPERATIVO --- Android Jelly Bean 4.2.2.
CONECTIVIDAD --- Wi-Fi IEEE 802.11b/g/n, Bluetooth 3.0/Conexion 2G.',
'1234567891234567','123456789123456','');


insert into categoria_servicio values
(null,1,'Desconozco del servicio, te lo encargo',null),
(null,1,'Cambio de pantalla',null),
                                      (null,1,'Cambio de puerto de carga',null),
                                      (null,1,'Cambio de puerto plu',null),
                                      (null,1,'Cambio de tapa trasera',null),
                                      (null,1,'Cambio de visor',null),
                                      (null,1,'Quitar cuenta Google',null),
                                      (null,1,'Hard reset (reseteo de fabrica)',23423.234),
                                      (null,1,'Cambio de sistema operativo (Flasheo)',null),
                                      (null,1,'Creacion de cuentas en apps',45.23);

SET lc_time_names = 'es_ES';

CREATE VIEW sec_servicio AS SELECT s.*,c.*,e.*,u.*
FROM servicio s
INNER JOIN estado e ON s.id_estado_servicio_FK = e.id_estado_PK
INNER JOIN cliente c ON s.id_cliente_FK = c.id_cliente_PK
INNER JOIN usuario u ON c.id_usuario_FK = u.id_usuario_PK;

CREATE VIEW service_evidence AS SELECT evi.*
FROM servicio s
INNER JOIN evidencia_servicio evi ON evi.id_servicio_FK = s.id_servicio_PK;
/*
create procedure sp_service_evidence
(IN id_service INT)select se.* from service_evidence se where id_service = se.id_servicio_PK;
*/
create procedure sp_service_evidence
()select * from service_evidence;


CREATE VIEW vw_cliente AS SELECT b.*,u.*,c.*
FROM cliente c
LEFT JOIN barrio_cliente b ON c.id_barrio_FK  = b.id_barrio_PK
INNER JOIN usuario u        ON c.id_usuario_FK = u.id_usuario_PK;

CREATE PROCEDURE sp_get_client_by_id
(IN id_cliente INT)
SELECT * FROM vw_cliente c
WHERE c.id_usuario_PK = id_cliente;

CREATE VIEW vw_utd_tecnico AS SELECT u.*,t.*
FROM tecnico t
INNER JOIN usuario u ON t.id_usuario_FK = u.id_usuario_PK;

CREATE PROCEDURE sp_get_tecnico_by_id(IN id_tecnico_en_sesion INT)
SELECT * FROM vw_utd_tecnico t 
WHERE t.id_usuario_PK = id_tecnico_en_sesion;

CREATE VIEW vw_r_usuario AS SELECT u.*,r.*
FROM usuario u
INNER JOIN rol r ON u.id_rol_FK = r.id_rol_PK;

CREATE VIEW vw_mce_artefacto AS SELECT a.*,m.*,ca.*
FROM artefacto a
INNER JOIN marca_artefacto m ON a.id_marca_artefacto_FK = m.id_marca_artefacto_PK
INNER JOIN categoria_artefacto ca ON a.id_categoria_artefacto_FK = ca.id_categoria_artefacto_PK;

CREATE VIEW vw_cat_ser AS SELECT * FROM categoria_servicio;
CREATE VIEW vw_barrio AS SELECT * FROM barrio_cliente;
CREATE VIEW vw_rol AS SELECT * FROM rol;
CREATE VIEW vw_marcas as select * from marca_artefacto where estado_ma = 1;
CREATE VIEW vw_tipos as select * from categoria_artefacto where estado_ca = 1;

CREATE PROCEDURE sp_getArtServicebyid(IN id_servicio_PK int)
SELECT *, s.id_servicio_PK, ars.id_servicio_FK, a.id_artefacto_PK
FROM artefacto_servicio ars
INNER JOIN artefacto a 				ON a.id_artefacto_PK = ars.id_artefacto_FK
INNER JOIN marca_artefacto ma 		ON ma.id_marca_artefacto_PK = ma.id_marca_artefacto_PK
INNER JOIN categoria_artefacto ca 	ON ca.id_categoria_artefacto_PK = ca.id_categoria_artefacto_PK
INNER JOIN servicio s 				ON s.id_servicio_PK = ars.id_servicio_FK
WHERE ars.id_servicio_FK = id_servicio_PK;

CREATE PROCEDURE sp_getCatServicebyid(IN id_servicio_PK int)
SELECT *, s.id_servicio_PK, csss.id_servicio_FK, cas.id_categoria_servicio_PK
FROM categorias_servicios csss
INNER JOIN servicio s 				ON s.id_servicio_PK = csss.id_servicio_FK
INNER JOIN categoria_servicio cas 	ON cas.id_categoria_servicio_PK = csss.id_categoria_servicio_FK
WHERE csss.id_servicio_FK = id_servicio_PK;

CREATE PROCEDURE sp_gettechservicebyid(IN id_servicio_PK int)
SELECT *
FROM tecnico_servicio ts
INNER JOIN servicio s 				ON s.id_servicio_PK = ts.id_servicio_FK
INNER JOIN tecnico t 				ON t.id_tecnico_PK = ts.id_tecnico_asignado_FK
INNER JOIN usuario u 				ON u.id_usuario_PK = t.id_usuario_FK
INNER JOIN rol r 					ON r.id_rol_PK = u.id_rol_FK
WHERE ts.id_servicio_FK = id_servicio_PK;

CREATE PROCEDURE sp_getdetservicebyid(IN id_servicio_PK int)
SELECT *,s.id_servicio_PK, rs.id_servicio_FK
FROM revision_servicio rs
INNER JOIN servicio s ON s.id_servicio_PK = rs.id_servicio_FK
WHERE rs.id_servicio_FK = id_servicio_PK;

create procedure sp_validate_user
(IN v_correo VARCHAR(200), IN v_clave VARCHAR(200))
SELECT u.*, r.nombre_rol as rol FROM usuario u
INNER JOIN rol r ON r.id_rol_PK = u.id_rol_FK
WHERE u.correo = v_correo
AND u.clave = v_clave;

create view vw_tecnicos_servicio_activo
as select t.id_tecnico_PK,
r.fecha_encuentro, r.hora_encuentro, r.fecha_hora_cita_virtual
from tecnico_servicio ts
INNER JOIN revision_servicio r ON ts.id_servicio_FK = r.id_servicio_FK
INNER JOIN tecnico t ON ts.id_tecnico_asignado_FK = t.id_tecnico_PK
INNER JOIN servicio s ON s.id_servicio_PK = ts.id_servicio_FK
WHERE s.id_estado_servicio_FK = 1 OR s.id_estado_servicio_FK = 2;

create procedure sp_technician_crossover
(IN id_tecnico INT)
select vwts.* from vw_tecnicos_servicio_activo vwts
WHERE id_tecnico = vwts.id_tecnico_PK;

call sp_technician_crossover(1);

create procedure sp_client_service
(IN id int)
select r.*,sec.* from revision_servicio r
inner join sec_servicio sec on sec.id_servicio_PK = r.id_servicio_FK
where sec.id_cliente_FK = id and sec.id_estado_servicio_FK != 4
order by sec.id_servicio_PK desc;

create procedure sp_clientartservice
(IN id int)
select ars.*,sec.* from vw_artefacto_servicio ars
inner join sec_servicio sec on sec.id_servicio_PK = ars.id_servicio_FK
where sec.id_cliente_FK = id and sec.id_estado_servicio_FK != 4
order by sec.id_servicio_PK desc;

create procedure sp_clienttechservice
(IN id int)
select ts.*,sec.* from vw_tecnico_servicio ts
inner join sec_servicio sec on sec.id_servicio_PK = ts.id_servicio_FK
where sec.id_cliente_FK = id and sec.id_estado_servicio_FK != 4
order by sec.id_servicio_PK desc;

create procedure sp_clientdisservice
(IN id int)
select css.*,sec.* from vw_categorias_servicios css
inner join sec_servicio sec on sec.id_servicio_PK = css.id_servicio_FK
where sec.id_cliente_FK = id and sec.id_estado_servicio_FK != 4
order by sec.id_servicio_PK desc;

create view vw_tecnico_servicio as select ts.*,t.* from tecnico_servicio ts
inner join tecnico t on t.id_tecnico_PK = ts.id_tecnico_asignado_FK;

create view vw_categorias_servicios as select css.*,cs.* from categorias_servicios css
inner join categoria_servicio cs on cs.id_categoria_servicio_PK = css.id_categoria_servicio_FK;

create view vw_artefacto_servicio as select ars.*,a.*,ma.*,ca.* from artefacto_servicio ars
inner join artefacto a on a.id_artefacto_PK = ars.id_artefacto_FK
inner join marca_artefacto ma on ma.id_marca_artefacto_PK = a.id_marca_artefacto_FK
inner join categoria_artefacto ca on ca.id_categoria_artefacto_PK = a.id_categoria_artefacto_FK;

create procedure sp_client_servicebyid
(IN id int, IN idCli int)
select r.*,sec.* from revision_servicio r
inner join sec_servicio sec on sec.id_servicio_PK = r.id_servicio_FK
where r.id_revision_servicio_PK = id and sec.id_cliente_FK = idCli;

create procedure sp_client_by_id
(IN id_cliente int)
select c.*,b.* from cliente c
INNER JOIN barrio_cliente b on b.id_barrio_PK = c.id_barrio_FK
where c.id_cliente_PK = id_cliente;

call sp_client_by_id(1);
SELECT MAX(id_barrio_PK) as id_barrio_PK FROM barrio_cliente;

create procedure sp_client_tech
(IN id int)
select ta.*,t.* from tecnico_servicio ta
inner join servicio s on s.id_servicio_PK = ta.id_servicio_FK
inner join tecnico t  on t.id_tecnico_PK  = ta.id_tecnico_asignado_FK
where s.id_cliente_FK = id;

CREATE PROCEDURE sp_getartservice()
SELECT *, s.id_servicio_PK, ars.id_servicio_FK, a.id_artefacto_PK
FROM artefacto_servicio ars
INNER JOIN artefacto a 				ON a.id_artefacto_PK = ars.id_artefacto_FK
INNER JOIN marca_artefacto ma 		ON ma.id_marca_artefacto_PK = ma.id_marca_artefacto_PK
INNER JOIN categoria_artefacto ca 	ON ca.id_categoria_artefacto_PK = ca.id_categoria_artefacto_PK
INNER JOIN servicio s 				ON s.id_servicio_PK = ars.id_servicio_FK;

CREATE PROCEDURE sp_getcatservice()
SELECT *
FROM categorias_servicios csss
INNER JOIN servicio s 				ON s.id_servicio_PK = csss.id_servicio_FK
INNER JOIN categoria_servicio cas 	ON cas.id_categoria_servicio_PK = csss.id_categoria_servicio_FK;

CREATE PROCEDURE sp_gettechservice()
SELECT *
FROM tecnico_servicio ts
INNER JOIN servicio s 				ON s.id_servicio_PK = ts.id_servicio_FK
INNER JOIN tecnico t 				ON t.id_tecnico_PK = ts.id_tecnico_asignado_FK
INNER JOIN usuario u 				ON u.id_usuario_PK = t.id_usuario_FK
INNER JOIN rol r 					ON r.id_rol_PK = u.id_rol_FK;

CREATE PROCEDURE sp_getdetservice()
SELECT *
FROM revision_servicio rs
INNER JOIN servicio s 				ON s.id_servicio_PK = rs.id_servicio_FK;

create view vw_ep as select count(id_servicio_PK) as en_proceso from servicio where id_estado_servicio_FK = 1;
create view vw_ee as select count(id_servicio_PK) as en_espera from servicio where id_estado_servicio_FK = 2;
create view vw_ef as select count(id_servicio_PK) as finalizado from servicio where id_estado_servicio_FK = 3;
create view vw_ec as select count(id_servicio_PK) as cancelado from servicio where id_estado_servicio_FK = 4;

create table aud_revision
(
id int not null auto_increment primary key,
id_revision int null,
id_servicio int null,

desc_c text null,
diagn    varchar(650) null,
direccion varchar(300) null,
soft 			bit null,
hard               bit null,
costo_r            float null,
costo_d         float null,
costo_t              float null,
fecha_i            datetime null,
fecha_f               datetime null,
fecha_e          date null,
hora_e           time null,
evidencia                text null,

desc_c_n text null,
diagno_n               varchar(650) null,
direccion_n varchar(300) null,
soft_n 			bit null,
hard_n               bit null,
costo_r_n            float null,
costo_d_n          float null,
costo_t_n             float null,
fecha_i_n             datetime null,
fecha_f_n                datetime null,
fecha_e_n          date null,
hora_e_n           time null,
evidencia_n                text null,
usuario             varchar(35) null,
fecha               datetime null
);

create trigger tg_revision after update on revision_servicio for each row
insert into aud_revision
values(
null,old.id_revision_servicio_PK,old.id_servicio_FK,old.descripcion_cliente,old.diagnostico,
old.direccion_lugar,old.is_software,old.is_hardware,old.costo_revision,old.costo_domicilio,old.costo_total,
old.fecha_inicio,old.fecha_fin,old.fecha_encuentro,old.hora_encuentro,

new.descripcion_cliente,new.diagnostico,new.direccion_lugar,new.is_software,new.is_hardware,
new.costo_revision,new.costo_domicilio,new.costo_total,
new.fecha_inicio,new.fecha_fin,new.fecha_encuentro,new.hora_encuentro,current_user(),now());

create view vw_aud_service as select * from aud_revision group by id_revision;

-- Total Ganancias
create view vw_weekly_income
as select round(sum(costo_total)) ganancias from revision_servicio;

select costo_total,fecha_fin from revision_servicio;

-- Ganancias Mensuales
create view vw_january_gain
as select round(sum(costo_total)) january_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'January'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_february_gain
as select round(sum(costo_total)) february_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'February'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_march_gain
as select round(sum(costo_total)) march_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'March'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_may_gain
as select round(sum(costo_total)) may_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'May'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_april_gain
as select round(sum(costo_total)) april_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'April'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_june_gain
as select round(sum(costo_total)) june_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'June'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_july_gain
as select round(sum(costo_total)) july_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'July'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_august_gain
as select round(sum(costo_total)) august_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'August'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_september_gain
as select round(sum(costo_total)) september_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'September'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_october_gain
as select round(sum(costo_total)) october_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'October'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_november_gain
as select round(sum(costo_total)) november_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'November'
AND YEAR(NOW()) = YEAR(fecha_fin);

create view vw_december_gain
as select round(sum(costo_total)) december_gain from revision_servicio
WHERE fecha_fin IS NOT NULL AND MONTHNAME(fecha_fin) = 'December'
AND YEAR(NOW()) = YEAR(fecha_fin);

create procedure sp_monthly_gain
()select *
from
vw_january_gain,
vw_february_gain,
vw_march_gain,
vw_may_gain,
vw_april_gain,
vw_june_gain,
vw_july_gain,
vw_august_gain,
vw_september_gain,
vw_october_gain,
vw_november_gain,
vw_december_gain;

-- Ganancias de la semana actual
create view vw_weekly_gain
as select round(sum(costo_total)) weekly_gain, WEEKOFYEAR(NOW()) semana from revision_servicio
WHERE fecha_fin IS NOT NULL group by semana;

create procedure sp_weekly_gain() select * from vw_weekly_gain;

-- Ganancia diaria
create view vw_daily_gain
as select round(sum(costo_total)) daily_gain, NOW() dia from revision_servicio
WHERE fecha_fin IS NOT NULL AND DAY(fecha_fin) = DAY(NOW());

create procedure sp_daily_gain() select * from vw_daily_gain;

/*
SELECT WEEK(NOW());
SELECT WEEKOFYEAR(NOW());
*/

-- Aparato con más conflictos
-- select id_artefacto_FK from artefacto_servicio;
create view vw_conflict_apparatus
as select id_artefacto_FK artefacto_con_mas_conflictos,
count(*) total from artefacto_servicio
group by id_artefacto_FK
limit 1;

create view vw_conflict_apparatus_data as
select * from artefacto where id_artefacto_PK =
(select artefacto_con_mas_conflictos from vw_conflict_apparatus);

create procedure sp_conflict_apparatus()
select * from vw_conflict_apparatus_data;


-- empleados que mas servicios ha hecho
-- select id_tecnico_asignado_FK from tecnico_servicio;

create view vw_best_employee
as select id_tecnico_asignado_FK best_employee,
count(*) total from tecnico_servicio
group by id_tecnico_asignado_FK
limit 1;

create view vw_best_employee_data as
select * from tecnico where id_tecnico_PK =
(select best_employee from vw_best_employee);

create procedure sp_best_employee()
select * from vw_best_employee_data;


-- El cliente que mas servicios haya comprado
-- select id_cliente_FK from servicio;

create view vw_best_client
as select id_cliente_FK as best_client,
count(*) as total from servicio
group by id_cliente_FK
limit 1;

-- select id_cliente_FK, count(*) total from servicio where not (id_cliente_FK < id_cliente_FK) group by id_cliente_FK limit 1;
select best_client from vw_best_client;


-- PAYpal
create procedure get_service_by_id
(IN id_servicio INT)select r.* from revision_servicio r 
inner join servicio s on s.id_servicio_PK = r.id_servicio_FK
where id_servicio = r.id_servicio_FK;
 
call get_service_by_id(3);

/*
CREATE TABLE rol_actualizado
(
anterior_id_rol_PK INTEGER,
nuevo_id_rol_PK INTEGER,
anterior_estado_rol BIT,
nuevo_estado_rol BIT,
anterior_nombre_rol VARCHAR(100),
nuevo_nombre_rol VARCHAR(100),
fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER tr_rol_bu BEFORE UPDATE ON rol FOR EACH ROW INSERT INTO rol_actualizado
(anterior_id_rol_PK, nuevo_id_rol_PK,
anterior_estado_rol, nuevo_estado_rol,
anterior_nombre_rol, nuevo_nombre_rol,
fecha)
VALUES
(
OLD.id_rol_PK, NEW.id_rol_PK,
OLD.estado_rol, NEW.estado_rol,
OLD.nombre_rol, NEW.nombre_rol, NOW());

CREATE TABLE servicio_actualizado
(
anterior_id_servicio_PK INTEGER,
nuevo_id_servicio_PK INTEGER,
anterior_id_estado_servicio_FK INTEGER,
nuevo_id_estado_servicio_FK INTEGER,
anterior_fecha_peticion DATE,
nuevo_fecha_peticion DATE,
anterior_hora_peticion TIME,
nuevo_hora_peticion TIME,
anterior_costo_servicio FLOAT,
nuevo_costo_servicio FLOAT,
id_cliente_FK INT,
fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER TR_servicio_BU BEFORE UPDATE ON servicio FOR EACH ROW INSERT INTO servicio_actualizado
(
anterior_id_servicio_PK, nuevo_id_servicio_PK,
anterior_id_estado_servicio_FK, nuevo_id_estado_servicio_FK,
anterior_fecha_peticion, nuevo_fecha_peticion,
anterior_hora_peticion, nuevo_hora_peticion,
id_cliente_FK,
fecha_actualizacion
)
VALUES
(
OLD.id_servicio_PK, NEW.id_servicio_PK,
OLD.id_estado_servicio_FK, NEW.id_estado_servicio_FK,
OLD.fecha_peticion, NEW.fecha_peticion,
OLD.hora_peticion, NEW.hora_peticion,
id_cliente_FK, NOW()
);
*/
-- REPORTES
/*create trigger Categorias after insert on categorias_servicios
for each row
insert into aud_revision(id_revision,id_servicio,descripcion_cliente,direccion_lugar,usuario,
fecha) values(old.id_revision_servicio_PK,old.id_servicio_FK,old.descripcion_cliente,old.direccion_lugar,current_user(),now());
*/
-- SP n° max de revisiones de un servicio (2)
/*CREATE PROCEDURE sp_getDetailsCount(IN id_servicio int)
SELECT *, MAX(rs.id_revision_servicio_PK) as maxRs
FROM servicio s
INNER JOIN revision_servicio rs ON rs.id_revision_servicio_PK = rs.id_revision_servicio_PK
WHERE id_servicio_PK = id_servicio;

call sp_getDetailsCount(1);

-- SP conteo citaciones de una revisión (3)
CREATE PROCEDURE sp_getCitationsCount(IN id_revision int)
SELECT *, COUNT(c.id_citacion_PK) as maxCitaciones
FROM revision_servicio rs
INNER JOIN Citacion c ON rs.id_citacion_FK = c.id_citacion_PK
WHERE id_revision_servicio_PK = id_revision;

call sp_getCitationsCount(1);
-- UPDATE rol SET nombre_rol = 'cliente Suspendido' WHERE id_rol_PK = 4;
-- SELECT * FROM rol_actualizado;
/*TRIGGER QUE GUARDA LOS DATOS DEL rol AL ACTUALIZAR*/
/*CREATE TABLE servicio_actualizado
(
anterior_id_servicio_PK INTEGER,
nuevo_id_servicio_PK INTEGER,
anterior_id_estado_servicio_FK INT,
nuevo_id_estado_servicio_FK INT,
anterior_nombre_rol VARCHAR(100),
nuevo_nombre_rol VARCHAR(100),
fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER TR_rol_BU BEFORE UPDATE ON rol FOR EACH ROW INSERT INTO rol_actualizado
(anterior_id_rol_PK, nuevo_id_rol_PK,
anterior_estado_rol, nuevo_estado_rol,
anterior_nombre_rol, nuevo_nombre_rol,
fecha)
VALUES
(
OLD.id_rol_PK, NEW.id_rol_PK,
OLD.estado_rol, NEW.estado_rol,
OLD.nombre_rol, NEW.nombre_rol, NOW());
*/

-- SP Promedio de ventas servicios totales (4)
/*CREATE PROCEDURE sp_getSalesService()
SELECT *, AVG(costo_servicio) as media
FROM servicio;
*/
-- descuento a los clientes frecuentes. mas de un millon de pesos. mas de 5 servicios
-- eso del if en que casos se usa teniendo en cuenta que el lenguaje sea PHP no lo pueda hacer
-- si el cliente es frecuente que le haga un descuento
-- si hay pocas unidades en stock si profe depende el capital de la empresa
-- Hacer procedimientos para el SI combinados con vistas

/*
create procedure SP_getAllClientService
(IN id int)
select r.*,sec.*,ars.*,css.*,ts.* from revision_servicio r
inner join SEC_servicio sec on sec.id_servicio_PK = r.id_servicio_FK
inner join VW_artefacto_servicio ars on sec.id_servicio_PK = ars.id_servicio_FK
inner join VW_categorias_servicios css on sec.id_servicio_PK = css.id_servicio_FK
inner join VW_tecnico_servicio ts on sec.id_servicio_PK = ts.id_servicio_FK
where sec.id_cliente_FK = id and sec.id_estado_servicio_FK != 4
group by id_revision_servicio_PK
order by sec.id_servicio_PK desc;

call SP_getAllClientService(3);
*/ -- solo uno de los detalles

/*
insert into Citacion values(null,1,'cll 52 chapinero alto'),
                           (null,1,'cll 40 sur 23-34 suba'),
                           (null,1,'carrera 51 fontibon');
*/
/*
insert into servicio values(null,'1','1','2020-01-01','12:12:12',50000),
                           (null,'2','2','2020-01-01','12:12:12',90000),
                           (null,'3','3','2020-01-01','12:12:12',10000),
                           (null,'4','3','2020-01-01','12:12:12',520000),
						   (null,'5','4','2020-01-01','12:12:12',40000);


insert into revision_servicio values
(null,'1',1,1,'Trabajo en construccion y se ensucio','Hay que cambiar el display',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87'),
(null,'2',0,0,'Se me cayó','Hay que cambiar el display',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87'),
(null,'3',1,1,'Se mojo','Hay que cambiar el display',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87'),
(null,'4',0,0,'Lo golpearon','Hay que cambiar el display',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87'),
(null,'5',1,1,'Le movieron algo allí','Hay que cambiar el puerto de carga',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87');
*/
/*INSERT servicio, estado_servicio, citación*/
/*Pensar en ponerle un estado al tecnico mas adelante*/
/*INCLUIR IF NOT EXIST, UNIQUE, ENCRIPTAR*/
/*ME FALTA INSERTAR DATOS*/


/**/

/*CONSULTAS MULTITABLA*/
/*select * from servicio inner join cliente on servicio.id_cliente_FK = cliente.id_cliente_PK; *//*servicio Y cliente*/

/*obtener datos*/
/*
CREATE VIEW datos_servicio_obt AS SELECT Ser.id_servicio_PK,Ser.id_citacion_FK,Ser.id_cliente_FK,
Ser.id_tecnico_asignado_FK,Ser.descripcion,Ser.fecha_peticion,Ser.hora_peticion,Ser.valor_precio_servicio,
esta.tipo,detalle.diagnostico, tec.nombre_tecnico, cli.nombre_cliente, cita.fecha_encuentro, cita.hora_encuentro
FROM servicio Ser
INNER JOIN estado_servicio esta ON Ser.id_estado_servicio_FK = esta.id_estado_servicio_PK
INNER JOIN revision_servicio detalle ON Ser.id_revision_servicio_FK = detalle.id_revision_servicio_PK
INNER JOIN tecnico tec ON Ser.id_tecnico_asignado_FK = tec.id_tecnico_PK
INNER JOIN cliente cli ON Ser.id_cliente_FK = cli.id_cliente_PK
INNER JOIN Citacion cita ON Ser.id_citacion_FK = cita.id_citacion_PK;*/


/*METER EN UNA VISTA PARA SIMPLIFICAR*/
/*CREATE VIEW datos_servicio AS SELECT Ser.id_servicio_PK,Ser.id_citacion_FK,Ser.id_cliente_FK,
Ser.id_tecnico_asignado_FK,Ser.id_revision_servicio_FK,Ser.id_estado_servicio_FK,Ser.descripcion,
Ser.fecha_hora_peticion,Ser.valor_precio_servicio,cli.id_barrio_FK,cli.id_usuario_FK,
cli.id_tipo_documento_FK,cli.numero_documento,cli.telefono,cli.direccion_residencia,esta.tipo FROM servicio
INNER JOIN cliente ON servicio.id_cliente_FK = cliente.id_cliente_PK INNER JOIN estado_servicio
ON servicio.id_estado_servicio_FK = estado_servicio.id_estado_servicio_PK;*/

/*
CREATE VIEW SCCTRE_servicio AS SELECT
									s.id_servicio_PK, -- servicio
                                     s.diagnostico,
									 -- date_format(s.fecha_hora_peticion,'%W, %D de %M,\n Hora: %h:%i%p') AS fecha_peticion,
									 s.fecha_peticion AS fp,
                                     s.hora_peticion,
									 s.valor_precio_servicio,
									 s.id_citacion_FK,
                                     -- date_format(cita.fecha_hora_encuentro,'%W, %D de %M,\n Hora: %h:%i%p') AS fecha_cita, -- cita
									 cita.fecha_encuentro, -- citacion
                                     cita.hora_encuentro,
                                     cita.direccion_lugar,
									 s.id_tecnico_asignado_FK, -- tecnico
                                     t.nombre_tecnico,
                                     t.numero_documento,
                                     t.foto_tecnico,
                                     s.id_estado_FK, -- estado
                                     e.tipo,
                                     s.id_categoria_servicio_FK, -- categoria
									 cat.nombre_categoria
                                    -- SUM(valor_precio_servicio) AS ingresos, -- TCS_estadistica /*Estadisticas Iniciales*/
									-- ROUND(SUM(t.id_tecnico_PK/2 + c.id_cliente_PK/3),0) AS num_usuarios,
                                    -- ROUND(SUM(s.id_servicio_PK-s.id_servicio_PK*60/100),0) AS num_venta_total,
                                    -- ROUND(SUM(s.id_servicio_PK-s.id_servicio_PK*60/100),0) AS num_venta_semana,
                                    -- FLOOR(RAND()*(11-1)+1) AS num_random		*/
-- ------------------------------------------------------------------------------------------
/*
-- Consulta MEGAMultitabla - accediendo a 3 dimensiones
SELECT ma.nombre as marca, a.modelo,rs.diagnostico,rs.fecha_inicio,rs.fecha_fin,s.descripcion as servicio  from revision_servicio as rs
		       inner join servicio as s on s.id_servicio_PK = rs.id_servicio_FK
               inner join artefacto as a on a.id_artefacto_PK = rs.id_artefacto_FK
               inner join marca_artefacto as ma on ma.id_marca_artefacto_PK = ma.id_marca_artefacto_PK;
*/
-- ------------------------------------------------------------------------------------------
-- VW Detail 6 (no se necesita)
/*CREATE VIEW VW_ASCCT_detail AS SELECT r.*,a.*,s.*,c.*,ca.*,t.*
FROM revision_servicio r
INNER JOIN artefacto a ON r.id_artefacto_FK = a.id_artefacto_PK
INNER JOIN servicio s ON r.id_servicio_FK = s.id_servicio_PK
INNER JOIN Citacion c ON r.id_citacion_FK = c.id_citacion_PK
INNER JOIN categoria_servicio ca ON r.id_categoria_servicio_FK = ca.id_categoria_servicio_PK
INNER JOIN tecnico t ON r.id_tecnico_asignado_FK = t.id_tecnico_PK;

/*
CREATE PROCEDURE sp_getDetailbyid(IN id_servicio_PK int)
SELECT *, a.modelo as nfacto, s.id_servicio_PK,c.id_citacion_PK  -- voy aqui
FROM revision_servicio rs
INNER JOIN artefacto a 				ON a.id_artefacto_PK = rs.id_artefacto_FK
INNER JOIN servicio s 				ON s.id_servicio_PK = rs.id_servicio_FK
INNER JOIN Citacion c 				ON c.id_citacion_PK = rs.id_citacion_FK
INNER JOIN categoria_servicio cs 	ON cs.id_categoria_servicio_PK = rs.id_categoria_servicio_FK
INNER JOIN categoria_artefacto ca 	ON ca.id_categoria_artefacto_PK = ca.id_categoria_artefacto_PK -- added
INNER JOIN marca_artefacto ma 	    ON ma.id_marca_artefacto_PK = ma.id_marca_artefacto_PK         -- added
INNER JOIN tecnico t 				ON t.id_tecnico_PK = rs.id_tecnico_asignado_FK
WHERE rs.id_servicio_FK = id_servicio_PK; -- ese es el berraco parametro

CALL sp_getDetailbyid(2);*/
-- ----------------------------------------------------------------------------------------
/*
insert into cliente values
(null,'1','5','Cédula de Ciudadanía','Dario',
'Vargas','1121665778','3274368118','Crr 116 n 66- 87',''),
(null,'1','4','Cédula de Ciudadanía','Hernan',
'Duque','1221665778','3264368118','Crr 116 n 66- 87',''),
(null,'1','6','Cédula de Ciudadanía','Jose',
'Díaz','1321665778','3254368118','Crr 116 n 66- 87',''),
(null,'1','7','Cédula de Ciudadanía','Sandra','Guevara',
'1421665778','3244368118','Crr 116 n 66- 87',''),
(null,'1','8','Cédula de Ciudadanía','Alejandro','Suarez',
'1621665778','3234368118','Crr 116 n 66- 87',''),
(null,'1','9','Cédula de Ciudadanía','Aratza','Contreras',
'N/A','N/A','Crr 116 n 66- 87','');
*/
/*
insert into tecnico values(null,'CC Cédula de Ciudadanía','1','Rodolfo','Vargas','3208557457','80115506','assets/img/technicians/papa.jpg','Técnico en reparación de dispositivos movíles y electricista','Carrera 116C #66-87'),
						  (null,'CE Cédula de Extranjería','3','Victor','Guerrero','3112710141','27168101','assets/img/technicians/vic.jpg','Técnico en reparación de dispositivos movíles','Villa Teresita'),
						  (null,'CC Cédula de Ciudadanía','2','Óscar Javier','Vargas Díaz','3133043714','1000620103','assets/img/technicians/os.jpg','Desarrollador de software','Carrera 116C #66-87');
*/

/*
insert into usuario values
(null,'1',1,'vargas.rodolfo0627@gmail.com','123',''),
(null,'1',1,'oscarjaviervargas@hotmail.com','delfinesvoladores2020',''),
(null,'2',1,'victor2014guerrero@gmail.com','124',''),
(null,'3',1,'hduque@gmail.com','132',''),
(null,'3',1,'na1@gmail.com','0000000',''),
(null,'3',1,'na2@gmail.com','0000000',''),
(null,'3',1,'na3@gmail.com','0000000',''),
(null,'3',1,'na4@gmail.com','0000000',''),
(null,'3',1,'na5@gmail.com','0000000','');
*/

-- --------------------------- VMO AQUI

-- SP Detail citaction  -- (2)
/*CREATE PROCEDURE sp_getCitServicebyid(IN id_servicio_PK int)
SELECT *, s.id_servicio_PK, cs.id_servicio_FK, c.id_citacion_PK
FROM Citacion_servicio cs
INNER JOIN servicio s 				ON s.id_servicio_PK = cs.id_servicio_FK
INNER JOIN Citacion c 				ON c.id_citacion_PK = cs.id_citacion_FK
WHERE cs.id_servicio_FK = id_servicio_PK; -- ese es el berraco parametro*/

-- CALL sp_getCitServicebyid(2);

-- SP Details category -- (3)

/*CREATE PROCEDURE sp_getdetserviceClientbyid
(IN idser int,IN idcli int)
SELECT rs.*,sec.*
FROM revision_servicio rs
INNER JOIN SEC_servicio sec ON sec.id_servicio_PK = rs.id_servicio_FK
where sec.id_cliente_FK = idcli and rs.id_servicio_FK = idser;

CALL sp_getdetserviceClientbyid(2,2);
*/

/*CREATE PROCEDURE sp_getDetClientServicebyid
(IN idC int)
SELECT rs.*,sec.*
FROM revision_servicio rs
INNER JOIN SEC_servicio sec ON sec.id_servicio_PK = rs.id_servicio_FK
where sec.id_cliente_FK = idcli and rs.id_servicio_FK = idser;

CALL sp_getdetserviceClientbyid(2,2);
*/

-- VW VALIDAR usuario
/*
CREATE VIEW VW_validate_user AS SELECT u.*,t.*,c.*, r.nombre_rol as rol
						FROM tecnico t
                        INNER JOIN usuario u ON u.id_usuario_PK = t.id_usuario_FK
                        INNER JOIN cliente c ON c.id_rol_PK = c.id_rol_PK
                        INNER JOIN rol r ON r.id_rol_PK = t.id_rol_FK;
*/
-- START DETAILS WITH PARAMETER------------------------------------------------------

-- SP Detail artifact -- (1)

/*
create table Citacion_servicio(
id_citacion_servicio_PK   int not null auto_increment,
id_servicio_FK 		      int not null,
id_citacion_FK 		      int not null,
foreign key (id_servicio_FK)       references  servicio      (id_servicio_PK),
foreign key (id_citacion_FK)       references  Citacion              (id_citacion_PK),
primary key (id_citacion_servicio_PK)
);
*/

/*
create table Citacion(
id_citacion_PK            int not null auto_increment,
estado_citacion           bit not null,
direccion_lugar           varchar(300)  not null,
-- fecha_encuentro           date not null,
-- hora_encuentro            time null,
primary key (id_citacion_PK)
);
*/


