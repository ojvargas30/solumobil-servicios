function swalError(title = 'Error', text = "", linkSupport = 'https://api.whatsapp.com/send?phone=573133043714&text=HolaTuveUnErrorConLaPlataformaAlGenerarUnServicio') {
  Swal.fire({
    icon: 'error',
    title: title,
    text: text,
    footer: `<a href=${linkSupport}>¿Por qué tengo este problema?</a>`
  })
}

function swalInfo(title = 'Atención', text = "", linkSupport = 'https://api.whatsapp.com/send?phone=573133043714&text=HolaTuveUnErrorConLaPlataformaAlGenerarUnServicio') {
  Swal.fire({
    icon: 'info',
    title: title,
    text: text,
    footer: `<a href=${linkSupport}>¿Por qué tengo este problema?</a>`
  })
}

// function swalQuestion() {
//   Swal.fire({
//     title: '<strong>Deseas generar otro servicio?</strong>',
//     icon: 'question',
//     showCloseButton: true,
//     showCancelButton: true,
//     confirmButtonText:
//       '<i class="fas fa-"></i> Si',
//     cancelButtonText:
//       '<i class="fa fa-thumbs-down"></i> No',
//   }, function (confirm) {
//     if (confirm) {
//       alert("dfgdfg");
//     }else{
//       alert("sdgfsdf");
//     }
//   })
// }



// swalQuestion();

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 4000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

function swalSimple(title = 'Guardado', icon = 'success') {
  Toast.fire({
    icon: icon,
    title: title
  })
}

