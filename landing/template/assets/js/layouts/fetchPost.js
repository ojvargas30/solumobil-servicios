function sendDataPostFetch(data, url = '') {
    callLoader(true);

    let options = {
        method: 'POST',
        body: "data=" + JSON.stringify(data),

        headers: {
            // multipart/form-data
            "Content-Type": "application/x-www-form-urlencoded"
        }
    };

    // let response = await fetch(url, options)

    return new Promise((resolve, reject) => {
        fetch(url, options)
            .then(response => {
                callLoader(false);
                if(response.ok){
                    return response.text()
                }
            })
            .then(result => {

                if (result == '"pastdateerror"') {
                    swalError("Error",'La fecha de la cita debe ser futura');
                    resolve("pastdateerror")

                } else if (result == '"crosserror"') {
                    swalInfo("Cruce de horarios", 'Para la fecha que especificaste alguno de los técnicos tiene otro servicio');

                    // alert("Error, El técnico asignado no esta disponible para la fecha de cita especificada")
                    resolve("crosserror")

                } else if (result == '"badate"') {

                    swalError("Campos de cita invalidos",'Debe diligenciar todos los campos de cita y deben ser validos');
                    resolve("badate")

                } else if (result == '"well"') {

                    resolve("well");
                    // return true;
                }

                return result
            })
            // .then(texto => customMsg(texto))
            .catch(e => reject(e));

    });
}



// let formData = new FormData();
//     formData.append("name","Oscar vargas");
//     formData.append("edad",34);

// let data = {
//     "name": "Juanito",
//     "age":23
// };

// let options = {
//     method: 'POST',
//     body: "data=" + JSON.stringify(data),

//     headers: {
//         // multipart/form-data
//         "Content-Type": "application/x-www-form-urlencoded"
//     }
// };


// fetch("?c=service&m=technicianCrossover", options)
//     .then(response => response.text())
//     .then(texto => console.log(texto));

    // async function call_server(){
    //     const req = await fetch("?c=service&m=technicianCrossover", options)
    //     const data = await response.text();
    //     console.log(data)
    // }

    // call_server();
