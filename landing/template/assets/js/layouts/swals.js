// function venConfirm(e) {
//   e.preventDefault();
//   if (Swal.fire({
//     title: '¿Esta seguro de querer cancelar el servicio?',
//     text: "Para revertir la operación consulte con soporte",
//     icon: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#4785e9',
//     confirmButtonText: 'Si',
//     cancelButtonColor: '#c41',
//     cancelButtonText: 'No'
//   }).then((result) => {
//     if (result.value) {
//       Swal.fire(
//         '¡Cancelado!',
//         'El servicio ha sido cancelado (Redirigiendo...)',
//         'success'
//       );
//       setTimeout(() => {
//         window.location.href = e.target.href;
//       }, 1000)
//     }
//   }));
// }

// function venConfirmClient(e) {
//   e.preventDefault();
//   if (Swal.fire({
//     title: '¿Esta seguro de querer cancelar el servicio?',
//     text: "Para revertir la operación de clic en reactivar",
//     icon: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#4785e9',
//     confirmButtonText: 'Si',
//     cancelButtonColor: '#c41',
//     cancelButtonText: 'No'
//   }).then((result) => {
//     if (result.value) {
//       Swal.fire(
//         '¡Cancelado!',
//         'El servicio ha sido cancelado (Redirigiendo...)',
//         'success'
//       );
//       setTimeout(() => {
//         window.location.href = e.target.href;
//       }, 1000)
//     }
//   }));
// }

// function venConfirm2(e) {
//   e.preventDefault();
//   if (Swal.fire({
//     title: '¿Esta seguro de finalizar el servicio?',
//     text: "Para revertir la operación consulte con soporte",
//     icon: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#4785e9',
//     confirmButtonText: 'Si',
//     cancelButtonColor: '#c41',
//     cancelButtonText: 'No'
//   }).then((result) => {
//     if (result.value) {
//       Swal.fire(
//         'Hecho ¡Buen trabajo!',
//         'El servicio ha sido finalizado con exito (Redirigiendo...)',
//         'success'
//       );
//       setTimeout(() => {
//         window.location.href = e.target.href;
//       }, 1000)
//     }
//   }));
// }

// function confirmActive(e) {
//   e.preventDefault();
//   if (Swal.fire({
//     title: '¿Esta seguro de querer activar el servicio?',
//     text: "Para revertir la operación consulte con soporte",
//     icon: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#4785e9',
//     confirmButtonText: 'Si',
//     cancelButtonColor: '#c41',
//     cancelButtonText: 'No'
//   }).then((result) => {
//     if (result.value) {
//       Swal.fire(
//         '¡Activado!',
//         'El servicio ha sido activado (Redirigiendo...)',
//         'success'
//       );
//       setTimeout(() => {
//         window.location.href = e.target.href;
//       }, 1000)
//     }
//   }));
// }

// let btnsCan = document.getElementsByClassName("btnCancelar");
// for (let btnCan of btnsCan) {
//   btnCan.addEventListener("click", venConfirm);
// }

// let btnsFin = document.getElementsByClassName("btnFinalizar");
// for (let btnFin of btnsFin) {
//   btnFin.addEventListener("click", venConfirm2);
// }

// let btnsCan2 = document.getElementsByClassName("btnCancelar2");
// for (let btnCan2 of btnsCan2) {
//   btnCan2.addEventListener("click", venConfirmClient);
// }

// let btnsActive = document.getElementsByClassName("btnActive");
// for (let btnActive of btnsActive) {
//   btnActive.addEventListener("click", confirmActive);
// }

// Esto es para evitar que se pase de longitud -> VALIDAR INPUTS NUMERUICOS
let input = document.querySelectorAll('.solonums');
// console.log(input);
input.forEach(e => {
  e.addEventListener('input', function () {
    if (this.value.length > 10)
      this.value = this.value.slice(0, 10);
  })
});

function swalN(msg = '', type = '', text = '') {
  Swal.fire({
    icon: type,
    title: msg,
    text: text,
    footer: '<a href="mailto:javivar2020@gmail.com">Reportar problema</a>'
  })
}


