
/* PASOS PARA USAR LA VALIDACIÓN VARGAS EN VISTA
- id al body: body
- incluir el html del loader
- id al contenido: content
- input completo,nombre de bad,nombre de good y su nombre de regla -> arreglar estilos para la clase
input_estado
*/

const msgStopCopyPage = (e) => {
    e.preventDefault();
    Swal.fire({
        title: '<span class="pad">Acción denegada</span>',
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        }
    });
}

function noCopyPageInInput(inputsName = '') { // con el punto
    let inps = document.querySelectorAll(inputsName);
    inps.forEach((inp) => {
        inp.addEventListener('paste', msgStopCopyPage)
        inp.addEventListener('copy', msgStopCopyPage)
    });
}

function equal(cadena = "", cadenaEqual = "") {
    if (!cadena) return false

    if (typeof cadena !== "string") return false

    if (!cadenaEqual) return false

    if (typeof cadenaEqual !== "string") return false

    return (cadena === cadenaEqual) ? true : false;
}

function valida(regex = "", cadena = "") {
    if (!(regex instanceof RegExp)) return false

    let valida = regex.test(cadena);

    return (valida) ? true : false;
}

function validar_clave(clave) {

    if (clave.length >= 8) {

        let mayuscula = false;
        let minuscula = false;
        let numero = false;
        let caracter_raro = false;

        for (let i = 0; i < clave.length; i++) {

            if (clave.charCodeAt(i) >= 65 && clave.charCodeAt(i) <= 90)

                mayuscula = true;

            else if (clave.charCodeAt(i) >= 97 && clave.charCodeAt(i) <= 122)

                minuscula = true;

            else if (clave.charCodeAt(i) >= 48 && clave.charCodeAt(i) <= 57)

                numero = true;

            else
                caracter_raro = true;

        }
        if (mayuscula == true && minuscula == true && caracter_raro == true && numero == true)

            return true;

        else
            return false;
    }
}


function validaPatron(
    cadena = "",
    type = "name",
    cadenaEqual = "",
    RegExpName = new RegExp("[A-Za-zñÑÁáÉéÍíÓóÚú\s]+$", "g"),
    RegExpEmail = new RegExp("[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})", "i"),
    RegExpPassword = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}"),
    RegExpAddress = new RegExp("(d+s*(?:[A-Z](?![A-Z]))?)")) {

    let res = false;

    if (!cadena) return res;

    if (typeof cadena !== "string") return res;

    switch (type) {
        case "name": if (valida(RegExpName, cadena, "El nombre")) res = true; break;
        case "email": if (valida(RegExpEmail, cadena, "El correo")) res = true; break;
        case "password": if (validar_clave(cadena)) res = true; break;
        case "equal": if (equal(cadena, cadenaEqual)) res = true; break;
        case "address": if (valida(RegExpAddress, cadena, "La dirección")) res = true; break; // dirección extranjera y estricta
    }

    return res;
}


// console.log(validaPatron("cll 123c N 23-24"));
// console.log(validaPatron("cll 123c #23-24"));
// console.log(validaPatron(""));
// console.log(validaPatron("asdkjasdf478y"));

// function NIF_CIFValidator() {

//     this.NIF_Letters = "TRWAGMYFPDXBNJZSQVHLCKET";
//     this.NIF_regExp = "^\\d{8}[a-zA-Z]{1}$";
//     this.CIF_regExp = "^[a-zA-Z]{1}\\d{7}[a-jA-J0-9]{1}$";

//     this.checkAll = function (value) {
//         if (this.checkCIF(value)) { // Comprueba el CIF
//             return true;
//         } else if (this.checkTR(value)) { // Comprueba tarjeta de residencia
//             return true;
//         } else if (this.checkNIF(value)) { // Comprueba el NIF
//             return true;
//         } else {           // Si no pasa por ninguno es false.
//             return false;
//         }
//     }

//     // VALIDA EL NIF
//     this.checkNIF = function (nif) {
//         // Comprueba la longitud. Los DNI antiguos tienen 7 digitos.
//         if ((nif.length != 8) && (nif.length != 9)) return false;
//         if (nif.length == 8) nif = '0' + nif; // Ponemos un 0 a la izquierda y solucionado

//         // Comprueba el formato
//         var regExp = new RegExp(this.NIF_regExp);
//         if (!nif.match(regExp)) return false;

//         var let = nif.charAt(nif.length - 1);
//         var dni = nif.substring(0, nif.length - 1)
//         var letra = this.NIF_Letters.charAt(dni % 23);
//         return (letra == let.toUpperCase());
//     }

//     // VALIDA TARJETA DE RESIDENCIA
//     this.checkTR = function (tr) {
//         if ((tr.length != 10) && (tr.length != 9)) return false;
//         if ((tr.charAt(0).toUpperCase() != "X") && (tr.charAt(0).toUpperCase() != "Y") && (tr.charAt(0).toUpperCase() != "Z")) return false;

//         var leftNum = '0';
//         if (tr.charAt(0).toUpperCase() == "Y") leftNum = '1';

//         if (tr.length == 9) {
//             return this.checkNIF(leftNum + tr.substring(1, tr.length));
//         } else {
//             return this.checkNIF(tr.substring(1, tr.length));
//         }
//     }

//     // VALIDA TARJETA DE RESIDENCIA
//     this.checkCIF = function (cif) {
//         var v1 = new Array(0, 2, 4, 6, 8, 1, 3, 5, 7, 9);
//         var tempStr = cif.toUpperCase(); // pasar a mayúsculas
//         var temp = 0;
//         var temp1;
//         var dc;

//         // Comprueba el formato
//         var regExp = new RegExp(this.CIF_regExp);
//         if (!tempStr.match(regExp)) return false;    // Valida el formato?
//         if (!/^[ABCDEFGHKLMNPQS]/.test(tempStr)) return false;  // Es una letra de las admitidas ?

//         for (i = 2; i <= 6; i += 2) {
//             temp = temp + v1[parseInt(cif.substr(i - 1, 1))];
//             temp = temp + parseInt(cif.substr(i, 1));
//         };
//         temp = temp + v1[parseInt(cif.substr(7, 1))];
//         temp = (10 - (temp % 10));
//         if (temp == 10) temp = 0;
//         dc = cif.toUpperCase().charAt(8);
//         return (dc == temp) || (temp == 1 && dc == 'A') || (temp == 2 && dc == 'B') || (temp == 3 && dc == 'C') || (temp == 4 && dc == 'D') || (temp == 5 && dc == 'E') || (temp == 6 && dc == 'F') || (temp == 7 && dc == 'G') || (temp == 8 && dc == 'H') || (temp == 9 && dc == 'I') || (temp == 0 && dc == 'J');
//     }
// }




function estadoInput(valor, bad, good, rule, input, type, objNameFields, campo, equalValue) {

    if (validaPatron(valor, type, equalValue)) {
        good.classList.remove('estado_oculto')
        bad.classList.add('estado_oculto')
        rule.style.display = 'none';
        input.classList.remove('input_incorrecto')
        input.classList.add('input_correcto')
        objNameFields[campo] = true;
    } else {
        bad.classList.remove('estado_oculto');
        good.classList.add('estado_oculto');
        rule.style.display = 'block';
        input.classList.remove('input_correcto')
        input.classList.add('input_incorrecto')
        objNameFields[campo] = false;
    }
}

function validateInputs(classQSAll = '', func = '') { // debe lleva el . antes
    let inputs = document.querySelectorAll(classQSAll);
    inputs.forEach((inp) => {
        inp.addEventListener('keyup', func)
        inp.addEventListener('keydown', func)
        inp.addEventListener('keypress', func)
        inp.addEventListener('change', func)
        inp.addEventListener('blur', func)
    });
}

function ele(nameElement) {
    let get = document.getElementById(nameElement);
    return get;
}

function eleVal(nameElement) {
    let get = document.getElementById(nameElement).value;
    return get;
}

function callLoader(ver = false) { // por hacer generico
    let loader = document.querySelector(".loader");
    let body = ele('body');
    let content = ele('content');

    if (ver) {
        body.classList.add("blanco_estricto");
        content.style.display = "none";
        loader.style.display = "block";
    }
    else {
        loader.style.display = "none";
        content.style.display = "block";
    }
}

function sendDataPost(
    arrCampos = [],
    url = '',
    sendParams = [],
    idAlerta = '',
    redirect = false,
    putInputValue = false,
    inputToCleanValue = '',
    showMsgSuccess = false,
    msgSuccess = "¡Genial!",
    msgUndefined = 'El recurso no existe en nuestra base de datos',
    msgJsonFail = 'Algo salio mal',
    msgIncorrectInputs = 'Los campos deben diligenciarse correctamente y en su totalidad'
) {

    callLoader(true);

    const isTrueFields = (currentValue) => currentValue === true;

    if (arrCampos.every(isTrueFields)) { // esperar a que cada uno se igual a tru o sino no

        $.post(url, sendParams, function (response) {
            // console.log(response.resp.errorMessage);
            // console.log(response.resp);
            // console.log(response);

            if (typeof response.error !== 'undefined') {

                callLoader(false);

                alert(msgUndefined);

            } else {

                callLoader(false);

                // console.log(response.resp.errorMessage);
                // console.log(response.resp);
                // console.log(response);
                if (response.resp.errorMessage) {

                    toastr.options.onHidden = function () {

                        if (putInputValue === false) {
                            if (redirect && response.urlC) {
                                location.href = response.urlC;
                            }
                        }
                    }

                    if (putInputValue) {
                        ele(inputToCleanValue).value = '';
                    }

                    if (response.resp.message != '') {
                        swalN(response.resp.message, "info");

                    } else if (response.resp.errorMessage != "Error al Iniciar Sesión. Verifique sus Credenciales") {
                        swalN(response.resp.errorMessage, "error");
                    } else {
                        toastr.error(response.resp.errorMessage, "", { timeOut: 4000, closeButton: true, positionClass: "toast-bottom-right", progressBar: true });
                    }

                } else {
                    if (showMsgSuccess == true) {
                        alert(msgSuccess);
                    }
                    if (redirect && response.urlC) {
                        location.href = response.urlC;
                    }
                }
            }
        }, 'json').fail(function (jqXHR, textStatus, errorThrown) { // por el bucle

            console.log(errorThrown.responseText);

            callLoader(false);

            if (jqXHR.status === 0)

                alert('Not connect: Verify Network.');

            else if (jqXHR.status == 404)

                alert('Requested page not found [404]');

            else if (jqXHR.status == 500)

                alert('Internal Server Error [500].');

            else if (textStatus === 'parsererror')

                if (jqXHR.responseText.includes('SQLSTATE[23000]')) {

                    // swalN("Ya existe un usuario con ese correo electrónico", 'error', 'Intenta de nuevo con otro correo');
                    toastE("Ya existe un usuario con ese correo electrónico");
                } else

                    alert('Requested JSON parse failed.' + jqXHR.responseText); // varios envios y duplicidad

            else if (textStatus === 'timeout')

                alert('Time out error.');

            else if (textStatus === 'abort')

                alert('Ajax request aborted.');

            else

                alert('Uncaught Error: ' + jqXHR.responseText);

            // alert(msgJsonFail);// al terminar la app se comenat
            toastE("Algo salio mal");

        });
    } else {
        callLoader(false);

        alert(msgIncorrectInputs);

        let alertaError = ele(idAlerta);
        console.log(alertaError);
        alertaError.classList.remove('d-none')

        setTimeout(() => {
            alertaError.classList.add('d-none')
        }, 5000);

        document.querySelectorAll('.estado_oculto').forEach((icon) => {
            icon.classList.add('d-none');
        });
    }
}