// DECLARAR ARRAY GLOBAL QUE CONTENGA LA LISTA DE REVISIONES O DETALLES
// var arrArtifacts = []
showArtifacts()

$("#artifact").change(function (e) {
    e.preventDefault()
    // console.log("Evitar recargo de la pagina al pulsar el botón con una función anonima")

    let idArtifact = $("#artifact").val()
    let nameArtifact = $("#artifact option:selected").text()

    // console.log(idArtifact,nameArtifact)

    if (idArtifact != '') {
        if (typeof existArtifact(idArtifact) === 'undefined') {
            arrArtifacts.push({
                'id_artefacto_PK': idArtifact,
                'modelo': nameArtifact
            })
        } else {
            let timerInterval
            Swal.fire({
                title: '¡El artefacto ya se encuentra seleccionado!',
                html: 'Vuelva a intentarlo con otro artefacto',
                timer: 2000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        }

        showArtifacts()
    } else {
        Swal.fire({
            title: 'Debe seleccionar un artefacto',
            text: '',
            imageUrl: 'https://unsplash.it/400/200',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
        })
    }

    console.log(arrArtifacts)
});

// Validar la no repetición
function existArtifact(idArtifact) {
    let existArtifact = arrArtifacts.find(function (artifact) {
        return artifact.id_artefacto_PK == idArtifact
    })
    return existArtifact
}

// mostrar elementos
function showArtifacts() {
    $("#list-artifacts").empty()

    arrArtifacts.forEach(function (artifact) {
        $("#list-artifacts").append('<div class="form-group border rounded-pill pt-1"><button onclick="removeArtifact(' + artifact.id_artefacto_PK + ')" class="btn btn-sm mb-1 ml-2 border-0"><i class="fas fa-times text-secondary fa-xs"></i></button> <span class="font-sm-important pl-1 text-muted">  ' + artifact.modelo + '</span></div>')
    })
}

// Quitar elementos
function removeArtifact(idArtifact) {
    let index = arrArtifacts.indexOf(existArtifact(idArtifact))
    arrArtifacts.splice(index, 1)
    showArtifacts()
}

// enviar elementos al back
$("#scheduleContinue").click(function (e) {
    e.preventDefault()

    let url = "?c=service&m=saveClient"
    let params = {
        artifacts: arrArtifacts,
        scategories: arrScategories,
        details: getDetail()
    }

    if (arrArtifacts.length != 0 && arrScategories.length != 0) {
        // metodo POST USANDO AJAX PARA ENVIAR LA INFO AL BACKEND
        $.post(url, params, function (response) {
            if (response.error !== false) {
                swalError("Error, valores de campos invalidos");
            } else {
                swalSimple('Enviado');
                // redireccion
                setTimeout(function () { location.href = "?c=service" }, 2000);
            }
        }, 'json').fail(function (error) {
            if (error.responseText.includes('SQLSTATE[23000]')) {
                swalError('Error, intentalo mas tarde');
            }
            // swal('<i class="fas fa-flushed"></i>  Hubo un problema', 'error', 'Vuelve a intentarlo mas tarde');
            // console.log("Inserción fallida ("+error.responseText+")")
        });
    } else {

        alert("Completa todos los campos")
    }
});

$("#update").click(function (e) {
    //deshabilitar el envio por HTTP
    e.preventDefault()

    let url = "?c=service&m=updateClient"
    let params = {
        id_servicio_PK: $("#id_servicio_PK").val(),
        artifacts: arrArtifacts,
        scategories: arrScategories,
        details: getDetail()
    }
    // console.log(params);

    if (arrArtifacts.length != 0 && arrScategories.length != 0) {
        $.post(url, params, function (response) {
            if (response.error !== false) {
                swalError("Error, valores de campos invalidos");

            } else {
                swalSimple('Actualizado');
                // redireccion
                setTimeout(function () { location.href = "?c=service" }, 2000);
            }
        }, 'json').fail(function (error) {
            if (error.responseText.includes('SQLSTATE[23000]')) {
                swalError('Error, intentalo mas tarde');
            }
            alert("Actualización Fallida(" + error.responseText + ")")
        });
    } else {
        alert("Completa todos los campos")
    }

});