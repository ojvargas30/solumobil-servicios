
function getDetail() {

    var arrDetails = []

    let idDetSer = $("#id_revision_servicio_PK").val();
    let isSoft = document.getElementById("is_software").checked;
    let isHard = document.getElementById("is_hardware").checked;
    let dCliente = $("#descripcion_cliente").val();
    let diagnostico = $("#diagnostico").val();
    let costo_revision = $("#costo_revision").val();
    let costo_domicilio = $("#costo_domicilio").val();
    let costo_total = $("#costo_total").val();
    let id_tecnico_FK = $("#id_tecnico_FK").val();
    let abono = $("#abono").val();
    // let evidencia = (arrImages.length != 0) ? arrImages : [];

    if (typeof virtualForm !== 'undefined') {
        if (virtualForm.classList.contains("d-none")) {
            let fechaEncuentro = $("#fecha_encuentro").val();
            let horaEncuentro = $("#hora_encuentro").val();
            let dirLugar = $("#direccion_lugar").val();

            if (diagnostico != '' && dCliente != '' &&
                costo_revision.trim() != '' &&
                costo_domicilio.trim() != '' &&
                costo_total.trim() != '' &&
                abono.trim() != '' && id_tecnico_FK != "0") {
                arrDetails.push({

                    'id_revision_servicio_PK': idDetSer,
                    'is_software': isSoft,
                    'is_hardware': isHard,
                    'descripcion_cliente': dCliente,
                    'diagnostico': diagnostico,
                    'costo_revision': costo_revision,
                    'costo_domicilio': costo_domicilio,
                    'costo_total': costo_total,
                    'fecha_encuentro': fechaEncuentro,
                    'hora_encuentro': horaEncuentro,
                    'direccion_lugar': dirLugar,
                    'abono': abono,
                    'id_tecnico_FK': id_tecnico_FK

                })
            } else swalError('Diligencie todos los campos');

        } else {
            let link_cita_virtual = $("#link").val();
            let fecha_hora_cita_virtual = $("#fecha_hora_cita_virtual").val();

            if (diagnostico != '' && link_cita_virtual != '' && fecha_hora_cita_virtual != '' && dCliente != '' &&
                costo_revision.trim() != '' &&
                costo_domicilio.trim() != '' &&
                costo_total.trim() != '' &&
                abono.trim() != '' && id_tecnico_FK != "0") {
                arrDetails.push({
                    'id_revision_servicio_PK': idDetSer,
                    'is_software': isSoft,
                    'is_hardware': isHard,
                    'descripcion_cliente': dCliente,
                    'diagnostico': diagnostico,
                    'costo_revision': costo_revision,
                    'costo_domicilio': costo_domicilio,
                    'costo_total': costo_total,
                    'link_cita_virtual': link_cita_virtual,
                    'fecha_hora_cita_virtual': fecha_hora_cita_virtual,
                    'abono': abono,
                    'id_tecnico_FK': id_tecnico_FK
                })
            } else swalError('Diligencie todos los campos');

        }

    } else {
        let fechaEncuentro = $("#fecha_encuentro").val();
        let horaEncuentro = $("#hora_encuentro").val();
        let dirLugar = $("#direccion_lugar").val();

        if (diagnostico != '' && dCliente != '' &&
            costo_revision.trim() != '' &&
            costo_domicilio.trim() != '' &&
            costo_total.trim() != '' &&
            abono.trim() != '' && id_tecnico_FK != "0") {
            arrDetails.push({

                'id_revision_servicio_PK': idDetSer,
                'is_software': isSoft,
                'is_hardware': isHard,
                'descripcion_cliente': dCliente,
                'diagnostico': diagnostico,
                'costo_revision': costo_revision,
                'costo_domicilio': costo_domicilio,
                'costo_total': costo_total,
                'abono': abono,
                'id_tecnico_FK': id_tecnico_FK,
                'fecha_encuentro': fechaEncuentro,
                'hora_encuentro': horaEncuentro,
                'direccion_lugar': dirLugar

            })
        } else swalError('Diligencie todos los campos');
    }

    return arrDetails
}

function getDetailToDraft() {

    var arrDetails = []

    let idDetSer = $("#id_revision_servicio_PK").val();
    let isSoft = document.getElementById("is_software").checked;
    let isHard = document.getElementById("is_hardware").checked;
    let dCliente = $("#descripcion_cliente").val();
    let diagnostico = $("#diagnostico").val();
    let costo_revision = $("#costo_revision").val();
    let costo_domicilio = $("#costo_domicilio").val();
    let costo_total = $("#costo_total").val();
    let id_tecnico_FK = $("#id_tecnico_FK").val();
    let abono = $("#abono").val();

    if (typeof virtualForm !== 'undefined') {
        if (virtualForm.classList.contains("d-none")) {
            let fechaEncuentro = $("#fecha_encuentro").val();
            let horaEncuentro = $("#hora_encuentro").val();
            let dirLugar = $("#direccion_lugar").val();

            // if (
            //     diagnostico != '' && dCliente != '' &&
            //     costo_revision.trim() != '' &&
            //     costo_domicilio.trim() != '' &&
            //     costo_total.trim() != '' &&
            //     abono.trim() != '' && id_tecnico_FK != "0"
            //     ) {
            arrDetails.push({

                'id_revision_servicio_PK': idDetSer,
                'is_software': isSoft,
                'is_hardware': isHard,
                'descripcion_cliente': dCliente,
                'diagnostico': diagnostico,
                'costo_revision': costo_revision,
                'costo_domicilio': costo_domicilio,
                'costo_total': costo_total,
                'fecha_encuentro': fechaEncuentro,
                'hora_encuentro': horaEncuentro,
                'direccion_lugar': dirLugar,
                'abono': abono,
                'id_tecnico_FK': id_tecnico_FK

            })
            // }

        } else {
            let link_cita_virtual = $("#link").val();
            let fecha_hora_cita_virtual = $("#fecha_hora_cita_virtual").val();

            // if (diagnostico != '' && link_cita_virtual != '' && fecha_hora_cita_virtual != '' && dCliente != '' &&
            //     costo_revision.trim() != '' &&
            //     costo_domicilio.trim() != '' &&
            //     costo_total.trim() != '' &&
            //     abono.trim() != '' && id_tecnico_FK != "0") {
            arrDetails.push({
                'id_revision_servicio_PK': idDetSer,
                'is_software': isSoft,
                'is_hardware': isHard,
                'descripcion_cliente': dCliente,
                'diagnostico': diagnostico,
                'costo_revision': costo_revision,
                'costo_domicilio': costo_domicilio,
                'costo_total': costo_total,
                'link_cita_virtual': link_cita_virtual,
                'fecha_hora_cita_virtual': fecha_hora_cita_virtual,
                'abono': abono,
                'id_tecnico_FK': id_tecnico_FK
            })
            // }
        }

    } else {
        let fechaEncuentro = $("#fecha_encuentro").val();
        let horaEncuentro = $("#hora_encuentro").val();
        let dirLugar = $("#direccion_lugar").val();

        // if (diagnostico != '' && dCliente != '' &&
        //     costo_revision.trim() != '' &&
        //     costo_domicilio.trim() != '' &&
        //     costo_total.trim() != '' &&
        //     abono.trim() != '' && id_tecnico_FK != "0") {
        arrDetails.push({

            'id_revision_servicio_PK': idDetSer,
            'is_software': isSoft,
            'is_hardware': isHard,
            'descripcion_cliente': dCliente,
            'diagnostico': diagnostico,
            'costo_revision': costo_revision,
            'costo_domicilio': costo_domicilio,
            'costo_total': costo_total,
            'abono': abono,
            'id_tecnico_FK': id_tecnico_FK,
            'fecha_encuentro': fechaEncuentro,
            'hora_encuentro': horaEncuentro,
            'direccion_lugar': dirLugar

        })
        // }
    }

    return arrDetails
}