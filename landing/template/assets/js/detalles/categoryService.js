// DECLARAR ARRAY GLOBAL QUE CONTENGA LA LISTA DE REVISIONES O DETALLES
// var arrScategories = []
showScategories()

// $("#addScategory").click(function(e) {
    // let addService = document.getElementById('addService');

$("#scategory").change(function (e) {
    e.preventDefault()
    // console.log("Evitar recargo de la pagina al pulsar el botón con una función anonima")

    let idScategory = $("#scategory").val()
    let nameScategory = $("#scategory option:selected").text()

    // console.log(idScategory,nameScategory)

    if (idScategory != '') {
        if (typeof existScategory(idScategory) === 'undefined') {
            arrScategories.push({
                'id_categoria_servicio_PK': idScategory,
                'nombre_cs': nameScategory
            })
        } else {
            let timerInterval
            Swal.fire({
                title: '¡La categoría ya se encuentra seleccionada!',
                html: 'Vuelva a intentarlo con otra categoría',
                timer: 2000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        }
        showScategories()
    } else {
        Swal.fire({
            title: 'Debe seleccionar una categoría',
            text: '',
            imageUrl: 'https://unsplash.it/400/200',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
        })
    }

    console.log(arrScategories)
});

// Validar la no repetición
function existScategory(idScategory) {
    let existScategory = arrScategories.find(function (scategory) {
        return scategory.id_categoria_servicio_PK == idScategory
    })
    return existScategory
}

// mostrar elementos
function showScategories() {
    $("#list-scategories").empty()

    arrScategories.forEach(function (scategory) {
        $("#list-scategories")
            .append('<div class="form-group border rounded-pill pt-1"><button onclick="removeScategories(' + scategory.id_categoria_servicio_PK + ')" class="btn btn-sm mb-1 ml-2 border-0"><i class="fas fa-times text-secondary fa-xs"></i></button> <span class="font-sm-important pl-1 text-muted">' + scategory.nombre_cs + '</span></div>')
    })
}

// Quitar elementos
function removeScategories(idScategory) {
    let index = arrScategories.indexOf(existScategory(idScategory))
    arrScategories.splice(index, 1)
    showScategories()
}