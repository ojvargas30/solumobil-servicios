let btnShowSubFormService = ele("showSubFormService");

eve(btnShowSubFormService, (e) => {
    e.preventDefault();
    ele("subFormService").classList.toggle('d-none');
});

eve(ele("submitSubFormService"), (e) => {
    e.preventDefault();
    let nombre_cs = ele("nombre_cs").value;
    let precio_cs = ele("precio_cs").value;

    if (nombre_cs.trim() != '') {

        let data = [
            nombre_cs,
            (precio_cs.trim() != '') ? precio_cs : ''
        ];

        let options = {
            method: 'POST',
            body: "data=" + JSON.stringify(data),

            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        },
            url = "?c=scategory&m=saveSubForm";

        fetch(url, options)
            .then(response => {
                if (response.ok) return response.text()
            })
            .then(result => {
                if(result.includes('SQLSTATE[23000]')) return alert("Ya existe un servicio con ese nombre");
                result = JSON.parse(result)
                console.log(result);
                // console.log(result);
                console.log(result[0].id_categoria_servicio_PK);

                if (result.length != 0) {

                    let select = ele("scategory"),
                        opti         = document.createElement("option");
                    opti.value       = result[0].id_categoria_servicio_PK;
                    opti.textContent = result[0].nombre_cs;
                    select.appendChild(opti);

                    // Seleccion
                    ele("seleccione").removeAttribute("selected");
                    opti.setAttribute("selected", "true");

                    //Auto clic
                    btnShowSubFormService.click();

                    toastr.success('Servicio generado');

                } else alert("Error, intentalo mas tarde")
            })
            .catch(e => console.log((e)));
    }
})