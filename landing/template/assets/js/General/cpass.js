// noCopyPageInInput('.input_v');

let changePassFields = {
    pass: false,
    cpass: false,
    idUser: false
}

$("#newpass").click((e) => {
    e.preventDefault()

    const p = {
        pass: eleVal('pass'),
        cpass: eleVal('cpass'),
        idUser: eleVal('idUser')
    }

    sendDataPost(
        [changePassFields['pass'], changePassFields['cpass']],
        "?c=user&m=cpass", p, 'alertChan',true, false, '', true,
        'Contraseña cambiada con exito, ya puede ingresar')
});


const validateChangePass = (e) => { // valida form de registro del clienet en home

    let pass = ele('pass'),
        cpass = ele('cpass'),

        g1 = document.querySelector('.g1'),
        b1 = document.querySelector('.b1'),
        rl_clave = ele('rl_clave'),

        g2 = document.querySelector('.g2'),
        b2 = document.querySelector('.b2'),
        rl_cclave = ele('rl_cclave'),

        // Reutilizable
        id = e.target.id,
        elementoInput = ele(id),
        valor = elementoInput.value;

    switch (e.target.id) {
        case "pass":
            estadoInput(valor, b1, g1, rl_clave, elementoInput, "password", changePassFields, id)
            estadoInput(valor, b2, g2, rl_cclave, cpass, "equal", changePassFields, id, cpass.value)
            break;
        case "cpass":
            estadoInput(valor, b2, g2, rl_cclave, elementoInput, "equal", changePassFields, id, pass.value)
            estadoInput(pass.value, b1, g1, rl_clave, pass, "password", changePassFields, pass.id)
            break;
    }
}

validateInputs('.input_v', validateChangePass);


// function getPass() {
//     var arrPass = [];

//     let pass = $("#pass").val();
//     let cpass = $("#cpass").val();
//     let id = $("#id").val();

//     if (pass != '' && cpass != '') {
//         if (pass !== cpass) {
//             alert('Las contraseñas deben ser iguales')
//         } else {
//             if (validar_clave(cpass)) {
//                 arrPass.push({
//                     'pass': pass,
//                     'cpass': cpass,
//                     'id': id
//                 })
//             } else {
//                 alert("La contraseña esta en un formato desconocido");
//             }
//         }
//     } else {
//         alert("Los campos no pueden estar vacíos");
//     }

//     return arrPass
// }

// function validar_clave(clave) {
//     if (clave.length >= 8) {
//         let mayuscula = false;
//         let minuscula = false;
//         let numero = false;
//         let caracter_raro = false;

//         for (let i = 0; i < clave.length; i++) {
//             if (clave.charCodeAt(i) >= 65 && clave.charCodeAt(i) <= 90) {
//                 mayuscula = true;
//             }
//             else if (clave.charCodeAt(i) >= 97 && clave.charCodeAt(i) <= 122) {
//                 minuscula = true;
//             }
//             else if (clave.charCodeAt(i) >= 48 && clave.charCodeAt(i) <= 57) {
//                 numero = true;
//             }
//             else {
//                 caracter_raro = true;
//             }
//         }
//         if (mayuscula == true && minuscula == true && caracter_raro == true && numero == true) {
//             return true;
//         }
//     }
//     return false;
// }