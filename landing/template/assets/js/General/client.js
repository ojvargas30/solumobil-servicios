// enviar elementos al back
$("#submitGhost").click(function (e) {
    e.preventDefault()

    let url = "?controller=user&method=saveClient"
    let params = {
        users: getUserClient(),
        clients: getClient()
    }
    // metodo POST USANDO AJAX PARA ENVIAR LA INFO AL BACKEND
    $.post(url, params, function (response) {
        if (typeof response.error !== 'undefined') {
            swal('Oops...', 'error', 'Algo salio mal con el usuario!');
        } else {
            // recargo del select, poner lo que se inserta
            // console.log(response);
            // // console.log(response.resp);
            // console.log(response.registerInsert);

            let id_cliente_PK = response.id_cliente_PK, nombre_cliente = response.nombre_cliente;

            let select = document.getElementById("id_cliente_FK"),

            option = document.createElement("option");

            option.value = id_cliente_PK;

            option.textContent = nombre_cliente;

            select.appendChild(option);

            // Seleccion
            document.getElementById("seleccioneC").removeAttribute("selected");

            option.setAttribute("selected", "");

            swal('Usuario generado. El cliente debe revisar el correo', 'success', '', 5000, true, 'center-middle');

            //Auto clic
            let clicBtnNew = document.getElementById('ghost-child');
            clicBtnNew.click();
        }
    }, 'json').fail(function (error) {
        if (error.responseText.includes('SQLSTATE[23000]')) {
            alert('Ya existe un usuario con ese telefono y/o número de identificación', 'error');
        }
        // swal('Error generando el cliente', 'error', 'Ya existe un usuario con ese correo', 6000, true);
        // console.log("Inserción fallida (" + error.responseText + ")");
        // console.log(error);
    });
});


function getUserClient() {

    var arrUserClient = []
    let correoUsu = $("#correo").val();
    let emailConfirm = $("#emailConfirm").val();

    // Patron para el correo
    let patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    if (correoUsu != '' && emailConfirm != '') {
        if (correoUsu == emailConfirm) {
            if (correoUsu.search(patron) == 0) {
                arrUserClient.push({
                    'correo': emailConfirm,
                    'clave': generatePasswordRand(13)
                })
            } else {
                swal('El correo esta en un formato desconocido', 'error', 'El correo no debe contener simbolos ni caracteres especiales(Ej: test@gmail.com)', 10000, true);
            }
        } else {
            swal('Las entradas del correo son diferentes', 'error', 'Asegurate de que sean iguales', 5000, true);
        }
    } else {
        swal('Verifique que los campos no esten vacíos', 'error', '', 5000, true);
    }
    return arrUserClient
}


function getClient() {

    var arrClient = [];
    let id_cliente_PK = $("#id_cliente_PK").val();
    let id_barrio_FK = $("#id_barrio_FK option:selected").val();
    let tipo_doc_cli = $("#tipo_doc_cli option:selected").text();
    let nombre_cliente = $("#nombre_cliente").val();
    let apellido_cliente = $("#apellido_cliente").val();
    let num_id_cli = $("#num_id_cli").val();
    let telefono = $("#telefono").val();
    let direccion_residencia = $("#direccion_residencia").val();

    if (direccion_residencia != '' && apellido_cliente != '' &&
        nombre_cliente != '' && num_id_cli != '' && id_barrio_FK != '') {
        if (telefono.length > 12) {
            swal("El telefono debe ser menor o igual a 15 digitos", "error", "", 5000, false);
        } else {
            if (num_id_cli.length > 12) {
                swal("El No. de identificación debe ser menor o igual a 10 digitos", "error", "", 5000, false);
            } else {
                arrClient.push({
                    'id_barrio_FK': id_barrio_FK,
                    'tipo_doc_cli': tipo_doc_cli,
                    'nombre_cliente': nombre_cliente,
                    'apellido_cliente': apellido_cliente,
                    'num_id_cli': num_id_cli,
                    'telefono': telefono,
                    'direccion_residencia': direccion_residencia
                })
            }
        }
    } else {
        swal('Diligencie el campo de barrio', '', '', 5000, false);
    }
    return arrClient;
}

