
function getArt() {

    var arrArts = []

    let id_artefacto_PK = $("#id_artefacto_PK").val();
    let id_marca_artefacto_FK = $("#id_marca_artefacto_FK option:selected").val();
    let id_categoria_artefacto_FK = $("#id_categoria_artefacto_FK option:selected").val();
    // let serie = $("#serie").val();
    let modelo = $("#modelo").val();
    let imei = $("#imei").val();
    // let caracteristicas = $("#caracteristicas").val();

    if (modelo != '' && id_marca_artefacto_FK != '' && id_categoria_artefacto_FK != ''
    // && serie != ''
    && imei != '' && id_marca_artefacto_FK != '0' && id_categoria_artefacto_FK != '0'
    // && caracteristicas != ''
    ) {
        arrArts.push({
            // 'id_artefacto_PK': id_artefacto_PK,
            'id_marca_artefacto_FK': id_marca_artefacto_FK,
            'id_categoria_artefacto_FK': id_categoria_artefacto_FK,
            // 'serie': serie,
            'modelo': modelo,
            'imei': imei
            // 'caracteristicas': caracteristicas,
        })
    } else {
        swal('Diligencie todos los campos');
    }
    return arrArts
}

// enviar elementos al back
$("#submitGhostArtifact").click(function (e) {
    e.preventDefault()

    let url = "?c=artifact&m=save"
    let params = {
        arts: getArt()
    }

    // metodo POST USANDO AJAX PARA ENVIAR LA INFO AL BACKEND
    $.post(url, params, function (response) {
        if (response.error !== false) {
            swalError("Error, Datos de artefacto invalidos");
            // swal('Oops...', 'error', '¡Algo salio mal con el guardado de la marca!');

        }  else {
            // recargo del select, poner lo que se inserta
            let { id_artefacto_PK, modelo } = response.registerInsert;
            let select = document.getElementById("artifact"),
                option = document.createElement("option");
            option.value = id_artefacto_PK;
            option.textContent = modelo;
            select.appendChild(option);

            // Seleccion
            let selecciArtifact = document.getElementById("seleccione")
            selecciArtifact.removeAttribute("selected");
            option.setAttribute("selected", "");

            // CLICK AL BTN DE CARRITO DE COMPRA
            selecciArtifact.click();
            option.click();

            console.log(response)

            swalSimple('Dispositivo generado');

            //Auto clic
            let clicBtnNew = document.getElementById('ghost-child-artifact');
            clicBtnNew.click();

        }
    }, 'json').fail(function (error) {
        if (error.responseText.includes('SQLSTATE[23000]')) {
            swalError('Ya existe un dispositivo con ese nombre');
        }
        // swal('<i class="fas fa-flushed"></i>  Caramba...', 'error', '¡Algo salio mal!');
        // console.log("Inserción fallida (" + error.responseText + ")")
    });
});

