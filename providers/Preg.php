<?php
function valNum($num)
{
    return preg_match("/^[0-9]+$/", $num);
}

function valNumBetweenARange($num)
{
    return preg_match("/^[0-9]{7,16}+$/", $num);
}

function valNumWithLimit($num, $limit)
{
    return preg_match("/^[0-9]{0,$limit}+$/", $num);
}


function valImei($num)
{
    return preg_match("/^[0-9]{14,16}+$/", $num);
}

function valAlphanumeric($mixed)
{
    return preg_match("/^[a-zA-Z0-9]+$/", $mixed);
}

function valAlphanumericAndSymbol($mixed)
{
    return preg_match("/^[a-zA-Z0-9 #\-]+$/i", $mixed);
}

function valUsuario($name)
{
    return preg_match("#^[a-z][\da-z_]{6,22}[a-z\d]\$#i", $name);
}
//ejemplo:
// if (valUsuario("EducacionIT")) {
//     echo "usuario valido";
// } else {
//     echo "usuario invalido";
// }
function valUsuario2($name)
{
    return preg_match("#^[a-zA-Z]((\.|_|-)?[a-zA-Z0-9]+){3}\$#i", $name);
}

function valUsuario3($name)
{
    return preg_match("/^[a-z0-9_-]{3,16}$/", $name);
}

function valCoincidenciaClave($name)
{
    return preg_match("/^[a-z0-9_-]{6,18}$/", $name);
}

function valCalle($streetName)
{
    return preg_match("#^[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?(( |\-)[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?)*\$#i", $streetName);
}

function valCalleWithNum($streetName)
{
    return preg_match("#^[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?(( |\-)[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?)* (((#|[nN][oO]\.?) ?)?\d{1,4}(( ?[a-zA-Z0-9\-]+)+)?)\$#i", $streetName);
}

function valUsaStreet($streetName)
{
    return preg_match("/^([0-9]+)\s([a-z]+)(,\s|\s)([a-z]+)(,\s|\s)([a-z]+)(,\s|\s)([a-z0-9]+)$/i", $streetName);
}

function valSensibleAddress($address)
{
    return preg_match("/^([a-z0-9]+([_\.\-]{1}[a-z0-9]+)*){1}([@]){1}([a-z0-9]+([_\-]{1}[a-z0-9]+)*)+(([\.]{1}[a-z]{2,6}){0,3}){1}$/i", $address);
}

function valEmail($email)
{
    $reg = "#^(((([a-z\d][\.\-\+_]?)*)[a-z0-9])+)\@(((([a-z\d][\.\-_]?){0,62})[a-z\d])+)\.([a-z\d]{2,6})$#i";
    return preg_match($reg, $email);
}
//ejemplo:
// if (valEmail("info@educacionit.com")) {
//     echo "email valido";
// } else {
//     echo "email invalido";
// }
function valEmail2($email)
{
    $reg = "#^[a-z0-9_\-]+(\.[_a-z0-9\-]+)*@([_a-z0-9\-]+\.)+([a-z]{2}|aero|asia|arpa|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|xxx)$#i";
    return preg_match($reg, $email);
}

function valRFC($email)
{
    $reg = "#^[A-Z]{3,4}[ \-]?[0-9]{2}((0{1}[1-9]{1})|(1{1}[0-2]{1}))((0{1}[1-9]{1})|([1-2]{1}[0-9]{1})|(3{1}[0-1]{1}))[ \-]?[A-Z0-9]{3}$#i";
    return preg_match($reg, $email);
}

function valFecha($fecha)
{
    $sep = "[\/\-\.]";
    $req = "#^(((0?[1-9]|1\d|2[0-8]){$sep}(0?[1-9]|1[012])|(29|30){$sep}(0?[13456789]|1[012])|31{$sep}(0?[13578]|1[02])){$sep}(19|[2-9]\d)\d{2}|29{$sep}0?2{$sep}((19|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00)))$#";
    return preg_match($req, $fecha);
}
//ejemplo:
// if (valFecha("05/01/2011")) {
//     echo "fecha valida";
// } else {
//     echo "fecha invalida";
// }

function valIP($ip)
{
    $val_0_to_255 = "(25[012345]|2[01234]\d|[01]?\d\d?)";
    $reg = "#^($val_0_to_255\.$val_0_to_255\.$val_0_to_255\.$val_0_to_255)$#";
    return preg_match($reg, $ip, $matches);
}
//ejemplo:
// if (valIP("190.210.132.55")) {
//     echo "IP valida";
// } else {
//     echo "IP invalida";
// }

function valTelefono($numero)
{
    $reg = "#^\(?\d{2}\)?[\s\.-]?\d{4}[\s\.-]?\d{4}$#";
    return preg_match($reg, $numero);
}
//ejemplo:
// if (valTelefono("(11)-4328-0457")) {
//     echo "telefono valido";
// } else {
//     echo "telefono invalido";
// }

function valTelefono2($numero)
{
    $reg = "/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/";
    return preg_match($reg, $numero);
}

function valCedulaConPunto($numero)
{
    $reg = "/(\.d*[0-9]{2})/";
    return preg_match($reg, $numero);
}

function valCedula($numero)
{
    $reg = "/(\W\d*[0-9]{2})/";
    return preg_match($reg, $numero);
}

function valTelefonoStrong($numero)
{
    $reg = "#^0{0,2}([\+]?[\d]{1,3} ?)?([\(]([\d]{2,3})[)] ?)?[0-9][0-9 \-]{6,}( ?([xX]|([eE]xt[\.]?)) ?([\d]{1,5}))?";
    return preg_match($reg, $numero);
}

function convertirURL($url)
{
    $host = "([a-z\d][-a-z\d]*[a-z\d]\.)+[a-z][-a-z\d]*[a-z]";
    $port = "(:\d{1,})?";
    $path = "(\/[^?<>\#\"\s]+)?";
    $query = "(\?[^<>\#\"\s]+)?";
    $reg = "#((ht|f)tps?:\/\/{$host}{$port}{$path}{$query})#i";
    return preg_replace($reg, "<a href='$1'>$1</a>", $url);
}
//ejemplo:
// echo convertirURL("visita https://www.educacionit.com");

function limpiarInsultos($string)
{
    function prep_regexp_array(&$item)
    {
        $item = "#$item#i";
    }
    function stars($matches)
    {
        return substr($matches[0], 0, 1) . str_repeat("*", strlen($matches[0]) - 1);
    }
    $swears = array("tonto", "idiota"); //palabras a bloquear
    array_walk($swears, "prep_regexp_array");
    return preg_replace_callback($swears, "stars", $string);
}
//ejemplo:
// echo limpiarInsultos("Juan es un idiota");

function limpiarTags($source, $tags = null)
{
    function clean($matched)
    {
        $attribs =
            "javascript:|onclick|ondblclick|onmousedown|onmouseup|onmouseover|" .
            "onmousemove|onmouseout|onkeypress|onkeydown|onkeyup|" .
            "onload|class|id|src|style";
        $quot = "\"|\'|\`";
        $stripAttrib = "' ($attribs)\s*=\s*($quot)(.*?)(\\2)'i";
        $clean = stripslashes($matched[0]);
        $clean = preg_replace($stripAttrib, '', $clean);
        return $clean;
    }
    $allowedTags = '<a><br><b><i><br><li><ol><p><strong><u><ul>';
    $clean = strip_tags($source, $allowedTags);
    $clean = preg_replace_callback('#<(.*?)>#', "clean", $source);
    return $source;
}
//ejemplo:
// echo limpiarTags("este código es malicioso <script>alert('hola!')</script>");

function convertirBBcode($string)
{
    $string = strip_tags($string);
    $patterns = array(
        "bold" => "#\[b\](.*?)\[/b\]#is",
        "italics" => "#\[i\](.*?)\[/i\]#is",
        "underline" => "#\[u\](.*?)\[/u\]#is",
        "link_title" => "#\[url=(.*?)](.*?)\[/url\]#i",
        "link_basic" => "#\[url](.*?)\[/url\]#i",
        "color" => "#\[color=(red|green|blue|yellow)\](.*?)\[/color\]#is"
    );
    $replacements = array(
        "bold" => "<b>$1</b>",
        "italics" => "<i>$1</i>",
        "underline" => "<u>$1</u>",
        "link_title" => "<a href=\"$1\">$2</a>",
        "link_basic" => "<a href=\"$1\">$1</a>",
        "color" => "<span style='color:$1;'>$2</span>"
    );
    return preg_replace($patterns, $replacements, $string);
}
//ejemplo:
// echo convertirBBcode("[b]letra negrita[/b]");
