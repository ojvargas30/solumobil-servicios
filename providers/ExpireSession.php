<?php

interface IExpireSession
{
    public static function createCookieExpired();
    public static function showMessageExpired($msg);
    public static function destroySession();
    public static function valCookieExpired();
}

class ExpireSession implements IExpireSession
{
    // Logica: Creamos una cookie y si esta o si se vence
    // Cuando esta en el home y recargue o valide si se vencio la sesión
    // Index
    public static function createCookieExpired($time_minuts = 30)
    {
        setCookie("sesion_expired", "000000", round(time()+60)*$time_minuts); // Se crea la de sesion_expired
        setCookie("show_alert", "00000", time() - 60 * 60 * 24 * 365 * 2); // Se quita otra diferente
    }

    public static function destroySession()
    {
        setCookie("PHPSESSID", "", time() - 60 * 60 * 24 * 365);
        session_unset();
        session_destroy();
    }

    public static function valCookieExpired()
    {
        if (!isset($_COOKIE["sesion_expired"])) {
            self::destroySession();
            setCookie("show_alert", "expirada", time() + 60 * 60 * 24 * 365 * 2);
            self::showMessageExpired("¡Tu sesión ha expirado!");
        }
    }

    public static function showMessageExpired($msg)
    {
        if (!isset($_COOKIE["show_alert"])) {

            echo "
             <script>
                   alert('$msg');
             </script>
            ";
        }
    }

}
