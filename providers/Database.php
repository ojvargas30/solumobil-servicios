<?php

/**
 * CONEXION Y ESTANDAR DE CONTROL DE LA CRUD
 * CRUD GENERICO
 */

class Database extends PDO
{
	// definir atributos
	private $host 		= DB_HOST;
	private $dbName 	= DB_NAME;
	private $user 		= DB_USER;
	private $password 	= DB_PASSWORD;
	private $driver 	= DB_DRIVER;
	private $charset 	= DB_CHARSET;

	// Modificacion
	private $lastInsertId = null;

	// sobrecarga al constructor con cadena de conexion a la BD
	public function __construct()
	{
		try {
			// ESTANDAR
			parent::__construct("{$this->driver}:host={$this->host};
				dbname={$this->dbName}; charset={$this->charset}", $this->user, $this->password);
			$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			echo "Conexión Fallida {$e->getMessage()}";
		}
	}
	/*
		@param $table String
		@param $where Array [key => value]
	*/
	public function getByWhere($table, $where)
	{
		try {
			$column = array_keys($where)[0]; // trae la llave o indice
			$value = array_values($where)[0]; // trae su valor
			$stmt = $this->prepare("SELECT * FROM {$table} WHERE {$column} = :columnvalue");
			$stmt->bindParam(":columnvalue", $value);
			$stmt->execute();
			return $stmt->fetch(PDO::FETCH_OBJ); // Solo objetos papa!
		} catch (Exception $e) {
			return false;
		}
	}

	public function select($strSql, $arrayData = array(), $fetchMode = PDO::FETCH_OBJ)
	{
		// prepara al query
		$query = $this->prepare($strSql);

		// asigna parametros al query si llegan como un array
		foreach ($arrayData as $key => $value) {

			$query->bindParam(":$key", $value);
		}

		// valida si se ejecuta o no el query
		if (!$query->execute()) echo "La consulta no se realizo";
		else
		// devuelve el objeto del query
		return $query->fetchAll($fetchMode);
	}
	/*
	* @param $table String "example"
	* @param $data Array ej: [fieldName => fieldValue]
	* @param $fetchMode PDO::__const__ OPTIONAL
	*/
	public function selectOne($table, $data = array(), $fetchMode = PDO::FETCH_OBJ)
	{
		$key = array_key_first($data);
		$value = $data[$key];
		$query = $this->prepare("SELECT * FROM {$table} WHERE {$key} = :{$key}");
		$query->bindParam(":{$key}", $value);
		$query->execute();
		return $query->fetch($fetchMode);
	}

	public function insert($table, $data)
	{

		try {
			ksort($data);
			unset($data['controller'], $data['method']);
			$fieldNames = implode('`, `', array_keys($data));
			$fieldValues = ':' . implode(', :', array_keys($data));
			$strSql = $this->prepare("INSERT INTO $table (`$fieldNames`)VALUES ($fieldValues)");

			foreach ($data as $key => $value) {
				$strSql->bindValue(":$key", $value);
			}
			$strSql->execute();
			$this->lastInsertId = $this->lastInsertId();
			return true;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getLastId()
	{
		return $this->lastInsertId;
	}

	public function update($table, $data, $where)
	{
		try {
			ksort($data);

			$fieldDetails = null;

			foreach ($data as $key => $value) $fieldDetails .= "`$key` =:$key,";

			$fieldDetails = rtrim($fieldDetails, ',');

			$strSql = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");

			foreach ($data as $key => $value) $strSql->bindValue(":$key", $value);

			$strSql->execute();

			return true;
		} catch (PDOException $e) {
			dd($e->getMessage());
		}
	}


	public function delete($table, $where)
	{
		try {
			return $this->exec("DELETE FROM $table WHERE $where");
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function erase($idName = '', $id, $nameTable)
	{
		$this->delete($nameTable, $idName.'='.$id);
	}
}
