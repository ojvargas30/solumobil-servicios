<?php
class HandleDate
{
    public function sumarHoras($arrHoursToSum)
    {
        $total = 0;

        foreach ($arrHoursToSum as $h) {

            $parts = explode(":", $h);

            $total += $parts[1] * 60 + $parts[0] * 3600;
        }

        return gmdate("H:i:s", $total);
    }

    // Devuelve true si la fecha ingresada esta entre el rango de startHour y limit
    // limit es la suma de startHour mas sumHour usando el método sumarHoras
    public function dateBetweenRange($mainHour = '', $startHour = '', $sumHour = '02:45:00')
    {
        $mainHour = $this->convertTime24To12AMPM($mainHour);

        $startHour = $this->convertTime24To12($startHour);

        // dd($mainHour,$startHour);
        // dd($mainHour,$this->convertTime24To12AMPM($startHour));

        $horasbd = [$startHour, $sumHour];

        $limit = $this->sumarHoras($horasbd);

        $limit = $this->convertTime24To12AMPM($limit);

        // dd($this->convertTime24To12AMPM($startHour),$limit);

        // dd($mainHour >= $this->convertTime24To12AMPM($startHour) && $mainHour <= $limit);

        if ($mainHour >= $this->convertTime24To12AMPM($startHour) && $mainHour <= $limit) return true; // Esta entre el rango

        return false; // No hay cruce de horarios
    }

    public function futureDate($formDate)
    {
        $today = date("Y-m-d H:i:s");

        if ($today <= $formDate) return true;
        return false;
    }

    public function sepDateTime($datetime)
    {
        if (strlen($datetime) > 20 || strlen($datetime) < 16) return false;

        $separated = (explode(" ", $datetime));

        // echo $fecha = "Fecha: " . $separated[0] . " Hora: " . $hora = $separated[1];
        return $separated;
    }

    public function convertTime24To12AMPM($time)
    {
        $timeAmPm = date("g:i a", strtotime($time));

        return $timeAmPm;
    }

    public function convertTime24To12($time)
    {
        $time12 = date("g:i:s", strtotime($time));

        return $time12;
    }
}
