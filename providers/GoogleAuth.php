<?php

function getGoogleData()
{
    $googleClient = new Google_Client();
    $auth = new GoogleAuth($googleClient);
    return $auth;
}

function getGoogleData2()
{
    $googleClient = new Google_Client();
    $auth         = new GoogleAuth2($googleClient);
    return $auth;
}

class GoogleAuth // CREAR USUARIO
{

    protected $client;

    /**
     * Class constructor of GoogleAuth.
     */
    public function __construct(Google_Client $googleClient = null)
    {
        $this->client = $googleClient;

        if ($this->client) {
            $this->client->setClientId(CLIENT_ID); // clave
            $this->client->setClientSecret(CLIENT_SECRET);
            $this->client->setRedirectUri(REDIRECT_URL);
            $this->client->setScopes('email');
        }
    }

    public function getAuthUrl()
    {
        // dd($this->client->createAuthUrl());
        return $this->client->createAuthUrl();
    }

    public function checkRedirectCodeAndGetAuthGoogleData()
    {
        if (isset($_GET['code'])) {

            try {
                $token = $this->client->fetchAccessTokenWithAuthCode($_GET['code']); // se obtiene el token
                $this->setToken($token['access_token']);
                $payLoad = $this->getPayLoad();
            } catch (Exception $e) {
                dd($e);
            }
            // dd($payLoad);
            return $payLoad;
        }

        return false;
    }

    public function setToken($token)
    {
        $_SESSION['access_token'] = $token;
        $this->client->setAccessToken($token); //Aqui se pasa el token
    }

    public function getPayLoad()
    {
        // $payLoad = $this->client->getOAuth2Service()->verifyIdToken()->getAttributes();
        // return $payLoad;

        $googleOAuth = new Google_Service_Oauth2($this->client);
        $google_account_info = $googleOAuth->userinfo->get();

        // dd($google_account_info);
        // $email = $google_account_info->email;
        // $name = $google_account_info->name;

        return $google_account_info;
    }
}

class GoogleAuth2 // INGRESAR
{

    protected $client;

    /**
     * Class constructor of GoogleAuth.
     */
    public function __construct(Google_Client $googleClient = null)
    {
        $this->client = $googleClient;

        if ($this->client) {
            $this->client->setClientId(CLIENT_ID2);
            $this->client->setClientSecret(CLIENT_SECRET2);
            $this->client->setRedirectUri(REDIRECT_URL2);
            $this->client->setScopes('email');
        }
    }

    public function getAuthUrl()
    {
        return $this->client->createAuthUrl();
    }

    public function checkRedirectCodeAndGetAuthGoogleData()
    {
        if (isset($_GET['code'])) {

            try {
                $token = $this->client->fetchAccessTokenWithAuthCode($_GET['code']); // se obtiene el token
                $this->setToken($token['access_token']);
                $payLoad = $this->getPayLoad();
            } catch (Exception $e) {
                dd($e);
            }
            // dd($payLoad);
            return $payLoad;
        }

        return false;
    }

    public function setToken($token)
    {
        $_SESSION['access_token'] = $token;
        $this->client->setAccessToken($token); //Aqui se pasa el token
    }

    public function getPayLoad()
    {
        // $payLoad = $this->client->getOAuth2Service()->verifyIdToken()->getAttributes();
        // return $payLoad;

        $googleOAuth = new Google_Service_Oauth2($this->client);
        $google_account_info = $googleOAuth->userinfo->get();

        // $email = $google_account_info->email;
        // $name = $google_account_info->name;

        return $google_account_info;
    }
}
