<?php
// Mostrar errores PHP (Desactivar en producción)
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once 'assets/plugins/PHPMailer/src/Exception.php';
require_once 'assets/plugins/PHPMailer/src/SMTP.php';
require_once 'assets/plugins/PHPMailer/src/PHPMailer.php';

function dd($array, $array2 = null, $stop = true)
{
    echo "<pre>";
    !empty($array2) ? var_dump($array, $array2) : var_dump($array);
    echo "</pre>";
    if ($stop) {
        exit();
    }
}

function httpResponse($type, $message, $params = null, $url = '', $throwEmail = false, $email = '')
{
    $response['message'] = $message;
    $response[array_key_first($type)] = array_values($type)[0];

    if (!empty($params)) {
        $params = (array) $params;
        $response[array_key_first($params)] = array_values($params)[0];
    }

    if ($throwEmail) $resp = ["resp" => $response, "urlC" => $url, "email" => $email];

    else $resp = ["resp" => $response, "urlC" => $url];

    echo json_encode($resp);
    return;
}

function sendEmail($correo, $user, $link, $msg, $temp, $verb)
{
    // Instantiation and passing `true` enables exceptions
    // $mail = new PHPMailer(true);

    try {
        // $mail->CharSet = "UTF-8";

        // // $mail->SMTPOptions = array(
        // //     'ssl' => array(
        // //         'verify_peer' => false,
        // //         'verify_peer_name' => false,
        // //         'allow_self_signed' => true
        // //     )
        // // );
        // // $mail->Encoding = 'base64';
        // // $mail->Encoding = "16bit";
        // //Server settings
        // $mail->SMTPDebug = SMTP::DEBUG_OFF;                      // Enable verbose debug output
        // // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        // // $mail->SMTPDebug = 2;                      // Enable verbose debug output

        // $mail->isSMTP();                                            // Send using SMTP
        // $mail->Host       = PHPMAILER_HOST;                    // Set the SMTP server to send through
        // $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        // $mail->Username   = PHPMAILER_USER;                     // SMTP username
        // $mail->Password   = PHPMAILER_PASS;
        // // SMTP password
        // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        // $mail->SMTPSecure = 'tls';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        // $mail->Port       = PHPMAILER_PORT;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        // //Recipients
        // $mail->setFrom(PHPMAILER_USER, TITLE_APP);
        // $mail->addAddress($correo, $user);     // Add a recipient

        // // Content
        // $mail->isHTML(true);                                  // Set email format to HTML
        // $mail->Subject = 'Bienvenido a Solupanel de Solumobil';
        // $mail->Body    = $temp($user, $link, $verb);
        // $mail->AltBody = 'Saludos ' . $correo . $msg . $link;

        // $mail->send();

        // ENVIAR CORREO CON MAIL() DE php
        // Cabeceras adicionales
        // ENVIAR CORREO CON MAIL() DE php
        // Cabeceras adicionales

        // ------------------------------------------------------------

        // ENVIAR CORREO CON MAIL() DE php
        // Cabeceras adicionales
        $cabeceras = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
        $cabeceras .= 'To: ' . ($user) ? $user : 'Usuario' . ' <' . $correo . '>' . "\r\n";
        $cabeceras .= 'From:  Solumobil <adtestjavis@gmail.com>' . "\r\n";
        if (mail($correo, "Bienvenido a Solupanel de Solumobil", '<p>' . $link . '</p>', $cabeceras)) {
            return true;
        } else {
            return false;
        }
        // echo 'El mensaje ha sido enviado';
        return true;
    } catch (Exception $e) {
        dd($e);
    }
}

// function sendEmail($correo, $user, $link, $msg, $temp, $verb)
// {
//     // Instantiation and passing `true` enables exceptions
//     $mail = new PHPMailer(true);

//     try {
//         //$mail->CharSet = "UTF-8";

//         $mail->SMTPOptions = array(
//        'ssl' => array(
//                 'verify_peer' => false,
//                 'verify_peer_name' => false,
//                 'allow_self_signed' => true
//             )
//         );
//         // $mail->Encoding = 'base64';
//         // $mail->Encoding = "16bit";
//         //Server settings
//         //$mail->SMTPDebug = SMTP::DEBUG_OFF;                      // Enable verbose debug output
//         // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
//         $mail->SMTPDebug = 6;                      // Enable verbose debug output

//         $mail->isSMTP();                                            // Send using SMTP
//         // $mail->Host       ='hostingbricks.com';                    // Set the SMTP server to send through
//         $mail->SMTPAuth   = true;
//         $mail->SMTPSecure = "tls";

//         // $mail->SMTPSecure = 'tls';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged

//         $mail->Username   ='adtestjavis@gmail.com';                     // SMTP username
//         $mail->Password   ='delfines_123';
//         // SMTP password
//         //$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
//         $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

//         //Recipients
//         $mail->setFrom('adtestjavis@gmail.com', TITLE_APP);
//         $mail->addAddress($correo, $user);     // Add a recipient

//         // Content
//         $mail->isHTML(true);                                  // Set email format to HTML
//         $mail->Subject = 'Bienvenido a Solupanel de Solumobil';
//         $mail->Body    = $temp($user, $link, $verb);
//         $mail->AltBody = 'Saludos ' . $correo . $msg . $link;

//         $mail->send();
//         // echo 'El mensaje ha sido enviado';
//         return true;
//     } catch (Exception $e) {
//         exit("Error de envio: {$mail->ErrorInfo}");
//     }
// }


// function sendEmail($correo, $user, $link, $msg, $temp, $verb)
// {
//     // Instantiation and passing `true` enables exceptions
//     $mail = new PHPMailer(true);

//     try {
//         // $mail->Encoding = 'base64';
//         // $mail->Encoding = "16bit";
//         //Server settings
//         // $mail->SMTPDebug = SMTP::DEBUG_OFF;                      // Enable verbose debug output
//         $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
//         // $mail->SMTPDebug = 2;                      // Enable verbose debug output
//         $mail->SMTPDebug = 4;                      // Enable verbose debug output

//         $mail->isSMTP();                                            // Send using SMTP
//         // $mail->SMTPAutoTLS = false;

//         // $mail->isSMTP();
//         // $mail->Username   = PHPMAILER_USER;                     // SMTP username
//         $mail->Username   = 'adtestjavis@gmail.com';                     // SMTP username
//         $mail->Password   = 'delfines_123';

//         // SMTP password
//         // $mail->Password   = PHPMAILER_PASS;

//         // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
//         // $mail->SMTPSecure = "ssl";
//         // $mail->SMTPSecure = '';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged

//         $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
//         // $mail->SMTPAuth = false;
//         $mail->SMTPSecure = 'tls';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged

//         $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
//         // $mail->Host       = PHPMAILER_HOST;                    // Set the SMTP server to send through
//         // $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
//         // $mail->Host = 'localhost';
//         // $mail->Port       = PHPMAILER_PORT;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
//         $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
//         // $mail->Port = 25;
//         $mail->SMTPOptions = array(
//             'ssl' => array(
//                 'verify_peer' => false,
//                 'verify_peer_name' => false,
//                 'allow_self_signed' => true
//             )
//         );


//         //Recipients
//         $mail->CharSet = "UTF-8";
//         $mail->setFrom('adtestjavis@gmail.com', 'asdfhgjhsdasd');
//         // $mail->setFrom(PHPMAILER_USER, TITLE_APP);
//         $mail->addAddress($correo, $user);     // Add a recipient

//         // Content
//         $mail->isHTML(true);                                  // Set email format to HTML
//         $mail->Subject = 'Bienvenido a Solupanel de Solumobil';
//         $mail->Body    = $temp($user, $link, $verb);
//         $mail->AltBody = 'Saludos ' . $correo . $msg . $link;

//         $mail->send();
//         // echo 'El mensaje ha sido enviado';
//         return true;
//     } catch (Exception $e) {
//         exit("Error de envio: {$mail->ErrorInfo}");
//     }
// }


function PlantillaEmailRecoveryPass($user, $enlace)
{
    $html = '<!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="utf-8">
        </head>

        <body style="background-color: #FFF; font-family: Verdana; font-size:14;">
            <div>
                <h2>Bienvenido usuario: ' . $user . '</h2>
                <p style="font-size:17px;">
                    Gracias por elegir ' . TITLE_APP . '.
                </p>
                <p style="padding:15px;background-color:#f2f2f2;">
                    Para recuperar su cuenta de <a class="" style="font-weight-bold; color:#9e0000" href="' . $enlace . '"
                        target="_blank">Click aqui &raquo;</a>
                </p>
                <p style="font-size:9px;">&copy;' . date('Y', time()) . ' ' . TITLE_APP . '.
                    Todos los derechos registrados.</p>
            </div>
        </body>
        </html>';

    return $html;
}
