// MODO OSCURO CON LOCAL STORAGE
const btnSwitch = document.querySelector('#switch');

if(btnSwitch){
btnSwitch.addEventListener('click', () => {
    document.body.classList.toggle('dark');//añade o sobreescribe clase
    btnSwitch.classList.toggle('active');

    //Guardar el modo en LocalStorage
    if (document.body.classList.contains('dark')) {
        localStorage.setItem('dark-mode', 'true');
    } else {
        localStorage.setItem('dark-mode', 'false');
    }
});

// Obtenemos el modo actual
if (localStorage.getItem('dark-mode') === 'true') {
    document.body.classList.add('dark');//añade a LocalStorage
    btnSwitch.classList.toggle('active');
} else {
    document.body.classList.remove('dark');//remueve de LocalStorage
    btnSwitch.classList.remove('active');
}
}

// TOASTR Y SWAL
$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    $('.swalDefaultSuccess').click(function () {
        Toast.fire({
            icon: 'success',
            title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.swalDefaultInfo').click(function () {
        Toast.fire({
            icon: 'info',
            title: 'Podras generar datos para completar datos',
            text: 'Si no lo encuentras en lista'
        })
    });
    $('.swalDefaultError').click(function () {
        Toast.fire({
            icon: 'error',
            title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.swalDefaultWarning').click(function () {
        Toast.fire({
            icon: 'warning',
            title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.swalDefaultQuestion').click(function () {
        Toast.fire({
            icon: 'question',
            title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });

    $('.toastrDefaultSuccess').click(function () {
        toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
    });
    $('.toastrDefaultInfo').click(function () {
        toastr.info('Puedes generar datos para una experiencia personalizada', "", { timeOut: 4000, closeButton: true, positionClass: "toast-bottom-left" })
    });
    $('.toastrDefaultError').click(function () {
        toastr.error('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
    });
    $('.toastrDefaultWarning').click(function () {
        toastr.warning('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
    });

    $('.toastsDefaultDefault').click(function () {
        $(document).Toasts('create', {
            title: 'Toast Title',
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.toastsDefaultTopLeft').click(function () {
        $(document).Toasts('create', {
            title: 'Toast Title',
            position: 'topLeft',
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.toastsDefaultBottomRight').click(function () {
        $(document).Toasts('create', {
            title: 'Toast Title',
            position: 'bottomRight',
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.toastsDefaultBottomLeft').click(function () {
        $(document).Toasts('create', {
            title: 'Atención',
            position: 'bottomLeft',
            body: 'Puedes generar datos para una experiencia personalizada'
        })
    });
    $('.toastsDefaultAutohide').click(function () {
        $(document).Toasts('create', {
            title: 'Toast Title',
            autohide: true,
            delay: 750,
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.toastsDefaultNotFixed').click(function () {
        $(document).Toasts('create', {
            title: 'Toast Title',
            fixed: false,
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.toastsDefaultFull').click(function () {
        $(document).Toasts('create', {
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.',
            title: 'Toast Title',
            subtitle: 'Subtitle',
            icon: 'fas fa-envelope fa-lg',
        })
    });
    $('.toastsDefaultFullImage').click(function () {
        $(document).Toasts('create', {
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.',
            title: 'Toast Title',
            subtitle: 'Subtitle',
            image: '../../dist/img/user3-128x128.jpg',
            imageAlt: 'User Picture',
        })
    });
    $('.toastsDefaultSuccess').click(function () {
        $(document).Toasts('create', {
            class: 'bg-success',
            title: 'Toast Title',
            subtitle: 'Subtitle',
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.toastsDefaultInfo').click(function () {
        $(document).Toasts('create', {
            class: 'bg-info',
            title: 'Toast Title',
            subtitle: 'Subtitle',
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.toastsDefaultWarning').click(function () {
        $(document).Toasts('create', {
            class: 'bg-warning',
            title: 'Toast Title',
            subtitle: 'Subtitle',
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.toastsDefaultDanger').click(function () {
        $(document).Toasts('create', {
            class: 'bg-danger',
            title: 'Toast Title',
            subtitle: 'Subtitle',
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
    $('.toastsDefaultMaroon').click(function () {
        $(document).Toasts('create', {
            class: 'bg-maroon',
            title: 'Toast Title',
            subtitle: 'Subtitle',
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
    });
});
// END

// FORM INTERNO GHOST
$(function () {
    $('#ghost-child').click(function () {
        $(this).next('#ghost-child-content').slideToggle();
        $(this).toggleClass('active');
    });
});

$(function () {
    $('#ghost-child-2').click(function () {
        $(this).next('#ghost-child-content-2').slideToggle();
        $(this).toggleClass('active');
    });
});

// vista cliente para mostrar artefactos y reparaciones
$(function () {
    $('#ghost-child-3').click(function () {
        $(this).next('#ghost-child-content-3').slideToggle();
        $(this).toggleClass('active');
    });
});

$(function () {
    $('#ghost-child-4').click(function () {
        $(this).next('#ghost-child-content-4').slideToggle();
        $(this).toggleClass('active');
    });
});

$(function () {
    $('#ghost-child-artifact').click(function () {
        $(this).next('#ghost-child-content-artifact').slideToggle();
        $(this).toggleClass('active');
    });
});

$(function () {
    $('#ghost-child-brand').click(function () {
        $(this).next('#ghost-child-content-brand').slideToggle();
        $(this).toggleClass('active');
    });
});



// AUTOFOCUS EN MODAL
$(function () {
    $('#myModalLeft').on('shown.bs.modal', function (e) {
        $('.focus').focus();
    })
});

$(function () {
    $('#modal').on('shown.bs.modal', function (e) {
        $('.focus').focus();
    })
});

// MODAL DENTRO DE MODAL
// $(document).on('click', '#Crear', function() {
// $('#ModalCrear').modal('show');
// });

$(document).on('click', '#modalA', function () {
    $('#modArtifact').modal('show');
});

// $(document).on('click', '#modalC', function() {
// $('#modCita').modal('show');
// });

// $(document).on('click', '#modalCa', function() {
// $('#modCategory').modal('show');
// });

// $(document).on('click', '#modalT', function() {
// $('#modTec').modal('show');
// });

// // VIDEO INSTRUCTIVO
// var video = document.getElementById("myVideo");
// var btn = document.getElementById("myBtn");

// function myFunction() {
//     if (video.paused) {
//         video.play();
//         btn.innerHTML = "Pause";
//     } else {
//         video.pause();
//         btn.innerHTML = "Play";
//     }
// }

/*SELECT CON BUSCADOR*/
$(document).ready(function () {
    $('.controlBuscador').select2();
});

/*EFECTO DEL FOOTER OCULTO*/
function ocultar() {
    document.getElementById('foot').style.display = 'none';
}

/*EFECTO HOVER DEL DROPDOWN*/
$(function () {
    $(".dropdown").hover(
        function () {
            $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
            $(this).toggleClass('open');
            $('b', this).toggleClass("caret caret-up");
        },
        function () {
            $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
            $(this).toggleClass('open');
            $('b', this).toggleClass("caret caret-up");
        });
});

// EFECTO CLIC DEL DROPDOWN NO FUNCIONA
// $('.dropdown-toggle').click(function () {
// $(this).next('.dropdown-menu').slideToggle(300);
// });

// $('.dropdown-toggle').focusout(function () {
// $(this).next('.dropdown-menu').slideUp(600);
// });

// DATATABLE EN ESPAÑOL: PRESENTABA ERROR CON EL COLSPAN DE LA TABLA
// $(document).ready(function () {
//     $('.datatable').DataTable({
//         "language": {
//             "url": "assets/plugins/datatable/js/Spanish.json",
//             "order": [
//                 [0, "desc"]
//             ],
//             "paging": false
//         }
//     }).order([0, "desc"]);
// });

// EFECTO DE MOSTRAR Y OCULTAR DASH
$("#menu-toggle").click(function (e) {
    e.preventDefault();
    $("#content-wrapper").toggleClass("toggled");
});

/*STACKTABLE*/

// $(document).ready(function() {
// $('.stackeame').cardtable();
// });

// TRUNCAR TEXTO
// function ocultar(id) {
// let elemento = document.getElementById('mostrarOcultar'+id);
// elemento.style.overflow = 'hidden';
// elemento.style.whiteSpace = 'nowrap';
// // elemento.style.display = 'none';
// let ocultar = document.getElementById('ocultar');
// ocultar.style.display = 'none';
// let mostrar = document.getElementById('mostrar');
// mostrar.style.display = 'block';
// }

// function mostrar(id) {
// let elemento = document.getElementById('mostrarOcultar'+id);
// elemento.style.overflow = 'visible';
// elemento.style.whiteSpace = 'normal';
// elemento.style.textOverflow = 'ellipsis';
// // elemento.style.display = 'none';
// let mostrar = document.getElementById('mostrar');
// mostrar.style.display = 'none';
// let ocultar = document.getElementById('ocultar');
// ocultar.style.display = 'block';
// }



// RELOJ
let dt = new Date();
let h = dt.getHours(), m = dt.getMinutes(),
    hoy = dt.toLocaleDateString();
if (m.length < 1) {
    m = m + 0;
}
let now = (h > 12) ? (h - 12 + ':' + m + ' PM') : (h + ':' + m + ' AM');
let boxWatch = document.getElementById("time");
if(boxWatch){
    boxWatch.textContent = now + ' ' + hoy;
}

function swal(msg = '', type = '', text = '', timer = 2000, showConfirmButton = false, position = '', background = '', padding = '', width = null) {
    Swal.fire({
        icon: type,
        title: `<span class="pad">${msg}</span>`,
        imageUrl: 'https://unsplash.it/400/200',
        imageWidth: 300,
        imageHeight: 100,
        imageAlt: '¡Splash!',
        text: text,
        width: width,
        position: position,
        showConfirmButton: showConfirmButton,
        timer: timer,
        padding: padding,
        backdrop: background,
        footer: '<a href="mailto:oscarjaviervargas@hotmail.com">Reportar problema</a>'
    })
}


$(function () {
    $("#modalToggle").click(function () {
        $("#modal").modal({
            backdrop: "static"
        });
    });

    $("#infoContinue").click(function (e) {
        e.preventDefault();
        $(".progress-bar").css("width", "40%");
        $(".progress-bar").html("Paso 2 de 4");
        $('#myTab a[href="#ads"]').tab("show");
    });

    $("#adsContinue").click(function (e) {
        e.preventDefault();
        $(".progress-bar").css("width", "60%");
        $(".progress-bar").html("Paso 3 de 4");
        $('#myTab a[href="#placementPanel"]').tab("show");
    });

    $("#placementContinue").click(function (e) {
        e.preventDefault();
        $(".progress-bar").css("width", "80%");
        $(".progress-bar").html("Paso 4 de 4");
        $('#myTab a[href="#schedulePanel"]').tab("show");
    });
});

// const changeTabsService = (tabBtnId, targetTab, percent, step) => {

//     $(`#${tabBtnId}-data-continue`).click(function (e) {
//         e.preventDefault();
//         $(".progress-bar").css("width", `${percent}%`);
//         $(".progress-bar").html(`Paso ${step} de 9`);
//         $(`#myTab a[href="#${targetTab}-data"]`).tab("show");
//     });
// }

// $(function () {
//     $("#modalToggleTechAdmin").click(function () {
//         $("#modal").modal({
//             backdrop: "static"
//         });
//     });

//     changeTabsService("service", "artifact", "10", "2")
//     changeTabsService("artifact", "client", "25", "3")
//     changeTabsService("client", "tech", "40", "4")
//     changeTabsService("tech", "diagnostico", "50", "5")
//     changeTabsService("diag", "cita", "65", "6")
//     changeTabsService("cita", "cost", "80", "7")
//     changeTabsService("cost", "evidence", "90", "8")
//     changeTabsService("evidence", "vsvdv", "100", "9")
// });

$(function () {
    $("#modalToggleTechAdmin").click(function () {
        $("#modal").modal({
            backdrop: "static"
        });
    });

    $("#service-data-continue").click(function (e) {
        e.preventDefault();
        $(".progress-bar").css("width", "10%");
        $(".progress-bar").html("Paso 2 de 6");
        $('#myTab a[href="#artifact-data"]').tab("show");
    });

    $("#artifact-data-continue").click(function (e) {
        e.preventDefault();
        $(".progress-bar").css("width", "45%");
        $(".progress-bar").html("Paso 3 de 6");
        $('#myTab a[href="#client_data"]').tab("show");
    });

    $("#client_data_continue").click(function (e) {
        e.preventDefault();
        $(".progress-bar").css("width", "70%");
        $(".progress-bar").html("Paso 4 de 6");
        $('#myTab a[href="#diag_data"]').tab("show");
    });

    // $("#tech_data_continue").click(function (e) {
    //     e.preventDefault();
    //     $(".progress-bar").css("width", "50%");
    //     $(".progress-bar").html("Paso 5 de 6");
    //     $('#myTab a[href="#diagnostico_data"]').tab("show");
    // });

    $("#diag_data_continue").click(function (e) {
        e.preventDefault();
        $(".progress-bar").css("width", "85%");
        $(".progress-bar").html("Paso 5 de 6");
        $('#myTab a[href="#cost_data"]').tab("show");
    });

    // $("#cita_data_continue").click(function (e) {
    //     e.preventDefault();
    //     $(".progress-bar").css("width", "80%");
    //     $(".progress-bar").html("Paso 7 de 9");
    //     $('#myTab a[href="#cost_data"]').tab("show");
    // });

    $("#cost_data_continue").click(function (e) {
        e.preventDefault();
        $(".progress-bar").css("width", "100%");
        $(".progress-bar").html("Paso 6 de 6");
        $('#myTab a[href="#evidence_data"]').tab("show");
    });

    // $("#evidence_data_continue").click(function (e) {
    //     e.preventDefault();
    //     $(".progress-bar").css("width", "100%");
    //     $(".progress-bar").html("Paso 9 de 9");
    //     $('#myTab a[href="#vsvdv"]').tab("show");
    // });
});


function generatePasswordRand(length, type) {
    switch (type) {
        case 'num':
            characters = "0123456789";
            break;
        case 'alf':
            characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            break;
        case 'rand':
            //FOR ↓
            break;
        default:
            characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            break;
    }
    var pass = "";
    for (i = 0; i < length; i++) {
        if (type == 'rand') {
            pass += String.fromCharCode((Math.floor((Math.random() * 100)) % 94) + 33);
        } else {
            pass += characters.charAt(Math.floor(Math.random() * characters.length));
        }
    }
    return pass;
}

function vnum(maxle, elems = '.vnum') {
    let input = document.querySelectorAll(elems);
    console.log(input);
    if (input) {
        input.forEach(e => {
            e.addEventListener('input', function () {
                if (this.value.length > maxle)
                    this.value = this.value.slice(0, maxle);
            })
        });
    }
}

window.addEventListener('keyup', (e) => {
    // console.log(e);
    // console.log(e.ctrlKey && e.key == 'x');
    // console.log(e.key);
    // console.log(e.keyCode);
    // console.log(e.type);
    // console.log(e.code);
    // console.log(e.shiftKey);
    // console.log(e.altKey);
    // // console.log(e.altKey && String.fromCharCode(e.key) == 'A');
    // console.log(e.ctrlKey && e.shiftKey && e.key == 'a');
    // console.log(e.key == 'a' && e.key == 'b');
    // console.log(e.key == 'a');
    // console.log(e.ctrlKey && e.shiftKey && e.key == 'a');
    // alert(e.code);
    if (e.ctrlKey && e.key == 'z') ele('barsBtnDash').click();
    if (e.ctrlKey && e.key == 'x') ele('closeSessionSort').click();
})

window.addEventListener('keydown', (e) => {
    if (e.ctrlKey && e.key == 'z') ele('barsBtnDash').click();
    if (e.ctrlKey && e.key == 'x') ele('closeSessionSort').click();
})
