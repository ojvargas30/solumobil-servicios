function fillArrays(options, arrModule, moldObj = {}) {

    let { elemid, idProp, nameProp, nameType, pluralElemId } = options;
    pluralElemId = firstLetterCap(pluralElemId);
    let idVal = $(`#${elemid}`).val()
    let nameVal = $(`#${elemid} option:selected`).text()

    if (idVal) {
        if (typeof itemExists(idVal, arrModule, idProp) === 'undefined') {

            Object.defineProperties(moldObj, {
                [idProp]: {
                    value: idVal,
                    writable: true,
                    configurable: true,
                    enumerable: true
                },
                [nameProp]: {
                    value: nameVal,
                    writable: true,
                    configurable: true,
                    enumerable: true
                }
            })

            switch (elemid) {
                case 'scategory':
                    arrScategories.push(moldObj)
                    break;
                case 'artifact':
                    arrArtifacts.push(moldObj)
                    break;
            }
        } else {
            let timerInterval
            Swal.fire({
                title: `¡La ${nameType} ya se encuentra seleccionada!`,
                html: `Vuelva a intentarlo con otra ${nameType}`,
                timer: 2000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) b.textContent = Swal.getTimerLeft()
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer)
                    console.log('I was closed by the timer')

            })
        }

        // Mostrar
        switch (elemid) {
            case 'scategory':
                showScategories()
                break;
            case 'artifact':
                showArtifacts()
                break;
        }

    } else {
        Swal.fire({
            title: `Debe seleccionar ${nameType}`,
            text: '',
            imageUrl: 'https://unsplash.it/400/200',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image'
        })
    }

    console.log(arrModule);
};

// Validar la no repetición
function itemExists(itemId, arrMod, idModule) {

    return arrMod.find(mod => mod[idModule] == itemId)
}


// ESTA PARTE AUN NO ES GENERICA
// mostrar elementos
function showScategories() {
    $(`#list-scategories`).empty()

    let idScat = 0;

    arrScategories.forEach(function (item) {
        idScat = item.id_categoria_servicio_PK
        $(`#list-scategories`)
            .append('<div class="form-group border rounded-pill pt-1"><button id="btnRemScat" class="btn btn-sm mb-1 ml-2 border-0"><i class="fas fa-times text-secondary fa-xs"></i></button> <span class="font-sm-important pl-1 text-muted">' + item.nombre_cs + '</span></div>')
    })

    $('#btnRemScat').click(() => removeScategory(idScat, arrScategories, 'id_categoria_servicio_PK'))
}

// Quitar elementos
function removeScategory(itemId, arrMod, idName) {
    let index = arrScategories.indexOf(itemExists(itemId, arrMod, idName))
    arrScategories.splice(index, 1)
    showScategories()
}

// ESTA PARTE AUN NO ES GENERICA
// mostrar elementos
function showArtifacts() {
    $(`#list-artifacts`).empty()

    let idArti = 0;

    arrArtifacts.forEach(function (item) {
        idArti = item.id_artefacto_PK
        $(`#list-artifacts`)
            .append('<div class="form-group border rounded-pill pt-1"><button id="btnRemArti" class="btn btn-sm mb-1 ml-2 border-0"><i class="fas fa-times text-secondary fa-xs"></i></button> <span class="font-sm-important pl-1 text-muted">' + item.modelo + '</span></div>')
    })

    $('#btnRemArti').click(() => removeArtifact(idArti, arrArtifacts, 'id_artefacto_PK'))
}

// Quitar elementos
function removeArtifact(itemId, arrMod, idName) {
    let index = arrArtifacts.indexOf(itemExists(itemId, arrMod, idName))
    arrArtifacts.splice(index, 1)
    showArtifacts()
}