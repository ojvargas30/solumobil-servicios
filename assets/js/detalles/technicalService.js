// DECLARAR ARRAY GLOBAL QUE CONTENGA LA LISTA DE REVISIONES O DETALLES
// var arrTechnicians = []
showTechnicians()

$("#technical").change(function(e) {
    e.preventDefault()
        // console.log("Evitar recargo de la pagina al pulsar el botón con una función anonima")

    let idTechnical = $("#technical").val()
    let nameTechnical = $("#technical option:selected").text()

    // console.log(idTechnical,nameTechnical)

    if (idTechnical != '') {
        if (typeof existTechnical(idTechnical) === 'undefined') {
            arrTechnicians.push({
                'id_tecnico_PK': idTechnical,
                'num_id_tec': nameTechnical
            })
        } else {
            let timerInterval
            Swal.fire({
                title: '¡El técnico ya se encuentra seleccionado!',
                html: 'Vuelva a intentarlo con otro técnico',
                timer: 2000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        }

        showTechnicians()
    } else {
        Swal.fire({
            title: 'Debe seleccionar un técnico',
            text: '',
            imageUrl: 'https://unsplash.it/400/200',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
        })
    }

    console.log(arrTechnicians)
});

// Validar la no repetición
function existTechnical(idTechnical) {
    let existTechnical = arrTechnicians.find(function(technical) {
        return technical.id_tecnico_PK == idTechnical
    })
    return existTechnical
}

// mostrar elementos
function showTechnicians() {
    $("#list-technicians").empty()

    if(typeof arrTechnicians !== 'undefined'){
        arrTechnicians.forEach(function(technical) {
            if(technical.num_id_tec != undefined){
                $("#list-technicians").append('<div class="form-group border rounded-pill pt-1"><button onclick="removeTechnical(' + technical.id_tecnico_PK + ')" class="btn btn-sm mb-1 ml-2 border-0"><i class="fas fa-times text-secondary fa-xs"></i></button> <span class="font-sm-important pl-1 text-muted">  ' + technical.num_id_tec + '</span></div>')
            }
            // else{
            //     $("#list-technicians").append('<div class="form-group border rounded-pill pt-1"><button onclick="removeTechnical(' + technical.id_tecnico_PK + ')" class="btn btn-sm mb-1 ml-2 border-0"><i class="fas fa-times text-secondary fa-xs"></i></button> <span class="font-sm-important pl-1 text-muted">No identificado</span></div>')
            // }
        })
    }
}

// Quitar elementos
function removeTechnical(idTechnical) {
    let index = arrTechnicians.indexOf(existTechnical(idTechnical))
    arrTechnicians.splice(index, 1)
    showTechnicians()
}