showScategories();

let optionScategory = {
    elemid: 'scategory',
    idProp: 'id_categoria_servicio_PK', // Sirve como idModule
    nameProp: 'nombre_cs',
    nameType: 'categoría',
    pluralElemId: 'scategories'
}

$("#scategory").change(() => fillArrays(optionScategory, arrScategories));
