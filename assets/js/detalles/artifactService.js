// DECLARAR ARRAY GLOBAL QUE CONTENGA LA LISTA DE REVISIONES O DETALLES
// var arrArtifacts = []
showArtifacts()

$("#artifact").change(function (e) {
    e.preventDefault()
    // console.log("Evitar recargo de la pagina al pulsar el botón con una función anonima")

    let idArtifact = $("#artifact").val()
    let nameArtifact = $("#artifact option:selected").text()

    // console.log(idArtifact,nameArtifact)

    if (idArtifact != '') {
        if (typeof existArtifact(idArtifact) === 'undefined') {
            arrArtifacts.push({
                'id_artefacto_PK': idArtifact,
                'modelo': nameArtifact
            })
        } else {
            let timerInterval
            Swal.fire({
                title: '¡El artefacto ya se encuentra seleccionado!',
                html: 'Vuelva a intentarlo con otro artefacto',
                timer: 2000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        }

        showArtifacts()
    } else {
        Swal.fire({
            title: 'Debe seleccionar un artefacto',
            text: '',
            imageUrl: 'https://unsplash.it/400/200',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
        })
    }

    console.log(arrArtifacts)
});

// Validar la no repetición
function existArtifact(idArtifact) {
    let existArtifact = arrArtifacts.find(function (artifact) {
        return artifact.id_artefacto_PK == idArtifact
    })
    return existArtifact
}

// mostrar elementos
function showArtifacts() {
    $("#list-artifacts").empty()

    arrArtifacts.forEach(function (artifact) {
        $("#list-artifacts").append('<div class="form-group border rounded-pill pt-1"><button onclick="removeArtifact(' + artifact.id_artefacto_PK + ')" class="btn btn-sm mb-1 ml-2 border-0"><i class="fas fa-times text-secondary fa-xs"></i></button> <span class="font-sm-important pl-1 text-muted">  ' + artifact.modelo + '</span></div>')
    })
}

function removeArtifact(idArtifact) {
    let index = arrArtifacts.indexOf(existArtifact(idArtifact))
    arrArtifacts.splice(index, 1)
    showArtifacts()
}

async function technician_crossover() {

    try {
        if (arrTechnicians.length != 0) {
            let detalle = getDetail(), fechaCita = {};
            detalle = detalle[0];

            let fechaVirtual = detalle.fecha_hora_cita_virtual;
            let fechaPresencial = detalle.fecha_encuentro;
            let horaPresencial = detalle.hora_encuentro;

            if (fechaVirtual) {
                if (fechaVirtual != '') {
                    // console.log(`FECHA VIRTUAL`);
                    fechaCita.fecha = fechaVirtual;
                }
            }
            else if (fechaPresencial && horaPresencial) {
                if (fechaPresencial != '' && horaPresencial != '') {
                    // console.log(`FECHA PRESENCIAL`);
                    fechaCita.fecha = fechaPresencial + ' ' + horaPresencial;
                }
            }

            // console.log(fechaCita);

            let tecnicosCita = arrTechnicians;

            tecnicosCita.forEach(e => delete e.num_id_tec);

            // console.log(tecnicosCita);

            let arrTecnicoCita = [tecnicosCita, fechaCita];

            // console.log(tecnicosCita);

            let resp = await sendDataPostFetch(arrTecnicoCita, '?c=service&m=technicianCrossover')
                .then(data => {
                    return data
                })
                .catch(e => console.error('ERROR', e))

            return resp;
        }
    } catch (e) {
        console.error(e);
    }
}

$("#submitService").click(async function (e) {
    e.preventDefault()

    try {
        if (getDetail().length != 0 && arrArtifacts.length != 0
            && arrScategories.length != 0
            // && arrTechnicians.length != 0
            && $("#id_cliente_FK").val() != '') {


            // let cruce = await technician_crossover()
            //     .then(data => { return data })
            //     .catch(e => console.error('ERROR', e))

            // if (cruce == "pastdateerror")
            //     return console.error(`Error de fecha debe ser futura`);

            // else if (cruce == "crosserror")
            //     return console.error(`CRUCE DE HORARIOS TÉCNICOS`);

            // else if (cruce == "badate")
            //     return console.error(`Fecha erronea`);

            // else if (cruce == "well") {
                let url = "?c=service&m=save"
                let params = {
                    id_cliente_FK: $("#id_cliente_FK").val(),
                    artifacts: arrArtifacts,
                    scategories: arrScategories,
                    // technicians: arrTechnicians,
                    details: getDetail()
                }

                // if (arrImages.length != 0) params.evidence = arrImages;

                console.log(params);
                console.log(`ok`);

                // keypress (e) e.keycode
                // metodo POST USANDO AJAX PARA ENVIAR LA INFO AL BACKEND
                $.post(url, params, function (response) {
                    if (typeof response.error !== 'undefined') {

                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Algo salio mal!',
                            footer: '<a href="mailto:oscarjaviervargas@hotmail.com">Reportar problema</a>'
                        })
                        console.log(response.message)
                    } else {

                        Swal.fire({
                            position: 'center-middle',
                            icon: 'success',
                            title: 'Guardado',
                            showConfirmButton: false,
                            timer: 2000
                        });

                        // swalSimple();
                        setTimeout(() => {
                            let again = confirm("¿Deseas generar otro servicio?");

                            if (again) {

                                $('#start').trigger('click');

                            } else location.href = "?c=service";
                        }, 2200);
                    }
                }, 'json').fail(function (error) {

                    Swal.fire({
                        icon: 'error',
                        title: '<i class="fas fa-flushed"></i>  Caramba...',
                        text: '¡Algo salio mal!',
                        footer: '<a href="mailto:oscarjaviervargas@hotmail.com">Reportar problema</a>'
                    })
                    console.log("Inserción fallida (" + error.responseText + ")")
                });
            }else {
                alert("Completa todos los campos");
            }
        // }
    } catch (e) {
        console.log(e);
    }
});


$("#update").click(function (e) {
    //deshabilitar el envio por HTTP
    e.preventDefault()

    let url = "?c=service&m=update"
    let params = {
        id_servicio_PK: $("#id_servicio_PK").val(),
        id_cliente_FK: $("#id_cliente_FK").val(),
        id_estado_servicio_FK: $("#id_estado_servicio_FK").val(),
        artifacts: arrArtifacts,
        scategories: arrScategories,
         technicians: arrTechnicians,
        details: getDetail()
    }

     if (arrImages.length != 0) params.evidence = arrImages;

    if (getDetail().length != 0 && arrArtifacts.length != 0
        && arrScategories.length != 0
         && arrTechnicians.length != 0
        && $("#id_cliente_FK").val() != '') {
        if (getDetail().length != 0 && arrArtifacts.length != 0
            && arrScategories.length != 0
             && arrTechnicians.length != 0
            && $("#id_cliente_FK").val() != '') {

            $.post(url, params, function (response) {
                if (typeof response.error !== 'undefined') {
                    alert(response.message)
                } else {
                    swalSimple("Actualización Satisfactoria")
                    setTimeout(() => {
                        location.href = "?c=service"
                    }, 2000)
                }
            }, 'json').fail(function (error) {
                alert("Actualización Fallida(" + error.responseText + ")")
            });

        } else {
            alert("Completa todos los campos");
        }
    } else {
        swalError("Por favor asigna al menos un técnico para el servicio");
    }
});



