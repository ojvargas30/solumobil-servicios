
function getBrand() {

    var arrBrands = []

    let id = $("#id_marca_artefacto_PK").val();
    let name = $("#nombre_marca").val();

    if (name != '' && name) {
        arrBrands.push({
            'id_marca_artefacto_PK': id,
            'nombre_marca': name
        })
    } else {
        swal('Diligencie el campo nombre marca correctamente');
    }
    return arrBrands
}

// enviar elementos al back
$("#submitGhostBrand").click(function (e) {
    e.preventDefault()

    console.log(`Marca Dispositivo`);
    let url = "?c=artifactbrand&m=save"
    let params = {
        brands: getBrand()
    }

    // metodo POST USANDO AJAX PARA ENVIAR LA INFO AL BACKEND
    $.post(url, params, function (response) {
        // console.log(response);

        if (response.error !== false) {
            swalError("Error, Nombre de marca invalido");
            // swal('Oops...', 'error', '¡Algo salio mal con el guardado de la marca!');

        } else {

            console.log(response);

            // recargo del select, poner lo que se inserta
            let { id_marca_artefacto_PK, nombre_marca } = response.registerInsert;
            let select = document.getElementById("id_marca_artefacto_FK"),
                option = document.createElement("option");
            option.value = id_marca_artefacto_PK;
            option.textContent = nombre_marca;
            select.appendChild(option);

            // Seleccion
            document.getElementById("seleccione").removeAttribute("selected");
            option.setAttribute("selected", "");


            swalSimple('Marca generada');

            //Auto clic
            let clicBtnNew = document.getElementById('ghost-child-2');
            clicBtnNew.click();

            // console.log(response)
        }
    }, 'json').fail(function (error) {
        if (error.responseText.includes('SQLSTATE[23000]')) {
            swalError('Ya existe una marca con ese nombre');
        }

        // console.log(error);

        // if(){

        // }
        // swal('<i class="fas fa-flushed"></i>  Caramba...', 'error', '¡Algo salio mal!');
        // console.log("Inserción fallida (" + error.responseText + ")")
    });
});

