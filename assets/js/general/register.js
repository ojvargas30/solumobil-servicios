// noCopyPageInInput('.input_v');
// noCopyPageInInput('.input_v_login');

// aqui va tu codigo
let loginFields = {
    correoLogin: false,
    passwordLogin: false
}


$("#accessIngresarUser").click((e) => {
    e.preventDefault()

    const p = {
        correo: eleVal('correoLogin'),
        clave: eleVal('passwordLogin')
    }

    sendDataPost(
        [loginFields['correoLogin'], loginFields['passwordLogin']],
        "?c=login&m=login", p, 'alertaErrorLogin', true, true, 'passwordLogin', false)

});

const validateAccess = (e) => { // valida form login

    let bad7 = document.querySelector('.bad7'), // icono correcto
        good7 = document.querySelector('.good7'), // icono incorrecto
        regla_correo_login = ele('regla_correo_login'),

        bad8 = document.querySelector('.bad8'),
        good8 = document.querySelector('.good8'),
        regla_clave_login = ele('regla_clave_login')

    // Reutilizable
    id = e.target.id,
        elementoInput = ele(id),
        valor = elementoInput.value;

    switch (e.target.id) {
        case "correoLogin":
            estadoInput(valor, bad7, good7, regla_correo_login, elementoInput, "email", loginFields, id)
            break;
        case "passwordLogin":
            estadoInput(valor, bad8, good8, regla_clave_login, elementoInput, "password", loginFields, id)
            break;
    }
}

validateInputs('.input_v_login', validateAccess);


let registerFields = {
    nombre_cliente: false,
    apellido_cliente: false,
    correo: false,
    emailConfirm: false,
    pass: false,
    passConfirm: false
}


$("#registro").click((e) => {
    e.preventDefault()

    var response = grecaptcha.getResponse(); // retorna un array
    if (response.length == 0) {
        alert("Por favor completa el Captcha")
    } else {
        const p = {
            user: {
                correo: eleVal('correo'),
                clave: eleVal('pass')
            }
            // ,
            // client: {
            //     nombre_cliente: eleVal('nombre_cliente'),
            //     apellido_cliente: eleVal('apellido_cliente'),
            // }
        }

        sendDataPost(
            [
                // registerFields['nombre_cliente'], registerFields['apellido_cliente'],
                registerFields['correo'],
                registerFields['emailConfirm'], registerFields['pass'], registerFields['passConfirm']],
            "?c=user&m=saveClientHome", p, 'alertaError', true, false, '', true,
            'Por favor revise su correo electrónico Hemos enviado un enlace de confirmación para activar su cuenta.')
    }
});


const validateRegisterHome = (e) => { // valida form de registro del clienet en home

    let // Elementos
        email = ele('correo'),
        cemail = ele('emailConfirm'),
        pass = ele('pass'),
        cpass = ele('passConfirm'),

        good = document.querySelector('.good'),
        bad = document.querySelector('.bad'),

        // reglas
        regla_nombre = ele('regla_nombre'),

        good2 = document.querySelector('.good2'), // icono correcto
        bad2 = document.querySelector('.bad2'), // icono incorrecto
        regla_correo = ele('regla_correo'),

        good3 = document.querySelector('.good3'),
        bad3 = document.querySelector('.bad3'),
        regla_apellido = ele('regla_apellido'),

        good4 = document.querySelector('.good4'),
        bad4 = document.querySelector('.bad4'),
        regla_ccorreo = ele('regla_ccorreo'),

        good5 = document.querySelector('.good5'),
        bad5 = document.querySelector('.bad5'),
        regla_clave = ele('regla_clave'),

        good6 = document.querySelector('.good6'),
        bad6 = document.querySelector('.bad6'),
        regla_cclave = ele('regla_cclave'),

        // Reutilizable
        id = e.target.id,
        elementoInput = ele(id),
        valor = elementoInput.value;

    switch (e.target.id) {
        case "nombre_cliente":
            estadoInput(valor, bad, good, regla_nombre, elementoInput, "name", registerFields, id)
            break;
        case "apellido_cliente":
            estadoInput(valor, bad3, good3, regla_apellido, elementoInput, "name", registerFields, id)
            break;
        case "correo":
            estadoInput(valor, bad2, good2, regla_correo, elementoInput, "email", registerFields, id)
            estadoInput(valor, bad4, good4, regla_ccorreo, cemail, "equal", registerFields, id, cemail.value)
            break;
        case "emailConfirm":
            estadoInput(valor, bad4, good4, regla_ccorreo, elementoInput, "equal", registerFields, id, email.value)
            estadoInput(email.value, bad2, good2, regla_correo, email, "email", registerFields, email.id)
            break;
        case "pass":
            estadoInput(valor, bad5, good5, regla_clave, elementoInput, "password", registerFields, id)
            estadoInput(valor, bad6, good6, regla_cclave, cpass, "equal", registerFields, id, cpass.value)
            break;
        case "passConfirm":
            estadoInput(valor, bad6, good6, regla_cclave, elementoInput, "equal", registerFields, id, pass.value)
            estadoInput(pass.value, bad5, good5, regla_clave, pass, "password", registerFields, pass.id)
            break;
    }
}

validateInputs('.input_v', validateRegisterHome);


