// enviar datos usuario tech
$("#submitTech").click(function (e) {
  e.preventDefault()
  // console.warn("Sii aqui esta haciendose la ruta");
  let url = "?c=user&m=saveTechnical"
  let params = {
    users: getUserTech(),
    technicians: getTechnical()
  }
  // metodo POST USANDO AJAX PARA ENVIAR LA INFO AL BACKEND
  $.post(url, params, function (response) {
    if (typeof response.error !== 'undefined') {
      swal('Oops...', 'error', 'Algo salio mal con el usuario!');
    } else {
      swalSimple('Usuario generado, Verifique su correo');

      setTimeout(() => {
        location.href = "?c=technical";
      }, 2000)
    }
  }, 'json').fail(function (error) {
    // swal('Error generando el técnico', 'error', 'Ya existe un usuario con ese correo, telefono o No de identificación', 3000, true);
    // console.log("Inserción fallida (" + error.responseText + ")");
    swalSimple('Usuario generado, Verifique su correo');
    setTimeout(() => {
      location.href = "?c=technical";
    }, 2000)
  });
});

function getUserTech() {

  var arrUserTech = []
  let correoUsu = $("#correo").val();
  let emailConfirm = $("#emailConfirm").val();
  let id_rol_FK = $("#id_rol_FK").val();;

  // Patron para el correo
  let patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
  if (correoUsu != '' && emailConfirm != '') {
    if (correoUsu == emailConfirm) {
      if (correoUsu.search(patron) == 0) {
        arrUserTech.push({
          'correo': emailConfirm,
          'clave': generatePasswordRand(13),
          'id_rol_FK': id_rol_FK
        })
      } else {
        swal('El correo esta en un formato desconocido', 'error', 'El correo no debe contener simbolos ni caracteres especiales(Ej: test@gmail.com)', 10000, true);
      }
    } else {
      swal('Las entradas del correo son diferentes', 'error', 'Asegurate de que sean iguales', 3000, true);
    }
  } else {
    swal('Verifique que los campos no esten vacíos', 'error', '', 3000, true);
  }
  return arrUserTech
}

function getTechnical() {
  // Este método es para recoger y retornar los datos
  var arrTechnicians = [];

  let id_tecnico_PK = $("#id_tecnico_PK").val();
  let tipo_doc_tec = $("#tipo_doc_tec option:selected").text();
  let telefono = $("#telefono").val();
  let nombre_tecnico = $("#nombre_tecnico").val();
  let apellido_tecnico = $("#apellido_tecnico").val();
  let num_id_tec = $("#num_id_tec").val();
  let desc_tecnico = $("#desc_tecnico").val();
  let direccion_residencia = $("#direccion_residencia").val();

  if (
    tipo_doc_tec != '' &&
    nombre_tecnico != '' &&
    apellido_tecnico != '' &&
    num_id_tec != ''
  ) {
    if (telefono.length > 12) {
      swal("El telefono debe ser menor o igual a 15 digitos", "error", "", 3000, false);
    } else {
      if (num_id_tec.length > 12) {
        swal("El No. de identificación debe ser menor o igual a 10 digitos", "error", "", 3000, false);
      } else {
        arrTechnicians.push({
          // 'id_usuario_FK': id_usuario_FK,
          'tipo_doc_tec': tipo_doc_tec,
          'telefono': telefono,
          'nombre_tecnico': nombre_tecnico,
          'apellido_tecnico': apellido_tecnico,
          'num_id_tec': num_id_tec,
          'desc_tecnico': desc_tecnico,
          'direccion_residencia': direccion_residencia,
        });
      }
    }
  } else {
    swal("Diligencie los campos", "error", "", 3000, false);
  }

  return arrTechnicians;
}