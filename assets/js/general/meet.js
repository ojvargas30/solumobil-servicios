window.URL_MEET = "http://g.co/meet/virtualService_";

// CAMBIO DE FORMS
let virtual = document.getElementById('virtual');
let virtualForm = document.getElementById('virtualForm');

let faceToFace = document.getElementById('face-to-face');
let faceToFaceForm = document.getElementById('face-to-faceForm');

const mixForm = () => {
    virtualForm.classList.toggle('d-none');
    faceToFaceForm.classList.toggle('d-none');
}

if(virtual && faceToFace){
    virtual.addEventListener('click', mixForm)
    faceToFace.addEventListener('click', mixForm)
}

// COPIAR LINK FUNC
const copyLink = (link = '', showMsg = true) => {

    const inputLink = document.getElementById("link"); //INPUT DONDE ESTA O VA A ESTAR EL LINK

    inputLink.value = link;

    inputLink.select(); // selecciona el txt del input

    if (document.execCommand("copy")) { // copia lo seleccionado
        if (showMsg) {
            Swal.fire("Enlace copiado", "", "success");
            return true;
        } else return true;
    }
};

// COPY LINK EXCLUSIVITY
const copyLinkStrict = () => {

    const inputLink = document.getElementById("link"); //INPUT DONDE ESTA O VA A ESTAR EL LINK

    inputLink.select(); // selecciona el txt del input

    if (document.execCommand("copy")) { // copia lo seleccionado
        toastr.success('Enlace copiado en el portapapeles');
        return true;
    }
};

// BTN DE COPIAR
let btnCopy = document.getElementById('copy');
if(btnCopy){

    btnCopy.addEventListener('click', copyLinkStrict);
}

// GENERAR MEET
const genMeet = (e) => {

    e.preventDefault();

    let numRand;

    numRand = Math.floor(Math.random() * 1000000);

    while (numRand < 99999) numRand = Math.floor(Math.random() * 1000000);


    const url = window.URL_MEET + numRand;

    if (copyLink(url, false))
        toastr.success('Enlace generado y copiado en el portapapeles');
};

let newMeet = document.getElementById('newMeet');
if(newMeet){

    newMeet.addEventListener('click', genMeet);
}

// GENERAR MEET AL DAR CLICK EN CITA VIRTUAL
const genMeetWithoutMsg = (e) => {

    e.preventDefault();

    let numRand;

    numRand = Math.floor(Math.random() * 1000000);

    while (numRand < 99999) numRand = Math.floor(Math.random() * 1000000);


    const url = window.URL_MEET + numRand;

    copyLink(url, false)
};

if(virtual){

    virtual.addEventListener('click', genMeetWithoutMsg);
}