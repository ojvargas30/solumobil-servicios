noCopyPageInInput('.input_v');

let recoveryField = { correo: false }

$("#recovery").click((e) => {
    e.preventDefault()

    var response = grecaptcha.getResponse();

    // console.log('response recaptcha');
    // console.log(response);

    if (response.length == 0) {
        console.log(`error recaptcha`);
        alert("Por favor completa el Captcha")
    } else {

        const correo = { correo: eleVal('correo') }

        sendDataPost([recoveryField['correo']], "?c=user&m=recoveryAccount", correo, 'alertaRec',
            true, false, '', true, 'Para recuperar su cuenta por favor verifique su correo');
    }
});


const validateRecovery = (e) => { // valida form rec

    let b = document.querySelector('.b'),
        g = document.querySelector('.g'),
        rl_correo = ele('rl_correo'),

        id = e.target.id,
        elementoInput = ele(id),
        valor = elementoInput.value;

    switch (e.target.id) {
        case "correo":
            estadoInput(valor, b, g, rl_correo, elementoInput, "email", recoveryField, id)
            break;
    }
}

validateInputs('.input_v', validateRecovery);

// let btnCorreo = document.getElementById('recovery');

// if(btnCorreo){
//     btnCorreo.addEventListener('click', () => {
//         setTimeout(() => {
//             btnCorreo.disabled = true;
//         }, 100000)
//     })
// }
