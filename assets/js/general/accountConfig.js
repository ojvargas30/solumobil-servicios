$("#dAcc").click(function (e) {
    e.preventDefault()
    let url = "?c=user&m=updateUserStatus"
    let params = {
        users: getUser()
    }
    
$.post(url, params, function (response) {
    if (typeof response.error !== 'undefined') {
    swal('Oops...', 'error', 'Algo salio mal con la acción!');
    } else {
        swalSimple('Cuenta borrada');
    
        setTimeout(() => {
            location.href = "?c=user";
        }, 2000)
    }
}, 'json').fail(function (error) {
    swalSimple('Cuenta borrada');
    setTimeout(() => {
        location.href = "?c=user";
    }, 2000)
    });
});

/* ----- */

let changePassFields = {
    newPassword: false,
    confPassword: false,
    idUser: false
}

$("#chg").click((e) => {
    e.preventDefault()

    const p = {
        newPassword: eleVal('newPassword'),
        confPassword: eleVal('confPassword'),
        idUser: eleVal('idUser')
    }

    sendDataPost(
        [changePassFields['newPassword'], changePassFields['confPassword']],
        "?c=user&m=cpass", p, 'alertChan',true, false, '', true,
        'Contraseña cambiada')

});