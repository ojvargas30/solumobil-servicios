<?php

/**
 * Clase HomeController para cargar el home del proyecto
 */

class HomeController
{
    // private $googleClient;
    private $auth;
    private $auth2;

    /**
     * Class constructor of Home.
     */
    public function __construct()
    {
        $this->auth = getGoogleData();
        $this->auth2 = getGoogleData2();
        // $this->googleClient = new Google_Client();
        // $this->auth = new GoogleAuth($this->googleClient);

        // if($this->auth->checkRedirectCode())
        // {
        //     // die($_GET['code']);
        //     // header('Location: ?c=home&m=index');
        // }
    }

    public function index()
    {
        require 'views/home/layoutHome.php';
        require 'views/home/home.php';
        require 'views/home/footerHome.php';
    }

    public function recoveryAccount()
    {
        require 'views/other/layoutOther.php';
        require 'views/other/recoveryAccount.php';
    }


    public function access()
    {
        $authUrl2 = $this->auth2->getAuthUrl();
        $authUrl = $this->auth->getAuthUrl();

        require 'views/home/head.php';
        require 'views/home/login/inicioSesion.php';
    }

    public function register()
    {
        $authUrl = $this->auth->getAuthUrl();
        $authUrl2 = $this->auth2->getAuthUrl();
        // require 'config/config.php';
        require 'views/home/head.php';
        echo '
        <script>
        $(document).ready(function(){
            $(".registrarse").click();
         });
        </script>
        ';
        require 'views/home/login/inicioSesion.php';
    }

    // public function contact()
    // {

    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/contact.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function us()
    // {

    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/us.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function product()
    // {

    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/product.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function service()
    // {

    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/service.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function php1()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php1/php1.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function php2()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php2/php2.1.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function php2_3()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php2/php2.2/php2.3.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function php2_3p2()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php2/php2.2/php2.3.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function php2_4()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php2/php2.2/php2.4.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function php3()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php3/php3.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function tablas()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php3/tablas/tablas.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function ej_9p3()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php3/Ej_9.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function ej_10p3()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php3/Ej_10.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function ej_11p3()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php3/Ej_11.php';
    //     require 'views/home/footerHome.php';
    // }

    // public function ej_14p3()
    // {
    //     require 'config/config.php';
    //     require 'views/home/layoutHome.php';
    //     require 'views/home/php/php3/Ej_14.php';
    //     require 'views/home/footerHome.php';
    // }
}
