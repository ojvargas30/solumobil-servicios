<?php

/**
 * CLASE ServiceController
 * CONTROLADOR ALGO ASI COMO UN JS
 */

require 'models/ArtifactBrand.php';

class ArtifactbrandController
{

    private $model;

    public function __construct()
    {
        $this->model = new ArtifactBrand;
    }

    public function index()
    {

    }

    // realiza el proceso de guardar
    public function save()
    {
        // dd($_POST);
        $arrayBrands = isset($_POST['brands']) ? $_POST['brands'] : [];
        // dd($arrayBrands);
        $registerInsert = false;

        if (!empty($arrayBrands)) {

            $respBrand = $this->model->saveBrand($arrayBrands);
            $registerInsert = $this->model->getByWhere(
                "marca_artefacto",
                ["id_marca_artefacto_PK" => $this->model->getLastId()]
            );
        } else {
            $respBrand = false;
        }

        $arrayResp = [];

        if ($respBrand == true) {

            $arrayResp = [
                'success' => true,
                'error' => false,
                'message' => "Marca generada"
            ];

            if ($registerInsert) {
                $arrayResp["registerInsert"] = $registerInsert;
            }
        } else {
            $arrayResp = [
                'error' => true,
                'message' => "Error generando la marca"
            ];
        }
        echo json_encode($arrayResp);
        return;
    }
}
