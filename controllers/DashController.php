<?php

/**
 * Clase DashController para cargar el home del proyecto
 */
require 'models/Dash.php';
require 'models/Login.php';
require 'models/District.php';
require 'models/Statistic.php';


class DashController
{
	private $model;
	private $login;
	private $district;
	private $client;
	private $statistic;

	public function __construct()
	{
		// dd($_SESSION['user']);
		if (!isset($_SESSION['user']['user'])) //que sesion va a destruir si no existe
			header('Location: ?c=home&m=access');

		$this->model = new Dash;
		$this->login = new Login;
		$this->district = new District;
		$this->client = new Client;
		$this->statistic = new Statistic;

		$id_usuario_PK = isset($_SESSION['user']['user']->id_usuario_PK)
			? $_SESSION['user']['user']->id_usuario_PK
			: '';

		$statusUser = $this->model->getStatusUserBySessionId($id_usuario_PK);

		if ($statusUser[0]->estado_usu == 2) $this->login->logout();
	}

	public function index()
	{
		if ($_SESSION['user']['user']->id_rol_FK == 1) {

			$conflictus_apparatus = $this->model->getConflictApparatus();
			$bestEmployee = $this->model->getBestEmployee();
			$weeklyGain = $this->model->getWeeklyGain();
			$dailyGain = $this->model->getDailyGain();

			if (!date('Y-m-d', strtotime($dailyGain[0]->dia)) == date('Y-m-d'))
				$dailyGain = null;

			$monthlyGain = $this->statistic->getMonthlyGain();
			$monthlyGain = (array)$monthlyGain[0];

			$arrMonthlyGain = [];

			foreach ($monthlyGain as $posicion => $ganancia) {
				if (is_null($ganancia)) $ganancia = 0;
				array_push($arrMonthlyGain, $ganancia);
			}
			// dd($weeklyGain);
			require 'views/layout.php';  // Esto hace que se muestre en todo lado
			require 'views/homedash.php';
			require 'views/other/separator.php';
			require 'views/footer.php';
		} elseif ($_SESSION['user']['user']->id_rol_FK == 2) {
			require 'views/partial/employee/layoutEmployee.php';  // Esto hace que se muestre en todo lado
			require 'views/other/separator.php';
			require 'views/footer.php';
		} elseif ($_SESSION['user']['user']->id_rol_FK == 3) {

			$validateData = $this->model->completeProfile();

			$flagNull = true;

			if (!empty($validateData)) {
				if ($validateData[0]->nombre_cliente == null) $flagNull = false;
				if ($validateData[0]->apellido_cliente == null) $flagNull = false;
				if ($validateData[0]->tipo_doc_cli == null) $flagNull = false;
				if ($validateData[0]->num_id_cli == null) $flagNull = false;
				if ($validateData[0]->id_barrio_FK == null) $flagNull = false;
				if ($validateData[0]->direccion_residencia == null) $flagNull = false;
				if ($validateData[0]->telefono == null) $flagNull = false;
			} else {
				$flagNull = false;
			}


			// if ($validateData !== '404') {
			// 	foreach($validateData[0] as $client)

			// 	if ($client == null) $flagNull = false;
			// 	if ($client == null) $flagNull = false;
			// 	if ($client == null) $flagNull = false;
			// 	if ($client == null) $flagNull = false;
			// 	if ($client == null) $flagNull = false;
			// }

			if ($flagNull) {
				$clientData = $this->client->getClientSession();
				// dd($clientData);
				require 'views/partial/client/layoutClient.php';
				require 'views/partial/client/profile.php';
				require 'views/footer.php';
			} else {
				$districts = $this->district->getActiveDistricts();

				require 'views/user/completeData.php';
			}
		}
	}

	public function using()
	{
		if ($_SESSION['user']['user']->id_rol_FK == 1) {
			require 'views/layout.php';  // Esto hace que se muestre en todo lado
			require 'views/home.php';
			require 'views/footer.php';
		} elseif ($_SESSION['user']['user']->id_rol_FK == 2) {
			require 'views/partial/employee/layoutEmployee.php';  // Esto hace que se muestre en todo lado
			require 'views/home.php';
			require 'views/footer.php';
		} elseif ($_SESSION['user']['user']->id_rol_FK == 3) {
			require 'views/partial/client/layoutClient.php';  // Esto hace que se muestre en todo lado
			require 'views/home.php';
			require 'views/footer.php';
		}
	}
}
