<?php

/**
 * CLASE ServiceController
 * CONTROLADOR ALGO ASI COMO UN JS
 */

require 'models/Scategory.php';

class ScategoryController
{

    private $model;

    public function __construct()
    {
        $this->model = new Scategory;
    }

    public function index()
    {
    }

    public function saveSubForm()
    {
        // dd($_POST["data"]);
        if (isset($_POST["data"])) {

            $scategory = json_decode($_POST["data"]);
            // dd($scategory);
            if (!empty($scategory)) {
                $newScategory = $this->model->saveScategorySubForm($scategory);

                if (!empty($newScategory)) {
                    if ($newScategory == 'SQLSTATE[23000]') {
                        echo json_encode('SQLSTATE[23000]');
                        return;
                    } else {
                        echo json_encode($newScategory);
                        return;
                    }
                } else {
                    echo json_encode("mal");
                    return;
                }
            }
        }
    }
}
