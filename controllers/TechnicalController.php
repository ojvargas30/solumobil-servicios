<?php

/**
 * CLASE technicalController
 * CONTROLADOR ALGO ASI COMO UN JS
 */

require 'models/Technical.php';
require 'models/User.php';
require 'models/Role.php';

class TechnicalController
{

    private $model;
    private $user;
    private $role;

    public function __construct()
    {
        if (!isset($_SESSION['user']['technical']))
            header('Location: ?controller=home');
        $this->model = new Technical;
        $this->user = new User;
        $this->role = new Role;
    }

    public function index()
    {
        $technicians = $this->model->getTechnicians();
        $roles = $this->role->getActiveRoles();

        require 'views/layout.php';
        require "views/technical/new.php";
        require "views/technical/list.php";
        require 'views/footer.php';
    }

    //muestra la vista de editar
    public function edit()
    {
        if (isset($_REQUEST['id_tecnico_PK'])) {
            $id_tecnico_PK = $_REQUEST['id_tecnico_PK'];
            $data = $this->model->getTechnicalById($id_tecnico_PK);
            $technicians = $this->model->getActiveTechnicians();
            require 'views/layout.php';
            require 'views/technical/edit.php';
            require 'views/footer.php';
        } else {
            echo "Hubo un error <i class='fas fa-sad-cry'></i>";
        }
    }

    public function save($arrayTechnicians)
    {
        try {
            if ($this->user->saveTechnical($arrayTechnicians)) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }

    // private function httpResponse($type, $message, $params = null)
    // {
    //     $response['message'] = $message;
    //     $response[array_key_first($type)] = array_values($type)[0];

    //     if (!empty($params)) {
    //         $response[array_key_first($params)] = array_values($params)[0];
    //     }
    //     echo json_encode($response);
    //     return;
    // }

    public function updateUserTechnicalStatus()
    {
        $technical = $this->user->getUserById($_REQUEST['id_usuario_PK']);

        if ($technical[0]->estado_usu == 1) {
            $data = [
                'id_usuario_PK' => $technical[0]->id_usuario_PK,
                'estado_usu' => 2
            ];
        } elseif ($technical[0]->estado_usu == 0) {
            $data = [
                'id_usuario_PK' => $technical[0]->id_usuario_PK,
                'estado_usu' => 1
            ];
        }
        $this->model->editUserStatus($data);
        header('Location: ?controller=technical');
    }
}
