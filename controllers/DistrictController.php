<?php

/**
 * CLASE ServiceController
 * CONTROLADOR ALGO ASI COMO UN JS
 */

require 'models/District.php';

class DistrictController
{

    private $model;

    public function __construct()
    {
        $this->model = new District;
    }

    public function index()
    {
        // $services = $this->model->getActiveServices();

        // require 'views/layout.php';
        // require "views/service/new.php";
        // require "views/service/list.php";
        // require 'views/footer.php';
    }

    // realiza el proceso de guardar
    public function save()
    {
        $arrayDistricts = isset($_POST['districts']) ? $_POST['districts'] : [];

        $registerInsert = false;

        if (!empty($arrayDistricts)) {

            $respDistrict = $this->model->saveDistrict($arrayDistricts);
            $registerInsert = $this->model->getByWhere(
                "barrio_cliente",
                ["id_barrio_PK" => $this->model->getLastId()]
            );
        } else {
            $respDistrict = false;
        }

        $arrayResp = [];

        if ($respDistrict == true) {

            $arrayResp = [
                'success' => true,
                'message' => "Barrio generado"
            ];

            if ($registerInsert) {
                $arrayResp["registerInsert"] = $registerInsert;
            }
        } else {
            $arrayResp = [
                'error' => false,
                'message' => "Error generando el barrio"
            ];
        }
        echo json_encode($arrayResp);
        return;
    }

    public function saveDistrictProfile()
    {
        if (isset($_POST["data"])) {

            $barrio = json_decode($_POST["data"]);

            if (!empty($barrio)) {
                $newBarrio = $this->model->newDistrictProfile($barrio);

                if (!empty($newBarrio)) {
                    echo json_encode($newBarrio);
                    return;
                } else {
                    echo json_encode("mal");
                    return;
                }
            }
        }
    }
}
