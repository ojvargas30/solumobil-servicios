<?php

require 'models/User.php';
require 'models/Login.php';

class UserController
{

    private $model;
    private $auth;

    public function __construct()
    {
        $this->model = new User;
        $this->login     = new Login;
        $this->pdo       = new Database;

        $this->auth = getGoogleData();
        // die($_GET['code']);
        // header('Location: ?c=home&m=index');

    }

    public function index()
    {
        // dd("ok");
        // var_dump("ok");
        // exit();
        if ($_SESSION['user']['user']) {
            if ($_SESSION['user']['user']->id_rol_FK) {
                if ($_SESSION['user']['user']->id_rol_FK == 1) {
                    // if (!isset($_SESSION['user']['user']->id_rol_FK) == 1)
                    //     header('Location: ?c=home');

                    $id_usuario_PK = isset($_SESSION['user']['user']->id_usuario_PK) ? $_SESSION['user']['user']->id_usuario_PK : '';
                    $statusUser = $this->model->getStatusUserBySessionId($id_usuario_PK);
                    // dd($id_usuario_PK);
                    // dd("ok");
                    if ($statusUser[0]->estado_usu == 2) {
                        // dd($_SESSION['user']['user']);
                        // dd($statusUser);
                        $this->model->logout();
                    }

                    $users = $this->model->getActiveUsers();

                    require 'views/layout.php';
                    require "views/user/list.php";
                    require 'views/footer.php';
                } else {
                    require "views/other/404.php";
                }
            } else {
                require "views/other/404.php";
            }
        } else {
            require "views/other/404.php";
        }
    }

    public function saveTechnical()
    {
        // if ($_SESSION['user']['user']->id_rol_FK == 1) {

        $arrayUsers       = isset($_POST['users'])       ? reset($_POST['users'])      : [];
        $arrayTechnicians = isset($_POST['technicians']) ? $_POST['technicians'][0] : [];

        if (!empty($arrayUsers) && !empty($arrayTechnicians)) {
            $user = $this->model->saveUser($arrayUsers);
            // dd($user,$arrayTechnicians);
            // dd($user);
            if ($user) {
                $arrayTechnicians["correo"]         = $user["correo"];
                $arrayTechnicians["keyreg"]         = $user["keyreg"];
                $arrayTechnicians["id_usuario_FK"]  = $user["id_usuario_PK"];
                $arrayTechnicians["clave"]          = $arrayUsers["clave"];

                if ($this->saveTech($arrayTechnicians)) {
                    httpResponse(['success' => true], "Usuario generado", ["id_usuario_PK" => $this->model->getLastId()]);
                } else {
                    // exit("DELETE OK");
                    // if ($this->pdo->delete("usuario", "id_usuario_PK= {$user["id_usuario_PK"]}")) {
                    //     httpResponse(['error' => true], "Tecnico no insertado. Usuario eliminado : {$user["id_usuario_PK"]}");
                    // } else {
                    //     httpResponse(['error' => true], "Tecnico no insertado. Usuario NO eliminado : {$user["id_usuario_PK"]}");
                    // }
                }
            } else {
                httpResponse(['error' => true], "No se insertó el usuario :D");
            }
        } else {
            httpResponse(['error' => true], "Llena los datos del técnico completamente");
        }
        // } else {
        //     $this->login->logout();
        // }
    }


    // realiza el proceso de guardar para el cliente
    public function saveClient()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {

            $arrayUsers       = isset($_POST['users'])       ? $_POST['users'][0]       : [];
            $arrayClients     = isset($_POST['clients']) ? $_POST['clients'][0] : [];
            $arrayUsers['id_rol_FK'] = 3;
            if (!empty($arrayUsers) && !empty($arrayClients)) {
                $user = $this->model->saveUser($arrayUsers);
                // dd($user);
                if ($user) {
                    $arrayClients["correo"]         = $user["correo"];
                    $arrayClients["keyreg"]         = $user["keyreg"];
                    $arrayClients["id_usuario_FK"]  = $user["id_usuario_PK"];
                    $arrayClients["clave"]          = $arrayUsers["clave"];

                    if ($this->saveCli($arrayClients)) {
                        $registerInsert = $this->model->getByWhere(
                            "cliente",
                            ["id_cliente_PK" => $this->model->getLastId()]
                        );

                        echo json_encode($registerInsert);
                        return;
                    }
                }
            } else {
                $respCliente = false;
            }
        } else {
            require "views/other/404.php";
        }
    }

    // realiza el proceso de guardar para el cliente
    public function saveClientHome()
    {
        // dd($_POST);
        if (isset($_POST)) {
            if (!empty($_POST)) {
                $arrayUser   = isset($_POST['user'])   ? $_POST['user']   : [];
                // $arrayClient = isset($_POST['client']) ? $_POST['client'] : [];
                $arrayUser['id_rol_FK'] = 3;

                if (!empty($arrayUser)
                // && !empty($arrayClient)
                ) {

                    $user = $this->model->saveUser($arrayUser);
                    // dd($user);

                    if ($user) {

                        $arrayClient["correo"]         = $user["correo"];
                        $arrayClient["keyreg"]         = $user["keyreg"];
                        $arrayClient["id_usuario_FK"]  = $user["id_usuario_PK"];
                        // dd($arrayClient);

                        if ($this->saveCliHome($arrayClient)) {

                            httpResponse(
                                ['success' => true],
                                "Cliente generado",
                                ["id_usuario_PK" => $this->model->getLastId()],
                                '?c=home&m=access'
                            );
                        } else {
                            // $strWhere = 'id_usuario_PK=' . $user["id_usuario_PK"];
                            // $this->pdo->delete('Usuario', $strWhere);
                        }
                    } else httpResponse(['error' => true], "No se insertó el usuario :D");
                } else httpResponse(['error' => true], "Llena los datos del Cliente completamente");

                // REGISTRO CON GOOGLE
            } else {
                $userClientGoogleData = $this->auth->checkRedirectCodeAndGetAuthGoogleData();

                $verified = false;

                foreach ($userClientGoogleData as $key => $value) {

                    if ($key == "verifiedEmail" && $value == true) $verified = true;
                }

                if ($verified) {

                    $user = $this->model->saveUserWithGoogle($userClientGoogleData);

                    if ($user != "23000") {

                        if ($user['id_usuario_PK'] != '') {

                            $userClientGoogleData->id_usuario_FK = $user['id_usuario_PK'];

                            $client = $this->model->saveClientWithGoogle($userClientGoogleData);

                            if ($client) {
                                $userClientGoogleData->correo = $userClientGoogleData->email;
                                $userClientGoogleData->clave = $userClientGoogleData->id;

                                $arrUser = (array) $userClientGoogleData;

                                $login = $this->login->validateUser($arrUser);

                                if ($login) {
                                    echo "<script>window.location='?c=dash&m=index'</script>";
                                }
                            }
                        }
                    } else { // Si ya existe que lo loguee

                        echo "<script>
                            alert('Ya existe el usuario que ingresaste, ingresa');
                            window.location='?c=home&m=access'
                        </script>";


                    }
                } else {
                    echo "<script>
                        alert('Error de credenciales de Google');
                        window.location='?c=home&m=register'
                        </script>";
                }
            }
        }
    }

    public function saveTech($arrayTechnicians)
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            try {
                if ($this->model->saveTechnical($arrayTechnicians)) {
                    return true;
                }
                return false;
            } catch (Exception $e) {
                exit($e->getMessage());
            }
        } else {
            require "views/other/404.php";
        }
    }

    public function saveCli($arrayClients)
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            try {
                if ($this->model->saveClient($arrayClients)) {
                    return true;
                }
                return false;
            } catch (Exception $e) {
                exit($e->getMessage());
            }
        } else {
            require "views/other/404.php";
        }
    }

    public function saveCliHome($arrayClients)
    {
        try {
            if ($this->model->saveClientHome($arrayClients)) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }

    public function edit()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            if (isset($_REQUEST)) {
                $id = $_REQUEST['id_usuario_PK'];

                $data = $this->model->getActiveUsers($id);
                require 'views/layout.php';
                require 'views/users/edit.php';
            } else {
                echo "Error, no se realizo.";
            }
        } else {
            require "views/other/404.php";
        }
    }

    public function verifyUserByEmail()
    {
        if (isset($_GET['key']) && isset($_GET['id'])) {
            $key = $_GET['key'];
            $id = $_GET['id'];
            $user = $this->pdo->selectOne("usuario", ["keyreg" => $key]);
            if ($user) {
                $strWhere = "keyreg='{$key}'";
                $this->pdo->update("usuario", ["estado_usu" => 1, "keyreg" => null], $strWhere);

                require 'views/other/layoutOther.php';
                require 'views/other/changePass.php';
            } else {
                require 'views/other/404.php';
            }
        } else {
            require 'views/other/404.php';
        }
    }

    public function cpass()
    {
        if (isset($_POST)) {
            if (!empty($_POST)) {
                if ($_POST['pass'] == $_POST['cpass']) {

                    $this->model->editPassword($_POST);
                    httpResponse(
                        ['success' => true],
                        "Contraseña cambiada con exito",
                        [],
                        '?c=home&m=access'
                    );
                }
            }
        } else httpResponse(['error' => true], "Hubo un problema");
    }

    public function recoveryAccount()
    {
        try {
            $correo = isset($_POST['correo']) ? $_POST['correo'] : [];
            if (!empty($correo)) {
                // dd('ok');
                if ($this->model->recoveryAccount($correo)) {
                    // dd('ok');

                    httpResponse(
                        ['success' => true],
                        "ok. revisa el correo",
                        [],
                        '?c=home&m=access'
                    );
                }
                // else{ // usuario no existe
                //     httpResponse(['error' => true], "Hubo un problema con el usuario ingresado");
                // }
            } else {
                httpResponse(['error' => true], "Hubo un problema");
            }
        } catch (Exception $e) {
            dd($e);
        }
    }

    public function records()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            $records = $this->model->getRecords();

            require 'views/layout.php';
            require 'views/records/list.php';
            require 'views/footer.php';
        }
    }

    public function updateUserStatus()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {

            $id = trim($_GET['id_usuario_PK']);
            $status = trim($_GET['estado_usu']);

            $data = [
                'id_usuario_PK' => $id,
                'estado_usu' => $status
            ];
            // dd("ok");
            $this->model->editUserStatus($data);
            header('Location: ?c=user');
        } else {
            require 'views/other/404.php';
        }
    }
}
