<?php

/**
 * CLASE ServiceController
 * CONTROLADOR ALGO ASI COMO UN JS
 */

require 'models/Login.php';
require 'models/Service.php';
// require 'models/Client.php';
require 'models/Status.php';
require 'models/Artifact.php';
require 'models/ArtifactBrand.php';
require 'models/ArtifactCategory.php';
require 'models/Scategory.php';
// require 'models/Technical.php';
require 'models/District.php';
// Include the main TCPDF library (search for installation path).
include 'assets/plugins/tcpdf/tcpdf.php';

class ServiceController
{

    private $model;
    private $login;
    // private $client;
    private $status;
    private $artifact;
    private $artBrand;
    private $artCategory;
    private $scategory;
    private $technical;
    private $district;

    public function __construct()
    {
        if (!isset($_SESSION['user']['user']))
            header('Location: ?c=home&m=access');

        $id_usuario_PK = isset($_SESSION['user']['user']->id_usuario_PK) ? $_SESSION['user']['user']->id_usuario_PK : '';

        $this->model = new Service; //instanciando la clase Service

        $statusUser = $this->model->getStatusUserBySessionId($id_usuario_PK);

        $this->login = new Login;

        if (!empty($statusUser)) {
            if ($statusUser[0]->estado_usu == 2) $this->login->logout();
        } else $this->login->logout();

        $this->client = new Client;
        $this->status = new Status;
        $this->artifact = new Artifact;
        $this->artBrand = new ArtifactBrand;
        $this->artCategory = new ArtifactCategory;
        $this->scategory = new Scategory;
        $this->technical = new Technical;
        $this->district = new District;
    }

    public function index()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            $allservices = $this->model->getAll();
            $services = $this->model->getActiveServices();
            $clients = $this->client->getActiveClients();
            $statuses = $this->status->getAll();
            $artifacts = $this->artifact->getActiveArtifacts();
            $brands          = $this->artBrand->getBrands();
            $types          = $this->artCategory->getTypes();
            $scategories = $this->scategory->getActiveScategories();
            $technicians = $this->technical->getActiveTechnicians();
            $details = $this->model->getDetService();
            $districts = $this->district->getAll();
            $status_reports = $this->model->getInProcessServices();
            $status_reports2 = $this->model->getInWaitServices();
            $status_reports3 = $this->model->getCountFinishedServices();
            $status_reports4 = $this->model->getCountCanceledServices();
            $commits = $this->model->getAuditService();
            $evidence_service = $this->model->getEvidenceService();
            // dd($evidence_service[0]->url_foto_evidencia);
            // dd($evidence_service);
            require 'views/layout.php';
            require "views/service/new.php";
            require "views/service/list.php";
            require 'views/footer.php';
        } elseif ($_SESSION['user']['user']->id_rol_FK == 2) {
            $services = $this->model->getActiveServices();
            $clients = $this->client->getActiveClients();
            $statuses = $this->status->getAll();
            $artifacts = $this->artifact->getActiveArtifacts();
            $scategories = $this->scategory->getActiveScategories();
            $details = $this->model->getDetService();
            $districts = $this->district->getAll();
            $technicians = $this->technical->getActiveTechnicians();
            $status_reports = $this->model->getInProcessServices();
            $status_reports2 = $this->model->getInWaitServices();
            $status_reports3 = $this->model->getCountFinishedServices();
            $status_reports4 = $this->model->getCountCanceledServices();

            require 'views/partial/employee/layoutEmployee.php';
            require "views/partial/employee/new.php";
            require "views/partial/employee/list.php";
            require 'views/footer.php';
        } elseif ($_SESSION['user']['user']->id_rol_FK == 3) {
            if (isset($_SESSION['user']['client']->id_cliente_PK)) {
                $idC = $_SESSION['user']['client']->id_cliente_PK;
                $tecnicos       = $this->model->SP_ClientTechService();
                $categorias      = $this->model->SP_ClientCatService();
                $artefactos     = $this->model->SP_ClientArtService();
                $cli_services   = $this->model->SP_getAllClientService();
                $artifacts      = $this->artifact->getActiveArtifacts();
                $brands          = $this->artBrand->getBrands();
                $types          = $this->artCategory->getTypes();
                $scategories    = $this->scategory->getActiveScategories();

                require 'views/partial/client/layoutClient.php';
                require "views/partial/client/new.php";
                require "views/partial/client/list.php";
                require 'views/footer.php';
            } else $this->login->logout();
        }
    }

    public function history()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            $allservices = $this->model->getAll();
            $fservices = $this->model->getFinishedServices();
            $cservices = $this->model->getCanceledServices();
            $clients = $this->client->getActiveClients();
            $statuses = $this->status->getAll();
            $artifacts = $this->artifact->getActiveArtifacts();
            $scategories = $this->scategory->getActiveScategories();
            $technicians = $this->technical->getActiveTechnicians();
            $details = $this->model->getDetService();
            $districts = $this->district->getAll();

            require 'views/layout.php';
            require "views/service/history.php";
            require 'views/footer.php';
        } elseif ($_SESSION['user']['user']->id_rol_FK == 2)
            $this->login->logout();
        else
            $this->login->logout();
    }

    public function listArtifacts()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getArtServiceById($id_servicio_PK);
                $artifacts = $this->artifact->getActiveArtifacts();
                require 'views/layout.php';
                require 'views/fragments/artifacts.php';
                require 'views/footer.php';
            } else {
                require "views/other/404.php";
            }
        } elseif ($_SESSION['user']['user']->id_rol_FK == 2) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getArtServiceById($id_servicio_PK);
                $artifacts = $this->artifact->getActiveArtifacts();
                require 'views/partial/employee/layoutEmployee.php';
                require 'views/partial/employee/fragments/artifacts.php';
                require 'views/footer.php';
            } else {
                require "views/other/404.php";
            }
        } else {
            $this->login->logout();
        }
    }


    public function listScategories()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getCatServiceById($id_servicio_PK);
                $scategories = $this->scategory->getActiveScategories();
                require 'views/layout.php';
                require 'views/fragments/scategories.php';
                require 'views/footer.php';
            } else {
                require "views/other/404.php";
            }
        } elseif ($_SESSION['user']['user']->id_rol_FK == 2) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getCatServiceById($id_servicio_PK);
                $scategories = $this->scategory->getActiveScategories();
                require 'views/partial/employee/layoutEmployee.php';
                require 'views/partial/employee/fragments/scategories.php';
                require 'views/footer.php';
            } else {
                require "views/other/404.php";
            }
        } elseif ($_SESSION['user']['user']->id_rol_FK == 3) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getCatServiceById($id_servicio_PK);
                $scategories = $this->scategory->getActiveScategories();
                require 'views/partial/client/layoutClient.php';
                require 'views/partial/client/fragments/scategories.php';
                require 'views/footer.php';
            } else {
                require "views/other/404.php";
            }
        }
    }


    public function listTechnicians()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getTechServiceById($id_servicio_PK);
                $technicians = $this->technical->getActiveTechnicians();
                require 'views/layout.php';
                require 'views/fragments/technicians.php';
                require 'views/footer.php';
            } else {
                require "views/other/404.php";
            }
        } elseif ($_SESSION['user']['user']->id_rol_FK == 2) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getTechServiceById($id_servicio_PK);
                $technicians = $this->technical->getActiveTechnicians();
                require 'views/partial/employee/layoutEmployee.php';
                require 'views/partial/employee/fragments/technicians.php';
                require 'views/footer.php';
            } else {
                require "views/other/404.php";
            }
        } else {
            $this->login->logout();
        }
    }

    public function listDetails()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getDetServiceById($id_servicio_PK);
                $technicians = $this->technical->getActiveTechnicians();
                // dd($technicians);
                require 'views/layout.php';
                require 'views/fragments/detail.php';
                require 'views/footer.php';
            } else {
                require "views/other/404.php";
            }
        } elseif ($_SESSION['user']['user']->id_rol_FK == 2) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getDetServiceById($id_servicio_PK);
                require 'views/partial/employee/layoutEmployee.php';
                require 'views/partial/employee/fragments/detail.php';
                require 'views/footer.php';
            } else {
                require "views/other/404.php";
            }
        } else {
            $this->login->logout();
        }
    }

    public function technicianCrossover()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            $tecnicosCita = json_decode($_POST["data"]);
            // dd(empty($tecnicosCita[1]));
            // dd(isset($tecnicosCita[1]->fecha));
            // dd($tecnicosCita[0]);
            if (isset($tecnicosCita[1]->fecha)) {
                $fechaCita = $tecnicosCita[1]->fecha;

                if ($fechaCita != '') {

                    if (strlen($fechaCita) <= 17) {

                        $realDate = $this->model->valDateCitation($fechaCita);

                        if ($realDate) {

                            $crossover = $this->model->valTechnicianCrossover($tecnicosCita);

                            if ($crossover) {

                                echo json_encode('well');
                                return;
                            } else {

                                echo json_encode('crosserror');
                                return;
                            }
                        } else {

                            echo json_encode('pastdateerror');
                            return;
                        }
                    }
                }
            }

            echo json_encode('badate');
            return;
        }
    }

    public function save() // realiza el proceso de guardar
    {
        $dataService = // DATOS SERVICIO
            [
                'id_cliente_FK'          =>  $_POST['id_cliente_FK'],
                'id_estado_servicio_FK'  =>  2,
                'fecha_peticion'         =>  date("Y-m-d"),
                'hora_peticion'          =>  date("H:i:s"),
            ];

        $arrayArtifacts   = isset($_POST['artifacts'])   ? $_POST['artifacts']   : [];
        $arrayScategories = isset($_POST['scategories']) ? $_POST['scategories'] : []; //llega del js
        // $arrayTechnicians = isset($_POST['technicians']) ? $_POST['technicians'] : []; //llega del js
        $arrayDetails     = isset($_POST['details'])     ? $_POST['details']     : []; //llega del js

        // dd($arrayDetails);
        // $arrayDetails[0]["is_software"] = ($arrayDetails[0]["is_software"] == "true") ? true : false;
        // $arrayDetails[0]["is_hardware"] = ($arrayDetails[0]["is_hardware"] == "true") ? true : false;

        // $arrEvidence = isset($_POST['evidence']) ? $_POST['evidence'] : [];

        // dd($arrEvidence);

        if (
            !empty($arrayArtifacts)
            && !empty($arrayScategories)
            // && !empty($arrayTechnicians)
            && !empty($arrayDetails)
        ) {

            $respService       = $this->model->newService($dataService); // Inserción servicio

            $lastIdService     = $this->model->getServiceLastId(); //obtener ultimo id del servicio

            // if (!empty($arrEvidence)) {
            //     $respEvidenceService = $this->model->saveEvidenceService(
            //         $arrEvidence,
            //         $lastIdService[0]->id_servicio_PK
            //     );
            // }

            $respArtService = $this->model->saveArtService(
                $arrayArtifacts, //insercion al detalle artefacto
                $lastIdService[0]->id_servicio_PK
            );

            $respCatService = $this->model->saveCatService(
                $arrayScategories, //insercion al detalle categoria
                $lastIdService[0]->id_servicio_PK
            );

            // $respTechService = $this->model->saveTechService(
            //     $arrayTechnicians,  //insercion al detalle Tecnico
            //     $lastIdService[0]->id_servicio_PK,
            // );

            $respDetService = $this->model->saveDetService(
                $arrayDetails, //insercion a los detalles
                $lastIdService[0]->id_servicio_PK
            );
        } else {
            $respService        = false;
            $respArtService     = false;
            $respCatService     = false;
            // $respTechService    = false;
            $respDetService     = false;
        }

        $arrayResp = [];

        if (
            $respService == true
            && $respArtService == true
            && $respCatService == true
            && $respDetService == true
            // && $respTechService == true
        ) {

            $arrayResp = [
                'success' => true,
                'message' => "Servicio generado"
            ];
        } else {
            $arrayResp = [
                'error' => false,
                'message' => "Error generando el servicio"
            ];
        }
        echo json_encode($arrayResp);
        return;
    }

    public function saveEmployee() // realiza el proceso de guardar
    {
        $dataService = // DATOS SERVICIO
            [
                'id_cliente_FK'          =>  $_POST['id_cliente_FK'],
                'id_estado_servicio_FK'  =>  2,
                'fecha_peticion'         =>  date("Y-m-d"),
                'hora_peticion'          =>  date("H:i:s")
            ];
        $arrayArtifacts   = isset($_POST['artifacts'])   ? $_POST['artifacts']   : []; //llega del js
        $arrayScategories = isset($_POST['scategories']) ? $_POST['scategories'] : []; //llega del js
        $arrayDetails     = isset($_POST['details'])     ? $_POST['details']     : []; //llega del js

        $arrayDetails[0]["is_software"] = ($arrayDetails[0]["is_software"] == "true") ? true : false;
        $arrayDetails[0]["is_hardware"] = ($arrayDetails[0]["is_hardware"] == "true") ? true : false;

        if (
            !empty($arrayArtifacts)
            && !empty($arrayScategories)
            && !empty($arrayDetails)
        ) {
            $respService       = $this->model->newService($dataService); // Inserción servicio
            $lastIdService     = $this->model->getServiceLastId(); //obtener ultimo id del servicio

            $respArtService = $this->model->saveArtService(
                $arrayArtifacts, //insercion al detalle artefacto
                $lastIdService[0]->id_servicio_PK
            );

            $respCatService = $this->model->saveCatService(
                $arrayScategories, //insercion al detalle categoria
                $lastIdService[0]->id_servicio_PK
            );

            $respTechService = $this->model->saveTechServiceEmployee(
                $lastIdService[0]->id_servicio_PK
            );

            $respDetService = $this->model->saveDetService(
                $arrayDetails, //insercion a los detalles
                $lastIdService[0]->id_servicio_PK
            );
        } else {
            $respService        = false;
            $respArtService     = false;
            $respCatService     = false;
            $respTechService    = false;
            $respDetService     = false;
        }

        $arrayResp = [];

        if (
            $respService == true
            && $respArtService == true
            && $respCatService == true
            && $respDetService == true
            && $respTechService == true
        ) {

            $arrayResp = [
                'success' => true,
                'message' => "Servicio generado"
            ];
        } else {
            $arrayResp = [
                'error' => false,
                'message' => "Error generando el servicio"
            ];
        }
        echo json_encode($arrayResp);
        return;
    }

    public function saveClient()
    {
        $dataService = // DATOS SERVICIO
            [
                'id_cliente_FK'          =>  $_SESSION['user']['client']->id_cliente_PK,
                'id_estado_servicio_FK'  =>  2,
                'fecha_peticion'         =>  date("Y-m-d"),
                'hora_peticion'          =>  date("H:i:s")
            ];

        $arrayArtifacts   = isset($_POST['artifacts'])   ? $_POST['artifacts']   : [];
        $arrayScategories = isset($_POST['scategories']) ? $_POST['scategories'] : [];
        $arrayDetails     = isset($_POST['details'])     ? $_POST['details']     : [];

        if (
            !empty($arrayArtifacts)
            && !empty($arrayScategories)
            && !empty($arrayDetails)
        ) {
            $respService       = $this->model->newService($dataService); // Inserción servicio
            $lastIdService     = $this->model->getServiceLastId(); //obtener ultimo id del servicio

            $respArtService = $this->model->saveArtService(
                $arrayArtifacts, //insercion al detalle artefacto
                $lastIdService[0]->id_servicio_PK
            );

            $respTechService = $this->model->saveTechServiceClient(
                $lastIdService[0]->id_servicio_PK,
            );

            $respCatService = $this->model->saveCatService(
                $arrayScategories, //insercion al detalle categoria
                $lastIdService[0]->id_servicio_PK
            );

            $respDetService = $this->model->saveDetServiceClient(
                $arrayDetails, //insercion a los detalles
                $lastIdService[0]->id_servicio_PK
            );
        } else {
            $respService        = false;
            $respArtService     = false;
            $respCatService     = false;
            $respDetService     = false;
            $respTechService    = false;
        }

        $arrayResp = [];

        if (
            $respService == true
            && $respArtService == true
            && $respCatService == true
            && $respDetService == true
            && $respTechService == true
        ) {

            $arrayResp = [
                'success' => true,
                'error' => false,
                'message' => "Servicio generado"
            ];
        } else {
            $arrayResp = [
                'error' => true,
                'message' => "Error generando el servicio"
            ];
        }
        echo json_encode($arrayResp);
        return;
    }

    // muestra la vista de editar
    public function edit()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            if (isset($_REQUEST['id_servicio_PK'])) {
                $id_servicio_PK = trim($_REQUEST['id_servicio_PK']);
                // dd(trim($id_servicio_PK));
                $data = $this->model->getServiceById($id_servicio_PK);
                $detailsArt = $this->model->getArtService($id_servicio_PK);
                $detailsCat = $this->model->getCatService($id_servicio_PK);
                $details = $this->model->getDetServiceById($id_servicio_PK);
                $clients = $this->client->getActiveClients();
                // dd($clients);
                $statuses = $this->status->getAll();
                $artifacts = $this->artifact->getActiveArtifacts();
                $scategories = $this->scategory->getActiveScategories();
                $detailsTech = $this->model->getTechService($id_servicio_PK);
                $technicians = $this->technical->getActiveTechnicians();
                require 'views/layout.php';
                require 'views/service/edit.php';
                require 'views/footer.php';
            } else {
                echo "Hubo un error <i class='fas fa-sad-cry'></i>";
            }
        } elseif ($_SESSION['user']['user']->id_rol_FK == 2) {
            if (isset($_REQUEST['id_servicio_PK'])) {

                $id_servicio_PK = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getServiceById($id_servicio_PK);
                $detailsArt = $this->model->getArtService($id_servicio_PK);
                $detailsCat = $this->model->getCatService($id_servicio_PK);
                $details = $this->model->getDetServiceById($id_servicio_PK);
                $clients = $this->client->getActiveClients();
                $statuses = $this->status->getAll();
                $artifacts = $this->artifact->getActiveArtifacts();
                $scategories = $this->scategory->getActiveScategories();
                require 'views/partial/employee/layoutEmployee.php';
                require "views/partial/employee/edit.php";
                require 'views/footer.php';
            } else {
                echo "Hubo un error <i class='fas fa-sad-cry'></i>";
            }
        } elseif ($_SESSION['user']['user']->id_rol_FK == 3) {
            // validar que el cliente relacion con el servicio

            if (isset($_REQUEST['id_revision_servicio_PK']) && isset($_REQUEST['id_servicio_PK'])) {
                $id = $_REQUEST['id_revision_servicio_PK'];
                $idServicio = $_REQUEST['id_servicio_PK'];
                $data = $this->model->getClientServiceById($id);
                $idCliSession = $_SESSION['user']['client']->id_cliente_PK;
                if (isset($data[0]->id_cliente_FK) == $idCliSession) {
                    $detailsArt = $this->model->getArtService($idServicio);
                    $detailsCat = $this->model->getCatService($idServicio);
                    $artifacts = $this->artifact->getActiveArtifacts();
                    $scategories = $this->scategory->getActiveScategories();
                    require 'views/partial/client/layoutClient.php';
                    require "views/partial/client/edit.php";
                    require 'views/footer.php';
                } else {
                    echo "<script>alert('No tienes permiso para ingresar a esta sección')</script>";
                    header('Location: ?c=service&m=edit');
                }
            } else {
                echo "Hubo un error";
            }
        }
    }

    // realiza el proceso de editar
    public function update()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {

            $arrayResp = [];

            if (isset($_POST)) {
                // dd($_POST);

                $dataService = // DATOS SERVICIO
                    [
                        'id_servicio_PK'         =>  $_POST['id_servicio_PK'],
                        'id_cliente_FK'          =>  $_POST['id_cliente_FK'],
                        'id_estado_servicio_FK'  =>  $_POST['id_estado_servicio_FK']
                    ];

                $arrayArtifacts   = isset($_POST['artifacts'])   ? $_POST['artifacts']   : []; //llega del js
                $arrayScategories = isset($_POST['scategories']) ? $_POST['scategories'] : []; //llega del js
                $arrayTechnicians = isset($_POST['technicians']) ? $_POST['technicians'] : []; //llega del js
                $arrayDetails     = isset($_POST['details'])     ? $_POST['details']     : []; //llega del js

                // $arrayDetails[0]["is_software"] = ($arrayDetails[0]["is_software"] == "true") ? true : false;
                // $arrayDetails[0]["is_hardware"] = ($arrayDetails[0]["is_hardware"] == "true") ? true : false;

                $arrEvidence = isset($_POST['evidence']) ? $_POST['evidence'] : [];

                if (
                    !empty($arrayArtifacts)
                    && !empty($arrayScategories)
                    && !empty($dataService)
                    && !empty($arrayTechnicians)
                    && !empty($arrayDetails)
                ) {
                    // dd("ok");
                    if (!empty($arrEvidence)) {
                        $respEvidenceService = $this->model->saveEvidenceService(
                            $arrEvidence,
                            $_POST['id_servicio_PK']
                        );
                    }

                    $respService = $this->model->editService($dataService);

                    $this->model->deleteArtService($_POST['id_servicio_PK']); //Quitar detalles
                    $this->model->deleteCatService($_POST['id_servicio_PK']);
                    $this->model->deleteTechService($_POST['id_servicio_PK']);

                    $respArtService = $this->model->saveArtService(
                        $arrayArtifacts, //insercion al detalle artefacto
                        $_POST['id_servicio_PK']
                    );

                    $respCatService = $this->model->saveCatService(
                        $arrayScategories, //insercion al detalle categoria
                        $_POST['id_servicio_PK']
                    );

                    $respTechService = $this->model->saveTechService(
                        $arrayTechnicians,  //insercion al detalle Tecnico
                        $_POST['id_servicio_PK'],
                    );

                    $respDetService = $this->model->editDetService($arrayDetails, $_POST['id_servicio_PK']);
                } else {
                    $respService        = false;
                    $respArtService     = false;
                    $respCatService     = false;
                    $respTechService    = false;
                    $respDetService     = false;
                }
            } else {
                $arrayResp = [
                    'error' => true,
                    'message' => "Error actualizando el servicio"
                ];
            }
            echo json_encode($arrayResp);
            return;
        } else {
            $this->login->logout();
        }
    }

    public function updateEmployee()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 2) {

            $arrayResp = [];

            if (isset($_POST)) {

                $dataService = // DATOS SERVICIO
                    [
                        'id_servicio_PK'         =>  $_POST['id_servicio_PK'],
                        'id_cliente_FK'          =>  $_POST['id_cliente_FK'],
                        'id_estado_servicio_FK'  =>  $_POST['id_estado_servicio_FK']
                    ];

                $arrayArtifacts   = isset($_POST['artifacts'])   ? $_POST['artifacts']   : []; //llega del js
                $arrayScategories = isset($_POST['scategories']) ? $_POST['scategories'] : []; //llega del js
                $arrayDetails     = isset($_POST['details'])     ? $_POST['details']     : []; //llega del js

                $arrayDetails[0]["is_software"] = ($arrayDetails[0]["is_software"] == "true") ? true : false;
                $arrayDetails[0]["is_hardware"] = ($arrayDetails[0]["is_hardware"] == "true") ? true : false;

                if (
                    !empty($arrayArtifacts)
                    && !empty($arrayScategories)
                    && !empty($arrayDetails)
                ) {

                    $respService = $this->model->editService($dataService);

                    $this->model->deleteArtService($_POST['id_servicio_PK']); //Quitar detalles
                    $this->model->deleteCatService($_POST['id_servicio_PK']);

                    $respArtService = $this->model->saveArtService(
                        $arrayArtifacts, //insercion al detalle artefacto
                        $_POST['id_servicio_PK']
                    );

                    $respCatService = $this->model->saveCatService(
                        $arrayScategories, //insercion al detalle categoria
                        $_POST['id_servicio_PK']
                    );

                    $respDetService = $this->model->editDetServiceEmployee($arrayDetails, $_POST['id_servicio_PK']);
                } else {
                    $respService        = false;
                    $respArtService     = false;
                    $respCatService     = false;
                    $respDetService     = false;
                }
            } else {
                $arrayResp = [
                    'error' => true,
                    'message' => "Error generando el servicio"
                ];
            }
            echo json_encode($arrayResp);
            return;
        } else {
            $this->login->logout();
        }
    }

    // Actualizar servicio del cliente
    public function updateClient()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 3) {
            $arrayResp = [];

            if (isset($_POST)) {

                $arrayArtifacts   = isset($_POST['artifacts'])   ? $_POST['artifacts']   : []; //llega del js
                $arrayScategories = isset($_POST['scategories']) ? $_POST['scategories'] : []; //llega del js
                $arrayDetails     = isset($_POST['details'])     ? $_POST['details']     : []; //llega del js

                if (
                    !empty($arrayArtifacts)
                    && !empty($arrayScategories)
                    && !empty($arrayDetails)
                ) {

                    $this->model->deleteArtService($_POST['id_servicio_PK']); //Quitar detalles
                    $this->model->deleteCatService($_POST['id_servicio_PK']);

                    $respArtService = $this->model->saveArtService(
                        $arrayArtifacts, //insercion al detalle artefacto
                        $_POST['id_servicio_PK']
                    );

                    $respCatService = $this->model->saveCatService(
                        $arrayScategories, //insercion al detalle categoria
                        $_POST['id_servicio_PK']
                    );

                    $respDetService = $this->model->editDetServiceClient($arrayDetails, $_POST['id_servicio_PK']);

                    if ($respDetService) {
                        $arrayResp = [
                            'error' => false,
                            'message' => "Actaulizado"
                        ];
                    } else {
                        $arrayResp = [
                            'error' => true,
                            'message' => "Error generando el servicio"
                        ];
                    }
                } else {
                    $respService        = false;
                    $respArtService     = false;
                    $respCatService     = false;
                    $respDetService     = false;

                    $arrayResp = [
                        'error' => true,
                        'message' => "Error generando el servicio"
                    ];
                }
            } else {
                $arrayResp = [
                    'error' => true,
                    'message' => "Error generando el servicio"
                ];
            }

            echo json_encode($arrayResp);
            return;
        } else {
            $this->login->logout();
        }
    }


    public function updateServiceStatus()
    {
        $id_servicio_PK        = trim($_GET["id_servicio_PK"]);
        $id_estado_servicio_FK = trim($_GET["id_estado_servicio_FK"]);

        $data =  [
            'id_servicio_PK'        => $id_servicio_PK,
            'id_estado_servicio_FK' => $id_estado_servicio_FK
        ];

        $this->model->editServiceStatus($data);

        if ($id_estado_servicio_FK == 3 || $id_estado_servicio_FK == 4) {
            $data2 = [
                'id_servicio_PK' => $id_servicio_PK
            ];

            $this->model->editDetailDate($data2, $id_estado_servicio_FK);
        }

        header('Location: ?c=service');
    }

    public function serviceEvidence()
    {
        // Por validar iomagenes
        if ($_SERVER['REQUEST_METHOD'] === "POST") {
            // $img = (object)$_FILES['img'];
            $imgs = array_values($_FILES);
            $pathImg = [];
            foreach ($imgs as $img) {

                $img = (object)$img;

                $path = 'assets/img/serviceEvidenceImg';
                // app a -> https://colombia,.com "venezuaela.com/assets/img/serviceEvidenceImg"
                // app b -> https://venezuaela.com -> assets/img/serviceEvidenceImg
                // Directorio
                if (!file_exists($path)) {
                    if (!mkdir($path, 0777, true)) {
                        http_response_code(400);
                    }
                }

                if (move_uploaded_file($img->tmp_name, $path . '/' . $img->name)) {
                    // http_response_code(200); http_respose_
                    array_push($pathImg, MAIN_URL . $path . '/' . $img->name);
                    // dd($pathImg);
                }
            }
            echo json_encode([
                "status" => 200,
                "ok" => true,
                "images" => $pathImg,
            ]);
            return;
        } else {
            http_response_code(400);
        }
    }

    // public function audit()
    // {
    //     if ($_SESSION['user']['user']->id_rol_FK == 1) {
    //         if (isset($_REQUEST['id_servicio_PK'])) {
    //             require 'views/layout.php';
    //             require 'views/list.php';
    //             require 'views/new.php';
    //             require 'views/footer.php';
    //         } else {
    //             echo "Hubo un error <i class='fas fa-sad-cry'></i>";
    //         }
    //     } else {
    //         $this->login->logout();
    //     }
    // }

    public function paymentService()
    {
        // CONSULTAMOS DATOS DE LA TRANSFERENCIA
        // dd($_GET);
        // dd($_GET['paymentID'], $_GET);
        // dd(json_decode($_GET['data']));
        // dd($_GET['paymentID'], $_GET['paymentToken']);

        $login = curl_init("https://api-m.sandbox.paypal.com/v1/oauth2/token");
        curl_setopt($login, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($login, CURLOPT_USERPWD, PayPalClientId . ":" . PayPalSecret);
        curl_setopt($login, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

        $result = curl_exec($login);

        $objResult = json_decode($result);

        $accessToken = $objResult->access_token;

        // $venta = curl_init("https://api.sandbox.paypal.com/v2/checkout/orders/".$_GET['orderID']);
        $venta = curl_init("https://api.sandbox.paypal.com/v1/payments/payment/" . $_GET['paymentID']);

        curl_setopt($venta, CURLOPT_POST, false);
        curl_setopt($venta, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($venta, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($venta, CURLOPT_HTTPHEADER, array("Content-Type:application/json", "Authorization: Bearer " . $accessToken));
        $repuestaVenta = curl_exec($venta);

        // if (!curl_errno($venta)) $info = curl_getinfo($venta);
        $respObj = json_decode($repuestaVenta);

        // dd($respObj);

        $email = $respObj->payer->payer_info->email;
        $state = $respObj->state;
        $total = $respObj->transactions[0]->amount->total;
        $currency = $respObj->transactions[0]->amount->currency; // USD
        $custom = explode("#%|", $respObj->transactions[0]->custom);
        $id = substr(openssl_decrypt($custom[0], COD, KEY), 3); // SERVICIO

        curl_close($venta);
        curl_close($login);

        $valTransfer = $this->model->valServiceTransfer($id, $total);

        // dd($valTransfer);
        // dd($state);

        if ($valTransfer) {
            if ($state == "approved") {

                // create new PDF document
                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

                // set document information
                $pdf->SetCreator('Solupanel');
                $pdf->SetAuthor('Solumovil');
                $pdf->SetTitle('Factura de transacción exitosa');
                $pdf->SetSubject('Factura de Solupanel');
                $pdf->SetKeywords('factura, PDF, transacción, servicio técnico, pago');

                // set default header data
                $pdf->SetHeaderData('S', PDF_HEADER_LOGO_WIDTH, 'Factura Solupanel', 'https://solumovil-tigo.negocio.site/?m=true', array(0, 64, 255), array(0, 64, 128));
                $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

                // set header and footer fonts
                $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                // set margins
                $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

                // set auto page breaks
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

                // set image scale factor
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                // set some language-dependent strings (optional)
                if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                    require_once(dirname(__FILE__) . '/lang/eng.php');
                    $pdf->setLanguageArray($l);
                }

                // ---------------------------------------------------------

                // set default font subsetting mode
                $pdf->setFontSubsetting(true);

                // Set font
                // dejavusans is a UTF-8 Unicode font, if you only need to
                // print standard ASCII chars, you can use core fonts like
                // helvetica or times to reduce file size.
                $pdf->SetFont('dejavusans', '', 14, '', true);

                // Add a page
                // This method has several options, check the source code documentation for more information.
                $pdf->AddPage();

                // set text shadow effect
                $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

                // Set some content to print
                $html = <<<EOD
<h1>Solumovil
<a href="?c=service">Volver</a>
</h1>
<i>Agradecemos su compra, esperamos halla tenido una buena experiencia con nosotros.</i><br><br><br>
<table class="table table-bordered">
		<tr>
			<th>
				<h4>Servicio</h4>
			</th>
			<th>
				<h4>Total</h4>
			</th>
		</tr>
		<tr>
			<td>#$id</td>
            <td><a href="#"> $total USD</a></td>
		</tr>
</table>
	<div class="row">
			<div class="col-xs-4">
				<h1><a href=" "><img alt="" src="assets/img/1.ico"/></a></h1>
			</div>
			<div class="col-xs-8">

				<div class="panel panel-info"  style="text-align: right;">
					<h6> "LA ALTERACI&Oacute;N, FALSIFICACI&Oacute;N O COMERCIALIZACI&Oacute;N ILEGAL DE ESTE DOCUMENTO ESTA PENADO POR LA LEY"</h6>
				</div>
		</div>
	</div>
EOD;

                // Print text using writeHTMLCell()
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                // ---------------------------------------------------------

                // Close and output PDF document
                // This method has several options, check the source code documentation for more information.
                $pdf->Output('factura_solumovil.pdf', 'I');

                //============================================================+
                // END OF FILE
                //============================================================+
                return;
            } else {
                $msg = "desaprobado";
                echo json_encode($msg);
                return;
            }
        }
    }


    public function costingService()
    {
        $arrCosts = json_decode($_POST['costing']);

        $costing = $this->model->editCostsService($arrCosts->idService, $arrCosts->arrCostsToEdit);

        if ($costing) {
            echo json_encode('well');
            return;
        } else {
            echo json_encode('bad');
            return;
        }
    }
}
