<?php

/**
 * CLASE CategoryController
 * CONTROLADOR ALGO ASI COMO UN JS
 */
require 'models/Client.php';
require 'models/District.php';
require 'models/User.php';

class ClientController extends Controller
{
    private $model;
    private $district;
    private $user;
    // private $user;

    public function __construct()
    {
        // parent::__construct();
        if (!isset($_SESSION['user']['user']->id_rol_FK) == 3)
            header('Location: ?controller=home');
        $this->model = new Client;
        $this->district = new District;
        $this->user = new User;
        // $this->user = new User;
    }

    public function index()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1 || $_SESSION['user']['user']->id_rol_FK == 2) {
            $clients = $this->model->getActiveClients();

            require 'views/layout.php';
            require "views/client/new.php";
            require "views/client/list.php";
            require 'views/footer.php';
        } else {
            require 'views/other/404.php';
        }
    }

    public function update()
    {
        // dd(json_decode($_POST["data"]));

        if (isset($_POST["data"])) {
            $clientCompleteData = json_decode($_POST["data"]);
            // dd($clientCompleteData[0]);
            if (!empty($clientCompleteData)) {
                // $data = strip_tags(htmlspecialchars($clientCompleteData,ENT_QUOTES));
                $updateClient = $this->model->updateClient($clientCompleteData);

                if ($updateClient) {
                    echo json_encode("bien");
                    return;
                } else {
                    echo json_encode("mal");
                    return;
                }
            }
        }
    }

    public function save($arrayClients)
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1 || $_SESSION['user']['user']->id_rol_FK == 2) {
            try {
                if ($this->user->saveClient($arrayClients)) {
                    return true;
                }
                return false;
            } catch (Exception $e) {
                exit($e->getMessage());
            }
        } else {
            require 'views/other/404.php';
        }
    }

    public function editProfile()
    {
        // dd(json_decode($_POST["data"]));

        $clientOrDistrictData = json_decode($_POST["data"]);

        // dd($clientOrDistrictData->data);
        $sepData = explode('&|', $clientOrDistrictData->data);

        $editData = [];

        if ($sepData[0] == 'cliente')
            $editData = $this->model->editInstantlyClient($sepData[1], $sepData[2]);
        elseif ($sepData[0] == 'barrio')
            $editData = $this->district->editInstantlyDistrict($sepData[1], $sepData[2]);

        if ($editData) {
            echo json_encode("bien");
            return;
        } else {
            echo json_encode("mal");
            return;
        }
        // dd($sepData);
    }
}
