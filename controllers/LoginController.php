<?php

require "models/Login.php";

class LoginController
{
    private $model;
    private $auth;

    public function __construct()
    {
        $this->auth = getGoogleData2();
        $this->model = new Login;
    }

    public function index()
    {
        if (isset($_SESSION['user']))
            header('Location: ?c=dash&m=index');
    }

    public function login()
    {
        if (isset($_POST)) {

            $normalRedirect = false;

            if (!empty($_POST)) {

                $arrLogin = $_POST;
                $correo = $_POST['correo'];

            } else {
                $arrLogin = (array) $this->auth->checkRedirectCodeAndGetAuthGoogleData();

                $verified = false;

                foreach ($arrLogin as $key => $value)
                    if ($key == "verifiedEmail" && $value == true) $verified = true;

                if ($verified) { //Registro normal?
                    $arrLogin['correo'] = $arrLogin['email'];
                    $arrLogin['clave'] = $arrLogin['id'];
                    $correo = $arrLogin['correo'];
                    $normalRedirect = true;
                } else {
                    echo "<script>
                        alert('Error, Verifica tus credenciales');
                        window.location='?c=home&m=access'
                        </script>";
                }
            }

            $user = $this->model->getTriesUserByEmail($correo);
            if ($user) {
                if ($user[0]->intentos >= 5)
                    $this->model->setStatusUserToFalseByEmail($correo);
            }


            $validateUser = $this->model->validateUser($arrLogin);

            if ($validateUser === true) {
                // Add session for system inventory
                require_once "./inventario/helpers/AccessData.php";
                require_once "./inventario/helpers/SessionToken.php";
                SessionToken::create($_SESSION);
                // end
                if (isset($_SESSION['intentos']))
                    if (array_key_exists('intentos', $_SESSION)) $_SESSION['intentos'] = 0;

                if ($user)
                    $this->model->cleanTriesUserByEmail($correo);


                $this->model->saveRecordUser();

                ExpireSession::createCookieExpired();

                if ($normalRedirect)
                    header('Location: ?c=dash&m=index');
                else {
                    httpResponse(
                        ['success' => true],
                        "",
                        [],
                        '?c=dash&m=index',
                        false,
                        ''
                    );
                }
            } else {

                try {
                    $error =
                        [
                            'errorMessage' => $validateUser,
                        ];

                    if (array_key_exists('intentos', $_SESSION)) {

                        $user = $this->model->getTriesUserByEmail($correo);

                        if (!empty($user)) {
                            if ($user[0]->intentos <= 5) {
                                $_SESSION['intentos'] = $_SESSION['intentos'] + 1;
                            }

                            if ($_SESSION['intentos'] > 2 && $_SESSION['intentos'] <= 5) {
                                $unlessTries = (6 - $_SESSION['intentos']);
                            }
                        } else {
                            if ($normalRedirect) {
                                echo "<script>
                                alert('Error, Verifica tus credenciales');
                                window.location='?c=home&m=access'
                                </script>";
                            } else {
                                httpResponse(
                                    ['error' => true],
                                    "",
                                    $error,
                                    '?c=home&m=access',
                                    true,
                                    $correo
                                );
                            }
                        }
                    } else
                        $_SESSION['intentos'] = 0;


                    if (!empty($user)) {
                        $this->model->updateTriesUserByEmail($correo);
                        $statusUser = $this->model->getStatusUserByEmail($correo);

                        if ($statusUser[0]->estado_usu == 2) {
                            if ($normalRedirect) {
                                echo "<script>
                                alert('$error');
                                window.location='?c=home&m=access'
                                </script>";
                                header('');
                            } else {
                                httpResponse(
                                    ['error' => true],
                                    "",
                                    $error,
                                    '?c=home&m=access',
                                    true,
                                    $correo
                                );
                            }
                        } else {
                            if (isset($unlessTries)) {
                                if ($unlessTries == 1) {

                                    if ($normalRedirect) {
                                        echo "<script>
                                alert('Error, Verifica tus credenciales');
                                window.location='?c=home&m=access'
                                </script>";
                                        header('');
                                    } else {
                                        httpResponse(
                                            ['error' => true],
                                            "Si olvidaste tu contraseña por favor cambiala.
                                        Te queda '{$unlessTries}' intento y tu cuenta quedará bloqueada",
                                            $error,
                                            '?c=home&m=access',
                                            true,
                                            $correo
                                        );
                                    }
                                } else {

                                    if ($normalRedirect) {
                                        echo "<script>
                                alert('Error, Verifica tus credenciales');
                                window.location='?c=home&m=access'
                                </script>";
                                        header('');
                                    } else {
                                        httpResponse(
                                            ['error' => true],
                                            "Si olvidaste tu contraseña por favor cambiala.
                                        Te quedan '{$unlessTries}' intentos y tu cuenta quedará bloqueada",
                                            $error,
                                            '?c=home&m=access',
                                            true,
                                            $correo
                                        );
                                    }
                                }
                            } else {

                                if ($normalRedirect) {
                                    echo "<script>
                                alert('Error, Verifica tus credenciales');
                                window.location='?c=home&m=access'
                                </script>";
                                    header('');
                                } else {
                                    httpResponse(
                                        ['error' => true],
                                        "",
                                        $error,
                                        '?c=home&m=access',
                                        true,
                                        $correo
                                    );
                                }
                            }
                        }
                    }
                } catch (Exception $e) {
                    dd($e);
                }
            }
        } else {
            header("Location: ?c=home&m=access");
        }
    }

    public function destroySession()
    {
        // Destroy session for system inventory
        require_once "./inventario/helpers/SessionToken.php";
        require_once "./inventario/helpers/AccessData.php";
        SessionToken::deleteSession();
        // end
        $this->model->logout();
    }
}
