<?php

/**
 * CLASE ServiceController
 * CONTROLADOR ALGO ASI COMO UN JS
 */

require 'models/Artifact.php';

class ArtifactController
{

    private $model;

    public function __construct()
    {
        $this->model = new Artifact;
    }

    public function index()
    {

    }

    // realiza el proceso de guardar
    public function save()
    {
        $arrayArtifacts = isset($_POST['arts']) ? $_POST['arts'] : [];
        // dd($arrayArtifacts);
        $registerInsert = false;

        if (!empty($arrayArtifacts)) {

            $respArtifact = $this->model->saveArtifact($arrayArtifacts);
            $registerInsert = $this->model->getByWhere(
                "artefacto",
                ["id_artefacto_PK" => $this->model->getLastId()]
            );
        } else {
            $respArtifact = false;
        }

        $arrayResp = [];

        if ($respArtifact == true) {

            $arrayResp = [
                'success' => true,
                'error' => false,
                'message' => "Artefacto generado"
            ];

            if ($registerInsert) {
                $arrayResp["registerInsert"] = $registerInsert;
            }
        } else {
            $arrayResp = [
                'error' => true,
                'message' => "Error generando el artefacto"
            ];
        }
        echo json_encode($arrayResp);
        return;
    }
}
