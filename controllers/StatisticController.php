<?php

/**
 * Clase DashController para cargar el home del proyecto
 */


require 'models/Statistic.php';
require 'models/Login.php';

class StatisticController
{
    private $model;
    private $login;

    public function __construct()
    {
        $this->login = new Login;
        $this->model = new Statistic;

        // dd($_SESSION['user']);
        if (!isset($_SESSION['user']['user'])) //que sesion va a destruir si no existe
            header('Location: ?c=home&m=access');

        if ($_SESSION['user']['user']->id_rol_FK != 1) $this->login->logout();
    }

    public function index()
    {
        if ($_SESSION['user']['user']->id_rol_FK == 1) {
            // dd($this->model->getMonthlyGain());
            $monthlyGain = $this->model->getMonthlyGain();
            $monthlyGain = (array)$monthlyGain[0];

            $arrMonthlyGain = [];
            foreach ($monthlyGain as $posicion => $ganancia) {
                if (is_null($ganancia)) $ganancia = 0;
                array_push($arrMonthlyGain, $ganancia);
            }

            // dd($arrMonthlyGain);
            require 'views/layout.php';
            require 'views/statistic/list.php';
            require 'views/other/separator.php';
            require 'views/footer.php';
        } elseif ($_SESSION['user']['user']->id_rol_FK == 2) {
            require 'views/partial/employee/layoutEmployee.php';  // Esto hace que se muestre en todo lado
            require 'views/other/separator.php';
            require 'views/footer.php';
        } elseif ($_SESSION['user']['user']->id_rol_FK == 3) {
            $this->login->logout();
        }
    }
}
