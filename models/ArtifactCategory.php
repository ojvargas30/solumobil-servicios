<?php

/**
 * Modelo de la tabla Citations
 * CRUD.php
 */
class ArtifactCategory extends Database
{
	private $pdo;

	public function __construct()
	{
		try {
			parent::__construct();
			$this->pdo = new Database;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getTypes()
	{
		try {
			$strSql = "SELECT * FROM vw_tipos";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getLastId()
	{
		try {
			return $this->pdo->getLastId();
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function saveType($data)
	{
		try {
			foreach ($data as $dat) {
				$data = [
					'estado_ca'      => 1,
					'nombre_categoria_artefacto'  => $dat['nombre_categoria_artefacto']
				];

				$this->pdo->insert('categoria_artefacto', $data);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
}
