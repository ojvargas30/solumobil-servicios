<?php

/**
 * Modelo de la tabla Servicers
 * CRUD.php
 */
/**
 * Modelo de la tabla User
 * CRUD.php
 */
// // Import PHPMailer classes into the global namespace
// // These must be at the top of your script, not inside a function
// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\SMTP;
// use PHPMailer\PHPMailer\Exception;

// // Load Composer's autoloader
// require 'assets/plugins/PHPMailer/src/SMTP.php';
// require 'assets/plugins/PHPMailer/src/PHPMailer.php';

// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\SMTP;
// use PHPMailer\PHPMailer\Exception;

// // Load Composer's autoloader
// include_once 'assets/plugins/PHPMailer/src/SMTP.php';
// include_once 'assets/plugins/PHPMailer/src/PHPMailer.php';

class Technical extends Database
{

	private $id_tecnico_PK;
	private $tipo_doc_tec;
	private $id_usuario_FK;
	private $nombre_tecnico;
	private $apellido_tecnico;
	private $telefono;
	private $num_id_tec;
	private $foto_tecnico;
	private $desc_tecnico;
	private $direccion_residencia;
	private $pdo;


	public function __construct()
	{
		try {
			parent::__construct();
			$this->pdo = new Database;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getActiveTechnicians()
	{
		try {
			$strSql = "SELECT * FROM vw_utd_tecnico WHERE estado_usu = 1";
			// llamado al metodo general que ejecuta un select a la bd
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getTechnicians()
	{
		try {
			$strSql = "SELECT * FROM vw_utd_tecnico";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getLastId()
	{
		try {
			return $this->pdo->getLastId();
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getTechnicalById($id)
	{
		try {
			$strSql = "SELECT * FROM tecnico WHERE id_tecnico_PK=:id_tecnico_PK";
			$arrayData = ['id_tecnico_PK' => $id];
			$query = $this->pdo->select($strSql, $arrayData);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}


	public function editTechnical($data)
	{
		try {
			$strWhere = 'id_tecnico_PK=' . $data['id_tecnico_PK'];
			$this->pdo->update('tecnico', $data, $strWhere);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function editUserStatus($data)
	{
		try {
			$strWhere = 'id_usuario_PK=' . $data['id_usuario_PK'];
			$this->pdo->update('usuario', $data, $strWhere);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}
}
