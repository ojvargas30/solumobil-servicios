<?php

/**
 * Modelo de la tabla Services
 * CRUD.php
 */


class Service
{

	private $id_servicio_PK;
	private $id_cliente_FK;
	private $estado_servicio;
	private $fecha_peticion;
	private $hora_peticion;
	private $costo_servicio;
	private $valDate;
	private $pdo;


	public function __construct()
	{
		try {
			$this->valDate = new HandleDate;
			$this->pdo = new Database;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getAll()
	{
		try {
			$strSql = "SELECT * FROM sec_servicio ORDER BY id_servicio_PK ASC";
			// llamado al metodo general que ejecuta un select a la bd
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getAuditService()
	{
		try {
			$strSql = "SELECT * FROM vw_aud_service ORDER BY id DESC";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function SP_getAllClientService()
	{
		try {
			$idCliSession = $_SESSION['user']['client']->id_cliente_PK;
			$strSql = "call sp_client_service($idCliSession)";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function SP_ClientTechService()
	{
		try {
			$idCliSession = $_SESSION['user']['client']->id_cliente_PK;
			$strSql = "call sp_clienttechservice($idCliSession)";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function SP_ClientArtService()
	{
		try {
			$idCliSession = $_SESSION['user']['client']->id_cliente_PK;
			$strSql = "call sp_clientartservice($idCliSession)";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function SP_ClientCatService()
	{
		try {
			$idCliSession = $_SESSION['user']['client']->id_cliente_PK;
			$strSql = "call sp_clientdisservice($idCliSession)";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getActiveServices()
	{
		try {
			$strSql = "SELECT * FROM sec_servicio WHERE id_estado_servicio_FK BETWEEN 1 AND 2 ORDER BY id_servicio_PK DESC";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getInactiveServices()
	{
		try {
			$strSql = "SELECT * FROM sec_servicio WHERE id_estado_servicio_FK BETWEEN 3 AND 4 ORDER BY id_servicio_PK DESC";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getFinishedServices()
	{
		try {
			$strSql = "SELECT * FROM sec_servicio WHERE id_estado_servicio_FK = 3 ORDER BY id_servicio_PK DESC";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getCanceledServices()
	{
		try {
			$strSql = "SELECT * FROM sec_servicio WHERE id_estado_servicio_FK = 4 ORDER BY id_servicio_PK ASC";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getInProcessServices()
	{
		try {
			$strSql = "SELECT * FROM vw_ep";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getInWaitServices()
	{
		try {
			$strSql = "SELECT * FROM vw_ee";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}


	public function getCountFinishedServices()
	{
		try {
			$strSql = "SELECT * FROM vw_ef";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getCountCanceledServices()
	{
		try {
			$strSql = "SELECT * FROM vw_ec";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}
	// IDEAS
	// 1. obtener servicios inactivos y listarlos
	// 2. Obtener servicios activos y listarlos aparte
	// 3. obtener todos los servicios y listarlos aparte
	// 4. ordenar de manera descendente los registros

	public function getArtService($service_id)
	{
		try {
			$strSql = "SELECT ars.*,a.modelo as modelo FROM artefacto_servicio ars
			INNER JOIN artefacto a ON a.id_artefacto_PK = ars.id_artefacto_FK
			WHERE ars.id_servicio_FK = '{$service_id}' ";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getCitService($service_id)
	{
		try {
			$strSql = "SELECT cs.*,c.direccion_lugar FROM citacion_servicio cs
			INNER JOIN citacion c ON c.id_citacion_PK = cs.id_citacion_FK
			WHERE cs.id_servicio_FK = '{$service_id}'";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getCatService($service_id)
	{
		try {
			$strSql = "SELECT csss.*,cs.nombre_cs
			FROM categorias_servicios csss
			INNER JOIN categoria_servicio cs
			ON cs.id_categoria_servicio_PK = csss.id_categoria_servicio_FK
			WHERE csss.id_servicio_FK = '{$service_id}'";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getTechService($service_id)
	{
		try {
			$strSql = "SELECT ts.*,t.*
			FROM tecnico_servicio ts
			INNER JOIN tecnico t ON t.id_tecnico_PK = ts.id_tecnico_asignado_FK
			WHERE ts.id_servicio_FK = '{$service_id}'";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getDetService()
	{
		try {
			$strSql = "call sp_getdetservice()";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	//peticion request
	public function newService($data)
	{
		try {
			// $data['costo_servicio'] = $data['costo_servicio']+$data['costo_revision'];
			$this->pdo->insert('servicio', $data);
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}


	public function getServiceLastId()
	{
		try {
			$strSql = "SELECT MAX(id_servicio_PK) as id_servicio_PK FROM servicio";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function getServiceById($id_servicio_PK)
	{
		try {
			$strSql = "SELECT * FROM servicio WHERE id_servicio_PK=:id_servicio_PK";
			$arrayData = ['id_servicio_PK' => $id_servicio_PK];
			$query = $this->pdo->select($strSql, $arrayData);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getClientServiceById($id)
	{
		try {
			$idCliSession = $_SESSION['user']['client']->id_cliente_PK;
			$strSql = "call sp_client_servicebyid($id,$idCliSession);";
			// $arrayData = ['id_revision_servicio_PK' => $id];
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getArtServiceById($id_servicio_PK)
	{
		try {
			$strSql = "call sp_getartservicebyid(:id_servicio_PK)";
			$arrayData = ['id_servicio_PK' => $id_servicio_PK];
			$query = $this->pdo->select($strSql, $arrayData);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getCitServiceById($id_servicio_PK)
	{
		try {
			$strSql = "call sp_getcitservicebyid(:id_servicio_PK)";
			$arrayData = ['id_servicio_PK' => $id_servicio_PK];
			$query = $this->pdo->select($strSql, $arrayData);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getCatServiceById($id_servicio_PK)
	{
		try {
			$strSql = "call sp_getcatservicebyid(:id_servicio_PK)";
			$arrayData = ['id_servicio_PK' => $id_servicio_PK];
			$query = $this->pdo->select($strSql, $arrayData);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}


	public function getTechServiceById($id_servicio_PK)
	{
		try {
			$strSql = "call sp_gettechservicebyid(:id_servicio_PK)";
			$arrayData = ['id_servicio_PK' => $id_servicio_PK];
			$query = $this->pdo->select($strSql, $arrayData);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getDetServiceById($id_servicio_PK)
	{
		try {
			$strSql = "call sp_getdetservicebyid(:id_servicio_PK)";
			$arrayData = ['id_servicio_PK' => $id_servicio_PK];
			$query = $this->pdo->select($strSql, $arrayData);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	// public function getDetServiceClientById($id_servicio_PK, $idcli)
	// {
	// 	try {
	// 		$strSql = "call sp_getdetserviceClientById($id_servicio_PK, $idcli)";
	// 		$query = $this->pdo->select($strSql);
	// 		return $query;
	// 	} catch (PDOException $e) {
	// 		die($e->getMessage());
	// 	}
	// }

	public function getDetClientServiceById()
	{
		try {
			$idCliSession = $_SESSION['user']['client']->id_cliente_PK;
			$strSql = "call sp_getdetclientservicebyid($idCliSession)";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function editService($data)
	{
		try {
			if ($data['id_estado_servicio_FK'] == 1) {
				$strWhere = 'id_servicio_FK=' . $data['id_servicio_PK'];

				$data2 = [
					'fecha_inicio' => date("Y-m-d H:i:s")
				];

				$this->pdo->update('revision_servicio', $data2, $strWhere);
			} elseif ($data['id_estado_servicio_FK'] == 3) {
				$strWhere = 'id_servicio_FK=' . $data['id_servicio_PK'];

				$data3 = [
					'fecha_fin' => date("Y-m-d H:i:s")
				];

				$this->pdo->update('revision_servicio', $data3, $strWhere);
			}

			$strWhere2 = 'id_servicio_PK=' . $data['id_servicio_PK'];
			$this->pdo->update('servicio', $data, $strWhere2);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function editDetService($arrayDetails, $id_servicio_PK)
	{
		try {
			// dd($arrayDetails);
			$strWhere = 'id_revision_servicio_PK=' . $arrayDetails[0]['id_revision_servicio_PK'];

			foreach ($arrayDetails as $detail) {
				$costo_total = $detail['costo_total'] + $detail['costo_revision'] + $detail['costo_domicilio'];
				$saldo = $costo_total - $detail['abono'];
				// dd($costo_total);

				$data = [
					'id_revision_servicio_PK'   => $detail['id_revision_servicio_PK'],
					'id_servicio_FK' 			=> $id_servicio_PK,
					// 'is_software'  				=> $detail['is_software'],
					// 'is_hardware'  				=> $detail['is_hardware'],
					'descripcion_cliente'  		=> $detail['descripcion_cliente'],
					'diagnostico'  				=> $detail['diagnostico'],
					'costo_revision'  			=> $detail['costo_revision'],
					'costo_domicilio'         	=> $detail['costo_domicilio'],
					'abono'         			=> $detail['abono'],
					'saldo'         			=> $saldo,
					'costo_total'         		=> $costo_total,
					// 'fecha_encuentro'  			=> $detail['fecha_encuentro'],
					// 'hora_encuentro'  			=> $detail['hora_encuentro'],
					// 'direccion_lugar'  			=> $detail['direccion_lugar']
				];

				$this->pdo->update('revision_servicio', $data, $strWhere);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function editDetServiceEmployee($arrayDetails, $id_servicio_PK)
	{
		try {
			$strWhere = 'id_revision_servicio_PK=' . $arrayDetails[0]['id_revision_servicio_PK'];

			foreach ($arrayDetails as $detail) {
				$data = [
					'id_revision_servicio_PK'   => $detail['id_revision_servicio_PK'],
					'id_servicio_FK' 			=> $id_servicio_PK,
					'is_software'  				=> $detail['is_software'],
					'is_hardware'  				=> $detail['is_hardware'],
					'diagnostico'  				=> $detail['diagnostico'],
					'costo_revision'  			=> $detail['costo_revision'],
					'costo_domicilio'         	=> $detail['costo_domicilio'],
					'costo_total'         		=> $detail['costo_total'] + $detail['costo_revision'] + $detail['costo_domicilio'],
					'fecha_encuentro'  			=> $detail['fecha_encuentro'],
					'hora_encuentro'  			=> $detail['hora_encuentro'],
					'direccion_lugar'  			=> $detail['direccion_lugar']
				];

				$this->pdo->update('revision_servicio', $data, $strWhere);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function editCostsService($idRevision, $arrCostsToEdit)
	{
		try {

			if (
				valNumWithLimit($arrCostsToEdit->costo_revision, 30) &&
				valNumWithLimit($arrCostsToEdit->costo_domicilio, 30) &&
				valNumWithLimit($arrCostsToEdit->abono, 30) &&
				valNumWithLimit($arrCostsToEdit->costo_total, 30)
			) {
				$costo_total = $arrCostsToEdit->costo_total + $arrCostsToEdit->costo_revision + $arrCostsToEdit->costo_domicilio;
				$saldo = $costo_total - $arrCostsToEdit->abono;

				$strWhere = 'id_revision_servicio_PK=' . $idRevision;
				$data = [
					'costo_revision'  			=> $arrCostsToEdit->costo_revision,
					'costo_domicilio'         	=> $arrCostsToEdit->costo_domicilio,
					'abono'         			=> $arrCostsToEdit->abono,
					'saldo'         			=> $saldo,
					'costo_total'         		=> $costo_total
				];


				if ($this->pdo->update('revision_servicio', $data, $strWhere)) return true;

				return false;
			} else return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function editDetServiceClient($arrayDetails, $id_servicio_PK)
	{
		try {
			if (
				valAlphanumericAndSymbol($arrayDetails[0]['descripcion_cliente']) &&
				valAlphanumericAndSymbol($arrayDetails[0]['direccion_lugar'])
			) {
				$strWhere = 'id_revision_servicio_PK=' . $arrayDetails[0]['id_revision_servicio_PK'];

				$data = [
					// 'id_servicio_FK' 			=> isset($id_servicio_PK) ? $id_servicio_PK : '',
					'descripcion_cliente'  		=> isset($arrayDetails[0]['descripcion_cliente']) ? $arrayDetails[0]['descripcion_cliente'] : '',
					'direccion_lugar'  			=> isset($arrayDetails[0]['direccion_lugar']) ? $arrayDetails[0]['direccion_lugar'] : ''
				];

				if ($this->pdo->update('revision_servicio', $data, $strWhere)) return true;
				return false;
			}
			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function editServiceStatus($data)
	{
		try {
			$strWhere = 'id_servicio_PK=' . $data['id_servicio_PK'];
			$this->pdo->update('servicio', $data, $strWhere);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function editDetailDate($data2, $id_estado_servicio_FK) // en construcción
	{
		try {
			$strWhere = 'id_servicio_FK=' . $data2['id_servicio_PK'];

			if ($id_estado_servicio_FK == 4) {
				$data = [
					'fecha_cancelado' => date("Y-m-d H:i:s")
				];
			} else {
				$data = [
					'fecha_fin' => date("Y-m-d H:i:s")
				];
			}

			$this->pdo->update('revision_servicio', $data, $strWhere);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function editDetailDateStartService($data3) // en construcción
	{
		try {
			$strWhere = 'id_servicio_FK=' . $data3['id_servicio_PK'];

			$data = [
				'fecha_inicio' => date("Y-m-d H:i:s")
			];

			$this->pdo->update('revision_servicio', $data, $strWhere);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function saveArtService($arrayArtifacts, $lastIdService)
	{
		try {
			foreach ($arrayArtifacts as $artifact) {
				$data = [
					'id_artefacto_FK' 			=> $artifact['id_artefacto_PK'],
					'id_servicio_FK' 			=> $lastIdService,
				];
				$this->pdo->insert('artefacto_servicio', $data);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveEvidenceService($arrEvidence, $lastIdService)
	{
		try {

			foreach ($arrEvidence as $urlImg) {
				$data = [
					'url_foto_evidencia' 		=> $urlImg,
					'id_servicio_FK' 			=> $lastIdService,
				];
				$this->pdo->insert('evidencia_servicio', $data);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveCitService($arrayCitations, $lastIdService)
	{
		try {
			foreach ($arrayCitations as $citation) {
				$data = [
					'id_servicio_FK' 			=> $lastIdService,
					'id_citacion_FK'  			=> $citation['id_citacion_PK'],
				];
				$this->pdo->insert('citacion_servicio', $data);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveCatService($arrayScategories, $lastIdService)
	{
		try {
			foreach ($arrayScategories as $scategory) {
				$data = [
					'id_servicio_FK' 			=> $lastIdService,
					'id_categoria_servicio_FK'  => $scategory['id_categoria_servicio_PK'],
				];
				$this->pdo->insert('categorias_servicios', $data);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}


	public function saveTechService($arrayTechnicians, $lastIdService)
	{
		try {
			foreach ($arrayTechnicians as $technical) {
				$data = [
					'id_servicio_FK' 			=> $lastIdService,
					'id_tecnico_asignado_FK'    => $technical['id_tecnico_PK']
				];

				$this->pdo->insert('tecnico_servicio', $data);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveTechServiceEmployee($lastIdService)
	{
		try {
			$data = [
				'id_servicio_FK' 			=> $lastIdService,
				'id_tecnico_asignado_FK'    => $_SESSION['user']['technical']->id_tecnico_PK,
			];
			$this->pdo->insert('tecnico_servicio', $data);
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveTechServiceClient($lastIdService)
	{
		try {
			$data = [
				'id_servicio_FK' 			=> $lastIdService,
				'id_tecnico_asignado_FK'    => null,
			];
			$this->pdo->insert('tecnico_servicio', $data);
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveDetService($arrayDetails, $lastIdService)
	{
		try {
			foreach ($arrayDetails as $detail) {

				$costo_total = $detail['costo_total'] + $detail['costo_revision'] + $detail['costo_domicilio'];
				$saldo = $costo_total - $detail['abono'];
				// dd($detail, $detail['fecha_hora_cita_virtual']);
				$data = [
					'id_servicio_FK' 			=> $lastIdService,
					// 'is_software'  				=> $detail['is_software'],
					// 'is_hardware'  				=> $detail['is_hardware'],
					'descripcion_cliente'  		=> $detail['descripcion_cliente'],
					'diagnostico'  				=> $detail['diagnostico'],
					// 'id_tecnico_FK'         	=> $detail['id_tecnico_FK'],
					'costo_revision'  			=> $detail['costo_revision'],
					'costo_domicilio'         	=> $detail['costo_domicilio'],
					'abono'         			=> $detail['abono'],
					'imei'         				=> ($detail['imei']) ? $detail['imei'] : '',
					'saldo'         			=> $saldo,
					'costo_total'         		=> $costo_total
					// 'fecha_encuentro'  			=> (isset($detail['fecha_encuentro'])) ? $detail['fecha_encuentro'] : null,
					// 'hora_encuentro'  			=> (isset($detail['hora_encuentro'])) ? $detail['hora_encuentro'] : null,
					// 'direccion_lugar'  			=> (isset($detail['direccion_lugar'])) ? $detail['direccion_lugar'] : null,
					// 'fecha_hora_cita_virtual'   => (isset($detail['fecha_hora_cita_virtual'])) ? $detail['fecha_hora_cita_virtual'] : null,
					// 'link_cita_virtual'  		=> (isset($detail['link_cita_virtual'])) ? $detail['link_cita_virtual'] : null,
				];

				$this->pdo->insert('revision_servicio', $data);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveDetServiceClient($arrayDetails, $lastIdService)
	{
		try {
			// dd(valAlphanumericAndSymbol($arrayDetails[0]['descripcion_cliente']),valAlphanumericAndSymbol($arrayDetails[0]['direccion_lugar']));
			if (
				valAlphanumericAndSymbol($arrayDetails[0]['descripcion_cliente']) &&
				valAlphanumericAndSymbol($arrayDetails[0]['direccion_lugar'])
			) {
				$data = [
					'id_servicio_FK' 			=> $lastIdService,
					'descripcion_cliente'  		=> $arrayDetails[0]['descripcion_cliente'],
					'direccion_lugar'  			=> $arrayDetails[0]['direccion_lugar'],
					'diagnostico'  				=> 'No se ha diagnósticado',
					'costo_revision'            => 10000,
					'costo_total'               => 0,
					// 'fecha_encuentro'           => date("00-00-0000"),
					// 'hora_encuentro'            => date("00:00:00"),
					'is_software'               => false,
					'is_hardware'               => false
				];

				if ($this->pdo->insert('revision_servicio', $data)) return true;
				return false;
			}
			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function deleteArtService($id_servicio_PK)
	{
		try {
			$strWhere = 'id_servicio_FK=' . $id_servicio_PK;
			$this->pdo->delete('artefacto_servicio', $strWhere);
			// var_dump($strWhere);
			// exit();
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function deleteCitService($id_servicio_PK)
	{
		try {
			$strWhere = 'id_servicio_FK=' . $id_servicio_PK;
			$this->pdo->delete('Citacion_servicio', $strWhere);
			// var_dump($strWhere);
			// exit();
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function deleteCatService($id_servicio_PK)
	{
		try {
			$strWhere = 'id_servicio_FK=' . $id_servicio_PK;
			$this->pdo->delete('categorias_servicios', $strWhere);
			// var_dump($strWhere);
			// exit();
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function deleteTechService($id_servicio_PK)
	{
		try {
			$strWhere = 'id_servicio_FK=' . $id_servicio_PK;
			$this->pdo->delete('tecnico_servicio', $strWhere);
			// var_dump($strWhere);
			// exit();
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function getStatusUserBySessionId($id_usuario_PK)
	{
		try {
			$strSql = "SELECT estado_usu FROM usuario WHERE id_usuario_PK=$id_usuario_PK";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function logout()
	{
		if (isset($_SESSION['user']))
			session_destroy();
		// setcookie(session_name(), session_id(), time() - (60 * 60 * 60 * 60));
		header('Location: ?c=home&m=access');
	}

	public function valTechnicianCrossover($arrTecnicosFecha)
	{
		try {
			// dd();

			$formDatetime = $arrTecnicosFecha[1]->fecha;
			$dateTime = explode("T", $formDatetime);
			// dd($dateTime[0]);
			$sepFormDate = $this->valDate->sepDateTime($dateTime[0]);
			// dd(explode("T",$formDatetime));
			// dd($sepFormDate);
			$formDate = $sepFormDate[0];
			$formHour = $sepFormDate[1];

			// dd($formDate);

			$tecnicosBD = [];

			// Traer todas las citas de los técnicos asignados
			//  en un array con array separados para cada técnico
			foreach ($arrTecnicosFecha[0] as $tecnicoCita) {
				$arrTecnicoCitaBD = $this->pdo->select("call sp_technician_crossover($tecnicoCita->id_tecnico_PK)");
				array_push($tecnicosBD, $arrTecnicoCitaBD);
			}

			// dd($tecnicosBD);
			if (count($arrTecnicosFecha[0]) != 1) {
				$arrTecnicosSep = array_chunk($tecnicosBD, 1);
			} else {
				$arrTecnicosSep = $tecnicosBD;
			}

			// dd($arrTecnicosSep);
			// dd(empty($arrTecnicosSep[0][0]));
			// dd(empty($arrTecnicosSep[0][0]));

			for ($i = 0; $i < count($arrTecnicosSep); $i++) {

				for ($j = 0; $j < count($arrTecnicosSep[$i]); $j++) {

					if (!empty($tecnicosBD[$i][$j])) {

						/** Si hay fecha virtual validamos que no este vacia*/
						if ($tecnicosBD[$i][$j]->fecha_hora_cita_virtual != null) {

							$sep = $this->valDate->sepDateTime($tecnicosBD[$i][$j]->fecha_hora_cita_virtual);

							if ($sep != false) {
								$virtualDate = $sep[0];
								$virtualHour = $sep[1];

								if ($formDate == $virtualDate) {
									$valVirtualHour = $this->valDate->dateBetweenRange($formHour, $virtualHour);

									if ($valVirtualHour === true) return false;
								}
							}
						}

						if ($tecnicosBD[$i][$j]->fecha_encuentro != null && $tecnicosBD[$i][$j]->hora_encuentro != null) {

							$presentialDate = $tecnicosBD[$i][$j]->fecha_encuentro;
							$presentialHour = $tecnicosBD[$i][$j]->hora_encuentro;

							// dd($formDate == $presentialDate);
							if ($formDate == $presentialDate) {

								$valPresentialHour = $this->valDate->dateBetweenRange($formHour, $presentialHour);

								if ($valPresentialHour === true) return false;
							}
						}
					}
				}
			}

			return true;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function valDateCitation($formDate)
	{
		try {
			$futureDate = $this->valDate->futureDate($formDate);
			if ($futureDate) return true;
			return false;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function valServiceTransfer($id_service, $total)
	{
		try {
			$strSql = "call get_service_by_id($id_service)";
			$servicio = $this->pdo->select($strSql);

			if ($servicio) {
				// dd($id_service);
				// dd($total);
				// dd($servicio[0]->costo_total);
				// dd("ok");
				// dd($servicio[0]->costo_total.'.00', $total);
				if ($servicio[0]->costo_total . '.00' == $total || $servicio[0]->costo_total . ',00' == $total) {
					// dd("ok");

					$data = [
						'costo_total' => $total - $total,
						'pagado' => 2,
					];

					$strWhere = 'id_servicio_FK=' . $id_service;
					$cleanAccount = $this->pdo->update('revision_servicio', $data, $strWhere);

					if ($cleanAccount) return true;
				}
			}
			return false;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getEvidenceService()
	{
		$strSql = "call sp_service_evidence()";
		$query = $this->pdo->select($strSql);
		return $query;
	}
}
