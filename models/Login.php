<?php

/**
 * Modelo de la tabla Services
 * CRUD.php
 */

require 'Client.php';
require 'Technical.php';

class Login
{
    private $client;
    private $technical;
    private $pdo;

    public function __construct()
    {
        try {
            $this->client = new Client;
            $this->technical = new Technical;
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function validateUser($data)
    {
        try {
            // dd($data);
            $data['clave'] = hash('sha256', $data['clave']);  // encriptar contraseña paso 2/2

            $strSql = "SELECT u.*, r.nombre_rol as rol FROM usuario u
                        INNER JOIN rol r ON r.id_rol_PK = u.id_rol_FK
                        WHERE u.correo = '{$data['correo']}'
                            AND u.clave = '{$data['clave']}'"; // un método para inner joincomo sltOne

            $query = $this->pdo->select($strSql);

            $query = empty($query) ? false : $query[0];

            if ($query) {
                if ($query->estado_usu == 1) {

                    if (in_array($query->id_rol_FK, [1, 2])) {

                        $tech = $this->pdo->selectOne("tecnico", ["id_usuario_FK" => $query->id_usuario_PK]);

                        $_SESSION['user'] = [
                            'user'      => $query,
                            'technical' => $tech
                        ];
                    } else if (in_array($query->id_rol_FK, [3])) {

                        $client = $this->pdo->selectOne("cliente", ["id_usuario_FK" => $query->id_usuario_PK]);
                        // dd($client,$query);

                        $_SESSION['user'] = [
                            'user'      => $query,
                            'client' => $client
                        ];
                    }
                    return true;
                } else {
                    return "Error al iniciar sesión. Su usuario esta inactivo";
                }
            } else {
                return 'Error al Iniciar Sesión. Verifique sus Credenciales';
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function saveRecordUser()
    {
        $name = '';

        if (isset($_SESSION['user']['technical']->nombre_tecnico))
            $name = $_SESSION['user']['technical']->nombre_tecnico . ' ' .
                $_SESSION['user']['technical']->apellido_tecnico;

        if (isset($_SESSION['user']['client']->nombre_cliente))
            $name = $_SESSION['user']['client']->nombre_cliente . ' ' .
                $_SESSION['user']['client']->apellido_cliente;

        $rol = '';

        if (isset($_SESSION['user']['user']->id_rol_FK)) $rol = $_SESSION['user']['user']->id_rol_FK;

        $identify = '';

        if (isset($_SESSION['user']['technical']->num_id_tec))
            $identify = $_SESSION['user']['technical']->num_id_tec;

        if (isset($_SESSION['user']['client']->num_id_cli))
            $identify = $_SESSION['user']['client']->num_id_cli;

        $data = [
            'username'      => $name,
            'userrole'      => $rol,
            'useridentify'  => $identify,
            'accessdate'    => date("Y-m-d H:i:s"),
            'countaccess'   => 1,
            'device'        => $_SERVER['HTTP_USER_AGENT']
        ];

        if (!empty($data)) {
            try {
                $this->pdo->insert('records', $data);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
    }

    public function getTriesUserByEmail($email)
    {
        try {
            // $query = $this->pdo->select('call sp_users(' . $email . ')');
            $query = $this->pdo->select(
                "SELECT intentos FROM usuario u
                    WHERE u.correo = '{$email}'"
            );
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getStatusUserByEmail($email)
    {
        try {
            $query = $this->pdo->select(
                "SELECT estado_usu FROM usuario u
                    WHERE u.correo = '{$email}'"
            );
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function setStatusUserToFalseByEmail($email)
    {
        try {
            $data = ['estado_usu' => 2];
            $this->pdo->update('usuario', $data, "correo='$email'");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function updateTriesUserByEmail($email)
    {
        try {
            $data = ['intentos' => $_SESSION['intentos']];
            $this->pdo->update('usuario', $data, "correo='$email'");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function cleanTriesUserByEmail($email)
    {
        try {
            $data = ['intentos' => 0];
            $this->pdo->update('usuario', $data, "correo='$email'");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function logout()
    {
        if (isset($_SESSION['user'])) session_destroy();
        // setcookie(session_name(), session_id(), time() - (60 * 60 * 60 * 60));
        header('Location: ?c=home&m=access');
    }
}
