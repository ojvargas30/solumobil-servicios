<?php
// Load Composer's autoloader
class User extends Database
{

	private $id_usuario_PK;
	private $id_rol_FK;
	private $estado_usu;
	private $correo;
	private $clave;
	private $tempEmailActiveAccount;
	private $pdo;

	public function __construct()
	{

		try {
			parent::__construct();
			$this->pdo = new Database;

			$this->tempEmailActiveAccount = function ($userName, $link, $verb) {
				$html = '<!DOCTYPE html>
				<html lang="en">

				<head>
					<meta charset="utf-8">
				</head>

				<body style="background-color: #FFF; font-family: Verdana; font-size:14;">
					<div>
						<h2>Bienvenido usuario: ' . $userName . '</h2>
						<p style="font-size:17px;">
							Agradecemos su registro en la aplicaci&oacute;n ' . TITLE_APP . '.
						</p>
						<p>Por favor, ' . $verb . ' su cuenta para conectarse al sitio web</p>
						<p style="padding:15px;background-color:#f2f2f2;">
							Para activar su cuenta de <a class="" style="font-weight-bold;
							color:#9e0000" href="' . $link . '"
								target="_blank">Click aqui &raquo;</a>
						</p>
						<p style="font-size:9px;">&copy;' . date('Y', time()) . ' ' . TITLE_APP . '.
							Todos los derechos registrados.</p>
					</div>
				</body>
				</html>';

				return $html;
			};
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getActiveUsers()
	{
		try {
			$strSql = "call sp_get_users()";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getUserById($id_usuario_PK)
	{
		try {
			$strSql = "SELECT * FROM vw_r_usuario WHERE id_usuario_PK=:id_usuario_PK";
			$arrayData = ['id_usuario_PK' => $id_usuario_PK];
			$query = $this->pdo->select($strSql, $arrayData);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getLastId()
	{
		try {
			return $this->pdo->getLastId();
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function saveUser($data)
	{
		try {
			// dd($data);
			$keyreg = md5(time()); // encriptar cada tiempo
			$user = [
				'estado_usu' => 2,
				'id_rol_FK'  	=> $data['id_rol_FK'], // jmmmm
				'correo'  		=> $data['correo'],
				'clave'  		=> hash('sha256', $data['clave']),  // encriptación paso 1/2
				// 'clave'  => $data['clave'],
				'keyreg'		=> $keyreg
			];

			if ($this->pdo->insert('usuario', $user)) {
				return [
					"id_usuario_PK" => $this->pdo->getLastId(),
					"correo" => $user['correo'],
					"keyreg" => $keyreg,
				];
			}
			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveTechnical($data)
	{
		try {

			// dd($data);
			$technical = [
				'tipo_doc_tec'      	 => $data['tipo_doc_tec'],
				'id_usuario_FK' 		 => $data['id_usuario_FK'],
				'nombre_tecnico'  		 => $data['nombre_tecnico'],
				'apellido_tecnico'       => $data['apellido_tecnico'],
				'telefono'  			 => $data['telefono'],
				'num_id_tec'  		     => $data['num_id_tec'],
				// 'foto_tecnico'  	     => $data['foto_tecnico'],
				'desc_tecnico' 			 => $data['desc_tecnico'],
				'direccion_residencia'   => $data['direccion_residencia'],
			];

			$id     = $data['id_usuario_FK'];
			$correo = $data["correo"];
			$keyreg = $data["keyreg"];
			$user = $technical['nombre_tecnico'] . ' ' . $technical['apellido_tecnico'];
			$link = MAIN_URL . '?c=user&m=verifyUserByEmail&key=' . $keyreg . '&id=' . $id;

			if (sendEmail(
				$correo,
				$user,
				$link,
				' Para activar su cuenta, por favor haga click en el enlace: ',
				$this->tempEmailActiveAccount,
				'active'
			)) {

				if ($this->pdo->insert('tecnico', $technical)) return true;
				else $this->pdo->erase('usuario', 'id_usuario_PK', $id);
			} else $this->pdo->erase('usuario', 'id_usuario_PK', $id);

			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveClient($data)
	{
		try {
			$client = [
				'id_barrio_FK'   		=> ($data['id_barrio_FK']) ? $data['id_barrio_FK'] : '1',
				'id_usuario_FK'  		=> $data["id_usuario_FK"],
				'tipo_doc_cli'  		=> $data['tipo_doc_cli'],
				'nombre_cliente'  		=> $data['nombre_cliente'],
				'apellido_cliente'  	=> $data['apellido_cliente'],
				'num_id_cli'  			=> $data['num_id_cli'],
				'telefono'  			=> $data['telefono'],
				'direccion_residencia'  => $data['direccion_residencia']
			];

			$id     = $data['id_usuario_FK'];
			$correo = $data["correo"];
			$keyreg = $data["keyreg"];
			$user = $client['nombre_cliente'] . ' ' . $client['apellido_cliente'];
			$link = MAIN_URL . '?c=user&m=verifyUserByEmail&key=' . $keyreg . '&id=' . $id;

			if (sendEmail(
				$correo,
				$user,
				$link,
				' Para activar su cuenta, por favor haga click en el enlace: ',
				$this->tempEmailActiveAccount,
				'active'
			)) {

				if ($this->pdo->insert('cliente', $client)) return true;
				else $this->pdo->erase('usuario', 'id_usuario_PK', $id);
			} else $this->pdo->erase('usuario', 'id_usuario_PK', $id);

			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveClientHome($data)
	{
		try {
			// dd($data);
			$client = [
				'id_barrio_FK'   		=> (isset($data['id_barrio_FK'])) ? $data['id_barrio_FK'] : null,
				'id_usuario_FK'  		=> $data["id_usuario_FK"],
				'tipo_doc_cli'  		=> (isset($data['tipo_doc_cli'])) ? $data['tipo_doc_cli'] : null,
				'nombre_cliente'  		=> (isset($data['nombre_cliente'])) ? $data['nombre_cliente'] : null,
				'apellido_cliente'  	=> (isset($data['apellido_cliente'])) ? $data['apellido_cliente'] : null,
				'num_id_cli'  			=> (isset($data['num_id_cli'])) ? $data['num_id_cli'] : null,
				'telefono'  			=> (isset($data['telefono'])) ? $data['telefono'] : null,
				'direccion_residencia'  => (isset($data['direccion_residencia'])) ? $data['direccion_residencia'] : null
			];

			$id     = $data['id_usuario_FK'];
			$correo = $data["correo"];
			$keyreg = $data["keyreg"];
			$user 	= (isset($data['nombre_cliente'])) ? $data['nombre_cliente'] : ' ';
			$link = MAIN_URL . '?c=user&m=verifyUserByEmail&key=' . $keyreg . '&id=' . $id;

			if (sendEmail(
				$correo,
				$user,
				$link,
				' Para activar su cuenta, por favor haga click en el enlace: ',
				$this->tempEmailActiveAccount,
				'active'
			)) {

				if ($this->pdo->insert('cliente', $client)) return true;
				else $this->pdo->erase('usuario', 'id_usuario_PK', $id);
			} else $this->pdo->erase('usuario', 'id_usuario_PK', $id);

			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveUserWithGoogle($data)
	{
		try {
			// VALIDAR DUPLICIDADES POSIBLES
			$emailsUsers = $this->pdo->select("SELECT correo FROM usuario");

			if ($emailsUsers) {
				foreach ($emailsUsers as $usersBD) {
					if ($usersBD->correo == $data->email) {
						return "23000";
					}
				}
			}

			$user = [
				'estado_usu'	=> 1,
				'id_rol_FK'  	=> 3,
				'correo'  		=> $data->email,
				'foto'			=> $data->picture,
				'clave'  		=> hash('sha256', $data->id),  // encriptación paso 1/2
				// 'clave'  => $data['clave'],
			];

			if ($this->pdo->insert('usuario', $user)) {
				return [
					"id_usuario_PK" => $this->pdo->getLastId()
				];
			}
			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function saveClientWithGoogle($data)
	{
		try {

			// dd($d}ata);
			$client = [
				'id_barrio_FK'   		=> (isset($data['id_barrio_FK'])) ? $data['id_barrio_FK'] : null,
				'id_usuario_FK'  		=> $data->id_usuario_FK,
				'tipo_doc_cli'  		=> (isset($data['tipo_doc_cli'])) ? $data['tipo_doc_cli'] : null,
				'nombre_cliente'  		=> (isset($data->name) && $data->name != '') ? $data->name : null,
				'apellido_cliente'  	=> (isset($data['apellido_cliente'])) ? $data['apellido_cliente'] : null,
				'num_id_cli'  			=> (isset($data['num_id_cli'])) ? $data['num_id_cli'] : null,
				'telefono'  			=> (isset($data['telefono'])) ? $data['telefono'] : null,
				'direccion_residencia'  => (isset($data['direccion_residencia'])) ? $data['direccion_residencia'] : null
			];

			if ($this->pdo->insert('cliente', $client)) return true;

			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function recoveryAccount($correo)
	{
		// Quitar try catch puede arreglarlo?
		try {

			// dd($correo);
			$usuario = $this->pdo->selectOne("usuario", ["correo" => $correo]);

			// dd($usuario);

			// if ($usuario) {
			if ($usuario !== false) {

				// dd('ok');

				$keyreg = md5(time()); // encriptar cada tiempo

				$id     = $usuario->id_usuario_PK;

				$strWhere = "id_usuario_PK='{$id}'";

				$upass = $this->pdo->update('usuario', ['keyreg' => $keyreg], $strWhere);

				if ($upass) {

					$usuario = (array)$usuario; // Parseo

					if ($usuario['id_rol_FK'] == 1 || $usuario['id_rol_FK'] == 2) {

						$tecnico = $this->pdo->selectOne("tecnico", ["id_usuario_FK" => $usuario["id_usuario_PK"]]);
						$user = isset($dataInsert['nombre_tecnico']) ? $dataInsert['nombre_tecnico'] . ' ' . $dataInsert['apellido_tecnico'] : '';
					} elseif ($usuario['id_rol_FK'] == 3) {

						$cliente = $this->pdo->selectOne("cliente", ["id_usuario_FK" => $usuario["id_usuario_PK"]]);
						$user = isset($dataInsert['nombre_cliente']) ? $dataInsert['nombre_cliente'] . ' ' . $dataInsert['apellido_cliente'] : '';
					}


					$correo = $usuario["correo"];
					$link = MAIN_URL . '?c=user&m=verifyUserByEmail&key=' .
						$keyreg . '&id=' .
						$usuario['id_usuario_PK'];

					// Función que envia los correos
					if (sendEmail(
						$correo,
						$user,
						$link,
						' Para recuperar su cuenta, por favor haga click en el enlace: ',
						$this->tempEmailActiveAccount,
						'recupere'
					)) return true;

					return false;
				}
			} else {
				return false;
			}
		} catch (PDOException $e) {
			dd($e->getMessage());
		}
	}

	public function editPassword($data)
	{
		try {
			$data2 = [
				'clave' => hash('sha256', $data['pass'])
			];
			$strWhere = 'id_usuario_PK=' . $data['idUser'];
			$this->pdo->update('usuario', $data2, $strWhere);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function editUserStatus($data)
	{
		try {
			// dd("modelo");
			// dd($data);
			$data2 = [
				'estado_usu' => $data['estado_usu']
			];
			$strWhere = 'id_usuario_PK=' . $data['id_usuario_PK'];
			$this->pdo->update('usuario', $data2, $strWhere);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getRecords()
	{
		try {
			$strSql = "call sp_records()";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getStatusUserBySessionId($id_usuario_PK)
	{
		try {
			// dd($id_usuario_PK);
			$strSql = "SELECT estado_usu FROM usuario WHERE id_usuario_PK=$id_usuario_PK";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function logout()
	{
		if (isset($_SESSION['user']))
			session_destroy();
		// setcookie(session_name(), session_id(), time() - (60 * 60 * 60 * 60));
		header('Location: ?controller=home&method=access');
	}
}
