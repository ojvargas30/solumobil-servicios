<?php

/**
 * Modelo de la tabla Citations
 * CRUD.php
 */
class ArtifactBrand extends Database
{
	private $pdo;

	public function __construct()
	{
		try {
			parent::__construct();
			$this->pdo = new Database;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getBrands()
	{
		try {
			$strSql = "SELECT * FROM vw_marcas";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getLastId()
	{
		try {
			return $this->pdo->getLastId();
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function saveBrand($data)
	{
		try {
			if (valAlphanumeric($data[0]['nombre_marca'])) {
				$brand = [
					'estado_ma'      => 1,
					'nombre_marca'  => $data[0]['nombre_marca']
				];

				if ($this->pdo->insert('marca_artefacto', $brand)) return true;
				return false;
			}
			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
}
