<?php

/**
 * Modelo de la tabla Services
 * CRUD.php
 */


class Statistic
{
    private $pdo;

    public function __construct()
    {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getMonthlyGain()
    {
        try {
            $strSql = "call sp_monthly_gain()";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
