<?php

/**
 * Modelo de la tabla Citations
 * CRUD.php
 */
class Artifact extends Database
{

	private $id_artefacto_PK;
	private $id_marca_artefacto_FK;
	private $id_categoria_artefacto_FK;
	private $estado_artefacto;
	private $modelo;
	private $caracteristicas;
	private $pdo;


	public function __construct()
	{
		try {
			parent::__construct();

			$this->pdo = new Database;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getActiveArtifacts()
	{
		try {
			$strSql = "SELECT * FROM vw_mce_artefacto WHERE estado_artefacto = 1";
			// llamado al metodo general que ejecuta un select a la bd
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}


	public function getLastId()
	{
		try {
			return $this->pdo->getLastId();
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	// && valImei($data[0]['imei'])
	public function saveArtifact($data)
	{
		try {
			if (
				valNum($data[0]['id_marca_artefacto_FK']) &&
				valNum($data[0]['id_categoria_artefacto_FK']) &&
				valAlphanumericAndSymbol($data[0]['modelo'])
			) {
				$data = [
					'estado_artefacto'      => 1,
					'id_marca_artefacto_FK'  => $data[0]['id_marca_artefacto_FK'],
					'id_categoria_artefacto_FK'  => $data[0]['id_categoria_artefacto_FK'],
					'modelo'  => $data[0]['modelo']
					// 'caracteristicas'  => $dat['caracteristicas'],
					// 'serie'  => $dat['serie']
				];

				if ($this->pdo->insert('artefacto', $data)) return true;
				return false;
			}
			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
}
