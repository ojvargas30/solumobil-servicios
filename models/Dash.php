<?php

/**
 * Modelo de la tabla Services
 * CRUD.php
 */


class Dash
{
	private $pdo;

	public function __construct()
	{
		try {
			$this->pdo = new Database;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getStatusUserBySessionId($id_usuario_PK)
	{
		try {
			$strSql = "SELECT estado_usu FROM usuario WHERE id_usuario_PK=$id_usuario_PK";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getConflictApparatus()
	{
		try {
			$strSql = "call sp_conflict_apparatus();";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getBestEmployee()
	{
		try {
			$strSql = "call sp_best_employee();";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getWeeklyGain()
	{
		try {
			if (CURRENT_HOST == "localhost") {
				$strSql = "call sp_weekly_gain();";
			} else {
				$strSql = "call sp_weekly_gain_plesk();";
			}

			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getDailyGain()
	{
		try {
			if (CURRENT_HOST == "localhost") {
				$strSql = "call sp_daily_gain();";
			} else {
				$strSql = "call sp_daily_gain_plesk();";
			}

			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function completeProfile()
	{
		try {
			$user = isset($_SESSION['user']['user']) ? $_SESSION['user']['user'] : '';

			// if(isset($user)){
			// 	if($user->id_rol_FK == 1 || $user->id_rol_FK == 2){
			// 		$strSql = "call sp_get_tecnico_by_id($user->id_usuario_PK)";
			// 		$query = $this->pdo->select($strSql);
			// 	}
			// }

			if ($user) {
				if ($user->id_rol_FK == 3) {
					$strSql = "call sp_get_client_by_id($user->id_usuario_PK)";
					$query = $this->pdo->select($strSql);
				}
			}

			return isset($query) ? $query : '404';
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}
}
