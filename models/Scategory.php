<?php

class Scategory
{
	private $pdo;

	public function __construct()
	{
		try {
			$this->pdo = new Database;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getActiveScategories()
	{
		try {
			$strSql = "SELECT * FROM vw_cat_ser WHERE estado_cs = 1";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function saveScategorySubForm($scategoryName)
	{
		if (trim($scategoryName[0]) != '') {
			$data = [
				'estado_cs'      => 1,
				'nombre_cs'  => $scategoryName[0]
			];
			if (
				$this->pdo->insert('categoria_servicio', $data)
			) {
				$strSql = "SELECT MAX(id_categoria_servicio_PK) as id_categoria_servicio_PK FROM categoria_servicio";
				$query = $this->pdo->select($strSql);

				// dd($query);

				$strSql2 = "SELECT nombre_cs FROM categoria_servicio WHERE id_categoria_servicio_PK =" . $query[0]->id_categoria_servicio_PK;
				$query2 = $this->pdo->select($strSql2);

				$query2[0]->id_categoria_servicio_PK = $query[0]->id_categoria_servicio_PK;

				// dd($query2);
				return $query2;
			} elseif (
				strpos($this->pdo->insert('categoria_servicio', $data), 'SQLSTATE[23000]')

			) {
				return 'SQLSTATE[23000]';
			} else return false;
		}

		return false;
	}
}
