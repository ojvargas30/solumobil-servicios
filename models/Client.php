<?php

/**
 * Modelo de la tabla Categorys
 * CRUD.php
 */
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\SMTP;
// use PHPMailer\PHPMailer\Exception;

// // Load Composer's autoloader
// include_once 'assets/plugins/PHPMailer/src/SMTP.php';
// include_once 'assets/plugins/PHPMailer/src/PHPMailer.php';

class Client extends Database
{

	private $id_cliente_PK;
	private $id_barrio_FK;
	private $id_usuario_FK;
	private $id_tipo_documento_FK;
	private $nombre_cliente;
	private $numero_documento;
	private $telefono;
	private $direccion_residencia;
	private $foto_cliente;
	private $pdo;


	public function __construct()
	{
		try {
			parent::__construct();
			$this->pdo = new Database;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getActiveClients()
	{
		try {
			$strSql = "SELECT * FROM vw_cliente WHERE id_rol_FK = 3 AND estado_usu = 1";
			// llamado al metodo general que ejecuta un select a la bd
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getLastId()
	{
		try {
			return $this->pdo->getLastId();
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function updateClient($client)
	{
		try {
			// dd($client[0]);
			$strWhere = 'id_cliente_PK=' . $client[0];

			$data = [
				'id_cliente_PK'   => strip_tags(htmlspecialchars($client[0], ENT_QUOTES)),
				'nombre_cliente' => strip_tags(htmlspecialchars($client[1], ENT_QUOTES)),
				'apellido_cliente' => strip_tags(htmlspecialchars($client[2], ENT_QUOTES)),
				'tipo_doc_cli' => strip_tags(htmlspecialchars($client[3], ENT_QUOTES)),
				'num_id_cli' => strip_tags(htmlspecialchars($client[4], ENT_QUOTES)),
				'id_barrio_FK' => strip_tags(htmlspecialchars($client[5], ENT_QUOTES)),
				'direccion_residencia' => strip_tags(htmlspecialchars($client[6], ENT_QUOTES)),
				'telefono' => strip_tags(htmlspecialchars($client[7], ENT_QUOTES))
			];

			if ($this->pdo->update('cliente', $data, $strWhere)) return true;

			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function editInstantlyClient($field, $value)
	{
		try {

			$validations = true;

			if ($field == 'num_id_cli') {
				if (valNumBetweenARange($value) != true) $validations = false;
			} elseif ($field == 'telefono') {
				if (valTelefono($value) != true) $validations = false;
			} elseif ($field == 'direccion_residencia') {
				if (valAlphanumericAndSymbol($value) != true) $validations = false;
			}

			if ($validations) {
				$idClientInSession = $_SESSION['user']['client']->id_cliente_PK;

				$data = [
					$field => $value
				];

				$strWhere = 'id_cliente_PK=' . $idClientInSession;
				if ($this->pdo->update('cliente', $data, $strWhere)) return true;

				return false;
			}

			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function getClientSession()
	{
		$idCliSession = $_SESSION['user']['client']->id_cliente_PK;
		$strSql = "call sp_client_by_id($idCliSession);";
		$query = $this->pdo->select($strSql);
		return $query;
	}

	public function getClientById($id)
	{
		try {
			$strSql = "SELECT * FROM cliente WHERE id_cliente_PK=:id_cliente_PK";
			$arrayData = ['id_cliente_PK' => $id];
			$query = $this->pdo->select($strSql, $arrayData);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function editClient($data)
	{
		try {
			$strWhere = 'id_cliente_PK=' . $data['id_cliente_PK'];
			$this->pdo->update('cliente', $data, $strWhere);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function saveClient($data)
	{
		try {
			foreach ($data as $dat) {
				$data = [
					'id_barrio_FK'   		=> $dat['id_barrio_FK'],
					'id_usuario_FK'  		=> $dat["id_usuario_FK"],
					'tipo_doc_cli'  		=> $dat['tipo_doc_cli'],
					'nombre_cliente'  		=> $dat['nombre_cliente'],
					'apellido_cliente'  	=> $dat['apellido_cliente'],
					'num_id_cli'  			=> $dat['num_id_cli'],
					'telefono'  			=> $dat['telefono'],
					'direccion_residencia'  => $dat['direccion_residencia']
				];

				$this->pdo->insert('cliente', $data);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
}
