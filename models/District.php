<?php

/**
 * Modelo de la tabla District
 * CRUD.php
 */
class District extends Database
{

	private $id_barrio_PK;
	private $estado_bc;
	private $nombre_barrio;
	private $pdo;



	public function __construct()
	{
		parent::__construct();
		try {
			$this->pdo = new Database;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getAll()
	{
		try {
			$strSql = "SELECT * FROM vw_barrio ORDER BY id_barrio_PK ASC";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getLastId()
	{
		try {
			return $this->pdo->getLastId();
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function getActiveDistricts()
	{
		try {
			$strSql = "SELECT * FROM vw_barrio WHERE estado_bc = 1";
			$query = $this->pdo->select($strSql);
			return $query;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function saveDistrict($data)
	{
		try {
			foreach ($data as $dat) {
				$data = [
					'estado_bc'      => 1,
					'nombre_barrio'  => $dat['nombre_barrio']
				];

				$this->pdo->insert('barrio_cliente', $data);
			}
			return true;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function newDistrictProfile($districtName)
	{
		if (trim($districtName[0]) != '') {
			$data = [
				'estado_bc'      => 1,
				'nombre_barrio'  => $districtName[0]
			];
			if (
				$this->pdo->insert('barrio_cliente', $data)
			) {
				$strSql = "SELECT MAX(id_barrio_PK) as id_barrio_PK FROM barrio_cliente";
				$query = $this->pdo->select($strSql);

				// dd($query);

				$strSql2 = "SELECT nombre_barrio FROM barrio_cliente WHERE id_barrio_PK =" . $query[0]->id_barrio_PK;
				$query2 = $this->pdo->select($strSql2);

				$query2[0]->id_barrio_PK = $query[0]->id_barrio_PK;

				// dd($query2);
				return $query2;
			} else return false;
		}


		return false;
	}

	public function editInstantlyDistrict($field, $value)
	{
		try {
			$idDistrictClientInSession = $_SESSION['user']['client']->id_barrio_FK;

			$data = [
				$field => $value
			];

			$strWhere = 'id_barrio_PK=' . $idDistrictClientInSession;
			if ($this->pdo->update('barrio_cliente', $data, $strWhere)) return true;

			return false;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
}
