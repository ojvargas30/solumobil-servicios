<?php
// include 'mimetodo.php';  // Este método para pasar un parametro y verificar
// en donde estoy ubicado y validar la sesion desde aqui
//     error_reporting( E_ALL );

include 'helpers/test.php';
include 'config/config.php';

require_once 'vendor/autoload.php';
// require_once 'providers/TLC.php';
require_once 'providers/GoogleAuth.php';
require_once 'providers/Date.php';
require_once 'providers/Preg.php';
include_once "controllers/Controller.php";
require_once 'providers/ExpireSession.php';

session_start();

// ExpireSession::valCookieExpired();
// dd(phpinfo());

// ini_set( 'display_errors', 1 );
//     error_reporting( E_ALL );
// $from = "prueba@solucionex.com";
// $to = "javiernuber@gmail.com";
// $subject = "Prueba de envio de email con PHP";
// $message = "Esto es un email de prueba enviado con PHP";
// $headers = "From:" . $from;
// if(mail($to,$subject,$message, $headers)){

// 	dd("Email enviado!!");
// }
// 	try {
// 		mail('dovj2020@gmail.com','hello','test mail function');

// 	} catch (\Throwable $th) {
// 		throw $th;
// 	}
// if(mail('dovj2020@gmail.com','hello','test mail function')){
// 	dd('sdfsdf');
// }


// bind_textdomain_codeset('spanish', 'UTF-8');
date_default_timezone_set("America/Bogota");
// setlocale(LC_ALL, "");
setlocale(LC_ALL, "spanish");
// if (CURRENT_HOST == 'localhost') {

    // setlocale(LC_ALL, 'spanish.UTF-8');
// } else {

// }
// setlocale(LC_ALL, "es_ES.UTF-8");
// setlocale(LC_ALL, "es_ES");

require 'providers/Database.php'; //llamando clase conexion

$controller = isset($_REQUEST["controller"]) ? $_REQUEST["controller"] : 'home';

if (!isset($_REQUEST["controller"]) && isset($_REQUEST["c"])) {

    $controller = isset($_REQUEST["c"]) ? $_REQUEST["c"] : 'index';
}

$method = isset($_REQUEST['method']) ? $_REQUEST['method'] : 'index';

if (!isset($_REQUEST["method"]) && isset($_REQUEST["m"])) {

    $method = isset($_REQUEST["m"]) ? $_REQUEST["m"] : 'index';
}

// function launchMessageExpired(){
// 	if (isset($_GET["type"])) {
// 		if($_GET["type"] === "expired"){
// 			echo "
// 			<script>
// 			   alert('¡Tu sesión ha expirado!');
// 			</script>
// 			";
// 		}
// 	}
// }

// launchMessageExpired();

// $id = isset($_REUEST["id"]) ? Q[$_REQUEST["id"]] : [];

// QUE SI NO EXISTE UNA SOLICITUD DEL FRONT
$controller = ucwords($controller) . 'Controller';

// funcion para llamar al controlador y ejecutar el metodo
if (file_exists('controllers/' . $controller . '.php')) {
    require_once 'controllers/' . $controller . '.php';
    if (class_exists($controller)) {
        $controller = new $controller;

        if (method_exists($controller, $method)) {
            // call_user_func_array(array($controller, $method),$id);
            call_user_func([$controller, $method]);
        } else {
            exit("View Error Method Not found : method not exists");
        }
    } else {
        exit("View Error Class Not found : class not exists");
    }
} else {
    exit("View Error Controller Not found : file not exists");
}
