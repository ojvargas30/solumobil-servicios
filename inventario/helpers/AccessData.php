<?php
$path = "./inventario/.env";
if (strlen(strstr($_SERVER["PHP_SELF"], "inventario")) > 0) {
    $path = "./.env";
}
$env = file_get_contents($path);
foreach (explode("\n", $env) as $dotenven) {
    if (strlen($dotenven) > 3) {
        $key = explode("=", $dotenven)[0];
        $value = explode("=", $dotenven)[1];
        $_ENV[$key] = $value;
    }
}

class AccessData
{
    private $db = null;
    private $table = "sesion";
    public function __construct()
    {
        $this->db = $this->connection();
    }
    private function connection()
    {
        $dbhost = trim($_ENV["DB_HOST"]);
        $dbname = trim($_ENV["DB_NAME"]);
        $dbuser = trim($_ENV["DB_USER"]);
        $dbpass = trim($_ENV["DB_PASSWORD"]);
        $charset = trim($_ENV["DB_CHARSET"]);
        $driver = trim($_ENV["DB_DRIVER"]);
        $dsn = "{$driver}:host={$dbhost};dbname={$dbname};charset={$charset}";

        try {
            $dbh = new PDO($dsn, $dbuser, $dbpass);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

            return $dbh;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    public function add($data)
    {
        $sql = "INSERT INTO {$this->table} (id_usuario,email_usuario,rol_usuario,ip_cliente,token,fecha_expiracion,fecha_creacion)VALUES(?,?,?,?,?,?,?)";
        try {
            $stmt = $this->db->prepare($sql);
            return $stmt->execute(array_values($data));
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }
    public function check($token)
    {
        $sql = "SELECT * FROM {$this->table} WHERE token = ?";
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute([$token]);
            return $stmt->fetch();
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }

    public function delete($token)
    {
        $sql = "DELETE FROM {$this->table} WHERE token = ? limit 1";
        try {
            $stmt = $this->db->prepare($sql);
            return $stmt->execute([$token]);
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }
    public function renewToken($data)
    {
        $sql = "UPDATE {$this->table} SET token = ?, fecha_expiracion = ? WHERE token = ?";
        try {
            $stmt = $this->db->prepare($sql);
            return $stmt->execute(array_values($data));
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }
}
