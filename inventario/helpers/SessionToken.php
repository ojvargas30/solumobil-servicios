<?php
class SessionToken
{
    private static $manager = null;
    private static $name_cookie = "SESSION_INVENTORY";

    public static function checkSession($cookie)
    {
        self::$manager = new AccessData();
        if (isset($cookie[self::$name_cookie])) {
            $result = self::$manager->check($cookie[self::$name_cookie]);
            if ($result) {
                $now = date("Y-m-d H:s:i");
                if (strtotime($now) <= strtotime($result->fecha_expiracion)) {
                    $_SESSION["id_user"] = 1;
                    $_SESSION["nickname_user"] = $result->email_usuario;
                    $_SESSION["name_user"] = $result->rol_usuario;
                    $_SESSION["lastname_user"] = "";
                    $_SESSION["rol_user"] = $result->rol_usuario;
                    $_SESSION["img_user"] = "assets/img/user/avatar.png";
                    self::renewCookieSession($cookie[self::$name_cookie]);
                    return true;
                }
            }
        }
        self::destroyData();
        header("Location:{$_ENV["DOMAIN_LOGIN"]}");
        exit("Verifica tus credenciales.<br>Redireccionando...");
        return false;
    }
    public static function destroyData()
    {
        unset($_SESSION["id_user"]);
        unset($_SESSION["nickname_user"]);
        unset($_SESSION["lastname_user"]);
        unset($_SESSION["rol_user"]);
        unset($_SESSION["img_user"]);
    }
    public static function create($session, $timeValid = 86400)
    {
        self::$manager = new AccessData();
        $timeValid = time() + $timeValid;
        if (isset($session["user"]["technical"])) {
            $user = (array)$session["user"]["user"];
            $data = [
                "id" => $user["id_usuario_PK"],
                "email" => $user["correo"],
                "rol" => ucwords($user["rol"]),
                "ip" => $_SERVER["REMOTE_ADDR"],
                "token" => hash("sha256", uniqid(50)),
                "fecha_expiracion" => date("Y-m-d H:s:i", $timeValid),
                "fecha_registro" => date("Y-m-d H:s:i")
            ];

            self::$manager->add($data);
            if (isset($_COOKIE[self::$name_cookie])) {
                self::setCookieSession(self::$name_cookie, $_COOKIE[self::$name_cookie], time() - $timeValid);
                self::setCookieSession(self::$name_cookie, $_COOKIE[self::$name_cookie], time() - $timeValid, "/");
            }
            self::setCookieSession(self::$name_cookie, $data["token"], $timeValid);
            self::setCookieSession(self::$name_cookie, $data["token"], $timeValid, "/");
        }
    }
    private static function renewCookieSession($token, $timeValid = 86400)
    {
        self::$manager = new AccessData();
        $timeValid = time() + $timeValid;
        $data = [
            "new_token" => hash("sha256", uniqid(50)),
            "new_expiration" => date("Y-m-d H:s:i", $timeValid),
            "last_token" => $token
        ];
        self::$manager->renewToken($data);
        self::setCookieSession(self::$name_cookie, $_COOKIE[self::$name_cookie], time() - $timeValid);
        self::setCookieSession(self::$name_cookie, $data["new_token"], $timeValid);
        // refresh token also in the path / or principal domain
        self::setCookieSession(self::$name_cookie, $_COOKIE[self::$name_cookie], time() - $timeValid, "/");
        self::setCookieSession(self::$name_cookie, $data["new_token"], $timeValid, "/");
    }
    private static function setCookieSession($name, $value, $time, $path = "/inventario")
    {
        setcookie($name, $value, $time, $path);
    }

    public static function deleteSession($cookie = [], $timeValid = 86400)
    {
        $cookie = $_COOKIE;
        self::$manager = new AccessData();
        if (isset($cookie[self::$name_cookie])) {
            $result = self::$manager->delete($cookie[self::$name_cookie]);
            if ($result) {
                self::destroyData();
                setcookie(self::$name_cookie, $cookie[self::$name_cookie], time() - $timeValid);
                setcookie(self::$name_cookie, $cookie[self::$name_cookie], time() - $timeValid, "/");
            }
        }
    }
}
