<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- se traen las librerias  -->
    <link href="libs/fontawesome-5.1/css/all.min.css" rel="stylesheet">
    <link href="libs/bootstrap-4.3/css/bootstrap.min.css" rel="stylesheet">
    <meta name="author" content="Andres Lobaton y Oscar Vargas">
    <meta name="descripcion" content="Proyecto tecnico Solumobil del Sena">
    <meta name="date" content="octubre-2019">
    <!-- librerias y demas -->
    <link rel="stylesheet" href="libs/charts/morris.css">
    <link href="libs/template-bootstrap/css/sb-admin.min.css" rel="stylesheet">
    <link href="libs/template-bootstrap/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="assets/js/sweetalert.min.js"></script>
    <link href="assets/css/style.css" rel="stylesheet">
    <!-- insertar icono -->
    <link class="icon" rel=icon sizes="32x32" type="image/png" href='../assets/img/1.ico'>

    <title>Inventario | Solumobil</title>

</head>

<body id="page-top">