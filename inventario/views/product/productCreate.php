    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-mobile-alt"></i>
            Registro de Productos
        </div>
        <div class="card-body">
            <form method="post" id="form-create-product" enctype="multipart/form-data">

                <input type="hidden" name="c" value="product">
                <input type="hidden" name="m" value="save">
                <input type="hidden" name="id_user" value="<?php echo $_SESSION["id_user"]; ?>">
                <input type="hidden" name="estado" value=1>

                <div class="form-group">
                    <label>Codigo</label>
                    <small class="text-muted">(Obligatorio)</small>
                    <input type="text" name="codigo" required class="form-control" placeholder="Ingrese el codigo">
                </div>

                <div class="form-group">
                    <label>Categoria</label>
                    <small class="text-muted">(Obligatorio)</small>
                    <select class="form-control" name="categoria" required=>
                        <option selected="on" disabled="on" value="">-Selecciona una categoria-</option>
                        <?php foreach ($cats as $cat) : ?>
                            <option value="<?php echo isset($cat->id_categoria_producto_PK) ? $cat->id_categoria_producto_PK : ''; ?>"><?php echo isset($cat->nombre_categoria_producto) ? $cat->nombre_categoria_producto : ''; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-goup">
                    <label>Marca</label>
                    <small class="text-muted">(Obligatorio)</small>
                    <select class="form-control" name="marca" required>
                        <option selected="on" disabled="on" value="">-Selecciona una marca-</option>
                        <?php foreach ($marcas as $marca) : ?>
                            <option value="<?php echo isset($marca->id_marca_producto_PK) ? $marca->id_marca_producto_PK : ''; ?>"><?php echo isset($marca->descripcion_marca_producto) ? $marca->descripcion_marca_producto : ''; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Referencia</label>
                    <small class="text-muted">(Obligatorio)</small>
                    <input type="text" name="referencia" class="form-control" placeholder="Ingrese la referencia" required>
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Stock (numero de existencias)</label>
                            <small class="text-muted">(Obligatorio)</small>
                            <input type="text" name="stock" class="form-control" placeholder="Ingrese el stock" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label>Nivel de importancia para muestra en stock</label>
                        <small class="text-muted">Uno es mas importante.</small>
                        <select name="importancia_producto" class="custom-select mr-sm-2">
                            <option value="1">Uno</option>
                            <option value="2">Dos</option>
                            <option value="3">Tres</option>
                            <option value="4">Cuatro</option>
                            <option selected value="5">Cinco</option>
                        </select>
                    </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Precio unitario</label>
                    <small class="text-muted">(Obligatorio)</small>
                    <input type="text" name="precio" class="form-control" placeholder="Ingrese la referencia" required>
                </div>
                <div class="form-group">
                    <label type="button" for="add-image-product" class="btn btn-info btn-lg btn-block w-50 mx-auto ">Selecciona las imagenes</label>
                    <div class="text-center text-muted"><small>Debes subir al menos dos imagenes y maximo veinte.</small></div>
                    <div class="text-center text-muted"><small>Extenciones permitidas: .jpeg, .png, .jpg, .gif, .svg</small></div>
                    <div class="text-center text-muted small text-danger">Al subir las imagenes te haces responsable de que esten libres de derechos de autor. (Las imagenes deben ser menor de 10 MB)</div>
                    <input type="file" name="images[]" id="add-image-product" required accept="image/*" multiple class="d-none">
                    <div id="render-images" class="text-center ">
                        <div class="rounded m-2 " style="width:200px;display:none">
                            <img id="img-src" src="" alt="">
                            <div class="quit"><i class="fas fa-trash"></i></div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" id="button-send-form" class="btn btn-success">Registrar Producto</button>
                    <button type="button" onclick="window.location.replace('?c=product&m=index');" class="btn btn-secondary">Cancelar</button>
                </div>
            </form>
        </div>
        <div class="card-footer small text-muted">Actualizado Hoy <?php echo date('h:i a'); ?></div>
    </div>
    </div>