-- drop database if exists Solumovil;
-- create database Solumovil;
-- use Solumovil;

create table if not exists sesion (
id_sesion_PK int not null auto_increment,
id_usuario int not null,
email_usuario varchar(200) not null,
rol_usuario varchar(200) not null,
token varchar(200) not null,
ip_cliente varchar(200) not null,
fecha_expiracion datetime NOT NULL,
fecha_creacion datetime NOT NULL,
primary key (id_sesion_PK)
);

create table if not exists estado (
id_estado_PK int not null auto_increment,
tipo_estado varchar(100) not null,
primary key (id_estado_PK)
);

create table if not exists tipo_documento (
id_tipo_documento_PK int not null auto_increment,
descripcion_tipo_documento varchar(100) not null,
abreviacion varchar(100)null,
primary key (id_tipo_documento_PK)
);

create table if not exists rol_usuario (
id_rol_usuario_PK int not null auto_increment,
tipo_rol_usuario varchar(100) not null,
primary key (id_rol_usuario_PK)
);

create table if not exists usuario (
id_usuario_PK int not null auto_increment,
id_tipo_documento_FK int null,
numero_documento_usuario varchar(100) null,
nombres_usuario varchar(100) not null,
apellidos_usuario varchar(100)  not null,
nickname_usuario varchar(100) not null,
clave_usuario varchar(100) not null,
correo_usuario varchar(100) not null,
telefono_usuario varchar(100)null,
direccion_usuario varchar(100) null,
id_estado_FK int null,
id_rol_usuario_FK int not null,
foto_usuario varchar(100) null,
fecha_creacion_usuario date not null,
inactivacion_usuario boolean not null default 1,
primary key (id_usuario_PK),
foreign key (id_tipo_documento_FK) references tipo_documento (id_tipo_documento_PK),
foreign key (id_estado_FK) references estado (id_estado_PK),
foreign key (id_rol_usuario_FK) references rol_usuario (id_rol_usuario_PK)
);


-- tablas de estadisticas
 create table if not exists registro_login(
 id_registro_login int not nulL auto_increment,
 id_usuario_FK int not null,
 fecha date not null,
 hora  time not null,
 primary key(id_registro_login),
 foreign key (id_usuario_FK) references usuario(id_usuario_PK)
 );
-- end
create table if not exists cliente (
id_cliente_PK int not null auto_increment,
id_tipo_documento_FK int null,
Numero_documento_Cliente varchar(100) null,
nombre_cliente varchar(100) not null,
apellido_cliente varchar(100) not null,
direccion_cliente varchar(100) null,
correo_cliente varchar(100) null,
telefono_cliente varchar(100) null,
id_estado_FK int null default 1,
inactivacion_cliente boolean not null default 0,
primary key (id_cliente_PK),
foreign key (id_estado_FK) references estado (id_estado_PK)

);
create table if not exists proveedor(
id_proveedor_PK int not null auto_increment,
nombre_proveedor varchar(100) not null,
apellido_proveedor varchar(100) not null,
id_estado_FK int not null default 1,
telefono_proveedor varchar(100) not null,
direccion_proveedor varchar(100) null,
inactivacion_proveedor boolean not null default 0,
primary key (id_proveedor_PK),
foreign key (id_estado_FK) references estado (id_estado_PK)
);

create table if not exists categoria_producto(
id_categoria_producto_PK int not null auto_increment,
nombre_categoria_producto varchar(100) not null,
descripcion_categoria_producto varchar(100) null,
id_estado_FK int not null default 1,
inactivacion_categoria boolean not null default 0,
primary key (id_categoria_producto_PK),
foreign key (id_estado_FK) references estado (id_estado_PK)
);

create table if not exists categoria_servicio(
id_categoria_servicio_PK int not null auto_increment,
nombre_categoria_servicio varchar (200)not null,
descripcion_categoria_servicio varchar (200) null,
id_estado_FK int not null default 1,
inactivacion_categoria boolean not null default 0,
primary key (id_categoria_servicio_PK),
foreign key (id_estado_FK) references estado(id_estado_PK)
);

create table if not exists marca_producto(
id_marca_producto_PK INT NOT NULL AUTO_INCREMENT,
estado_marca_producto boolean not null,
descripcion_marca_producto varchar(100),
Primary key(id_marca_producto_PK)
);

create table if not exists producto(
id_producto_PK int not null auto_increment,
codigo_producto_PK varchar(100) null unique,
id_categoria_producto_FK int null,
id_marca_producto_FK int null,
referencia_producto varchar(100) not null,
stock_producto int not null,
id_estado_FK int null default 1,
importancia_producto int not null default 5, -- 1 mas importante | 5 menos importante
precio_producto double not null,
fecha_registro_producto date null,
inactivacion_producto boolean not null default 0,
primary key (id_producto_PK),
foreign key (id_marca_producto_FK) references marca_producto (id_marca_producto_PK),
foreign key (id_categoria_producto_FK) references categoria_producto (id_categoria_producto_PK),
foreign key (id_estado_FK) references estado (id_estado_PK)
);

create table if not exists imagen (
id_imagen_PK INT NOT NULL AUTO_INCREMENT,
id_producto_FK INT NOT NULL,
url varchar(500) NOT NULL,
fecharegistro DATETIME NOT NULL,
primary key (id_imagen_PK),
foreign key (id_producto_FK) references producto (id_producto_FK)
);

create table if not exists servicio (
id_servicio_PK int not null auto_increment,
id_categoria_servicio_FK int not null,
descripcion_servicio varchar (300) not null,
precio_servicio int not null,
id_estado_FK int null default 1,
inactivacion_servicio boolean not null default 0,
primary key (id_servicio_PK),
foreign key (id_categoria_servicio_FK) references categoria_servicio (id_categoria_servicio_PK)
);


create table if not exists compra(
id_compra_PK int not null auto_increment,
id_usuario_FK int not null,
id_proveedor_FK int not null,
total_compra int(15) not null, -- campo calculado
fecha_compra datetime,
inactivacion_compra boolean not null,
primary key (id_compra_PK),
foreign key (id_usuario_FK) references usuario(id_usuario_PK),
foreign key (id_proveedor_FK) references proveedor (id_proveedor_PK)
);

create table if not exists estado_venta (
id_estado_venta_PK int not null auto_increment,
tipo_estado_Venta varchar(100) not null,-- pagado/pendiente
primary key (id_estado_venta_PK)
);

create table if not exists venta(
id_venta_PK int not null auto_increment,
id_usuario_FK int not null,
id_cliente_FK int not null,
subtotal_venta double not null,
descuento_venta double null,
total_venta int not null, -- campo calculado
fecha_venta datetime not null,
id_estado_venta_FK int,
inactivacion_venta boolean not null,
primary key (id_venta_PK),
foreign key (id_usuario_FK) references usuario (id_usuario_PK),
foreign key (id_cliente_FK) references cliente (id_cliente_PK),
foreign key (id_estado_venta_FK) references estado_venta (id_estado_venta_PK)
);
create table if not exists detalle_venta(
id_venta_FK int not null,
id_producto_FK int  null,
id_servicio_FK int  null,
cantidad_detalle_venta int not nulL,
precio_detalle_venta int not null,
descripcion_detalle_venta  varchar (300) null,
foreign key (id_venta_FK) references venta (id_venta_PK),
foreign key (id_producto_FK) references producto (id_producto_PK),
foreign key (Id_servicio_FK) references servicio (id_servicio_PK)
);

create table if not exists detalle_compra(
id_compra_FK int not null auto_increment,
id_producto_FK int not null,
cantidad_detalle_compra int not null,
precio_detalle_compra int not null,
descripcion_detalle_compra  varchar (300) null,
foreign key (id_compra_FK) references compra (id_compra_PK),
foreign key (Id_Producto_FK) references producto (Id_Producto_PK)
);
-- tabla de las notificaciones
create table if not exists tipo_notificacion(
id_tipo_notificacion_PK int not null auto_increment,
tipo_notificacion varchar(100),
primary key(id_tipo_notificacion_PK)
);
create table if not exists notificacion(
id_notificacion_PK int not null,
id_rol_usuario_FK int not null,
id_usuario_FK int not null,
id_tipo_notificacion_FK int not null,
tipo varchar(100) not null,
fecha datetime not null,
primary key(id_notificacion_PK),
foreign key (id_usuario_FK) references usuario (id_usuario_PK),
foreign key (id_tipo_notificacion_FK) references tipo_notificacion (id_tipo_notificacion_PK)
);
create table if not exists notificacion_personalizada(
id_notificacion_FK int not null ,
mi_usuario int,
id_usuario_FK int not null,
leido boolean not null,
visualizado boolean null,
foreign key (id_usuario_FK) references usuario (id_usuario_PK),
foreign key (id_notificacion_FK) references notificacion (id_notificacion_PK)
);

-- creacion de vistas
-- consulta de todos los ptoductos
create view consulta_productos as
select p.id_producto_PK,p.codigo_producto_PK,cp.nombre_categoria_producto,mp.descripcion_marca_producto,p.precio_producto,
p.referencia_producto,p.stock_producto,e.tipo_estado,p.fecha_registro_producto
 from producto as p inner join categoria_producto as cp on cp.id_categoria_producto_PK = p.id_categoria_producto_FK
					inner join marca_producto as mp on mp.id_marca_producto_PK = p.id_marca_producto_FK
                    inner join estado as e on e.id_estado_PK = p.id_estado_FK where inactivacion_producto = 0;

-- consulta de historial de login al sitema
create view historial_registro_login as
select rl.id_registro_login,u.nombres_usuario,u.apellidos_usuario,rl.fecha,rl.hora
from registro_login as rl inner join usuario as u on u.id_usuario_PK = rl.id_usuario_FK order by rl.id_registro_login desc;

-- consulta de servicios
create view consulta_servicios as
select s.id_servicio_PK,c.nombre_categoria_servicio,s.descripcion_servicio,s.precio_servicio,e.tipo_estado from
 servicio as s inner join categoria_servicio as c on s.id_categoria_servicio_FK =  c.id_categoria_Servicio_PK
               inner join estado as e on e.id_estado_PK = s.id_estado_FK where inactivacion_servicio = 0;
-- consulta de notificaciones
create view notificaciones as
SELECT n.id_notificacion_PK,n.id_usuario_FK,u.nickname_usuario,n.tipo,np.leido,n.fecha
FROM notificacion as n  inner join notificacion_personalizada as np on n.id_notificacion_PK = np.id_notificacion_FK
					   inner join usuario as u on u.id_usuario_PK = n.id_usuario_FK
                       where leido = 0 group by id_notificacion_PK  order by id_notificacion_PK desc limit 10;
       -- date_format(fecha_venta,"%Y-%c-%d")
-- datos de la venta
create view consulta_ventas as
select v.id_venta_PK, u.nombres_usuario, c.nombre_cliente,c.apellido_cliente ,v.total_venta, date_format(v.fecha_venta,"%Y-%m-%d") as fecha_venta ,v.id_estado_venta_FK,v.inactivacion_venta
          from venta as v inner join usuario as u  on u.id_usuario_PK = v.id_usuario_FK
						  inner join cliente as c on  c.id_cliente_PK = v.id_cliente_FK ORDER BY v.id_venta_PK DESC;


create view consulta_compras as
select c.id_compra_PK, u.nombres_usuario, p.nombre_proveedor ,c.total_compra, date_format(c.fecha_compra,"%Y-%m-%d") as fecha_compra ,c.inactivacion_compra
          from compra as c inner join usuario as u  on u.id_usuario_PK = c.id_usuario_FK
						  inner join proveedor as p on  p.id_proveedor_PK = c.id_proveedor_FK
                          where inactivacion_compra = 0;
-- reportes por fechas

-- select avg(total_venta) from venta where fecha_venta <= curdate() and fecha_venta >= date_sub(curdate(),interval 1 week) group by fecha_venta;

-- creacion de procedimientos almacenados
create procedure guardar_proveedor(NOMBRE varchar(50),APELLIDO varchar(50),DIRECCION varchar(50),TELEFONO varchar(50))
INSERT INTO proveedor(nombre_proveedor, apellido_proveedor,direccion_proveedor,telefono_proveedor) VALUES (NOMBRE,APELLIDO,DIRECCION,TELEFONO);

create procedure consulta_proveedor()
SELECT * FROM proveedor where inactivacion_proveedor = 0;

create procedure productos_DV (VALOR varchar(100))
SELECT p.id_producto_PK,p.inactivacion_producto,p.codigo_producto_PK,cp.nombre_categoria_producto,m.descripcion_marca_producto,p.referencia_producto,p.stock_producto, p.precio_producto FROM producto as p inner join categoria_producto as cp on P.id_categoria_producto_FK = cp.id_categoria_producto_PK
                            inner join marca_producto as m on p.id_marca_producto_FK = m.id_marca_producto_PK WHERE (cp.nombre_categoria_producto
                            like VALOR  OR m.descripcion_marca_producto like VALOR) AND p.inactivacion_producto = 0;

create procedure producto_getone_DV (ID INT)
SELECT p.id_producto_PK,p.codigo_producto_PK,cp.nombre_categoria_producto,m.descripcion_marca_producto,p.referencia_producto,p.stock_producto, p.precio_producto FROM producto as p inner join categoria_producto as cp on P.id_categoria_producto_FK = cp.id_categoria_producto_PK
                            inner join marca_producto as m on p.id_marca_producto_FK = m.id_marca_producto_PK
                            WHERE id_producto_PK = ID;

create procedure servicios_DV (VALOR varchar(100))
SELECT s.id_servicio_PK,cs.nombre_categoria_servicio,s.descripcion_servicio,s.precio_servicio
           FROM servicio as s inner join categoria_servicio as cs on s.id_categoria_servicio_FK = cs.id_categoria_servicio_PK
           WHERE (cs.nombre_categoria_servicio like VALOR  OR s.descripcion_servicio like VALOR)  AND s.inactivacion_servicio = 0;

create procedure servicio_getone_DV (ID INT)
SELECT s.id_servicio_PK,cs.nombre_categoria_servicio,s.descripcion_servicio,s.precio_servicio
           FROM servicio as s inner join categoria_servicio as cs on s.id_categoria_servicio_FK = cs.id_categoria_servicio_PK
           WHERE s.id_servicio_PK = ID;

create procedure venta (ID INT)
select v.id_venta_PK,v.subtotal_venta,v.descuento_venta, u.nombres_usuario, c.nombre_cliente,c.apellido_cliente ,v.total_venta, date_format(v.fecha_venta,"%Y-%m-%d") as fecha_venta ,v.id_estado_venta_FK,v.inactivacion_venta
          from venta as v inner join usuario as u  on u.id_usuario_PK = v.id_usuario_FK
						  inner join cliente as c on  c.id_cliente_PK = v.id_cliente_FK WHERE v.id_venta_PK = ID;





create procedure consulta_proveedor_uno(ID INT)
SELECT * FROM proveedor WHERE id_proveedor_PK = ID and inactivacion_proveedor = 0;

create procedure actualizar_proveedor(NOMBRE varchar(50),APELLIDO varchar(50),DIRECCION varchar(50),TELEFONO varchar(50),ESTADO INT,ID INT)
UPDATE proveedor SET   nombre_proveedor       = NOMBRE,
                       apellido_proveedor     = APELLIDO ,
					   direccion_proveedor    = DIRECCION,
                       telefono_proveedor     = TELEFONO,
					   id_estado_FK           = ESTADO
            WHERE id_proveedor_PK = ID;

create procedure eliminar_proveedor(ID INT)
UPDATE proveedor SET inactivacion_proveedor = 1 WHERE id_proveedor_PK = ID;
-- insercion de datos
insert into tipo_notificacion values(null,"producto"),
                                    (null,"servicio"),
                                    (null,"venta"),
                                    (null,"compra"),
                                    (null,"usuario"),
                                    (null,"cliente"),
                                    (null,"proveedor"),
                                    (null,"categoria"),
                                    (null,"reporte"),
                                    (null,"estadistica");
insert into estado values(null,"Activo"),
                          (null,"Inactivo");

insert into estado_venta values(null,"Pagado"),
                                (null,"Pendiente"),
                                (null,"Anulado");

insert into tipo_documento values(null,"Cédula de Ciudadanía","CC"),
                                  (null,"Cédula de Extranjeria","CE"),
                                  (null,"Numero de Identificacion Tributaria","NIT");

insert into rol_usuario values(null,"Administrador"),
                               (null,"Vendedor");
-- PASSWORD: kasdgh6745_SH
insert into usuario values (null,2,'1002435648','Oscar Javier','Vargas Diaz',"Sixtharlings94",'$2y$10$zNM5dqGEnfILV5JDf23HWuEz9xVjxflTJVZGocY96ck4UAxn/p02C',
                           'oscarvar_94@gmail.com','3456783675','kra 94 #32a-54',1,2,"assets/img/user/avatar.png","2019-04-23",1);

insert into categoria_producto values(null,"Celular","",1,0);
insert into marca_producto values(null,1,"Huawei");

insert into producto values(null,"P01",2,2,"Y-23",0,1,6000,"2018-01-22",0);
						--    (null,"P02",2,2,"Y-34",23,1,10000,"2018-02-22",0),
                        --    (null,"P03",5,2,"Y-6",2,1,30000,"2018-03-20",0),
                        --    (null,"P04",5,3,"Y-5",0,1,200000,"2018-04-22",0),
                        --    (null,"P05",1,3,"Y-563",0,1,40000,"2018-05-22",0),
                        --    (null,"P06",1,3,"Y-90",0,1,68000,"2018-06-22",0),
                        --    (null,"P07",4,1,"Y-89",0,1,33400,"2018-07-22",0),
                        --    (null,"P08",4,5,"Y-34",0,1,304300,"2018-08-22",0),
                        --    (null,"P09",1,5,"Y-24",0,1,3003220,"2018-09-22",0),
                        --    (null,"P10",3,5,"Y-243",12,1,300230,"2018-10-22",0),
                        --    (null,"P11",3,3,"Y-34",56,1,6000,"2018-11-22",0),
                        --    (null,"P12",3,1,"Y-23",23,1,3000,"2018-12-22",0),
                        --    (null,"P13",4,3,"Y-26",12,1,4000,"2019-01-22",0),
                        --    (null,"P14",4,4,"Y-34",23,1,2000,"2019-01-22",0),
                        --   (null,"P15",5,4,"Y-76",54,1,4000,"2019-01-22",0);

insert into cliente values(null,2,"100674565","Juan","Lopez",null,"juanlopez_90@gmail.com","45457984",1,0);
                        --   (null,1,"100456723","Pedro","Perez",null,"p0edro_perez_23@gmail.com","31275485",1,0),
                        --   (null,1,"528956742","Camilo","Ortiz",null,"asada_89@gmail.com","312675675",1,0),
                        --   (null,1,"672312905","Andres","Alvarado",null,"lacasa_roja21@gmail.com","413767545",1,0),
                        --   (null,2,"986534762","Salomon","Salamanca",null,"juancholomon@gmail.com","315897878",1,0),
                        --   (null,3,"123124663","Andrea","Perdomo",null,"andrea_per@gmail.com","311232454",1,0),
                        --   (null,1,"323232323","Camila","Diaz",null,"camila_d@gmail.com","32189345345",2,0),
                        --   (null,2,"860945609","Jose","Vivaz",null,"joce_vi@gmail.com","3167893434",1,0),
                        --   (null,1,"845874655","Jolman","Romero",null,"jolaman123@gmail.com","314678673",2,0),
                        --   (null,1,"100786454","Rocio","Rodriguez",null,"rocio_cacha@gmail.com","3125789789",1,0),
                        --   (null,1,"100398811","Jeison","Medina",null,"jeison_medina6002@gmail.com","3234242343",2,0);

insert into proveedor values (null,"Alfredo","Olaya",1,"32167321","cll34 #12-76",0);
                            --  (null,"Ramon","Murillo",1,"321132313","cll56 #16-76",0),
                            --  (null,"Felipe","Cardozo",1,"356778543","cll45 #09-7",0),
                            --  (null,"Javier","Herrera",1,"3345345345",null,0),
                            --  (null,"Dimedez","Pantoja",1,"3534436565",null,0),
                            --  (null,"James","Galan",1,"34535455",null,0),
                            --  (null,"Falcao","Calderon",2,"67868677878",null,0),
                            --  (null,"Santiago","Amado",2,"35756756756",null,0),
                            --  (null,"Pablo","Lopez",2,"3675656767",null,0),
                            --  (null,"Marcela","Amaya",2,"378978979789",null,0),
                            --  (null,"Juliana","Montero",2,"34234234234",null,0);

insert into categoria_servicio values (null,"Cambio de pantalla","Equipos moviles y tablets",1,0);
                                    --   (null,"Cambio de pin","",1,0),
                                    --   (null,"Cambio targeta madre","Equipos moviles y computadores",1,0),
                                    --   (null,"Formateo","Equipos moviles y computadores",1,0),
                                    --   (null,"Cambio de logica","",2,0);

insert into servicio values(null,1,"huawei p20",251400,1,0);
                        --    (null,2,"samsung",25100,2,0),
                        --    (null,2,"smartphone 3",295000,1,0),
						--    (null,1,"cambio de logica de un huawei p30 ",20090,1,0),
						--    (null,5,"cambio de sistema operativo de un huawei y360",200550,2,0);

-- insert into venta values (null,1,1,30000,3200,26800,"2018-05-12 12:34:32",1,1),
--                          (null,1,2,50000,0,50000,"2018-06-02 12:34:32",2,1),
--                          (null,1,3,20000,0,20000,"2018-07-12 12:34:32",1,1),
--                          (null,1,4,10000,0,10000,"2018-08-12 12:34:32",1,0),
--                          (null,1,5,20000,5000,15000,"2018-09-09 12:34:32",1,1),
--                          (null,1,6,9000,3000,6000,"2018-10-10 12:34:32",1,0),
--                          (null,1,5,60000,0,60000,"2018-12-12 12:34:32",1,0),
--                          (null,1,3,450000,50000,400000,"2019-02-23 12:34:32",2,1),
--                          (null,2,4,230000,20000,210000,"2019-03-12 12:34:32",2,0),
--                          (null,2,5,130000,0,130000,"2019-03-12 10:23:32",2,0),
--                          (null,2,1,35000,0,35000,"2019-10-20 12:34:32",1,0),
--                          (null,2,1,35000,0,35000,"2019-10-25 12:34:32",1,0);

-- insert into detalle_venta values (1,2,null,5,30000,null),
--                                  (1,3,null,2,15000,null),
--                                  (1,4,null,1,3000,null),
--                                  (2,null,2,5,50000,null),
--                                  (2,null,1,5,40000,null),
--                                  (2,4,null,2,20000,null),
--                                  (3,2,null,2,10000,null),
--                                  (3,null,4,2,100000,null),
--                                  (4,null,5,1,300000,null),
--                                  (5,null,4,2,500000,null),
--                                  (6,null,4,1,30000,null);


-- insert into compra values (null,1,2,100000,"2018-01-12 12:34:32",0),
-- 						  (null,1,2,200000,"2018-02-13 12:34:32",0),
--                           (null,1,2,100000,"2018-03-14 12:34:32",0),
--                           (null,1,1,300000,"2018-04-02 12:34:32",0),
--                           (null,1,2,200000,"2018-05-22 12:34:32",0),
--                           (null,2,4,400000,"2018-06-82 12:34:32",0),
--                           (null,2,4,500000,"2018-07-12 12:34:32",0),
--                           (null,2,4,800000,"2019-01-19 12:34:32",0),
--                           (null,2,1,150000,"2019-02-13 12:34:32",0),
--                           (null,3,2,120000,"2019-03-12 12:34:32",0),
--                           (null,3,3,90000,"2019-04-14 12:34:32",0),
--                           (null,3,3,20000,"2019-05-29 12:34:32",0),
--                           (null,3,5,100000,"2019-05-12 12:34:32",0);

-- insert into detalle_compra values (1,2,40,30000,null),
--                                   (2,3,3,20000,null),
--                                   (1,3,50,18000,null),
--                                   (1,3,10,40000,null),
--                                   (2,2,20,16000,null),
--                                   (2,8,30,55000,null),
--                                   (3,8,40,15000,null),
--                                   (3,4,50,3000,null),
--                                   (3,6,5,50000,null),
--                                   (4,4,8,7000,null),
--                                   (5,4,9,30000,null),
--                                   (6,3,2,207000,null),
--                                   (7,2,9,306000,null),
--                                   (8,3,8,500000,null),
--                                   (9,3,8,30000,null),
--                                   (10,6,10,30000,null),
--                                   (10,6,10,30000,null),
--                                   (10,7,5,90000,null);
-- cabio de datos a la base de datos en detalle de venta en id_producto_FK Y id_servicio_PK de not null a null;
