const cleanInputImages = (input) => {
    input.value = ""
};
const validateNumberMinImages = (input,container)=>{
    if (input.files.length < 2) {
        swal({
            icon: 'info',
            title: 'Heyy!',
            text: 'Debes subir mínimo dos imagenes!',
        })
        container.innerHTML = ""
        cleanInputImages(input)
    }
};
const deleteImageList = (input,image)=>{
    let newFilesList = new DataTransfer()
    Array.from(input.files).forEach(item=>{
        if(item.name != image.previousSibling.title){
            newFilesList.items.add(item)
        }
    })
    input.files = newFilesList.files
    image.previousSibling.parentNode.outerHTML= ""

};

const appProduct = () => {
    const input = document.querySelector("#add-image-product");
    const container = document.querySelector("#render-images");
    let size = 0;
    input.addEventListener("change", (e) => {
        size = 0;
        container.innerHTML = ""
        if (input.files.length > 20){
            container.innerHTML = ""
            swal({
                icon: 'info',
                title: `Se permite maximo 20 imagenes`,
                text:`Has añadido ${input.files.length} imagenes.`
            })
            cleanInputImages(input)
            return
        }
        Array.from(input.files).map(image => {
            let img = document.createElement("img");
            let subcontainer = document.createElement("span");
            let quit = document.createElement("button");
            quit.type="button"
            quit.classList = "btn btn-danger quit "
            quit.innerHTML = '<i class="fas fa-trash"></i>'
            img.classList.add("rounded");
            img.classList.add("m-2");
            img.src = window.URL.createObjectURL(image);
            img.alt = image.name;
            img.title = image.name;
            img.style.width = "200px"
            size += image.size / 1048576;
            quit.addEventListener("click",(e)=>{
                deleteImageList(input,e.currentTarget)
                validateNumberMinImages(input,container)
            })
            subcontainer.appendChild(img)
            subcontainer.appendChild(quit)
            container.appendChild(subcontainer)
        })
        if (size > 10) {
            swal({
                icon: 'error',
                title: 'Exceso de tamaño en imagesnes',
                text: 'El tamaño total de las imagenes no debe ser mayor a 10 MB',
            })
            container.innerHTML = ""
            cleanInputImages(input)
        }
        validateNumberMinImages(input,container)
    })
    const form = document.querySelector("#button-send-form")
    form.addEventListener("click", (e) => {
        if (container.children.length == 0 || container.children.length == 1) {
            swal({
                icon: 'error',
                title: 'Por favor agregar imagenes del producto.',
            })
        }
    })
}

document.addEventListener("DOMContentLoaded", appProduct)