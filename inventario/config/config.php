<?php
// se declaran las contantes de la base de datos

$env = file_get_contents("./.env");
foreach (explode("\n", $env) as $dotenven) {
    if (strlen($dotenven) > 3) {
        $key = explode("=", $dotenven)[0];
        $value = explode("=", $dotenven)[1];
        $_ENV[$key] = $value;
    }
}

define("DB_HOST", trim($_ENV["DB_HOST"]));
define("DB_NAME", trim($_ENV["DB_NAME"]));
define("DB_USER", trim($_ENV["DB_USER"]));
define("DB_PASSWORD", trim($_ENV["DB_PASSWORD"]));
define("DB_DRIVER",  trim($_ENV["DB_DRIVER"]));
define("DB_CHARSET", trim($_ENV["DB_CHARSET"]));
