<?php
require_once "models/Notificacion.php";
require_once "models/Producto.php";

class ProductController
{
   private $model;

   public function __construct()
   {
      if (empty($_SESSION["id_user"]) || !isset($_SESSION["id_user"])) {
         header("location:?msg=session_expired&type=info");
      }
      $this->notificacion = new Notificacion();
      $this->model = new Producto();
   }


   public function index()
   {

      $response = $this->model->getall();
      $rows = $response;

      $link = "?c=product&m=index";
      $name = "Productos";
      $metodo = "Listado";
      $content = "product/productList.php";
      require_once "views/template/home/content.php";
   }


   public function create()
   {
      $cats = $this->model->categorias();
      $marcas = $this->model->marcas();

      $link = "?c=product&m=index";
      $name = "Productos";
      $metodo = "Crear";
      $content = "product/productCreate.php";
      require_once "views/template/home/content.php";
   }


   public function show()
   {
      $id = filter_var($_REQUEST["id"], FILTER_SANITIZE_NUMBER_INT);
      $cats = $this->model->categorias();
      $marcas = $this->model->marcas();
      $response = $this->model->getone($id);
      $link = "?c=product&m=index";
      $name = "Productos";
      $metodo = "Actualizar";

      if ($response) {
         $images = $this->model->getImagesProduct($id);
         $content = "product/productUpdate.php";
         require_once "views/template/home/content.php";
      } else {
         header("location:?c=product");
      }
   }

   public function save()
   {
      /*
      * $maxImageSave : int : Numero maximo de imagenes a guardar por producto
      */
      $maxImageSave = 20; 
      $response = $this->model->insert($_POST);
      $images_not_insert = [];
      if ($response) {
         $images_not_insert = $this->model->insertImages($response, $_FILES["images"],$maxImageSave);
      }
      $msg = $response != false ? "saved" : "dont_saved";
      header("location:?c=product&m=index&msg=" . $msg . "&images_not_insert=" . json_encode($images_not_insert));
   }



   public function update()
   {

      $response = $this->model->update($_POST);
      $msg = $response != false ? "updated" : "dont_updated";
      header("location:?c=product&m=index&msg=" . $msg);
   }

   public function delete()
   {
      $this->notificacion->insertar(1, "ha eliminado un producto");
      $response = $this->model->delete($_REQUEST["id"]);
      $msg = $response != false ? "deleted" : "dont_deleted";
      header("location:?c=product&m=index&msg=" . $msg);
   }
}
