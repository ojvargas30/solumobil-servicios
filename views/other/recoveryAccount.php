<div class="login-box" id="content">
  <div class="login-logo">
    <a href="?c=home&m=access"><b>Solu</b>panel</a>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
      <div class="alert alert-success d-none" id="alertaSuc" role="alert">
        Revisa tu correo por favor
      </div>
      <div>
        <p class="login-box-msg">Recuperación de la cuenta</p>
        <div class="input-group mb-3 justify-content-center">
          <input id="correo" placeholder="Correo electrónico" name="correo" type="email" minlength="5" maxlength="320" class="input_v recInput" required="true" class="form-control" autofocus autocomplete="on">
          <i class="fas fa-times-circle text-danger b inpStatusRec estado_oculto"></i>
          <i class="fas fa-check-circle text-success g inpStatusRec2 estado_oculto"></i>
          <p class="politica text-danger d-none text-left mb-0" id="rl_correo">El correo debe ser real y sin espacios</p>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope" id="icono"></span>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="g-recaptcha" data-sitekey="6LcjJj8aAAAAAGBe5M9f-pY1t9oH16hns2MJvF8m"></div>
          <!-- <div class="g-recaptcha" data-sitekey="<?php //echo PUBLIC_KEY_RECAPTCHA;
                                                      ?>"></div> -->
          <div class="alert alert-danger p-2 pt-1 m-auto col-12 justify-content-center mt-0 d-none alert-dismissible fade show" id="alertaRec" role="alert">
            <i class="fas fa-exclamation-triangle fa-sm"></i> <span class="text-sm-strict"> <strong>Error</strong> Por favor completa todos los campos correctamente </span>
          </div>
          <div class="col-12 mt-3 mb-2 d-flex justify-content-center">
            <button type="submit" id="recovery" class="btn bg-gradient-success text-center">Enviar</button>
          </div>
        </div>
        <div class="text-center mt-2">
          <p class="mt-3 mb-1 d-inline">
            <a href="?c=home&m=access" class="border-bottom">Iniciar Sesión</a>
          </p>
          <p class="mb-0 ml-5 d-inline">
            <a href="?c=home&m=register" class="text-center border-bottom">Registrarse</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="assets/js/layouts/getAndValidate.js"></script>
<script src="assets/js/general/recoveryAccount.js"></script>