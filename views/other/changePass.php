<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Solupanel | Recuperar contraseña</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" lang="es" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="Author" lang="es" content="Óscar Javier Vargas Diaz, oscarjaviervargas@hotmail.com">
  <meta name="DC.identifier" lang="es" content="">
  <!--Aqui va la pagina SOLUMOBIL.............................-->
  <META http-equiv="Expires" lang="es" content="0">
  <!--ESTA NOSE PARA QUE ES.-->
  <meta name="Keywords" lang="es" content="Engativa,Colombia, Bogota,
	servicio tecnico, localidad, Quitar Cuenta Google,
	reparacion, celulares, pantalla, dañada, puerto de carga, tablets, baratos,
	flasheo de celulares o tablets, cambio de pantalla, cambio de repuestos.">
  <META http-equiv="PICS-Label" content='
	(PICS-1.1 "http://www.gcf.org/v2.5"
	labels on "1994.11.05T08:15-0500"
	until "1995.12.31T23:59-0000"
	for "http://w3.org/PICS/Overview.html"
	ratings (suds 0.5 density 0 color/hue 1))
 '>
  <!--Esto es para ayudar a los padres y a las escuelas a controlar los lugares a los
   que pueden acceder los niños en Internet, también facilita otros usos para las etiquetas,
  incluyendo firmas de código, privacidad, y gestión de los derechos de la propiedad
  intelectual.-->
  <META name="copyright" content="&copy; 2020 Solumobil Company.">
  <meta name="Description" lang="es" content="Pagina de servicio profesional
  enfocada en el mantenimiento y reparacion de celulares en la ciudad de Bogota-Colombia.
  Servicio tecnico de moviles.">
  <META name="date" content="19:05:09, sábado 29, febrero 2020 -05">
  <meta name="generator" content="HTML-KIT 2.9" />
  <meta name="language" content="es" />
  <meta name="revisit-after" content="1 month" />
  <meta name="robots" content="index, follow" />
  <meta name="application-name" content="servicio tecnico web de reparacion de celulares" />
  <meta name="encoding" charset="utf-8" />
  <meta http-equiv=»X-UA-Compatible». />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
  <meta name="organization" content="Solumobil Company" />
  <meta name="revisit" content="7" />
  <noscript>
    <meta http-equiv="refresh" content="60; url=https://www.youtube.com/watch?v=XyW1XiNBsaQ">
  </noscript>

  <!----------------------------------------------------------------------------------->
  <!----------------------------------------------------------------------------------->
  <!--TERMINA AQUI --------------------------------------------------------------META-->
  <!----------------------------------------------------------------------------------->
  <!-- <link rel="stylesheet" href="assets/css/other.css"> -->
  <link rel="stylesheet" type="text/css" href="assets/css/login.css">

  <link rel="stylesheet" href="<?php echo ADMLTE_CSS; ?>">
  <!-- <link rel="stylesheet" href="assets\plugins\swal\dist\sweetalert2.min.css"> -->
  <link rel="stylesheet" href="assets/plugins/datatable/css/dataTables.bootstrap4.min.css">
  <!-- Bootstrap CSS-->
  <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="assets/css/normalize.css">

  <!-- FONTAWESOME -->
  <link rel="stylesheet" href="assets/font/css/all.css">

  <!-- Favicon -->
  <link rel="Shortcut Icon" href="assets/img/1.ico" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body class="hold-transition login-page" id="body">
  <div class="paleta loader">
    <img src="assets/img/1.ico" alt="Solumovil" width="95" height="95">
  </div>
  <!-- Jquery -->
  <script src="assets/js/jquery-1.11.2.min.js"></script>

  <!-- Bootstrap JS-->
  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>
  <script src="assets/js/modernizr.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/layouts/homeLayout.js"></script>


  <div class="login-box" id="content">
    <div class="login-logo">
      <a href="?c=home"><b>Solu</b>panel</a>
    </div>
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Estas a un paso de cambiar tu contraseña. Cambiala ahora</p>
        <div class="input-group mb-3">
          <input id="idUser" type="hidden" class="d-none" style="display: none;" value="<?php echo $id; ?>">
          <input id="pass" type="password" minlength="8" autofocus="autofocus" maxlength="25" placeholder="Ej: Test_20202" class="input_v form-control" autocomplete="true" required="true">
          <i class="fas fa-times-circle text-danger b1 inpStatusCP estado_oculto"></i>
          <i class="fas fa-check-circle text-success g1 inpStatusCP estado_oculto"></i>
          <p class="politica text-danger d-none text-left mb-0" id="rl_clave">Debe tener minimo 8 y maximo 25 caracteres, números, mayúsculas, minúsculas y algún caracter
            especial como: *?!@#$/(){}=.,;:</p>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input id="cpass" type="password" minlength="8" maxlength="25" class="input_v form-control" placeholder="Confirmar contraseña" autocomplete="true" required="true">
          <i class="fas fa-times-circle text-danger b2 inpStatusCP estado_oculto"></i>
          <i class="fas fa-check-circle text-success g2 inpStatusCP estado_oculto"></i>
          <p class="politica text-danger d-none text-left mb-0" id="rl_cclave">Las contraseñas no son iguales</p>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <p class="politica text-left mb-0">Deben tener minimo 8 y maximo 25 caracteres, números, mayúsculas, minúsculas y algún caracter
          especial como: *?!@#$/(){}=.,;: y deben ser iguales</p>
        <div class="alert alert-danger p-2 my-1 pt-1 m-auto col-12 justify-content-center d-none alert-dismissible fade show" id="alertChan" role="alert">
          <i class="fas fa-exclamation-triangle fa-sm"></i> <span class="text-sm-strict"> <span class="font-weight-bold">Error</span> Por favor completa todos los campos correctamente </span>
        </div>
        <div class="row mt-1">
          <div class="col-12">
            <button type="submit" id="newpass" class="btn btn-primary btn-block">Cambiar contraseña</button>
          </div>
        </div>

        <p class="mt-2 mb-1 text-center">
          <a href="?c=home&m=access">Iniciar Sesión</a>
        </p>
      </div>
    </div>
  </div>
  <script src="assets/js/layouts/getAndValidate.js"></script>
  <script src="assets/js/general/cpass.js"></script>