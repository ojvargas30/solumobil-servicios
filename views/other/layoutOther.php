<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Solupanel | Recuperar contraseña</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" lang="es" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="Author" lang="es" content="Óscar Javier Vargas Diaz, oscarjaviervargas@hotmail.com">
  <meta name="DC.identifier" lang="es" content="">
  <!--Aqui va la pagina SOLUMOBIL.............................-->
  <META http-equiv="Expires" lang="es" content="0">
  <!--ESTA NOSE PARA QUE ES.-->
  <meta name="Keywords" lang="es" content="Engativa,Colombia, Bogota,
	servicio tecnico, localidad, Quitar Cuenta Google,
	reparacion, celulares, pantalla, dañada, puerto de carga, tablets, baratos,
	flasheo de celulares o tablets, cambio de pantalla, cambio de repuestos.">
  <META http-equiv="PICS-Label" content='
	(PICS-1.1 "http://www.gcf.org/v2.5"
	labels on "1994.11.05T08:15-0500"
	until "1995.12.31T23:59-0000"
	for "http://w3.org/PICS/Overview.html"
	ratings (suds 0.5 density 0 color/hue 1))
 '>
  <!--Esto es para ayudar a los padres y a las escuelas a controlar los lugares a los
   que pueden acceder los niños en Internet, también facilita otros usos para las etiquetas,
  incluyendo firmas de código, privacidad, y gestión de los derechos de la propiedad
  intelectual.-->
  <META name="copyright" content="&copy; 2020 Solumobil Company.">
  <meta name="Description" lang="es" content="Pagina de servicio profesional
  enfocada en el mantenimiento y reparacion de celulares en la ciudad de Bogota-Colombia.
  Servicio tecnico de moviles.">
  <META name="date" content="19:05:09, sábado 29, febrero 2020 -05">
  <meta name="generator" content="HTML-KIT 2.9" />
  <meta name="language" content="es" />
  <meta name="revisit-after" content="1 month" />
  <meta name="robots" content="index, follow" />
  <meta name="application-name" content="servicio tecnico web de reparacion de celulares" />
  <meta name="encoding" charset="utf-8" />
  <meta http-equiv=»X-UA-Compatible». />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
  <meta name="organization" content="Solumobil Company" />
  <meta name="revisit" content="7" />
  <noscript>
    <meta http-equiv="refresh" content="60; url=https://www.youtube.com/watch?v=XyW1XiNBsaQ">
  </noscript>

  <!----------------------------------------------------------------------------------->
  <!----------------------------------------------------------------------------------->
  <!--TERMINA AQUI --------------------------------------------------------------META-->
  <!----------------------------------------------------------------------------------->
  <!-- <link rel="stylesheet" href="assets/css/other.css"> -->
  <link rel="stylesheet" type="text/css" href="assets/css/login.css">

  <link rel="stylesheet" href="<?php echo ADMLTE_CSS; ?>">
  <!-- <link rel="stylesheet" href="assets\plugins\swal\dist\sweetalert2.min.css"> -->
  <!-- <link rel="stylesheet" href="assets/plugins/datatable/css/dataTables.bootstrap4.min.css"> -->
  <!-- Bootstrap CSS-->
  <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css"> -->
  <!-- <link rel="stylesheet" href="assets/css/normalize.css"> -->
  <link rel="stylesheet" href="assets\plugins\toastr\toastr.min.css">

  <!-- FONTAWESOME -->
  <link rel="stylesheet" href="assets/font/css/all.css">

  <!-- Favicon -->
  <link rel="Shortcut Icon" href="assets/img/1.ico" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <script src='https://www.google.com/recaptcha/api.js' async defer></script>
 <script src='https://www.google.com/recaptcha/api.js?render=6LcjJj8aAAAAAGBe5M9f-pY1t9oH16hns2MJvF8m'></script>
  <!-- <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src='https://www.google.com/recaptcha/api.js?render=<?php //echo PUBLIC_KEY_RECAPTCHA; ?>'></script> -->
</head>

<body class="hold-transition login-page" id="body">
  <div class="paleta loader">
    <img src="assets/img/1.ico" alt="Solumobil" width="95" height="95">
  </div>

  <script src="assets/plugins/toastr/toastr.min.js"></script>
  <!-- <script src="assets/js/jquery.min.js"></script> -->

  <!-- Jquery -->
  <script src="assets/js/jquery-1.11.2.min.js"></script>

  <!-- Bootstrap JS-->
  <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script> -->
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>
  <script src="assets/js/modernizr.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

	<script src="assets/plugins/swal/dist/sweetalert2.all.min.js"></script>
	<script src="assets/js/general/methods.js"></script>
  <script src="assets/js/layouts/homeLayout.js"></script>