<script src="assets/js/layouts/funcs.js"></script>

<!-- <script src="assets/js/layouts/swals.js"></script> -->
<script>
    // VIDEO INSTRUCTIVO
    let video = document.getElementById("myVideo");
    let btn = document.getElementById("myBtn");

    function myFunction() {
        if (video.paused) {
            video.play();
            btn.innerHTML = "<i class='fas fa-pause'></i>";
        } else {
            video.pause();
            btn.innerHTML = "<i class='fas fa-play'></i>";
        }
    }
</script>
<footer class="main-footer">
    <strong>Derechos de autor &copy; 2019 - <?php echo date('Y'); ?> <a href="https://api.whatsapp.com/send?phone=573133043714&text=%C2%A1Hola!%20Quisiera%20saber%20mas%20acerca%20de%20los%20servicios%20y%20productos%20tecnologicos%20que%20ofrece" class="text-danger">Solumobil</a>.</strong>
    Todos los derechos reservados.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 2.0
    </div>
</footer>

</body>
</html>