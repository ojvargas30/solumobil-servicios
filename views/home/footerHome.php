
<div class="container-fluid w-100">
    <div class="flex-md-row-reverse w-100">
        <footer class="page-footer font-small py-2 footerHome w-100 border-right-0 border-left-0" id="foot">
            <a href="javascript:document.getElementById('foot').style.display='none';void0" class="a-hide-foot btn btn-danger"><i class="fas fa-eye"></i></a>
            <div class="container-fluid w-100">
                <div class="row">
                    <div class="col-12 col-sm-12 text-center">
                        <h4 class="h5 text-light">Síguenos en</h4>
                        <ul class="list-unstyled list-social-icons text-center row justify-content-center">
                            <li class="d-block col-sm-1 col-md-1">
                                <a href="https://www.facebook.com/SMQCgoogle/">
                                    <i class="fab fa-facebook fa-2x fb"></i>
                                </a>
                            </li>
                            <li class="d-block col-sm-1 col-md-1">
                                <a href="https://api.whatsapp.com/send?phone=573208557457&text=%C2%A1Hola!%20Quisiera%20saber%20mas%20acerca%20de%20los%20servicios%20y%20productos%20tecnologicos%20que%20ofrece">
                                    <i class="fab fa-whatsapp fa-2x wtp"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://goo.gl/maps/ogCXtfoq6sXqgs1b7">
                                    <i class="fas fa-map-marker-alt fa-2x map"></i>
                                </a>
                            </li>
                            <li class="d-block col-sm-1 col-md-1">
                                <a href="https://www.youtube.com/channel/UCSnYERmeZDilOz3ydEabDFA?view_as=subscriber">
                                    <i class="fab fa-youtube fa-2x yt"></i>
                                </a>
                            </li>
                        </ul>
                        <h6 class="h6 text-light"> &copy; 2019 - <?php echo date('Y') ?> Solumobil Todos los derechos reservados.</h6>
                        <p class="politica3">
                            <span class="politica3 text-white d-block text-break">NIT: 80115506-0 - Regimén simplificado</span>
                            <a href="views/home/login/politicaPrivacidad.php" class="text-danger d-inline text-break">Política de protección de datos personales y privacidad </a>
                            <!-- <a href="creditos.html" class="politica3 text-danger d-inline">Creditos</a> -->
                            <a href="tel:3133043714" class="text-blue d-block text-break">Desarrollado por Óscar V.</a> - <a href="views/home/creditos.php" target="_blank" class="text-blue d-inline text-break">Creditos</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
</body>

</html>