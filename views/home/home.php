<link rel="stylesheet" type="text/css" href="assets/css/slider/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/slider/chosen.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/slider/styleSlider.css">
<link rel="stylesheet" type="text/css" href="assets/css/slider/color-01.css">
<div class="content-page">
    <div class="cover-background row m-0 p-0 justify-content-center align-items-center align-middle" id="inicio">
        <div class="jumbotron align-self-center align-items-center justify-content-center text-center pb-0" style="width: 100%;">
            <h1 class="display-4 animated animate__tada animation animate__slower">Bienvenido a Solumobil</h1>
            <p class="lead animate__bounceInUp animated animate__slower text-uppercase">Servicio técnico de celulares y
                computadoras
            </p>
            <hr class="my-1 border-light">
            <p></p>
            <a href="https://www.whatsapp.com/catalog/573133043714/?app_absent=0" target="_blank" class="btnCool btnCool-3 btnCool-4 btn-danger bt-dash btn-lg text-sm border-light animated animate__tada animation animate__slower">
                <i class="fas fa-shopping-cart"></i> Catálogo
            </a>

            <a href="#location" target="_blank" class="btnCool-3 btn-secondary mx-3 my-1 bt-dash btn-lg border-light animated animate__tada animation animate__slower">
                <i class="fas fa-map-marker-alt"></i> Ubicación
            </a>

            <a href="?controller=home&method=register" target="_blank" class="btnCool-3 btn-danger bt-dash btn-lg border-light animated animate__tada animation animate__slower">
                <i class="fas fa-toolbox"></i> Servicios
            </a>

            <div class="mu-scrolldown-area py-4">
                <a href="#our-s" class="mu-scrolldown" id="mu-scrolldown"><i class="fa fa-chevron-down flecha_abajo" aria-hidden="true"></i></a>
            </div>
        </div>

    </div>

    <!--On Sale-->
    <div class="wrap-show-advance-info-box style-1 bg-white">
        <!-- has-countdown -->
        <h3 class="title-box bg-solumobil">
            <span class="pl-2">
                En Oferta
            </span>
        </h3>
        <!-- <div class="wrap-countdown mercado-countdown" data-expire="2020/12/12 12:34:56"></div> -->
        <div class="wrap-products slide-carousel owl-carousel style-nav-1 equal-container p-3" data-items="5" data-loop="false" data-nav="true" data-dots="false" data-responsive='{"0":{"items":"1"},"480":{"items":"2"},"768":{"items":"3"},"992":{"items":"4"},"1200":{"items":"5"}}'>

            <div class="product product-style-2 equal-elem ">
                <div class="product-thumnail">
                    <a href="https://www.facebook.com/marketplace/item/861799854368607/" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                        <figure><img loading="lazy" src="assets/img/productsOnSale/MACBOOK_AIR_M1_DE_512GB.jpg" width="220" height="220" alt="MACBOOK AIR M1 DE 512GB.jpg"></figure>
                    </a>
                    <div class="group-flash">
                        <span class="flash-item sale-label bg-solumobil">En oferta</span>
                    </div>
                    <div class="wrap-btn">
                        <a href="https://www.facebook.com/marketplace/item/861799854368607/" target="_blank" class="function-link">
                            Vista Rápida
                        </a>
                    </div>
                </div>
                <div class="product-info">
                    <a href="https://www.facebook.com/marketplace/item/861799854368607/" target="_blank" class="product-name">
                        <span>
                            MACBOOK AIR M1 DE 512GB
                        </span>
                    </a>
                    <div class="wrap-price">
                        <ins>
                            <p class="product-price">COP $5.499.990</p>
                        </ins>
                        <del>
                            <p class="product-price">COP $5.700.000</p>
                        </del>
                    </div>
                </div>
            </div>

            <div class="product product-style-2 equal-elem ">
                <div class="product-thumnail">
                    <a href="https://www.facebook.com/marketplace/item/166523838685574/" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                        <figure><img loading="lazy" src="assets/img/productsOnSale/APPLE_WATCH_SERIE_6_40MM.jpg" width="220" height="220" alt="APPLE WATCH SERIE 6 40MM"></figure>
                    </a>
                    <div class="group-flash">
                        <span class="flash-item sale-label bg-solumobil">En oferta</span>
                    </div>
                    <div class="wrap-btn">
                        <a href="https://www.facebook.com/marketplace/item/166523838685574/" target="_blank" class="function-link">
                            Vista Rápida
                        </a>
                    </div>
                </div>
                <div class="product-info">
                    <a href="https://www.facebook.com/marketplace/item/166523838685574/" target="_blank" class="product-name">
                        <span>
                            APPLE WATCH SERIE 6 40MM
                        </span>
                    </a>
                    <div class="wrap-price">
                        <ins>
                            <p class="product-price">COP $1.750.000</p>
                        </ins>
                        <del>
                            <p class="product-price">COP $1.850.000</p>
                        </del>
                    </div>
                </div>
            </div>

            <div class="product product-style-2 equal-elem ">
                <div class="product-thumnail">
                    <a href="https://www.facebook.com/marketplace/item/744066796270840/" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                        <figure>
                            <img loading="lazy" src="assets/img/productsOnSale/xiaomi-redmi-note-10.jpg" width="220" height="220" alt="XIAOMI NOTE 10.jpg">
                        </figure>
                    </a>
                    <div class="group-flash">
                        <span class="flash-item sale-label bg-solumobil">En oferta</span>
                    </div>
                    <div class="wrap-btn">
                        <a href="https://www.facebook.com/marketplace/item/744066796270840/" target="_blank" class="function-link">
                            Vista Rápida
                        </a>
                    </div>
                </div>
                <div class="product-info">
                    <a href="https://www.facebook.com/marketplace/item/744066796270840/" target="_blank" class="product-name">
                        <span>
                            XIAOMI REDMI NOTE 10 TRADICIONAL
                        </span>
                    </a>
                    <div class="wrap-price">
                        <ins>
                            <p class="product-price">COP $730.000</p>
                        </ins>
                        <del>
                            <p class="product-price">COP $840.000</p>
                        </del>
                    </div>
                </div>
            </div>

            <div class="product product-style-2 equal-elem ">
                <div class="product-thumnail">
                    <a href="https://www.facebook.com/marketplace/item/460477055239720/" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                        <figure><img loading="lazy" src="assets/img/productsOnSale/MOTO_e6s.jpg" width="220" height="220" alt="MOTO e6s.jpg"></figure>
                    </a>
                    <div class="group-flash">
                        <span class="flash-item sale-label bg-solumobil">En oferta</span>
                    </div>
                    <div class="wrap-btn">
                        <a href="https://www.facebook.com/marketplace/item/460477055239720/" target="_blank" class="function-link">
                            Vista Rápida
                        </a>
                    </div>
                </div>
                <div class="product-info">
                    <a href="https://www.facebook.com/marketplace/item/460477055239720/" target="_blank" class="product-name">
                        <span>
                            MOTO e6s [32GB - RAM 2GB]
                        </span>
                    </a>
                    <div class="wrap-price">
                        <ins>
                            <p class="product-price">COP $420.000</p>
                        </ins>
                        <del>
                            <p class="product-price">COP $460.000</p>
                        </del>
                    </div>
                </div>
            </div>

            <div class="product product-style-2 equal-elem ">
                <div class="product-thumnail">
                    <a href="https://articulo.mercadolibre.com.co/MCO-619351068-hyundai-e475-dual-sim-16-gb-oro-512-mb-ram-_JM" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                        <figure><img loading="lazy" src="assets/img/productsOnSale/hyundai_e475.jpg" width="220" height="220" alt="MOTO e6s.jpg"></figure>
                    </a>
                    <div class="group-flash">
                        <span class="flash-item sale-label bg-solumobil">En oferta</span>
                    </div>
                    <div class="wrap-btn">
                        <a href="https://articulo.mercadolibre.com.co/MCO-619351068-hyundai-e475-dual-sim-16-gb-oro-512-mb-ram-_JM" target="_blank" class="function-link">
                            Vista Rápida
                        </a>
                    </div>
                </div>
                <div class="product-info">
                    <a href="https://articulo.mercadolibre.com.co/MCO-619351068-hyundai-e475-dual-sim-16-gb-oro-512-mb-ram-_JM" target="_blank" class="product-name">
                        <span>
                            HYUNDAI E475 [16GB - 512MB RAM]
                        </span>
                    </a>
                    <div class="wrap-price">
                        <ins>
                            <p class="product-price">COP $205.000</p>
                        </ins>
                        <del>
                            <p class="product-price">COP $220.000</p>
                        </del>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <section class="section pb-3">

        <div class="wrap-main-slide">
            <div class="slide-carousel owl-carousel style-nav-1" data-items="1" data-loop="1" data-nav="true" data-dots="false">
                <div class="item-slide">
                    <img loading="lazy" src="assets/img/ads/adTech.jpg" alt="MI SMART BAND 5 COP $195.000" class="img-slide">
                    <div class="slide-info slide-1">
                        <h2 class="f-title">MI SMART BAND 5 <b>Relojes</b></h2>
                        <span class="subtitle">Compra todos tus productos Smart por internet.</span>
                        <p class="sale-info">Por tan solo: <span class="price">COP $195.000</span></p>
                        <a href="https://www.facebook.com/marketplace/item/531207391248203/" target="_blank" class="btn-link">Compra ahora</a>
                    </div>
                </div>
                <!-- <div class="item-slide">
                    <img src="assets/img/ads/adTech.jpg" alt="" class="img-slide">
                    <div class="slide-info slide-2">
                        <h2 class="f-title">Extra 25% Off</h2>
                        <span class="f-subtitle">On online payments</span>
                        <p class="discount-code">Use Code: #FA6868</p>
                        <h4 class="s-title">Get Free</h4>
                        <p class="s-subtitle">TRansparent Bra Straps</p>
                    </div>
                </div>
                <div class="item-slide">
                    <img src="assets/img/ads/adTech.jpg" alt="" class="img-slide">
                    <div class="slide-info slide-3">
                        <h2 class="f-title">Great Range of <b>Exclusive Furniture Packages</b></h2>
                        <span class="f-subtitle">Exclusive Furniture Packages to Suit every need.</span>
                        <p class="sale-info">Stating at: <b class="price">$225.00</b></p>
                        <a href="#" class="btn-link">Shop Now</a>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <section class="OS-phones section pb-3" id="our-s">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xs-12 text-center">
                    <h3 class="titulo-servicio text-danger col-12 col-xs-12 texto-borde-4 text-uppercase">
                        Algunos servicios que ofrecemos
                        <p class="h6 subs text-lowercase">
                            reparación de celulares, computadoras y dispositivos electrónicos
                        </p>
                    </h3>
                </div>
            </div>
            <div class="row ourServices ">
                <div class="col-md-4">
                    <div class="solu-area">
                        <div class="caja">
                            <div class="home-icon-services text-danger">
                                <i class="fa fa-tablet-alt hover-services"></i>
                            </div>
                            <div class="caja-body">
                                <div class="card mb-3" style="max-width: 540px; height:335px;">
                                    <div class="row no-gutters">
                                        <div class="col-md-12">
                                            <img loading="lazy" src="assets/img/service/1.jpg" height="170" class="card-img" alt="...">
                                            <div class="card-body">
                                                <h5 class="card-title">Tablet</h5>
                                                <p class="card-text font-peque">Mantenimiento y
                                                    optimizacion de su tablet.</p>
                                                <!-- <p class="card-text">
                                                    <small class="text-muted politica">Servicios de reparación y arreglo
                                                        tablets Colombia, reparación y
                                                        arreglo de tablets Iphone,Samsung Galaxy,
                                                        reparación y arreglo de tablets
                                                        Lenovo, Motorola, Huawei, Asus,HP,LG,
                                                        Toshiba, Reparación y arreglo
                                                        de tablets Dell, etc.
                                                    </small>
                                                </p> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="solu-area">
                        <div class="caja">
                            <div class="home-icon-services text-danger">
                                <i class="fa fa-mobile-alt hover-services"></i>
                            </div>
                            <div class="caja-body">
                                <div class="card mb-3" style="max-width: 540px; height:335px;">
                                    <div class="row no-gutters">
                                        <div class="col-md-12">
                                            <img loading="lazy" src="assets/img/service/11.png" height="170" class="card-img" alt="...">
                                            <div class="card-body">
                                                <h5 class="card-title">Celular</h5>
                                                <p class="card-text font-peque">Entendemos la importancia de tu
                                                    movil en tu vida
                                                    diaria y de
                                                    negocios; Por ello
                                                    atendemos tu urgencia técnica
                                                    de forma
                                                    inmediata.</p>
                                                <!-- <p class="card-text">
                                                    <small class="text-muted politica">Tomamos acción inmediata sobre
                                                        tus datos, Damos
                                                        prioridad a salvar
                                                        tu información, Contactos, fotos y videos. Componentes y repuestos para
                                                        cambio de ultima
                                                        generación y de
                                                        excelente calidad.
                                                    </small>
                                                </p> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="solu-area">
                        <div class="caja">
                            <div class="home-icon-services text-danger">
                                <i class="fa fa-laptop hover-services"></i>
                            </div>
                            <div class="caja-body">
                                <div class="card mb-3" style="max-width: 540px; height:335px;">
                                    <div class="row no-gutters">
                                        <div class="col-md-12">
                                            <img loading="lazy" src="assets/img/service/12.jpg" height="170" class="card-img-top" alt="...">
                                            <div class="card-body">
                                                <h5 class="card-title">Computadoras</h5>
                                                <p class="card-text font-peque">Reparación de PCs portatiles, Laptops, Notebook</p>
                                                <!-- <p class="card-text">
                                                    <small class="text-muted politica">Técnicos especializados en reparación y mantenimiento
                                                        de equipos de computo en bogotá. ofrecemos
                                                        servicios con las marcas dell, hp, lenovo,
                                                        toshiba, acer, asus, sony vaio, samsung y
                                                        apple
                                                    </small>
                                                </p> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <div class="solu-area">
                        <div class="caja">
                            <div class="home-icon-services text-danger">
                                <i class="fa fa-life-ring hover-services"></i>
                            </div>
                            <div class="caja-body">
                                <div class="card mb-3" style="max-width: 540px; height:335px;">
                                    <div class="row no-gutters">
                                        <div class="col-md-12">
                                            <img loading="lazy" src="assets/img/service/9.jpg" height="170" class="card-img-top" alt="...">
                                            <div class="card-body">
                                                <h5 class="card-title">Configuracion de equipos</h5>
                                                <p class="card-text font-peque">Reinstalaciones de Windows
                                                    (Formateos), instalación
                                                    de Windows y Office, Instalación y actualización de Antivirus.</p>
                                                <!-- <p class="card-text">
                                                    <small class="text-muted politica">Eliminación de Virus,
                                                        Soporte para Pc, configuración,
                                                        mantenimiento y actualizaciones de Software, Migraciones de datos.
                                                    </small>
                                                </p> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="solu-area">
                        <div class="caja">
                            <div class="home-icon-services text-danger">
                                <i class="fa fa-code hover-services"></i>
                            </div>
                            <div class="caja-body">
                                <div class="card mb-3" style="max-width: 540px; height:335px;">
                                    <div class="row no-gutters">
                                        <div class="col-md-12">
                                            <img loading="lazy" src="assets/img/service/10.jpg" height="170" class="card-img-top" alt="...">
                                            <div class="card-body">
                                                <h5 class="card-title">Tu sitio web</h5>
                                                <p class="card-text font-peque">Desarrollo y programación de páginas web
                                                    a través de CMS y a medida, Diseñamos tu
                                                    página web para que puedas tener presencia
                                                    en Internet.</p>
                                                <!-- <p class="card-text">
                                                    <small class="text-muted politica">Desarrollamos y programamos
                                                        tu página web a la medida para que puedas gestionas
                                                        los contenidos y modificarlos de una
                                                        forma sencilla.
                                                    </small>
                                                </p> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="solu-area">
                        <div class="caja">
                            <div class="home-icon-services text-danger">
                                <i class="fa fa-laptop-code hover-services">

                                </i>
                            </div>
                            <div class="caja-body">
                                <div class="card mb-3" style="max-width: 540px; height:335px;">
                                    <div class="row no-gutters">
                                        <div class="col-md-12">
                                            <img loading="lazy" src="assets/img/service/8.jpg" height="170" class="card-img-top" alt="...">
                                            <div class="card-body">
                                                <h5 class="card-title">Sistemas de información</h5>
                                                <p class="card-text font-peque">Desarrollo de sistemas de administración de la información de tu negocio</p>
                                                <!-- <p class="card-text">
                                                    <small class="text-muted politica">Análisis y desarrollo de sistemas
                                                        software 100% especializados
                                                        y de acuerdo a los requerimientos
                                                        del cliente
                                                    </small>
                                                </p> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
    <section class="parte-img py-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-6 text-center d-flex">
                    <p class="text-light text-uppercase texto-borde tamaño-texto col-12 col-sm-12">
                        Elige la buena atención y calidad en tu servicio con Solumobil
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="news-promo-content section pt-4" id="service">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xs-12 text-center">
                    <h3 class="titulo-servicio text-danger texto-borde-4 text-uppercase">Servicio técnico
                        <p class="h6 subs text-lowercase">
                            Cambio y mejora de repuestos, software y hardware
                        </p>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-lg-6 px-0 justify-content-center text-center border-right">
                    <h5 class="titulo-servicio-4 text-danger texto-borde-4">Reparación o cambio de
                        <u>
                            <em class="font-weight-bolder text-uppercase text-secondary">repuestos
                            </em>
                        </u>
                    </h5>
                    <div class="principal-container-services">
                        <a href="#!">
                            <p class="icon-service"><i class="fa fa-tablet-alt"></i></p>
                            <p class="tittle-service border-bottom pb-1">Cambio de pantalla</p>
                            <p class="description-service">
                                Pachas, displays, etc.
                            </p>
                        </a>
                        <a href="#!">
                            <p class="icon-service"><i class="fa fa-battery-empty"></i></p>
                            <p class="tittle-service border-bottom pb-1">Cambio de puerto de carga o plu</p>
                            <p class="description-service">
                                ¿No carga? ¡Tenemos la solución!
                            </p>
                        </a>
                        <a href="#!">
                            <p class="icon-service"><i class="fas fa-microphone"></i></p>
                            <p class="tittle-service border-bottom pb-1">Cambio de altavoz, microfono</p>
                            <p class="description-service">
                                Yo lo oigo pero ellos a mi no
                            </p>
                        </a>
                        <a href="#!">
                            <p class="icon-service"><i class="fas fa-camera"></i></p>
                            <p class="tittle-service border-bottom pb-1">Cambio de cámara y sensor</p>
                            <p class="description-service">
                                Cambio de sistema operativo
                            </p>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 px-0 justify-content-center text-center">
                    <h5 class="titulo-servicio-4 text-danger texto-borde-4">Reparación o cambio de
                        <u>
                            <em class="font-weight-bolder text-uppercase text-secondary">software</em>
                        </u>
                    </h5>

                    <div class="principal-container-services pl-2">
                        <a href="#!" class="pl-1">
                            <p class="icon-service"><i class="fab fa-google"></i></p>
                            <p class="tittle-service border-bottom pb-1">Quitar cuenta Google</p>
                            <p class="description-service">
                                Permitir uso del dispositivo
                            </p>
                        </a>

                        <a href="#!">
                            <p class="icon-service"><i class="fab fa-android"></i></p>
                            <p class="tittle-service border-bottom pb-1">Flasheo</p>
                            <p class="description-service">
                                Cambio de sistema operativo
                            </p>
                        </a>
                        <a href="#!">
                            <p class="icon-service"><i class="fas fa-hand-sparkles"></i></p>
                            <p class="tittle-service border-bottom pb-1">Hard reset</p>
                            <p class="description-service">
                                Restablecimiento de fabrica
                            </p>
                        </a>
                        <a href="#!">
                            <p class="icon-service"><i class="fas fa-stethoscope"></i></p>
                            <p class="tittle-service border-bottom pb-1">Optimización</p>
                            <p class="description-service">
                                Optimización de velocidad, corrección de errores, recuperación de datos, etc.
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start main content -->
    <main>
        <!-- Start About -->
        <section id="mu-about">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mu-about-area">
                            <!-- Start Feature Content -->
                            <div class="row">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="mu-about-left">
                                        <img loading="lazy" height="350" class="rounded rounded-pill" style="opacity: 0.5;" src="assets/img/us/t26.jpg" alt="img">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mu-about-right">
                                        <div class="row">
                                            <div class="col-md-4 pl-3 pt-4 ">
                                                <h2>Acerca de nosotros</h2>
                                                <p>Comprendiendo la necesidad de un servicio de calidad
                                                    ofrecemos servicio técnico de dispositivos móviles, electrónicos o
                                                    tecnológicos en su amplia variedad.
                                                </p>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <!-- Start Why Us -->
                                                        <section id="mu-why-us" style="padding: 0 !important;">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 mt-4 ">
                                                                        <div class="mu-why-us-area">
                                                                            <h2 clas>¿Porque nosotros?</h2>
                                                                            <div class="mu-why-us-content mt-3">
                                                                                <div class="row">
                                                                                    <div class="col-md-4">
                                                                                        <div class="mu-why-us-single">
                                                                                            <div class="my-why-us-single-icon d-flex align-items-center justify-content-center">
                                                                                                <i class="fa fa-business-time text-danger" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h3>Rápidez en el servicio</h3>
                                                                                            <!-- <p>Eficiente comprendiendo su tiempo</p> -->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                        <div class="mu-why-us-single">
                                                                                            <div class="my-why-us-single-icon d-flex align-items-center justify-content-center">
                                                                                                <i class="fa fa-thumbs-up text-danger" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h3>Calidad con solución</h3>
                                                                                            <!-- <p>Calidad en el repuesto y en el servicio</p> -->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                        <div class="mu-why-us-single">
                                                                                            <div class="my-why-us-single-icon d-flex align-items-center justify-content-center">
                                                                                                <i class="fa fa-home text-danger" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h3>Ventas de celulares</h3>
                                                                                            <!-- <p>Todas las marcas</p> -->
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-md-12 pt-5">
                                                <section id="mu-video" class="">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="mu-video-area p-0">
                                                                    <h2 class="text-dark">Reparación reciente</h2>
                                                                    <p class="mu-title-content">En este video vemos como reparamos el software de un celular
                                                                        marca Huawei</p>
                                                                    <div class="mu-video-content mt-0">
                                                                        <iframe width="400" height="220" src="https://www.youtube.com/embed/a4i8DCVe6kM" allowfullscreen></iframe>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Feature Content -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End About -->

        <section class="news-promo-content section">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-xs-12 text-center">
                        <p class="titulo-servicio text-danger texto-borde-4 text-uppercase title-home-responsive">Novedades</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 text-center">
                        <div class="content-dest">
                            <img loading="lazy" src="assets/img/us/t24.jpg" alt="Comprar cargadores, celualres xiaomi, samsung. cragadores originales" class="img-responsive center-box-content img-fluid">
                            <h3>Tienda</h3>
                            <p>Comprar celulares, smartwatch, macbook</p>
                            <p>
                                Procesos y opciones de compra de celulares, smartwatch, macbook
                            </p>
                            <a href="https://www.whatsapp.com/catalog/573133043714/?app_absent=0" target="_blank" class="btn btn-success">Catálogo</a>
                            <a href="https://solumobil.kyte.site/" target="_blank" class="btn btn-outline-success">Comprar</a>
                            <hr class="visible-xs">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-center">
                        <div class="content-dest">
                            <img loading="lazy" src="assets/img/us/t22.jpg" alt="Cotizar celulares" class="img-responsive center-box-content img-fluid">
                            <h3>Cotizar</h3>
                            <p>
                                Actualizaciones, Cotizaciones, Contacto, Ubicaciones, Galería, Quiénes Somos, Testimonios, Horario de Atención
                            </p>
                            <a href="https://solumovil-tigo.negocio.site/?m=true" target="_blank" class="btn btn-success">Cotizar</a>
                            <hr class="visible-xs">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-center">
                        <div class="content-dest">
                            <img loading="lazy" src="assets/img/home/nov.png" alt="sustitución de repuestos, formateo, recuperación de datos" class="img-responsive center-box-content img-fluid">
                            <h3 class="my-3">¡Arreglos a bajo costo!</h3>
                            <a href="https://api.whatsapp.com/send?phone=573208557457&text=%C2%A1Hola!%20Quisiera%20saber%20mas%20acerca%20de%20los%20servicios%20y%20productos%20tecnologicos%20que%20ofrece" class="btn btn-success">Enviar mensaje</a>
                            <hr class="visible-xs">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-center">
                        <div class="content-dest">
                            <img loading="lazy" src="assets/img/home/nov2.png" alt="Danos tu anterior dispositivo y obtén un gran descuento en el nuevo" class="img-responsive center-box-content img-fluid">
                            <h3 class="my-3">¡Queremos tu antiguo teléfono!</h3>
                            <a href="https://api.whatsapp.com/send?phone=573208557457&text=%C2%A1Hola!%20Quisiera%20saber%20mas%20acerca%20de%20los%20servicios%20y%20productos%20tecnologicos%20que%20ofrece" class="btn btn-success">Enviar mensaje</a>
                            <hr class="visible-xs">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <section class="news-promo-content section pt-4" id="location">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xs-12 text-center">
                    <p class="titulo-servicio text-danger texto-borde-4 text-uppercase title-home-responsive">Ubicación</p>
                </div>
            </div>
        </div>
        <div id="mu-google-map" class="bg-white">
            <iframe class="bg-white" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3976.3288175494304!2d-74.13979488523762!3d4.7128267965769455!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f834dd949a43f%3A0x1220eb0d864c1c52!2sSolumovil%20Servicio%20Tecnico!5e0!3m2!1ses!2sco!4v1593618475710!5m2!1ses!2sco" width="850" height="450" allowfullscreen></iframe>
        </div>
    </section>
    <!-- Start Contact -->
    <section id="mu-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-contact-area">
                        <h2>Contáctenos</h2>
                        <p>Daremos solución lo antes posible</p>
                        <!-- Start Contact Content -->
                        <div class="mu-contact-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mu-contact-form-area">
                                        <div id="form-messages"></div>
                                        <form id="ajax-contact" method="post" action="views/home/mail.php" class="mu-contact-form">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="Nombre" id="name" name="name" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="tel" class="form-control" placeholder="Teléfono" id="phone" name="tel" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="email" class="form-control" placeholder="Correo electrónico" id="email" name="email" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="Asunto" id="subject" name="subject" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="Mensaje" id="message" name="message" required></textarea>
                                            </div>
                                            <button type="submit" id="btnSend" class="mu-send-msg-btn btn-danger"><span>Envíar</span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
<script src="assets/js/slider/jquery-1.12.4.minb8ff.js?ver=1.12.4"></script>
<script src="assets/js/slider/jquery-ui-1.12.4.minb8ff.js?ver=1.12.4"></script>
<script src="assets/js/slider/jquery.flexslider.js"></script>
<script src="assets/js/slider/chosen.jquery.min.js"></script>
<script src="assets/js/slider/owl.carousel.min.js"></script>
<script src="assets/js/slider/jquery.countdown.min.js"></script>
<script src="assets/js/slider/jquery.sticky.js"></script>
<script src="assets/js/slider/functions.js"></script>
<!-- <script>
    owl-prev disabled
</script> -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>