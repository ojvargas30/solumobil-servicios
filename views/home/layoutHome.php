<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" lang="es" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="Author" lang="es" content="Óscar Javier Vargas Diaz, oscarjaviervargas@hotmail.com">
    <meta name="DC.identifier" lang="es" content="1000620103">
    <!--Aqui va la pagina Solumobil.............................-->
    <META http-equiv="Expires" lang="es" content="0">
    <!--ESTA NOSE PARA QUE ES.-->
    <meta name="Keywords" lang="es" content="Engativa - Bogotá -Colombia,
	servicio tecnico, localidad de Engativá, Quitar Cuenta Google, Venta de celulares,
    queremos tu teléfono viejo y te damos descuento en el nuevo
	reparacion, celulares, pantalla, dañada, puerto de carga, tablets, baratos,
	flasheo de celulares o tablets, cambio de pantalla, cambio de repuestos.">
    <META http-equiv="PICS-Label" content='(PICS-1.1 "http://www.gcf.org/v2.5"labels on "1994.11.05T08:15-0500"until "1995.12.31T23:59-0000"
	for "http://w3.org/PICS/Overview.html"ratings (suds 0.5 density 0 color/hue 1))'>
    <META name="copyright" content="&copy; 2020 Solumobil Company.">
    <meta name="Description" lang="es" content="Pagina de servicio profesional
	enfocada en el mantenimiento y reparacion de celulares en la ciudad de Bogotá-Colombia.
	Servicio tecnico de moviles.">
    <META name="date" content="19:05:09, sábado 29, febrero 2020 -05">
    <meta name="generator" content="HTML-KIT 2.9" />
    <meta name="language" content="es" />
    <meta name="revisit-after" content="1 month" />
    <meta name="robots" content="index, follow" />
    <meta name="application-name" content="Venta y Servicio técnico de celulares" />
    <meta name="encoding" charset="utf-8" />
    <meta http-equiv=»X-UA-Compatible». />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="organization" content="Solumobil Company" />
    <meta name="revisit" content="7" />
    <noscript>
        <meta http-equiv="refresh" content="30; url=https://www.youtube.com/watch?v=XyW1XiNBsaQ">
    </noscript>

    <!----------------------------------------------------------------------------------->
    <!--TERMINA AQUI --------------------------------------------------------------META-->

    <!-- Style -->
    <link rel="stylesheet" href="assets/css/homeStyle.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/font/css/all.css">

    <!-- SWAL -->
    <link rel="stylesheet" href="assets\plugins\swal\dist\sweetalert2.min.css">

    <!-- normalize -->
    <link rel="stylesheet" href="assets/css/normalize.css">

    <!-- Favicon -->
    <link rel="Shortcut Icon" href="assets/img/1.ico" />

    <link rel="stylesheet" href="assets\plugins\toastr\toastr.min.css">

    <!-- Theme color -->
    <!-- <link id="switcher" href="assets/plugins/theme-color/Solumobil-theme.css" rel="stylesheet"> -->

    <!-- ANIMATE.CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.compat.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.min.css">

    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '8874321c93f5eb8fa5692d5ebdb71746326098c8';
        window.smartsupp || (function(d) {
            var s, c, o = smartsupp = function() {
                o._.push(arguments)
            };
            o._ = [];
            s = d.getElementsByTagName('script')[0];
            c = d.createElement('script');
            c.type = 'text/javascript';
            c.charset = 'utf-8';
            c.async = true;
            c.src = 'https://www.smartsuppchat.com/loader.js?';
            s.parentNode.insertBefore(c, s);
        })(document);
    </script>

    <script data-ad-client="ca-pub-4415779182320006" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <title>Solumobil</title>
</head>

<body>
    <a id="back-to-top" href="#top" class="btn btn-default shadow d-none rounded-circle border text-lightred back-to-top-2" role="button" aria-label="Scroll to top">
        <i class="fas fa-angle-double-up fa-sm"></i>
    </a>
    <div>
        <form action="" class="formWhatsapp p-3 d-none form-group" id="formWhatsapp">
            <h6 class="">Nuevo mensaje</h6>
            <label for="name">Nombre:</label>
            <input type="text" id="name" class="form-control" placeholder="Nombre">
            <label for="msg">Mensaje:</label>
            <textarea class="form-control" id="msg" cols="25" rows="5" placeholder="Mensaje"></textarea>
            <button type="button" class="btn btn-success text-center mt-2 btn-sm col-12" id="send">
                Enviar mensaje
            </button>
        </form>
        <img src="assets/img/home/whatsapp.png" class="whatMsg img-circle rounded-circle img-fluid position-fixed" alt="Enviar mensaje" id="btnWhatsapp">
    </div>

    <!-- oncontextmenu="return false" -->
    <div class="page-container bg-danger" id="top">

        <nav class="site-header sticky-top setting navUniversal">
            <div class="d-flex bg-light justify-content-between border-bottom border">
                <ul class="list-unstyled setting navigation-list text-center pt-2">
                    <li class="mr-5 pr-5">
                        <div class="d-flex justify-content-center text-center align-items-center ">
                            <a href="javascript:urlRandom()" class="d-md-inline-block text-decoration-none">
                                <img src="assets/img/1.ico" width="50" height="48" class="pb-1 animate__animated animate__rotateIn" alt="Logo Solumobil">
                            </a>
                            <h5 class="titulo-servicio2" href="#">Solumobil</h5>
                        </div>
                    </li>

                    <li class="px-3">
                        <a class="py-2 font-weight-bold d-none d-md-inline-block" target="_blank" href="https://solumobil.kyte.site/">
                            <span class="fas fa-shopping-cart"></span> Tienda
                        </a>
                    </li>
                    <li class="px-3">
                        <a class="py-2 font-weight-bold d-none d-md-inline-block" target="_blank" href="https://solumovil-tigo.negocio.site/?m=true">
                            <span class="fas fa-tools"></span> Cotizar
                        </a>
                    </li>

                    <li class="px-3">
                        <a class="py-2 font-weight-bold d-none d-md-inline-block animate__animated animate__bounce animate__fast" href="tel:+573208557457">
                            <span class="fas fa-phone"></span> Llamar
                        </a>
                    </li>

                    <li class="px-3">
                        <a class="py-2 font-weight-bold d-none d-md-inline-block" href="#mu-about">
                            <span class="fas fa-info-circle"></span> Acerca de
                        </a>
                    </li>

                    <!-- <li class="">
                        <a class="acceder-btn-crear py-2 btn btn-outline-secondary d-none d-md-inline-block bk" id="register" href="?c=home&m=register">Cuenta nueva</a>
                    </li>
                    <li class="mt-2">
                        <a class="acceder-btn blanco-obligatorio py-2 btn btn-danger d-md-inline-block" href="?c=home&m=access">Entrar</a>
                    </li> -->
                </ul>
                <div class="d-lg-none d-md-none d-sm-block">
                    <i class="fas fa-bars fa-2x text-danger btn-mobile"></i>
                </div>
            </div>
        </nav>


        <noscript>
            <div class="position-fixed col-4 justify-content-md-start bg-danger rounded ml-3">
                <p class="text-light h6">Bienvenido al portal Solumobil</p>
                <p class="text-light h6">La página que estás
                    viendo requiere para su funcionamiento el uso de JavaScript.
                    Si lo has deshabilitado intencionalmente, por favor vuelve a activarlo.</p>
                <p class="text-light">Se te redigira a un tutorial en 30 segundos</p>
            </div>
        </noscript>

        <script>
            // Visita RANDOM
            let urls = [
                "https://www.facebook.com/SMQCgoogle/",
                "https://api.whatsapp.com/send?phone=573133043714&text=%C2%A1Hola!%20Quisiera%20saber%20mas%20acerca%20de%20los%20servicios%20y%20productos%20tecnologicos%20que%20ofrece",
                "https://www.google.com/maps/place/Solumobil+Servicio+Tecnico/@4.7128321,-74.1397949,17z/data=!3m1!4b1!4m5!3m4!1s0x8e3f834dd949a43f:0x1220eb0d864c1c52!8m2!3d4.7128268!4d-74.1376062?shorturl=1",
                "https://www.youtube.com/channel/UCSnYERmeZDilOz3ydEabDFA?view_as=subscriber",
                "https://solumobil.kyte.site/",
                "https://solumovil-tigo.negocio.site/?m=true"
            ];

            function urlRandom() {
                iRandom = Math.floor(Math.random() * urls.length);
                window.location = urls[iRandom];
            }

            /*EFECTO DEL FOOTER OCULTO*/
            function ocultar() {
                document.getElementById('foot').style.display = 'none';
            }
        </script>
        <script src="assets/plugins/toastr/toastr.min.js"></script>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>

        <!-- SWEETALERT -->
        <script src="assets/plugins/swal/dist/sweetalert2.all.min.js"></script>

        <!-- Slick -->
        <!-- <script src="assets/plugins/slick/slick.min.js"></script> -->
        <script src="assets/js/layouts/homeLayout.js"></script>
        <script>
            $(function() {
                $('a[href*=#]').click(function() {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                        location.hostname == this.hostname) {
                        var $target = $(this.hash);
                        $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
                        if ($target.length) {
                            var targetOffset = $target.offset().top;
                            $('html,body').animate({
                                scrollTop: targetOffset
                            }, 2000);
                            return false;
                        }
                    }
                });
            });
        </script>

        <!-- CLIENGO -->
        <!-- Código de instalación Cliengo para adventurejavis@gmail.com -->
        <script type="text/javascript">
            (function() {
                var ldk = document.createElement('script');
                ldk.type = 'text/javascript';
                ldk.async = true;
                ldk.src = 'https://s.cliengo.com/weboptimizer/60669c0caf2c30002a614c9b/60669c0eaf2c30002a614c9e.js?platform=registration';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ldk, s);
            })();
        </script>
        <!-- END CLIENGO -->

        <script>
            document.addEventListener('DOMContentLoaded', () => {
                var flagChat = true;

                if (flagChat) {
                    setInterval(() => {
                        let chcl = document.getElementById('chatIframe');

                        if (chcl) {
                            chcl.classList.remove('collapsed-height')
                            chcl.classList.add('cliengoChat')

                            flagChat = false
                        }

                    }, 500)
                }
            })
        </script>