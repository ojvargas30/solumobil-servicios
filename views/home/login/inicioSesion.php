<body id="body">
  <div class="paleta loader">
    <img src="assets/img/1.ico" alt="Solumobil" width="95" height="95">
  </div>

  <div id="content" class="form cont justify-content-center text-center">
    <div class="f2 sign-in pt-5">
      <div class="text-center pb-3">
        <div class="row justify-content-center text-center">
          <div class="col-6 d-flex justify-content-center text-center border-bottom pb-1 align-items-center align-middle">
            <a href="?controller=home">
              <img src="assets/img/logo.png" title="Volver" class="imagen-logo" alt="logo sm">
            </a>
            <h5 class="titulo-servicio2 text-center">
              Solumobil
            </h5>
          </div>
        </div>
        <h2 class="pt-3 titulo-servicio2">Iniciar sesión</h2>
      </div>
      <?php if (isset($error['errorMessage'])) { ?>
        <div class="alert-danger alert-dismissable text-center alert-width col-12" role="alert">
          <button class="close" data-dismiss="alert">&times;</button>
          <p class="text-dark p-2"><?php echo $error['errorMessage']; ?></p>
        </div>
      <?php } ?>

      <!-- Inicio de sesión -->
      <!-- <form action="?controller=login&method=login" method="post"> -->
      <div class="row justify-content-center mb-1">
        <div class="col-12">
          <label class="pb-0 my-0 text-left">
            <span class="">Correo electrónico</span>
            <input id="correoLogin" placeholder="Correo electrónico" name="correo" type="email" minlength="5" maxlength="320" class="input_v_login" value="<?php echo isset($error['correo']) ? $error['correo'] : '' ?>" required class="copypasteno" autofocus autocomplete="on">
            <i class="fas fa-times-circle text-danger bad7 input_estado_8 estado_oculto"></i>
            <i class="fas fa-check-circle text-success good7 input_estado_11 estado_oculto"></i>
            <p class="politica text-danger d-none text-left mb-0" id="regla_correo_login">El correo debe ser real y sin espacios</p>
          </label>
        </div>
        <div class="col-12 pb-3">
          <label class="pt-0 mt-3 text-left">
            <span>Contraseña</span>
            <div class="input-group">
              <input type="password" autocomplete="on" name="clave" id="passwordLogin" minlength="8" maxlength="25" placeholder="Contraseña" class="input_v_login" required class="copypasteno">
              <div class="input-group-append  d-none">
                <i class="btn p-0 m-0 fas fa-eye fa-sm pl-1 mt-2" id="btnSHPass"></i>
              </div>
              <i class="fas fa-times-circle text-danger bad8 input_estado_10 estado_oculto"></i>
              <i class="fas fa-check-circle text-success good8 input_estado_9 estado_oculto"></i>
              <p class="politica text-danger d-none text-left mb-0 mr-4" id="regla_clave_login">Debe tener minimo 8 y maximo 25 caracteres, números, mayúsculas, minúsculas y algún caracter
                especial como: *?!@#$/(){}=.,;:</p>
            </div>
          </label>
        </div>
      </div>
      <div class="alert alert-danger p-2 pt-1 w-75 m-auto col-12 justify-content-center mt-0 d-none alert-dismissible fade show" id="alertaErrorLogin" role="alert">
        <i class="fas fa-exclamation-triangle fa-sm"></i> <span class="text-sm-strict"> <strong>Error</strong> Por favor completa todos los campos correctamente </span>
      </div>
      <div class="my-2"></div>
      <button type="submit" class="button w-50 gina m-auto titulo-servicio2" style="margin-bottom: 0.5rem !important;" id="accessIngresarUser">Ingresar
        <div class="side-top-bottom"></div>
        <div class="side-left-right"></div>
      </button>
      <a href="?c=home&m=recoveryAccount" class="forgot-pass display-6">¿Olvidaste tu contraseña?</a>
      <div class="row justify-content-center text-center">
        <div class="col-md-6 col-sm-5">
          <a class="btn btn-default border shadow d-block mt-2" id="loginWithGoogle" href='<?php echo $authUrl2; ?>'>
            <img src="assets/img/home/google.svg" width="25">
            Ingresar con Google
          </a>
        </div>
      </div>

      <!-- <button type="button" class="fb-btn">Connect with <span>facebook</span></button> -->
      <!-- </form> -->
    </div>
    <div class="sub-cont titulo-servicio2">
      <div class="img">
        <div class="img__text m--up">
          <h2>¿Nuevo aquí?</h2>
          <p>Registrate y descubre la solución a tus problemas</p>
        </div>
        <div class="img__text m--in">
          <h2>¿Uno de los nuestros?</h2>
          <p>Si ya tienes una cuenta, solo tienes que ingresar. ¡Te extrañabamos!</p>
        </div>
        <div class="img__btn">
          <span class="m--up registrarse" style="font-size: 13px !important;">Registrarse</span>
          <span class="m--in">Acceder</span>
        </div>
      </div>
      <div class="f2 sign-up text-center justify-content-center pt-0 mt-3">
        <div class="row justify-content-center text-center pt-0 mt-0">
          <div class="col-6 d-flex justify-content-center pt-4 mt-0 text-center border-bottom pb-1 align-items-center align-middle">
            <a href="?c=home">
              <img src="assets/img/logo.png" title="Volver" class="imagen-logo" alt="logo sm">
            </a>
            <h5 class="titulo-servicio2 text-center">
              Solumobil
            </h5>
          </div>
        </div>
        <!-- <p class="p-0 m-0 h4 titulo-servicio2">Crea tu cuenta</p>pt-5 titulo-servicio2 -->
        <h4 class="titulo-servicio2 mt-3 mb-0 pb-0">Crear cuenta</h4>

        <!-- Registro -->
        <form id="registerForm" action="" class="form border-0" method="post">
          <div id="error" class="justify-content-center col-12 ml-2 alert alert-danger esconder" role="alert">
            <span class="text-sm p-1">
              La contraseña de verificación no coincide. ¡Vuelve a intentar!
            </span>
          </div>
          <div id="ok" class="justify-content-center col-12 alert alert-success esconder text-sm p-1" role="alert">
            Verifica tu usuario en la bandeja de tu correo(Procesando formulario...)
          </div>
          <div class="row">
            <div class="col-6">
              <div class="row justify-content-center mb-0 pb-0">
                <!-- <div class="col-12">
                  <label for="nombre_cliente" class="mt-0 pt-0 text-left position-relative">
                    <span class="">Nombre</span>
                    <input id="nombre_cliente" type="text" name="name" autofocus tabindex="1" minlength="1" maxlength="35" class="focus input_v" required="true" placeholder="Ingrese su nombre">
                    <i class="fas fa-times-circle text-danger bad input_estado_1 estado_oculto"></i>
                    <i class="fas fa-check-circle text-success good input_estado_2 estado_oculto"></i>
                    <p class="politica text-danger d-none text-left mb-0" id="regla_nombre">Debe tener maximo 35 caracteres alfanuméricos y sin espacios injustificados</p>
                  </label>
                </div> -->
                <div class="col-12">
                  <label for="correo" class="mt-0 pt-0 text-left">
                    <span class="">Correo electrónico</span>
                    <input id="correo" type="email" minlength="5" name="email" tabindex="3" placeholder="Ej: clau234@gmail.com" maxlength="320" class="input_v" required="">
                    <i class="fas fa-times-circle text-danger bad2 input_estado_3 estado_oculto"></i>
                    <i class="fas fa-check-circle text-success good2 input_estado_4 estado_oculto"></i>
                    <p class="politica text-danger d-none text-left mb-0" id="regla_correo">El correo debe ser real y sin espacios</p>
                  </label>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="row justify-content-center mb-0 pb-0">
                <!-- <div class="col-12">
                  <label for="apellido_cliente" class="mt-0 pt-0 text-left">
                    <span class="">Apellidos</span>
                    <input id="apellido_cliente" type="text" tabindex="2" minlength="1" maxlength="35" class="input_v" required="true" placeholder="Ingrese sus apellidos">
                    <i class="fas fa-times-circle text-danger bad3 input_estado_7 estado_oculto"></i>
                    <i class="fas fa-check-circle text-success good3 input_estado_4 estado_oculto"></i>
                    <p class="politica text-danger d-none text-left mb-0" id="regla_apellido">Debe tener maximo 35 caracteres alfanuméricos y sin espacios injustificados</p>
                  </label>
                </div> -->
                <div class="col-12">
                  <label for="emailConfirm" class="mt-0 pt-0 text-left">
                    <span class="">Confirmar correo electrónico</span>
                    <input placeholder="Confirme su correo" id="emailConfirm" type="email" tabindex="4" minlength="5" maxlength="320" class="input_v" required="">
                    <i class="fas fa-times-circle text-danger bad4 input_estado_3 estado_oculto"></i>
                    <i class="fas fa-check-circle text-success good4 input_estado_4 estado_oculto"></i>
                    <p class="politica d-none text-danger text-left mb-0" id="regla_ccorreo">Los correos deben ser iguales</p>
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-6">
              <div class="row justify-content-center mb-0 pb-0">
                <div class="col-12">
                  <label for="pass" class="mt-0 pt-1 text-left">
                    <span>Contraseña</span>
                    <input type="password" autocomplete="on" id="pass" minlength="8" tabindex="5" maxlength="25" placeholder="Ej: Test_20202" class="input_v" required>
                    <div class="input-group-append   d-none">
                      <i class="btn p-0 m-0 fas fa-eye fa-sm pl-1 mt-2" id="btnSHRegisterPass"></i>
                    </div>
                    <i class="fas fa-times-circle text-danger bad5 input_estado_12 estado_oculto"></i>
                    <i class="fas fa-check-circle text-success good5 input_estado_4 estado_oculto"></i>
                    <p class="politica text-danger d-none text-left mb-0" id="regla_clave">Debe tener minimo 8 y maximo 25 caracteres, números, mayúsculas, minúsculas y algún caracter
                      especial como: *?!@#$/(){}=.,;:</p>
                  </label>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="row justify-content-center pb-0">
                <div class="col-12">
                  <label for="passConfirm" class="mt-0 pt-1 text-left">
                    <span>Confirmar contraseña</span>
                    <input type="password" autocomplete="on" id="passConfirm" minlength="8" tabindex="6" maxlength="25" class="input_v" required placeholder="Confirmar contraseña">
                    <div class="input-group-append   d-none">
                      <i class="btn p-0 m-0 fas fa-eye fa-sm pl-1 mt-2" id="btnSHRegisterConfirmPass"></i>
                    </div>
                    <i class="fas fa-times-circle text-danger bad6 input_estado_12 estado_oculto"></i>
                    <i class="fas fa-check-circle text-success good6 input_estado_4 estado_oculto"></i>
                    <p class="politica text-danger d-none text-left mb-0" id="regla_cclave">Las contraseñas no son iguales</p>
                  </label>
                </div>
              </div>
            </div>
            <div class="col-md-12 my-2 d-flex justify-content-center text-center py-2">
              <div class="g-recaptcha" data-sitekey="6LcjJj8aAAAAAGBe5M9f-pY1t9oH16hns2MJvF8m"></div>
              <!-- <div class="g-recaptcha" data-sitekey="<?php //echo PUBLIC_KEY_RECAPTCHA;
                                                          ?>"></div> -->
            </div>
          </div>
          <!-- <input type="hidden" class="d-none" name="recaptcha_response" id="recaptchaResponse"> -->
          <!-- </form> -->
          <div class="row pt-0 pb-0 mb-0 mt-5 justify-content-center text-center">
            <div class="col-12 my-1 pt-0">
              <p class="politica3 text-center pb-0 mb-0">Al registrarte,
                aceptas nuestros terminos y condiciones
                <a href="views/home/login/pp.php"> Politica de privacidad</a>,
                la Política de datos y la Política de
                cookies.
              </p>
            </div>
          </div>
          <div class="alert alert-danger mt-0 d-none alert-dismissible fade show" id="alertaError" role="alert">
            <span class="text-sm politica">
              <i class="fas fa-exclamation-triangle"></i>
              <strong>Error</strong>
              Por favor completa todos los campos correctamente
            </span>
          </div>
          <!-- <div class="g-recaptcha" data-sitekey="6LdAktgZAAAAAKjhdGQIlR_PmRKpAYbJPb8V3W8p"></div> -->
          <!-- <div class="g-recaptcha" data-sitekey="<?php //echo PUBLIC_KEY_RECAPTCHA;
                                                      ?>"></div> -->
          <div class="row justify-content-center mt-0 pt-0 text-center">
            <div class="col-md-6 mt-0 pt-0 col-sm-6">
              <button type="submit" id="registro" class="button m-auto">
                Registrarse
                <div class="side-top-bottom"></div>
                <div class="side-left-right"></div>
              </button>
            </div>
            <div class="col-md-6 mt-0 pt-0 col-sm-6">
              <a class="btn btn-default border shadow d-block" href='<?php echo $authUrl; ?>'><img src="assets/img/home/google.svg" width="25">Registrarse con Google</a>
            </div>
          </div>
      </div>
    </div>
  </div>
  </form>
  </div>
</body>

</html>

<script>
  // ESTILO DE CAMBIO ENTRE INGRESO Y REGISTRO EN LOGIN HOME
  document.querySelector('.img__btn').addEventListener('click', function() {
    document.querySelector('.cont').classList.toggle('s--signup');
  });
</script>

<script>
  // Ojo
  let btnSHPass = ele('btnSHPass'),
    btnSHRegisterPass = ele('btnSHRegisterPass'),
    btnSHRegisterConfirmPass = ele('btnSHRegisterConfirmPass'),
    passwordLoginInput = ele('passwordLogin');
  passwordRegisterInput = ele('pass');
  passwordRegisterConfirmInput = ele('passConfirm');

  const showBtnSHPass = (input, btn) => {
    input.classList.add('w-90');
    btn.parentNode.classList.remove('d-none');
    btn.parentNode.classList.add('d-inline-flex');
  }

  function eyeSlashInput(btnEye, input) {
    eve(btnEye, () => {
      if (btnEye.classList.contains('fa-eye-slash')) {
        btnEye.classList.remove('fa-eye-slash')
        btnEye.classList.add('fa-eye')
        input.type = 'password'

      } else {
        btnEye.classList.remove('fa-eye')
        btnEye.classList.add('fa-eye-slash')
        input.type = 'text'
      }
    })
  }

  eyeSlashInput(btnSHPass, passwordLoginInput)
  eyeSlashInput(btnSHRegisterPass, passwordRegisterInput)
  eyeSlashInput(btnSHRegisterConfirmPass, passwordRegisterConfirmInput)

  eveFull(passwordLoginInput, () => showBtnSHPass(passwordLoginInput, btnSHPass))
  eveFull(passwordRegisterInput, () => showBtnSHPass(passwordRegisterInput, btnSHRegisterPass))
  eveFull(passwordRegisterConfirmInput, () => showBtnSHPass(passwordRegisterConfirmInput, btnSHRegisterConfirmPass))
</script>

<script src="assets/js/layouts/tostada.js"></script>
<script src="assets/js/layouts/getAndValidate.js"></script>
<script src="assets/js/general/register.js"></script>