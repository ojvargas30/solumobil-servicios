<div class="row mb-5 mx-1 h-100 ">
    <div class="col-md-12 mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-success overflow-auto">
                    <div class="card-header">
                        <h3 class="card-title">Solicitudes de servicio técnico activas</h3>

                        <div class="card-tools">
                            <!-- <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="widgets.html" data-source-selector="#card-refresh-content" data-load-on-init="false"><i class="fas fa-sync-alt"></i></button> -->
                            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="card p-1 px-2 carta-modo-osc">
                            <h5 class="shadow card-header fondo-titulo text-uppercase text-dark pt-3 text-center mb-2 mt-1">
                                Servicios <img src="https://img.icons8.com/emoji/48/000000/star-struck.png" />
                                <small>
                                    <p class="text-secondary text-sm">En proceso y en espera para su solución</p>
                                </small>
                            </h5>
                            <a href="?controller=service&method=add" class="btn btn-success rounded-pill buttonAdd toastrDefaultInfo mb-3 text-center mt-1 align-self-center" data-toggle="modal" data-target="#myModalLeft">
                                <i class="fas fa-folder-plus fa-2x"></i>
                            </a>

                            <table class="shadow table listado border-dark border-left pr-0
                                        table-bordered table-active
                                         table-hover table-sm table-responsive
                                         datatable">
                                <thead class="title-medium text-dark cabeza-table pr-0">
                                    <tr class="pr-0">
                                        <th class="head-hover colIdServicio">
                                            <small class="font-weight-bold cabeza-table colIdServicio"><small>
                                        </th>
                                        <th scope="col" class="head-hover tcliente">
                                            <small class="font-weight-bold cabeza-table">
                                                <i class="fas fa-user-circle"></i> Cliente
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover testado colIdServicio">
                                            <small class="font-weight-bold cabeza-table">
                                                <i class="fas fa-shield-alt fa-xs pr-0 pb-2 pl-1"></i> Estado
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover tpeticion">
                                            <small class="font-weight-bold cabeza-table">
                                                <i class="fas fa-calendar-alt fa-xs"></i> Petición
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover tpeticion">
                                            <small class="font-weight-bold cabeza-table" title="Fecha de encuentro definida">
                                                <i class="fas fa-calendar-alt fa-xs"></i> Citación
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover tpeticion">
                                            <small class="font-weight-bold cabeza-table" title="Lugar - Dirección de encuentro">
                                                <i class="fas fa-map-marker-alt fa-xs"></i> Citación
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover ttotal">
                                            <small class="font-weight-bold cabeza-table">
                                                <i class="fas fa-dollar-sign fa-xs"></i>
                                                Total<small>
                                        </th>
                                        <th scope="col" class="head-hover ttotal colIdServicio">
                                            <small class="font-weight-bold cabeza-table float-left pl-1 pr-3">
                                                <i class="fas fa-info-circle fa-xs pr-0 pb-2"></i>
                                                Más
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover colIdServicio pr-0">
                                            <small class="font-weight-bold cabeza-table pr-4">
                                                <i class="fas fa-file-signature fa-xs pr-0 pl-2 pb-2"></i>
                                                Acciones<small>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="cuerpo-table pr-0">
                                    <?php $cont = 0; //echo count($services);
                                    ?>
                                    <?php foreach ($services as $service) : ?>
                                        <?php $cont++; ?>
                                        <tr class="shadow-sm text-dark" <?= (count($services) === $cont) ? "style='background:#fcfcfc'" : "" ?>>
                                            <td class="align-middle text-wrap colIdServicio texto-negro text-center">
                                                <span class="d-block h4 colIdServicio">
                                                    <?php echo $service->id_servicio_PK; ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-center texto-negro tcliente">
                                                <span class="d-block">
                                                    <h6 class="card-title text h6">
                                                        <?php if ($client->foto == null) { ?>

                                                            <img src="assets/img/clients/sinfoto.png" class="rounded-circle img-perfil" alt="foto del cliente :)" />

                                                        <?php } elseif ($client->foto != null) { ?>

                                                            <img src="<?php echo $client->foto; ?>" class="rounded-circle img-perfil" alt="foto del cliente :)" />

                                                        <?php } ?>

                                                        <span class="font-weight-bolder d-block"><?php echo $service->nombre_cliente ?></span>

                                                    </h6>
                                                    <span class="font-weight-bolder text-sm"><?php echo $service->num_id_cli ?></span>

                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap texto-negro testado">
                                                <span class="d-block">
                                                    <h6 class="card-title text-center float-none text-uppercase h5">

                                                        <?php switch ($service->id_estado_servicio_FK) {
                                                            case 1:
                                                        ?>
                                                                <span class="right badge badge-success"><?php echo $service->nombre_estado;
                                                                                                        break; ?></span>
                                                            <?php
                                                            case 2: ?>
                                                                <span class="right badge badge-info"><?php echo $service->nombre_estado;
                                                                                                        break; ?></span>
                                                            <?php
                                                            case 3: ?>
                                                                <span class="right badge badge-warning"><?php echo $service->nombre_estado;
                                                                                                        break; ?></span>
                                                            <?php
                                                            case 4: ?>
                                                                <span class="right badge badge-danger"><?php echo $service->nombre_estado;
                                                                                                        break; ?></span>

                                                        <?php } ?>
                                                    </h6>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-center texto-negro tpeticion">
                                                <span class="d-block">
                                                    <h6 class="card-title text h6">
                                                        <?php
                                                        // FORMATEAR FECHA
                                                        // $date = new DateTime($service->fecha_peticion);

                                                        // setlocale(LC_TIME, "es_CO");
                                                        // echo $date->format('l-j-F-Y');
                                                        // 2.
                                                        // $fecha = $service->fecha_peticion;

                                                        // $fecha_pe = str_replace('-', '/', strftime('%l/%j/%F/%Y',strtotime($fecha)));

                                                        // echo $fecha_pe;
                                                        // 3.intento
                                                        // $fecha = $_REQUEST['fecha_peticion'];
                                                        // $fecha = $_REQUEST[$service->fecha_peticion];
                                                        // $fecha = strtoupper(strftime("%B", mktime(0, 0, 0, $fecha)));
                                                        //


                                                        //METODO DEFINITIVO
                                                        //   FECHA
                                                        $fecha = $service->fecha_peticion;
                                                        echo ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
                                                        ?>
                                                        <h6 class="card-title text h5 float-none">
                                                            <em>
                                                                <!-- HORA -->
                                                                <?php

                                                                $time = new DateTime($service->hora_peticion);
                                                                echo $time->format('g:ia');

                                                                ?>
                                                            </em>
                                                        </h6>
                                                    </h6>
                                                </span>
                                            </td>

                                            <?php foreach ($details as $det) : ?>
                                                <?php if ($service->id_servicio_PK == $det->id_servicio_FK) { ?>
                                                    <td class="align-middle text-wrap text-center texto-negro ttotal">
                                                        <span class="d-block">
                                                            <h6 class="card-title text h6 text-center float-none">
                                                                <?php
                                                                //   FECHA
                                                                $fecha = $det->fecha_encuentro;
                                                                echo ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
                                                                ?>
                                                                <h6 class="card-title text h5 float-none">
                                                                    <em>
                                                                        <!-- HORA -->
                                                                        <?php

                                                                        $hora = new DateTime($det->hora_encuentro);
                                                                        echo $hora->format('g:ia');

                                                                        ?>
                                                                    </em>
                                                                </h6>
                                                        </span>
                                                    </td>
                                                    <td class="align-middle text-wrap text-center texto-negro ttotal">
                                                        <span class="d-block">
                                                            <h6 class="card-title text h6 text-center float-none">
                                                                <?php echo $det->direccion_lugar; ?>
                                                            </h6>
                                                        </span>
                                                    </td>


                                                    <td class="align-middle text-wrap text-center texto-negro ttotal">
                                                        <span class="d-block">
                                                            <h6 class="card-title text h6 text-center float-none">
                                                                <?php echo number_format($det->costo_total); ?>
                                                            </h6>
                                                        </span>
                                                    </td>
                                            <?php }
                                            endforeach ?>

                                            <!-- More info -->
                                            <td class="align-middle text-wrap text-center p-2 texto-negro colIdServicio ">
                                                <a href="?controller=service&method=listDetails&id_servicio_PK=
                                        <?php echo $service->id_servicio_PK ?>" class="btnDetail btn btn-info rounded-circle btn-sm border" title="Mas info del servicio"><i class="fas fa-info-circle"></i>
                                                </a>
                                            </td>
                                            <!-- ----------------acciones------------------------- -->
                                            <td class="align-middle text-wrap text-center p-2 texto-negro">
                                                <?php
                                                if ($service->id_estado_servicio_FK <= 2) {
                                                ?>
                                                    <a href="?controller=service&method=edit&id_servicio_PK=
                                    <?php echo $service->id_servicio_PK ?>" class="btn btn-sm btn-warning rounded-circle buttonEditar border-dark">
                                                        <i class="fas fa-pen-alt p-2"></i>
                                                    </a>
                                                    <div class="dropdown-divider"></div>

                                                    <div class="dropdown">
                                                        <button class="btn btn-sm btn-dark rounded-pill dropdown-toggle button-inactive border-dark" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-power-off fa-lg p-2"></i>
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item btnFinalizar" href="?controller=service&method=updateserviceStatus&id_servicio_PK=<?php echo $service->id_servicio_PK ?>&id_estado_servicio_FK=3">
                                                                <i class="fas fa-eject fa-xs"></i>
                                                                Finalizar
                                                            </a>
                                                            <a class="dropdown-item btnCancelar" href="?controller=service&method=updateserviceStatus&id_servicio_PK=<?php echo $service->id_servicio_PK ?>&id_estado_servicio_FK=4">
                                                                <i class="fas fa-window-close fa-xs"></i> Cancelar
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <p></p>
                                                <?php } ?>
                                                <?php if ($service->id_estado_servicio_FK <= 4 && $service->id_estado_servicio_FK > 2) { ?>
                                                    <div class="dropdown">
                                                        <button class="btn btn-success btn-sm rounded-pill dropdown-toggle button-list-active" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-plug fa-lg p-2"></i> Activar
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="?controller=service&method=updateserviceStatus&id_servicio_PK=<?php echo $service->id_servicio_PK ?>&id_estado_servicio_FK=1">
                                                                <i class="fas fa-screwdriver fa-xs"></i> En proceso
                                                            </a>
                                                            <a class="dropdown-item" href="?controller=service&method=updateserviceStatus&id_servicio_PK=<?php echo $service->id_servicio_PK ?>&id_estado_servicio_FK=2">
                                                                <i class="fas fa-pause-circle fa-xs"></i> En espera
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <p></p>
                                                </span>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <h6 class="border-bottom p-2 text-warning d-none">
                                <?php echo "Fecha de última modificación del fichero fuente: '" . strftime('%A %d de %B del %Y', strtotime(date("F d Y H:i:s.", getlastmod()))) . "'"; ?>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- </div> -->
</div>
</div>
</div>
</div>
