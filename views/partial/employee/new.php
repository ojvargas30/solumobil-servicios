<div id="myModalLeft" data-backdrop="static" data-keyboard="false" class="modal fade modalNew" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="text-left col-11 border-left">
                    <div class="row pl-2">
                        <h5 class="modal-title text-dark">
                            <strong>
                                SOLICITUD DE SERVICIO TÉCNICO
                            </strong>
                        </h5>
                        <h6 class="text-dark">Genere la información del servicio</h6>
                    </div>
                </div>
                <div class="col-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="card card-success mt-1 shadow">
                    <div class="card-header shadow">
                        <h2 class="card-title">Los campos en rojo son obligatorios</h2>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row d-none">
                            <div class="col-sm-12">
                                <div class="form-group">

                                    <input type="hidden" class="d-none" id="id_servicio_PK" value="<?php echo $data[0]->id_servicio_PK ?>">
                                    <input type="hidden" class="d-none" id="id_revision_servicio_PK" value="<?php echo $details[0]->id_revision_servicio_PK ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label class="col-12">Reparación</label>
                                    <select id="scategory" class="custom-select selector2 controlBuscador" required="">
                                        <option selected value="" aria-readonly="">Seleccione...</option>
                                        <?php
                                        foreach ($scategories as $scategory) {

                                        ?>
                                            <option value="<?php echo $scategory->id_categoria_servicio_PK  ?>">
                                                <?php echo '#' . $scategory->id_categoria_servicio_PK . ' -' ?>
                                                <?php echo $scategory->nombre_cs ?>
                                            </option>
                                        <?php

                                        }
                                        ?>
                                    </select>
                                    <div class="col-md-2 mt-2">
                                        <a href="#" id="addScategory" class="btn btn-info rounded-circle border p-2"><i class="fas fa-plus fa-sm p-1"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12 mt-2 text-center justify-content-center">
                                        <?php if (isset($detailsCat) > 0) {
                                            $arrCategoryService = [];
                                            foreach ($detailsCat as $detCat) {
                                                array_push(
                                                    $arrCategoryService,
                                                    [
                                                        'id_categoria_servicio_PK'     => $detCat->id_categoria_servicio_FK,
                                                        'nombre_cs'  => $detCat->nombre_cs
                                                    ]
                                                );
                                            }
                                        ?>

                                            <script>
                                                var arrScategories = <?php echo json_encode($arrCategoryService); ?>
                                            </script>

                                        <?php } else { ?>

                                            <script>
                                                var arrScategories = [];
                                            </script>

                                        <?php } ?>

                                        <div class="form-group border-success border-bottom" id="list-scategories"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="col-12">Tipo de arreglo</label>
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="is_software">
                                        <label for="is_software" class="custom-control-label">Software</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="is_hardware">
                                        <label for="is_hardware" class="custom-control-label">Hardware</label>
                                    </div>
                                </div>
                                <div class="col-12 text-center justify-content-center">
                                    <div class="form-group border-success border-bottom"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Dispositivo</label>
                                    <select id="artifact" class="custom-select selector2 controlBuscador" required="">
                                        <option selected value="" aria-readonly="">Seleccione...</option>
                                        <?php foreach ($artifacts as $artifact) { ?>
                                            <option value="<?php echo $artifact->id_artefacto_PK ?>">
                                                <?php echo $artifact->id_artefacto_PK . ' . ' ?>
                                                <?php echo $artifact->modelo ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <div class="col-md-2 mt-2">
                                        <a href="#" id="addArtifact" class="btn btn-info rounded-circle border p-2"><i class="fas fa-plus fa-sm p-1"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12 mt-2 text-center justify-content-center">
                                        <?php if (isset($detailsArt) > 0) {
                                            $arrArtifactService = [];
                                            foreach ($detailsArt as $detArt) {
                                                array_push(
                                                    $arrArtifactService,
                                                    [
                                                        'id_artefacto_PK'  => $detArt->id_artefacto_FK,
                                                        'modelo'           => $detArt->modelo
                                                    ]
                                                );
                                            }
                                        ?>
                                            <script>
                                                var arrArtifacts = <?php echo json_encode($arrArtifactService); ?>
                                            </script>

                                        <?php } else { ?>

                                            <script>
                                                var arrArtifacts = [];
                                            </script>

                                        <?php } ?>

                                        <div class="form-group border-success border-bottom" id="list-artifacts"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label class="col-12">Cliente</label>
                                    <select id="id_cliente_FK" class="custom-select selector2 controlBuscador" required="">
                                        <option selected value="" id="seleccione">Seleccione...</option>
                                        <?php foreach ($clients as $client) : ?>
                                            <option value="<?php echo $client->id_cliente_PK ?>">
                                                <?php echo $client->id_cliente_PK . " "; ?>
                                                <?php echo isset($client->correo) ? $client->correo : ''; ?>
                                                <?php echo isset($client->num_id_cli) ? $client->num_id_cli : ''; ?>
                                                <?php echo isset($client->nombre_cliente) ? $client->nombre_cliente : ''; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 justify-content-center align-middle d-flex">
                                <div class="form-group">
                                    <div class="ghost">
                                        <a id="ghost-child" class="btnNew btn btn-success btn-sm shadow align-self-center text-center m-auto" href="#">Nuevo Cliente</a>

                                        <div id="ghost-child-content">
                                            <div class="d-none">
                                                <input type="text" id="id_rol_FK" value="3">
                                            </div>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Nombre</span>
                                                </div>
                                                <input id="nombre_cliente" type="text" class="form-control" placeholder="Nombre" required>
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Apellido</span>
                                                </div>
                                                <input id="apellido_cliente" type="text" class="form-control" placeholder="Apellido" required>
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Correo</span>
                                                </div>
                                                <input id="correo" type="email" class="form-control" placeholder="Correo electrónico" required>
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Contraseña</span>
                                                </div>
                                                <input id="clave" type="password" class="form-control" placeholder="Contraseña" required>
                                            </div>
                                            <div class="row mb-3 justify-content-center text-center">
                                                <div class="col-sm-12">
                                                    <label for="tipo_doc_cli" class="text-sm text-muted col-12">Tipo de Identificación</label>
                                                    <select id="tipo_doc_cli" class="form-control form-control-sm col-12" required="">
                                                        <option selected value="">Seleccione...</option>
                                                        <option value="">CC Cédula de Ciudadanía</option>
                                                        <option value="">CE Cédula de Extranjería</option>
                                                        <option value="">PA Pasaporte</option>
                                                        <option value="">RC Registro Civil</option>
                                                        <option value="">TI Tarjeta de Identidad</option>
                                                        <option value="">Otro</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">No. Identificación</span>
                                                </div>
                                                <input id="num_id_cli" min="0000000000" max="9999999999" type="number" class="form-control" placeholder="Número de documento" required>
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Teléfono</span>
                                                </div>
                                                <input id="telefono" type="number" class="form-control" placeholder="Teléfono de contacto" required>
                                            </div>
                                            <div class="row mb-3 justify-content-center text-center">
                                                <div class="col-sm-6">
                                                    <label for="id_barrio_FK" class="text-sm text-muted col-12">Barrio</label>

                                                    <select id="id_barrio_FK" class="form-control-sm custom-select selector2 controlBuscador" required="">
                                                        <option selected value="" id="seleccione">Seleccione...</option>
                                                        <?php foreach ($districts as $district) : ?>
                                                            <option value="<?php echo $district->id_barrio_PK ?>">
                                                                <?php echo $district->nombre_barrio ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="ghost">
                                                        <a id="ghost-child-2" class="btnBarrio btn btn-success shadow" href="#">Nuevo Barrio</a>
                                                        <div id="ghost-child-content-2">
                                                            <div class="input-group input-group-sm mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">Barrio</span>
                                                                </div>
                                                                <input id="nombre_barrio" type="text" class="form-control" placeholder="Nombre barrio" required>
                                                                <div class="row justify-content-center">
                                                                    <button type="submit" id="submitGhost-2" class="btn btn-success btn-sm text-center" title="Guardar barrio">
                                                                        <i class="fas fa-save pl-1 text-center"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Dirección de residencia</span>
                                                </div>
                                                <input id="direccion_residencia" type="text" class="form-control" placeholder="Dirección de residencia" required>
                                            </div>
                                            <div class="row justify-content-center">
                                                <input id="submitGhost" type="submit" class="inserta text-center" value="Guardar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center justify-content-center">
                                <div class="form-group border-success border-bottom"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- textarea -->
                                <div class="form-group">
                                    <label>Descripción Cliente</label>
                                    <textarea class="form-control" rows="3" id="descripcion_cliente" placeholder="Descripción del cliente"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Diagnóstico</label>
                                    <textarea class="form-control" id="diagnostico" rows="3" placeholder="Diagnóstico del servicio" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row-cols-sm-12 justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Fecha de citación</label>
                                    <input type="date" placeholder="Fecha de citación" class="form-control" id="fecha_encuentro" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Hora de citación</label>
                                    <input type="time" placeholder="Hora de citación" class="form-control" id="hora_encuentro" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-12 col-lg-12 justify-content-center">
                                <div class="form-group">
                                    <label>Dirección citación</label>
                                    <input type="text" placeholder="Dirección residencia del cliente" id="direccion_lugar" required="" class="form-control">
                                    <div class="custom-control custom-checkbox text-center justify-content-center d-block">
                                        <div class="col-md-5 col-sm-5 d-inline">
                                            <input class="custom-control-input" id="localAddress" type="checkbox">
                                            <label for="localAddress" class="p-1 bg-light custom-control-label h6 mr-3 font-weight-normal">En el local de Engativá</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>Costo de revisión</label>
                                        <input type="number" class="form-control" placeholder="Precio revisión" id="costo_revision">
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>Costo de Domicilio</label>
                                        <input type="number" class="form-control" placeholder="Precio domicilio" id="costo_domicilio">
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>Costo total</label>
                                        <input type="number" placeholder="Precio total" id="costo_total" required="" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-2 justify-content-center text-center">
                                    <div class="form-group">
                                        <button id="submit" type="submit" class="btn btn-outline-success shadow mt-4">
                                            <i class="fas fa-save fa-2x"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let localAddressCheck = document.getElementById('localAddress');
    let input_address = document.getElementById('direccion_lugar');
    localAddressCheck.addEventListener('change', () => {
        if (input_address.disabled == true) {
            input_address.disabled = false
            input_address.value = '';
        } else {
            input_address.value = 'Carrera 116c # 66A 16 Villa Teresita Engativa';
            input_address.disabled = true
        }
    })
    // vnum(16);
</script>

<script src="assets/js/general/client.js"></script>
<script src="assets/js/general/district.js"></script>
<script src="assets/js/detalles/detailEmployee/categoryService.js"></script>
<script src="assets/js/detalles/detailEmployee/detailService.js"></script>
<script src="assets/js/detalles/detailEmployee/artifactService.js"></script>