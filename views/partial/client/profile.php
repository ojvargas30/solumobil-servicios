    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-6 mt-4 mb-3">

                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile" id="clickbox" data-toggle="tooltip" data-placement="right" title="Edita tu información haciendo doble click en el campo">
                            <div class="text-center">
                                <img src="<?php if (isset($_SESSION['user']['user']->foto)) {
                                                if (trim($_SESSION['user']['user']->foto) != '') echo $_SESSION['user']['user']->foto;
                                                else echo "assets/img/clients/sinfoto.png";
                                            } else echo "assets/img/clients/sinfoto.png";

                                            ?>" class="img-circle profile-user-img img-fluid elevation-1" alt="Foto Usuario">
                            </div>

                            <h5 class="text-left mt-3">
                                Nombre:
                                <?php echo (trim($clientData[0]->nombre_cliente) != '')
                                    ? '<span class="text-sm font-weight-normal">' . $clientData[0]->nombre_cliente . '</span>'
                                    : '<span class="text-danger text-sm">No se encontro nombre</span>' ?>
                            </h5>

                            <h5 class="text-left">
                                Apellido:
                                <?php echo (trim($clientData[0]->apellido_cliente) != '')
                                    ? '<span class="text-sm font-weight-normal">' . $clientData[0]->apellido_cliente . '</span>'
                                    : '<span class="text-danger text-sm">No se encontro apellido</span>' ?>
                            </h5>

                            <p class="text-muted text-center text-sm">
                                <?php echo isset($_SESSION['user']['user']->correo) ?
                                    $_SESSION['user']['user']->correo : '<span class="text-danger">No se encontro correo electrónico</span>' ?>
                            </p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b class="">Teléfono</b>
                                    <a class="float-right inst-edit form-group mt-1" id="telefono">

                                        <?php echo (trim($clientData[0]->telefono) != '')
                                            ? $clientData[0]->telefono
                                            : '<span class="text-danger text-sm">No se encontro teléfono</span>' ?>
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>Barrio</b>
                                    <!-- <a class="float-right inst-edit" id="nombre_barrio"> -->
                                    <a class="float-right">
                                        <?php echo (trim($clientData[0]->nombre_barrio) != '')
                                            ? $clientData[0]->nombre_barrio
                                            : '<span class="text-danger text-sm">No se encontro barrio</span>' ?>

                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>Residencia</b>
                                    <a class="float-right inst-edit" id="direccion_residencia">
                                        <?php echo (trim($clientData[0]->direccion_residencia) != '')
                                            ? $clientData[0]->direccion_residencia
                                            : '<span class="text-danger text-sm">No se encontro dirección de residencia</span>' ?>

                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>Tipo de identificación</b>
                                    <!-- <a class="float-right inst-edit" id="tipo_doc_cli"> -->
                                    <a class="float-right">
                                        <?php echo (trim($clientData[0]->tipo_doc_cli) != '')
                                            ? $clientData[0]->tipo_doc_cli
                                            : '<span class="text-danger text-sm">No se encontro Tipo de identificación</span>'; ?>
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>No. de identificación</b>
                                    <a class="float-right inst-edit" id="num_id_cli">
                                        <?php echo (trim($clientData[0]->num_id_cli) != '')
                                            ? $clientData[0]->num_id_cli
                                            : '<span class="text-danger text-sm">No se encontro número de identificación</span>'; ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <script>
                    let instEdit = document.querySelectorAll('.inst-edit');
                    let values = [];
                    // console.log(values);

                    instEdit.forEach((elemento, i) => {
                        elemento.addEventListener('dblclick', (e) => {
                            // Mostrar input
                            let input = document.createElement('input')
                            input.value = e.target.textContent.trim();
                            input.id = e.target.id + 'inp';
                            input.classList.add('form-control');

                            // console.log(values);

                            // Evento para mostrar y ocultar
                            eventShowNormal(e.target.id)

                            // Ponerle el valor al input
                            values[e.target.id] = e.target.textContent.trim();

                            // Limpiar valor del texto anterior al input
                            e.target.textContent = '';

                            // Incluir input en el html
                            e.target.appendChild(input);

                            // darle el foco al input
                            input.focus()

                            // SOLO UNO A LA VEZ
                            for (const input in values)
                                if (input != e.target.id) showNormal(input)

                            function showNormal(idNormal = '') {
                                try {
                                    let input = ele(idNormal + 'inp'),
                                        data = [];

                                    if (input) {
                                        if (input.value != null) {

                                            ele(idNormal).textContent = input.value;

                                            let type = '';

                                            if (idNormal == "nombre_barrio") type = 'barrio'
                                            else type = 'cliente'

                                            const data = {
                                                data: type + '&|' + idNormal + '&|' + input.value,
                                            }

                                            const formData = new FormData()
                                            const json = JSON.stringify(data)
                                            formData.append('data', json)

                                            let url = '?c=client&m=editProfile';

                                            fetch(url, {
                                                    method: 'POST',
                                                    body: formData
                                                })
                                                .then(res => {
                                                    if (res.ok) return res.text()
                                                })
                                                .then(result => {
                                                    // console.log(result)
                                                    if (result == '"mal"') {
                                                        alert("Error, valor inadmitido");
                                                        location.reload();
                                                    }
                                                })
                                                .catch(e => console.error(e));
                                        }
                                    }

                                    if (typeof input != 'object') ele(idNormal).removeChild(input);

                                } catch (e) {
                                    console.error(e);
                                }
                            }

                            function eventShowNormal(nameCurrent) {
                                window.addEventListener('click', (e) => {
                                    if (!ele('clickbox').contains(e.target)) {
                                        showNormal(nameCurrent)
                                    }
                                });

                                window.addEventListener('keypress', (e) => {
                                    if (e.keyCode === 13 && !e.shiftKey) {
                                        e.preventDefault();
                                        showNormal(nameCurrent)
                                    }
                                })
                            }
                        })
                    })
                </script>

                <!-- <div class="col-md-8 mt-4">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#settings" data-toggle="tab">Cambiar contraseña</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#conf" data-toggle="tab">Configuración</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active text-center" id="settings">
                                    <form action="" method="POST">
                                        <div class="form-group row justify-content-center">
                                            <label for="inputName" class="col-sm-12 text-center">Nueva contraseña</label>
                                            <div class="col-sm-6">
                                                <input type="password" autocomplete="on" class="form-control" id="newPassword" placeholder="Ingresa una contraseña">
                                            </div>
                                        </div>
                                        <div class="form-group row justify-content-center">
                                            <label for="inputEmail" class="col-sm-12 text-center">Confirmar contraseña</label>
                                            <div class="col-sm-6">
                                                <input type="password" autocomplete="on" class="form-control" id="confPassword" placeholder="Confirma la contraseña">
                                            </div>
                                        </div>
                                        <button class="btn btn-success text-center" type="submit">Cambiar</button>
                                    </form>
                                </div>

                                <div class="tab-pane" id="conf">
                                    <a class="text-decoration-none text-danger">Eliminar cuenta</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>
    </section>

    <script src="assets/js/general/accountConfig.js"></script>