<div class="row h-100 ">
    <div class="col-md-12 ">
        <div class="row ">
            <div class="col-md-12 mt-3">
                <?php if (count($cli_services) == 0) { ?>
                    <div class="card card-white overflow-auto shadow" data-toggle="tooltip" data-placement="top" title="¡Oh!, aún no tienes servicios, Para solicitar un servicio puedes visitarnos en Carrera 116c #66A 16 Villa Teresita Engativá Bogotá - Colombia y podras hacer seguimiento desde aquí">
                    <?php } else { ?>
                        <div class="card card-white overflow-auto shadow-sm">
                        <?php } ?>
                        <div class="card-header bg-gradient-red p-3">
                            <h5 class="card-title pr-1 text-white">Seguimiento de mis Servicios Solumobil</h5>
                            <button class="btn bg-gradient-success text-center ml-2 btn-sm" id="modalToggle">Solicitar servicio</button>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="maximize">
                                    <i class="fas fa-expand text-white"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus text-white"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times text-white"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <script>
                                let totCost = '';
                            </script>
                            <div class="row align-items-stretch justify-content-center">
                                <?php $cont = 0;
                                foreach ($cli_services as $cli_service) : $cont++; ?>
                                    <div class="col d-flex mt-3 align-items- justify-content-center">
                                        <!-- <div class="col-12 col-sm-6 col-md-4 d-flex mt-3 align-items-stretch"> -->
                                        <div class="card hovCard shadow border card-custom item-card" <?= (count($cli_services) === $cont) ? "style='background:#F5F5F6'" : "" ?>>
                                            <div class="card-body border-top pt-0 d-none noVisibleTecnico carta-modo-osc mb-0 pb-0">
                                                <div class="row mt-2">
                                                    <div class="col-12 colCardHov">
                                                        <h6 class="font-weight-bolder">
                                                            <span class="text-olive text-left">
                                                                Técnico/s Asignado/s
                                                            </span>
                                                        </h6>
                                                        <?php foreach ($tecnicos as $tec) {
                                                            if ($tec->id_servicio_FK == $cli_service->id_servicio_PK) { ?>
                                                                <ul class="ml-4 mb-0 fa-ul text-muted border-top border-bottom ">
                                                                    <li class="small my-2 py-1 text-dark">
                                                                        <span class="fa-li">
                                                                            <i class="fas fa-lg fa-signature"></i>
                                                                        </span>
                                                                        <strong class="">Nombre:</strong>
                                                                        <?php echo $tec->nombre_tecnico . ' ' . $tec->apellido_tecnico; ?>
                                                                    </li>
                                                                    <li class="small my-2 py-1 text-dark">
                                                                        <span class="fa-li">
                                                                            <i class="fas fa-lg fa-phone"></i>
                                                                        </span> <strong>Telefono: </strong>
                                                                        <?php echo ($tec->telefono) ? '(+57) ' . $tec->telefono : ''; ?>
                                                                    </li>
                                                                    <li class="small my-2 py-1 text-dark">
                                                                        <span class="fa-li">
                                                                            <i class="fas fa-lg fa-user-tag"></i>
                                                                        </span> <strong>Descripción: </strong>
                                                                        <?php echo ($tec->desc_tecnico) ? $tec->desc_tecnico : ''; ?>
                                                                    </li>
                                                                </ul>
                                                        <?php }
                                                        }  ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body border-top pt-0 d-none shadow-sm noVisibleDispositivo carta-modo-osc mb-0 pb-0">
                                                <div class="row mt-2">
                                                    <div class="col-12 colCardHov">
                                                        <h6 class="font-weight-bolder text-dark text-left">Dispositivos</h6>
                                                        <?php foreach ($artefactos as $art) {
                                                            if ($art->id_servicio_FK == $cli_service->id_servicio_PK) { ?>
                                                                <ul class="ml-4 mb-0 fa-ul text-muted border-top border-bottom">
                                                                    <li class="small my-2 py-1 text-dark">
                                                                        <span class="fa-li">
                                                                            <i class="fab fa-buromobelexperte fa-lg"></i>
                                                                        </span>
                                                                        <strong>Modelo:</strong>
                                                                        <?php echo ($art->modelo) ? $art->modelo : ''; ?>
                                                                    </li>
                                                                    <li class="small my-2 py-1 text-dark">
                                                                        <span class="fa-li">
                                                                            <i class="fas fa-lg fa-desktop"></i>
                                                                        </span>
                                                                        <strong>Marca:</strong>
                                                                        <?php echo ($art->nombre_marca) ? $art->nombre_marca : ''; ?>
                                                                    </li>
                                                                    <li class="small my-2 py-1 text-dark">
                                                                        <span class="fa-li">
                                                                            <i class="fas fa-lg fa-laptop-medical"></i>
                                                                        </span>
                                                                        <strong>Tipo:</strong>
                                                                        <?php echo ($art->nombre_categoria_artefacto) ? $art->nombre_categoria_artefacto : ''; ?>
                                                                    </li>
                                                                    <li class="small my-2 py-1 text-dark">
                                                                        <span class="fa-li">
                                                                            <i class="fas fa-lg fa-sort-numeric-down-alt"></i>
                                                                        </span>
                                                                        <strong>Serial:</strong>
                                                                        <?php echo ($art->serie) ? $art->serie : ''; ?>
                                                                    </li>
                                                                </ul>
                                                        <?php }
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body border-top pt-0 d-none shadow-sm noVisibleReparacion carta-modo-osc mb-0 pb-0">
                                                <div class="row mt-2">
                                                    <div class="col-12 colCardHov">
                                                        <h6 class="font-weight-bolder text-navy text-left">Reparaciones</h6>
                                                        <?php foreach ($categorias as $cat) {
                                                            if ($cat->id_servicio_FK == $cli_service->id_servicio_PK) { ?>
                                                                <ul class="ml-4 mb-0 fa-ul text-muted border-top border-bottom">
                                                                    <li class="small my-2 py-1 text-dark">
                                                                        <span class="fa-li">
                                                                            <i class="fas fa-lg fa-tools"></i>
                                                                        </span>
                                                                        <strong>Servicio:</strong>
                                                                        <?php echo ($cat->nombre_cs) ? $cat->nombre_cs : ''; ?>
                                                                    </li>
                                                                </ul>
                                                        <?php }
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-header text-muted border-bottom-0">
                                                <div class="row justify-content-center text-center">
                                                    <div class="col-6">
                                                        <?php switch ($cli_service->id_estado_servicio_FK) {
                                                            case 1:
                                                        ?>
                                                                <span class="right badge bg-gradient-olive"><?php echo $cli_service->nombre_estado;
                                                                                                            break; ?></span>
                                                            <?php
                                                            case 2: ?>
                                                                <span class="right badge bg-gradient-blue"><?php echo $cli_service->nombre_estado;
                                                                                                            break; ?></span>
                                                            <?php
                                                            case 3: ?>
                                                                <span class="right badge bg-gradient-yellow"><?php echo $cli_service->nombre_estado;
                                                                                                                break; ?></span>
                                                            <?php
                                                            case 4: ?>
                                                                <span class="right badge badge-danger"><?php echo $cli_service->nombre_estado;
                                                                                                        break; ?></span>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-6 border-bottom">
                                                        <?php echo '#' . $cli_service->id_servicio_PK; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body pt-0">
                                                <div class="row">
                                                    <div class="col-sm-9 col-md-9 colCardHov">
                                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                                            <li class="small my-2">
                                                                <span class="fa-li">
                                                                    <i class="fas fa-lg fa-paper-plane"></i>
                                                                </span>
                                                                <strong>Petición:</strong>
                                                                <?php $fecha = $cli_service->fecha_peticion;
                                                                echo ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
                                                                ?> a las
                                                                <?php $time = new DateTime($cli_service->hora_peticion);
                                                                echo $time->format('g:ia'); ?>
                                                            </li>
                                                            <li class="small my-2">
                                                                <span class="fa-li">
                                                                    <i class="fas fa-lg fa-building"></i>
                                                                </span> <strong>Entrega: </strong>

                                                                <?php
                                                                if ($cli_service->fecha_hora_cita_virtual != null || $cli_service->fecha_hora_cita_virtual != '') {
                                                                    if ($cli_service->link_cita_virtual != '' || $cli_service->link_cita_virtual != null) {

                                                                        $fecha = $cli_service->fecha_hora_cita_virtual;
                                                                        echo ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
                                                                ?>
                                                                        <?php
                                                                        $hora = new DateTime($cli_service->fecha_hora_cita_virtual);
                                                                        $time = $hora->format('g:ia');
                                                                        echo ($cli_service->fecha_hora_cita_virtual != '00:00:00') ? $time : '<span class="text-danger politica">Sin definir hora</span><div></div>';

                                                                        echo ' por medio de <span class="text-success">Google meet:</span> ';

                                                                        ?>
                                                                        <a href="<?php echo $cli_service->link_cita_virtual ?>" target="_blank">Enlace de entrega virtual</a>
                                                                    <?php
                                                                    }
                                                                } else {
                                                                    if ($cli_service->fecha_encuentro != null || $cli_service->fecha_encuentro != '') {
                                                                        $fecha = $cli_service->fecha_encuentro;
                                                                        echo ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
                                                                    ?>

                                                                        <?php
                                                                        $hora = new DateTime($cli_service->hora_encuentro);
                                                                        $time = $hora->format('g:ia');
                                                                        echo ($cli_service->hora_encuentro != '00:00:00') ? $time : '<span class="text-danger politica">Sin definir hora</span><div></div>';
                                                                        ?>
                                                                        en
                                                                <?php echo $cli_service->direccion_lugar;
                                                                    } else {
                                                                        echo "<span class='text-danger'>Todavía no se ha programado la entrega</span>";
                                                                    }
                                                                }
                                                                ?>
                                                            </li>
                                                            <li class="small my-2">
                                                                <span class="fa-li">
                                                                    <i class="fas fa-lg fa-play"></i>
                                                                </span> <strong>Inicio:</strong>
                                                                <?php
                                                                if ($cli_service->fecha_inicio != null || $cli_service->fecha_inicio != '') {
                                                                    $fecha = $cli_service->fecha_inicio;
                                                                    echo strftime('%A %d de %B del %Y', strtotime($fecha)) . "\n";

                                                                    $time = new DateTime($cli_service->fecha_inicio);
                                                                    echo $time->format('g:ia');
                                                                } else {
                                                                    echo "<span class='text-danger'>Sin fecha de inicio</span>";
                                                                }
                                                                ?>
                                                            </li>
                                                            <li class="small my-2">
                                                                <span class="fa-li">
                                                                    <i class="fas fa-lg fa-dollar-sign"></i>
                                                                </span> <strong>Total: </strong>
                                                                <?php
                                                                if ($cli_service->costo_total != null || $cli_service->costo_total != '') {
                                                                    if ($cli_service->costo_total == 0) {
                                                                        if ($cli_service->pagado == 1)
                                                                            echo "<span class='text-danger'>Aún no se ha dado precio</span>";
                                                                        elseif ($cli_service->pagado == 2)
                                                                            echo "<span class='text-success'>Pagado</span>";
                                                                    } else {
                                                                        $cli_service->costo_total = $cli_service->costo_total * 0.000280;
                                                                        $total_cost = number_format($cli_service->costo_total);
                                                                        echo $total_cost . ' USD';

                                                                        if (
                                                                            $cli_service->id_estado_servicio_FK != 4
                                                                        ) {

                                                                ?>


                                                                            <div class='paypal-button-container mt-3' title='<?php echo openssl_encrypt('#%|' . $cli_service->id_servicio_PK, COD, KEY); ?>' id='<?php echo $total_cost ?>'>
                                                                            </div>
                                                                <?php
                                                                        }
                                                                    }
                                                                } else {
                                                                    if ($cli_service->pagado == 1)
                                                                        echo "<span class='text-danger'>Aún no se ha dado precio</span>";
                                                                    elseif ($cli_service->pagado == 2)
                                                                        echo "<span class='text-success'>Pagado</span>";
                                                                }
                                                                ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3 justify-content-center text-left colCardHov ml-0 pl-0">
                                                        <ul class="ml-1 mb-0 fa-ul text-muted">
                                                            <li class="small">
                                                                <strong class="font-weight-bolder">Dispositivos:</strong>
                                                                <button type="button" class="btn bg-gradient-dark shadow-sm btn-lg d-block verDispositivos fas fa-eye" data-device="1"></button>
                                                            </li>
                                                            <li class="small my-3">
                                                                <strong class="font-weight-bolder">Reparaciones:</strong>
                                                                <button type="button" class="btn bg-gradient-navy shadow-sm btn-lg d-block verReparaciones fas fa-eye" data-fix="1"></button>
                                                            <li>
                                                                <!-- <li class="small mt-2">
                                                                <strong class="font-weight-bolder">Técnico/s:</strong>
                                                                <button type="button" class="btn bg-gradient-olive shadow-sm btn-lg d-block verTecnicos fas fa-eye" data-technical="1"></button>
                                                            <li> -->
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer cardPie">
                                                <div class="row justify-content-around">
                                                    <?php if ($cli_service->id_estado_servicio_FK <= 2) { ?>
                                                        <div class="col-sm-12 col-md-8 d-flex justify-content-around">

                                                            <!-- Cancelar -->
                                                            <!-- <button class="btn bg-gradient-red border" onclick="cancelService(<?php echo $cli_service->id_servicio_PK ?>)" title="Cancelar el servicio">
                                                                <i class="fas fa-times"></i>
                                                            </button> -->

                                                            <!-- Solucitud de edición PROXIMAMENTE -->
                                                            <!-- <button class="btn bg-gradient-yellow ml-1" onclick="editService(<?php echo $cli_service->id_revision_servicio_PK ?>, <?php echo $cli_service->id_servicio_PK ?>)">
                                                                <i class="fas fa-pen-alt"></i>
                                                            </button> -->

                                                            <!-- Ver mas -->
                                                            <input type="button" class="btnDetail btn ml-5 bg-gradient-primary border-light verMas" title="Mas info del servicio" value="Ver Mas">
                                                        </div>
                                                    <?php } else if ($cli_service->id_estado_servicio_FK == 3) { ?>

                                                        <!-- Ver mas unicamente -->
                                                        <div class="col-sm-12 col-md-8 d-flex justify-content-around">
                                                            <input type="button" class="btnDetail btn ml-3 bg-gradient-primary border-light verMas" title="Mas info del servicio" value="Ver Mas">
                                                        </div>

                                                    <?php } ?>

                                                    <?php if ($cli_service->id_estado_servicio_FK == 4) { ?>
                                                        <div class="col-4">
                                                            <button type="button" onclick="activeService(<?php echo $cli_service->id_servicio_PK ?>)" class="btn btn-success btn-sm" href="">
                                                                Reactivar
                                                            </button>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="card-body pt-0 d-none cardNovisible carta-modo-osc">
                                                <div class="row mt-2">
                                                    <div class="col-12 colCardHov">
                                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                                            <li class="small my-3">
                                                                <span class="fa-li">
                                                                    <i class="fas fa-lg fa-stethoscope"></i>
                                                                </span>
                                                                <strong>Diagnóstico:</strong>
                                                                <?php if ($cli_service->diagnostico != null || $cli_service->diagnostico != '')
                                                                    echo $cli_service->diagnostico;
                                                                else
                                                                    echo "<span class='text-danger'>No se encontro diagnóstico</span>";
                                                                ?>
                                                            </li>
                                                            <li class="small my-3">
                                                                <span class="fa-li">
                                                                    <i class="fas fa-lg fa-exclamation-circle"></i>
                                                                </span> <strong>Problema: </strong>
                                                                <?php
                                                                if ($cli_service->descripcion_cliente != null || $cli_service->descripcion_cliente != '')
                                                                    echo $cli_service->descripcion_cliente;
                                                                else
                                                                    echo "<span class='text-danger'>No se encontro descripción del cliente</span>";
                                                                ?>
                                                            </li>
                                                            <li class="small my-3">
                                                                <span class="fa-li">
                                                                    <i class="fas fa-lg fa-play"></i>
                                                                </span> <strong>Fecha de inicio: </strong>
                                                                <?php if ($cli_service->fecha_inicio != null || $cli_service->fecha_inicio != '') {
                                                                    $fecha = $cli_service->fecha_inicio;
                                                                    echo strftime('%A %d de %B del %Y', strtotime($fecha)) . "\n";
                                                                    echo '<span class="h6">';
                                                                    $time = new DateTime($cli_service->fecha_inicio);
                                                                    echo $time->format('g:ia') . '</span>';
                                                                } else {
                                                                    echo "<span title'Puede que el servicio no haya empezado aún' class='text-danger'><small class='text-primary'></small></span>";
                                                                } ?>
                                                            </li>
                                                            <li class="small my-3">
                                                                <span class="fa-li">
                                                                    <i class="fas fa-lg fa-calendar-check"></i>
                                                                </span> <strong>Fecha de acabado: </strong>
                                                                <?php

                                                                if ($cli_service->fecha_fin != null || $cli_service->fecha_fin != '') {
                                                                    $fecha = $cli_service->fecha_fin;

                                                                    if (CURRENT_HOST == "localhost") {
                                                                        echo strftime('%A %d de %B del %Y', strtotime($fecha)) . "\n";
                                                                    } else {
                                                                        echo substr($fecha, 0, -9) . ' ';
                                                                    }

                                                                    $time = new DateTime($cli_service->fecha_fin);
                                                                    echo $time->format('g:ia');
                                                                } else {
                                                                    echo '<span title="Puede que el servicio no haya terminado aún"></span>';
                                                                }

                                                                ?>
                                                            </li>
                                                            <div class="border-left pl-1 ml-0 ">
                                                                <span class="fa-li" style="left: 0.2em !important; margin-top: 1em;">
                                                                    <i class="fas fa-lg fa-dollar-sign"></i>
                                                                </span>
                                                                <li class="small mt-3">
                                                                    <i class=" mt-2"></i>
                                                                    <strong>Costo diagnóstico: </strong>
                                                                    <?php if ($cli_service->costo_revision != null || $cli_service->costo_revision != '')
                                                                        echo number_format($cli_service->costo_revision) . ' COP';
                                                                    else
                                                                        echo "<span class='text-danger'></span>";
                                                                    ?>
                                                                </li>
                                                                <li class="small">
                                                                    <i class=" mt-2"></i>
                                                                    <strong>Costo domicilio: </strong>
                                                                    <?php if ($cli_service->costo_domicilio != null || $cli_service->costo_domicilio != '')
                                                                        echo number_format($cli_service->costo_domicilio) . ' COP';
                                                                    else
                                                                        echo "<span class='text-danger'></span>";
                                                                    ?>
                                                                </li>
                                                                <li class="small mb-3">
                                                                    <i class=" mt-2"></i>
                                                                    <strong>Abono: </strong>
                                                                    <?php if ($cli_service->abono != null || $cli_service->abono != '')
                                                                        echo number_format($cli_service->abono) . ' COP';
                                                                    else
                                                                        echo "<span class='text-danger'></span>";
                                                                    ?>
                                                                </li>
                                                            </div>
                                                            <!-- <li class="small my-3">
                                                                <strong>Categorías de arreglo:</strong>
                                                                <?php
                                                                // if ($cli_service->is_software != false && $cli_service->is_hardware != false) {
                                                                //     echo '<span class="textR">S<span class="textR2 ">H</span></span>';
                                                                // } elseif ($cli_service->is_software != false) {
                                                                //     echo '<span class="textR">S</span>';
                                                                // } elseif ($cli_service->is_hardware != false) {
                                                                //     echo '<span class="textR2">H</span>';
                                                                // } else {
                                                                //     echo "<span class='text-danger'>No se encontraron categorías para el servicio</span>";
                                                                // }
                                                                ?>
                                                            </li> -->
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>


<script>
    // Toca Evitar csrf token
    function cancelService(idService) {
        console.log(`click`);
        if (confirm("¿Esta seguro de que el servicio se cancelo?"))
            location.href = `?c=service&m=updateServiceStatus&id_servicio_PK=${idService}&id_estado_servicio_FK=4`;
    }

    function editService(idRevision, idService) {
        location.href = `?c=service&m=edit&id_revision_servicio_PK=${idRevision}&id_servicio_PK=${idService}`;
    }

    function activeService(idService) {
        location.href = `?c=service&m=updateServiceStatus&id_servicio_PK=${idService}&id_estado_servicio_FK=2`
    }
</script>


<!-- Include the PayPal JavaScript SDK -->
<script src="https://www.paypal.com/sdk/js?client-id=sb&currency=USD"></script>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
    let pay = document.querySelectorAll('.paypal-button-container')
    pay.forEach((elemento, i) => {
        console.log(elemento);
        console.log(elemento.id);
        console.log(elemento.title);

        paypal.Button.render({
            env: '<?php echo PayPalENV; ?>', // sandbox - production
            style: {
                label: 'checkout',
                size: 'responsive',
                shape: 'pill',
                color: 'blue',
            },
            client: {
                <?php if (ProPayPal) {
                ?>
                    production: '<?php echo PayPalClientId; ?>',
                <?php } else {
                ?>
                    sandbox: '<?php echo PayPalClientId; ?>'
                <?php }
                ?>
            },
            payment: function(data, actions) {
                return actions.payment.create({
                    payment: {
                        transactions: [{
                            amount: {
                                total: elemento.id,
                                currency: 'USD'
                            },
                            description: `Compra de servicio técnico a Solumobil: $${elemento.id} USD`,
                            custom: elemento.title
                        }]
                    }
                });
            },
            onAuthorize: (data, actions) => {
                return (actions.payment.execute()
                    .then(() => {
                        console.log(data);
                        // window.alert("Transacción satisfactoria");
                        // window.location = "?c=service&m=paymentService&dataPaymentId" + data.paymentID + "&payerID=" + data.payerID + "&token=" + data.paymentToken + "&pid=" + elemento.title;
                        window.location = "?c=service&m=paymentService&paymentToken=" + data.paymentToken +
                            "&paymentID=" + data.paymentID;
                        // window.location = "?c=service&m=paymentService&data="+data;

                        // let url = '?c=service&m=paymentService&paymentToken=' + data.paymentToken +
                        //     '&paymentID=' + data.paymentID;

                        // fetch(url, {
                        //         method: 'GET',
                        //     })
                        //     .then(res => {
                        //         if (res.ok) return res.text()
                        //     })
                        //     .then(result => console.log(result))
                        //     .catch(e => console.error(e));
                    }));
            }
        }, elemento)
    });
</script>

<script>
    const verMas = document.querySelectorAll(".verMas");

    if (verMas) {
        verMas.forEach((ele, i) => {
            console.log(i);
            console.log(ele);
            ele.addEventListener('click', (e) => {
                // let o = e.target;
                if (e.target.value == "Ver Mas") {
                    e.target.classList.remove("bg-gradient-primary");
                    e.target.classList.add("bg-gradient-danger");
                    e.target.value = "Ocultar";
                    document.getElementsByClassName("cardNovisible")[i].classList.remove("d-none");
                } else {
                    e.target.classList.remove("bg-gradient-danger");
                    e.target.classList.add("bg-gradient-primary");
                    e.target.value = "Ver Mas";
                    document.getElementsByClassName("cardNovisible")[i].classList.add("d-none");
                }
            });
        });
    }

    const verDispositivos = document.querySelectorAll(".verDispositivos");

    if (verDispositivos) {
        verDispositivos.forEach((element, index) => {
            element.addEventListener('click', (e) => {
                let o = e.target;
                if (o.dataset.device == "1") {
                    o.classList.remove("btn-default");
                    o.classList.add("btn-danger");
                    o.dataset.device = "2";
                    document.getElementsByClassName("noVisibleDispositivo")[index].classList.remove("d-none");
                } else {
                    o.classList.remove("btn-danger");
                    o.classList.add("btn-default");
                    o.dataset.device = "1";
                    document.getElementsByClassName("noVisibleDispositivo")[index].classList.add("d-none");
                }
            });
        });
    }

    const verReparaciones = document.querySelectorAll(".verReparaciones");

    if (verReparaciones) {
        verReparaciones.forEach((element, index) => {
            element.addEventListener('click', (e) => {
                let o = e.target;
                if (o.dataset.fix == "1") {
                    o.classList.remove("btn-default");
                    o.classList.add("btn-danger");
                    o.dataset.fix = "2";
                    document.getElementsByClassName("noVisibleReparacion")[index].classList.remove("d-none");
                } else {
                    o.classList.remove("btn-danger");
                    o.classList.add("btn-default");
                    o.dataset.fix = "1";
                    document.getElementsByClassName("noVisibleReparacion")[index].classList.add("d-none");
                }
            });
        });
    }

    const verTecnicos = document.querySelectorAll(".verTecnicos");

    if (verTecnicos) {
        verTecnicos.forEach((element, index) => {
            element.addEventListener('click', (e) => {
                let o = e.target;
                if (o.dataset.technical == "1") {
                    o.classList.remove("btn-default");
                    o.classList.add("btn-danger");
                    o.dataset.technical = "2";
                    document.getElementsByClassName("noVisibleTecnico")[index].classList.remove("d-none");
                } else {
                    o.classList.remove("btn-danger");
                    o.classList.add("btn-default");
                    o.dataset.technical = "1";
                    document.getElementsByClassName("noVisibleTecnico")[index].classList.add("d-none");
                }
            });
        });
    }
</script>