<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="text-left col-11 border-left">
                    <div class="row pl-2">
                        <h5 class="modal-title text-dark col-10">
                            <strong>
                                SOLICITUD DE SERVICIO TÉCNICO
                            </strong>
                        </h5>
                        <h6 class="text-dark col-10 text-sm">Los campos con <span class="text-danger"> * </span> son
                            obligatorios</h6>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active text-danger" data-toggle="tab" href="#infoPanel" role="tab">Servicios</a>
                    <li>
                    <li class="nav-item">
                        <a class="nav-link text-danger" data-toggle="tab" href="#ads" role="tab">Dispositivo</a>
                    <li>
                    <li class="nav-item">
                        <a class="nav-link text-danger" data-toggle="tab" href="#placementPanel" role="tab">Problema y residencia</a>
                    <li>
                </ul>

                <div class="tab-content mt-2">
                    <div class="tab-pane fade show active p-1 justify-content-center text-center" id="infoPanel" role="tabpanel">
                        <!-- <h5 class="m-1">Servicio</h5> -->
                        <div class="d-none">
                            <input id="id_servicio_PK" value="<?php echo $cli_services[0]->id_servicio_PK ?>">
                            <input id="id_revision_servicio_PK" value="<?php echo $cli_services[0]->id_revision_servicio_PK ?>">
                        </div>
                        <div class="row justify-content-center ml-2 pl-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h6 class="text-center mt-1 font-weight-bold">¿Que servicios deseas?<span class="text-danger"> * </span></h6>
                                    <select id="scategory" autofocus style="width: 100%;" class="custom-select selector2 controlBuscador" required="">
                                        <option selected value="" aria-readonly="" id="seleccione2">Seleccione...</option>
                                        <?php foreach ($scategories as $scategory) {
                                        ?>
                                            <option value="<?php echo $scategory->id_categoria_servicio_PK  ?>">
                                                <span>
                                                    <?php echo $scategory->nombre_cs ?>
                                                </span>
                                            </option>
                                        <?php }
                                        ?>
                                    </select>
                                    <div class="col-md-12 mt-2 text-center justify-content-center">
                                        <?php if (isset($detailsCat) > 0) {
                                            $arrCategoryService = [];
                                            foreach ($detailsCat as $detCat) {
                                                array_push(
                                                    $arrCategoryService,
                                                    [
                                                        'id_categoria_servicio_PK'     => $detCat->id_categoria_servicio_FK,
                                                        'nombre_cs'                     => $detCat->nombre_cs
                                                    ]
                                                );
                                            }
                                        ?>
                                            <script>
                                                var arrScategories = <?php echo json_encode($arrCategoryService); ?>
                                            </script>

                                        <?php } else { ?>

                                            <script>
                                                var arrScategories = [];
                                            </script>

                                        <?php } ?>

                                        <div class="form-group text-left pl-2 border-success border-bottom" id="list-scategories">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-secondary mt-1 text-center" id="infoContinue">Siguiente</button>
                    </div>
                    <div class="tab-pane fade p-1 justify-content-center text-center" id="ads" role="tabpanel">
                        <div class="row justify-content-center ml-2 pl-2">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <h6 class="text-center mt-1 font-weight-bold">Dispositivo<span class="text-danger"> * </span></h6>
                                    <select id="artifact" style="width: 100%;" class="custom-select selector2 controlBuscador" required="">
                                        <option selected value="" aria-readonly="">Seleccione...</option>
                                        <?php foreach ($artifacts as $artifact) { ?>
                                            <option value="<?php echo $artifact->id_artefacto_PK ?>">
                                                <?php echo $artifact->nombre_categoria_artefacto . '* ' ?>
                                                <?php echo $artifact->modelo ?>
                                            </option>
                                        <?php } ?>
                                    </select>

                                    <div class="col-md-12 mt-2 text-center justify-content-center">
                                        <?php if (isset($detailsArt) > 0) {
                                            $arrArtifactService = [];
                                            foreach ($detailsArt as $detArt) {
                                                array_push(
                                                    $arrArtifactService,
                                                    [
                                                        'id_artefacto_PK'  => $detArt->id_artefacto_FK,
                                                        'modelo'           => $detArt->modelo
                                                    ]
                                                );
                                            }
                                        ?>
                                            <script>
                                                var arrArtifacts = <?php echo json_encode($arrArtifactService); ?>
                                            </script>

                                        <?php } else { ?>

                                            <script>
                                                var arrArtifacts = [];
                                            </script>

                                        <?php } ?>

                                        <div class="form-group text-left pl-2 border-success border-bottom" id="list-artifacts">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 justify-content-center align-middle d-flex">
                                <div class="form-group">
                                    <div class="ghost">
                                        <a id="ghost-child-artifact" class="btn btn-success shadow align-self-center text-center m-auto" href="#">Nuevo</a>

                                        <div id="ghost-child-content-artifact">
                                            <div class="row mb-3 justify-content-center text-center">
                                                <div class="col-sm-8">
                                                    <label for="id_marca_artefacto_FK" class="text-sm text-muted col-12"><span class="text-danger">*</span>Marca <i class="fas fa-desktop"></i></label>
                                                    <select id="id_marca_artefacto_FK" style="width: 100%;" class="form-control-sm custom-select selector2 controlBuscador" required="">
                                                        <option selected value="0" id="seleccione">Seleccione...</option>
                                                        <?php foreach ($brands as $brand) : ?>
                                                            <option value="<?php echo $brand->id_marca_artefacto_PK ?>">
                                                                <?php echo $brand->nombre_marca ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="ghost">
                                                        <a id="ghost-child-2" class="btn btn-sm btn-success shadow mr-4 mt-3" href="#">Otra marca</a>
                                                        <div id="ghost-child-content-2">
                                                            <form>
                                                                <div class="input-group input-group-sm mb-3">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fas fa-desktop"></i></span>
                                                                    </div>
                                                                    <input id="nombre_marca" type="text" class="form-control" placeholder="Nombre de la marca" required>
                                                                    <div class="row justify-content-center">
                                                                        <button type="submit" id="submitGhostBrand" class="btn btn-success btn-sm text-center" title="Guardar marca">
                                                                            <i class="fas fa-save pl-1 text-center"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3 justify-content-center text-center">
                                                <div class="col-sm-12">
                                                    <label for="id_categoria_artefacto_FK" class="text-sm text-muted p-0 m-0 col-12"><span class="text-danger">*</span>Tipo <i class="fas fa-laptop-medical"></i></label>
                                                    <select id="id_categoria_artefacto_FK" style="width: 100%;" class="form-control-sm custom-select selector2 controlBuscador" required="">
                                                        <option selected value="0" id="seleccione">Seleccione...</option>
                                                        <?php foreach ($types as $type) : ?>
                                                            <option value="<?php echo $type->id_categoria_artefacto_PK; ?>">
                                                                <?php echo $type->nombre_categoria_artefacto; ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-3 justify-content-center text-center">
                                                <div class="col-sm-12 d-flex">
                                                    <label for="modelo" class="text-sm text-muted"><span class="text-danger">*</span></label>
                                                    <div class="input-group input-group-sm mt-2">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fab fa-buromobelexperte"></i></span>
                                                        </div>
                                                        <input id="modelo" maxlength="30" type="text" class="form-control" placeholder="Modelo" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row justify-content-center">
                                                <input id="submitGhostArtifact" maxlength="100" type="submit" class="inserta text-center" value="Guardar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-secondary text-center" id="adsContinue">Siguiente</button>

                    </div>
                    <div class="tab-pane fade p-1 justify-content-center text-center" id="placementPanel" role="tabpanel">
                        <div class="row justify-content-center">
                            <div class="col-sm-12 col-md-5">
                                <h6 class="text-center mt-1 font-weight-bold">¿Que sucedio con tu dispositivo?<span class="text-danger"> * </span></h6>
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" id="descripcion_cliente" placeholder="Descripción del problema" minlength="10" maxlength="500" autocomplete="on" required="true"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12 col-md-5">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 class="text-center mt-1 font-weight-bold">Lugar Entrega<span class="text-danger"> * </span></h6>
                                        <div class="form-group text-left">
                                            <input type="text" placeholder="Ingrese Lugar de Entrega" autocomplete="street-address" minlength="3" maxlength="75" id="direccion_lugar" required="true" class="form-control">
                                            <div class="custom-control custom-checkbox text-center justify-content-center d-block">
                                                <div class="col-md-5 col-sm-5 d-inline">
                                                    <input class="custom-control-input" id="localAddress" type="checkbox">
                                                    <label for="localAddress" class="p-1 bg-light custom-control-label h6 mr-3 font-weight-normal">En el punto de Engativá</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <button type="submit" class="btn btn-success text-left" id="scheduleContinue"><i class="fas fa-paper-plane"></i> Enviar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="progress mt-5">
                    <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 33,3%" aria-valuenow="33,3" aria-valuemin="0" aria-valuemax="100">Paso 1 de 4</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <!-- <button type="button" class="btn btn-secondary">Guardar borrador</button> -->
            </div>
        </div>
    </div>
</div>

<script>
    let localAddressCheck = document.getElementById('localAddress');
    let input_address = document.getElementById('direccion_lugar');

    if (localAddressCheck) {
        localAddressCheck.addEventListener('change', () => {
            if (input_address.disabled == true) {
                input_address.disabled = false
                input_address.value = '';
            } else {
                input_address.value = 'Carrera 116c # 66A 16 Villa Teresita Engativa';
                input_address.disabled = true
            }
        })
    }

    vnum(16);
</script>
<!-- <script src="assets/js/layouts/getAndValidate.js"></script> -->
<script src="assets/js/general/brand.js"></script>
<script src="assets/js/general/artifact.js"></script>
<script src="assets/js/detalles/detailClient/categoryService.js"></script>
<script src="assets/js/detalles/detailClient/detailService.js"></script>
<script src="assets/js/detalles/detailClient/artifactService.js"></script>