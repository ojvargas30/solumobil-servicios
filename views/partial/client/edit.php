<div class="card col-8 justify-content-center ml-5">
    <div class="card-header text-dark">
        <h2 class="card-title font-italic font-weight-bold">Renovación del Servicio</h2>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row d-none">
            <div class="col-sm-12">
                <div class="form-group">
                    <input type="hidden" class="d-none" id="id_servicio_PK" value="<?php echo $data[0]->id_servicio_PK ?>">
                    <input type="hidden" class="d-none" id="id_revision_servicio_PK" value="<?php echo $data[0]->id_revision_servicio_PK ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Reparaciones</label>
                    <select id="scategory" class="custom-select selector2 controlBuscador" required="">
                        <option value="" aria-readonly="">Buscar categoría</option>
                        <?php
                        foreach ($scategories as $scategory) {

                        ?>
                            <option selected value="<?php echo $scategory->id_categoria_servicio_PK  ?>">
                                <?php echo '#' . $scategory->id_categoria_servicio_PK . ' -' ?>
                                <?php echo $scategory->nombre_cs ?>
                            </option>
                        <?php

                        }
                        ?>
                    </select>
                    <div class="col-md-12 mt-2 text-center justify-content-center">
                        <?php if (count($detailsCat) > 0) {
                            $arrCategoryService = [];
                            foreach ($detailsCat as $detCat) {
                                array_push(
                                    $arrCategoryService,
                                    [
                                        'id_categoria_servicio_PK'     => $detCat->id_categoria_servicio_FK,
                                        'nombre_cs'  => $detCat->nombre_cs
                                    ]
                                );
                            }
                        ?>

                            <script>
                                var arrScategories = <?php echo json_encode($arrCategoryService); ?>
                            </script>

                        <?php } else { ?>

                            <script>
                                var arrScategories = [];
                            </script>

                        <?php } ?>

                        <div class="form-group text-left pl-2 border-success border-bottom" id="list-scategories"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-12">Artefactos</label>
                    <select id="artifact" class="custom-select selector2 controlBuscador" required="">
                        <option value="" aria-readonly="">Seleccione...</option>
                        <?php foreach ($artifacts as $artifact) { ?>
                            <option selected value="<?php echo $artifact->id_artefacto_PK ?>">
                                <?php echo $artifact->modelo ?>
                            </option>
                        <?php } ?>
                    </select>
                    <div class="col-md-12 mt-2 text-center justify-content-center">
                        <?php if (count($detailsArt) > 0) {
                            $arrArtifactService = [];
                            foreach ($detailsArt as $detArt) {
                                array_push(
                                    $arrArtifactService,
                                    [
                                        'id_artefacto_PK'  => $detArt->id_artefacto_FK,
                                        'modelo'           => $detArt->modelo
                                    ]
                                );
                            }
                        ?>
                            <script>
                                var arrArtifacts = <?php echo json_encode($arrArtifactService); ?>
                            </script>

                        <?php } else { ?>

                            <script>
                                var arrArtifacts = [];
                            </script>

                        <?php } ?>

                        <div class="form-group text-left pl-2 border-success border-bottom" id="list-artifacts"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>Problema descrito por ti</label>
                    <textarea class="form-control" maxlength="200" rows="2" id="descripcion_cliente" placeholder="Descripción del cliente"><?php echo $data[0]->descripcion_cliente; ?></textarea>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Dirección domicilio</label>
                            <input type="text" maxlength="200" value="<?php echo $data[0]->direccion_lugar ?>" placeholder="Dirección de citación" id="direccion_lugar" required="" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group text-right">
                            <button id="update" type="submit" class="btn btn-outline-success mt-4">
                                <i class="fas fa-save fa-2x"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>


<script src="assets/js/detalles/detailClient/categoryService.js"></script>
<script src="assets/js/detalles/detailClient/detailService.js"></script>
<script src="assets/js/detalles/detailClient/artifactService.js"></script>