<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" lang="es" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="Author" lang="es" content="Óscar Javier Vargas Diaz, oscarjaviervargas@hotmail.com">
    <meta name="DC.identifier" lang="es" content="">
    <!--Aqui va la pagina Solumovil.............................-->
    <META http-equiv="Expires" lang="es" content="0">
    <!--ESTA NOSE PARA QUE ES.-->
    <meta name="Keywords" lang="es" content="Engativa,Colombia, Bogota,
	servicio tecnico, localidad, Quitar Cuenta Google,
	reparacion, celulares, pantalla, dañada, puerto de carga, tablets, baratos,
	flasheo de celulares o tablets, cambio de pantalla, cambio de repuestos.">
    <META http-equiv="PICS-Label" content='
	(PICS-1.1 "http://www.gcf.org/v2.5"
	labels on "1994.11.05T08:15-0500"
	until "1995.12.31T23:59-0000"
	for "http://w3.org/PICS/Overview.html"
	ratings (suds 0.5 density 0 color/hue 1))
 '>
    <!--Esto es para ayudar a los padres y a las escuelas a controlar los lugares a los
   que pueden acceder los niños en Internet, también facilita otros usos para las etiquetas,
  incluyendo firmas de código, privacidad, y gestión de los derechos de la propiedad
  intelectual.-->
    <META name="copyright" content="&copy; 2020 Solumovil Company.">
    <meta name="Description" lang="es" content="Pagina de servicio profesional
  enfocada en el mantenimiento y reparacion de celulares en la ciudad de Bogota-Colombia.
  Servicio tecnico de moviles.">
    <META name="date" content="19:05:09, sábado 29, febrero 2020 -05">
    <meta name="generator" content="HTML-KIT 2.9" />
    <meta name="language" content="es" />
    <meta name="revisit-after" content="1 month" />
    <meta name="robots" content="index, follow" />
    <meta name="application-name" content="servicio tecnico web de reparacion de celulares" />
    <meta name="encoding" charset="utf-8" />
    <meta http-equiv=»X-UA-Compatible». />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="organization" content="Solumovil Company" />
    <meta name="revisit" content="7" />
    <noscript>
        <meta http-equiv="refresh" content="60; url=https://www.youtube.com/watch?v=XyW1XiNBsaQ">
    </noscript>

    <!----------------------------------------------------------------------------------->
    <!----------------------------------------------------------------------------------->
    <!--TERMINA AQUI --------------------------------------------------------------META-->
    <!----------------------------------------------------------------------------------->

    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="assets/css/adminlte.css">

    <!-- Favicon -->
    <link rel="Shortcut Icon" href="assets/img/1.ico" />

    <!-- SWAL -->
    <link rel="stylesheet" href="assets\plugins\swal\dist\sweetalert2.min.css">

    <!-- TOSTR -->
    <link rel="stylesheet" href="assets\plugins\toastr\toastr.min.css">

    <!-- SELECT CON BUSCADOR -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css">

    <!-- DATATABLE -->
    <!-- <link rel="stylesheet" href="assets/plugins/datatable/css/bootstrap.css"> -->
    <link rel="stylesheet" href="assets/plugins/datatable/datatables-editor/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="assets/plugins/datatable/datatables-editor/css/editor.dataTables.min.css">
    <link rel="stylesheet" href="assets/plugins/datatable/datatables-editor/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="assets/plugins/datatable/datatables-editor/css/select.dataTables.min.css">


    <!-- FONTAWESOME -->
    <link rel="stylesheet" href="assets/font/css/all.css">

    <!-- ANIMATE.CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.compat.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.min.css">

    <title>Solupanel</title>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- NAVBAR -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-secondary" id="barsBtnDash" data-widget="pushmenu" href="#" role="button">
                        <i class="fas fa-bars"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="?c=dash&m=index">Inicio</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <!-- <li class="nav-item">
                    <a class="nav-link text-secondary " href="?c=dash&m=using">
                        <i class=" text-secondary fas fa-question-circle fa-lg"></i>
                    </a>
                </li> -->
                <li class="nav-item ">
                    <a class="nav-link text-secondary" id="closeSessionSort" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i class="fas fa-sort-down"></i></a>
                </li>
            </ul>
        </nav>
        <!-- END NAVBAR -->

        <aside class="main-sidebar sidebar-light-primary shadow-right">
            <!-- Brand Logo -->
            <a href="?c=dash&m=index" class="brand-link">
                <img src="assets/img/logo_disminuido.png" alt="Solupanel" class="brand-image img-circle elevation-1" style="opacity: .8">
                <span class="brand-text font-weight-light">Solupanel</span>
            </a>
            <div class="sidebar">
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="
                        <?php
                        if (isset($_SESSION['user']['user']->foto)) {
                            if ($_SESSION['user']['user']->foto != null) echo $_SESSION['user']['user']->foto;
                            else echo "assets/img/clients/sinfoto.png";
                        } else echo "assets/img/clients/sinfoto.png";

                        ?>" class="img-circle elevation-2" alt="Foto Usuario">
                    </div>
                    <div class="info">
                        <a href="?c=dash&m=index" class="d-block">
                            <?php echo isset($_SESSION['user']['technical']->nombre_tecnico) ?
                                $_SESSION['user']['technical']->nombre_tecnico . ' ' .
                                $_SESSION['user']['technical']->apellido_tecnico : ''
                            ?>

                            <?php echo isset($_SESSION['user']['client']->nombre_cliente) ?
                                $_SESSION['user']['client']->nombre_cliente . ' ' .
                                $_SESSION['user']['client']->apellido_cliente : $_SESSION['user']['user']->correo ?>

                        </a>
                        <small>
                            <u>
                                <strong>
                                    <a href="?c=dash&m=index" class="d-block">
                                        <?php echo isset($_SESSION['user']['user']->rol) ? $_SESSION['user']['user']->rol : ""; ?>
                                    </a>
                                </strong>
                            </u>
                        </small>
                    </div>
                </div>
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link text-secondary">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Servicio Técnico
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="?controller=service&method=index" class="nav-link text-secondary">
                                        <i class="fas fa-hand-sparkles nav-icon"></i>
                                        <p>Seguimiento</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <div class="content-wrapper">
            <div class="content fondo">
                <div class="container-fluid pt-1">
                    <a id="back-to-top" href="#" class="btn btn-default btn-sm rounded-circle text-lightred back-to-top" role="button" aria-label="Scroll to top">
                        <i class="fas fa-chevron-up fa-sm"></i>
                    </a>
                    <aside class="control-sidebar control-sidebar-light">
                        <aside class="control-sidebar control-sidebar-light d-block">
                            <div class="p-3 control-sidebar-content">
                                <h5 class="">Opciones</h5>
                                <hr class="mb-2">
                                <!-- <div class="d-flex">
                                    <div class="d-flex flex-wrap mb-3">
                                        <h2 class="main_time reloj-font" id="time"></h2>
                                        <h3 class="mt-1 reloj-font" id="format"></h3>
                                    </div>
                                </div> -->
                                <!-- <h6 class="mt-2">Modo Semioscuro</h6>
                                <div class="d-flex"></div>
                                <div class="d-flex flex-wrap mb-3">
                                    <button class="switch" id="switch">
                                        <span>
                                            <li class="fas fa-sun"></li>
                                        </span>
                                        <span>
                                            <li class="fas fa-moon"></li>
                                        </span>
                                    </button>
                                </div> -->

                                <div class="my-5">
                                    <a id="exit" class="btn bg-gradient-red text-white cursor-pointer shadow">Cerrar sesion</a>
                                </div>
                            </div>
                        </aside>
                    </aside>

                    <noscript>
                        <div class="position-fixed col-4 justify-content-md-start bg-danger rounded ml-3">
                            <p class="text-light h6">Bienvenido al portal Solumovil</p>
                            <p class="text-light h6">La página que estás
                                viendo requiere para su funcionamiento el uso de JavaScript.
                                Si lo has deshabilitado intencionalmente, por favor vuelve a activarlo.</p>
                            <p class="text-light">Se te redigira a un tutorial en 60 segundos</p>
                        </div>
                    </noscript>

                    <script src="assets/js/jquery-3.5.1.min.js"></script>
                    <script src="assets/js/bootstrap.bundle.min.js"></script>
                    <script src="assets/js/popper.min.js"></script>

                    <!-- SWEETALERT -->
                    <script src="assets/plugins/swal/dist/sweetalert2.all.min.js"></script>

                    <!-- OPTIONAL SCRIPTS -->
                    <script src="assets/js/dashboard3.js"></script>
                    <script src="assets/js/localStorage.js"></script>

                    <!-- SELECT2 CON BUSCADOR -->
                    <!-- <script src="assets/plugins/datatable/js/jquery-3.5.1.js"></script> -->
                    <script src="assets/plugins/select2/select2.min.js"></script>

                    <!-- TOASTR -->
                    <!-- <script src="assets/plugins/toastr/toastr.min.js"></script> -->

                    <script src="assets/js/adminlte.js"></script>
                    <script src="assets/js/general/methods.js"></script>
                    <script src="assets/js/layouts/dashLayout.js"></script>
                    <script src="assets/js/layouts/swalAlerts.js"></script>
                    <script src="assets/js/layouts/fillArrays.js"></script>

                    <script>
                        let exit = ele("exit");
                        if (exit) {
                            exit.addEventListener('click', (e) => {
                                e.preventDefault();

                                let areyousure = confirm("¿Estas seguro de que quieres cerrar sesión?");
                                if (areyousure) location.href = "?c=login&m=destroySession"

                            })
                        }
                    </script>

                    <script>
                        $(document).ready(function() {
                            $('[data-toggle="tooltip"]').tooltip();
                        });
                    </script>