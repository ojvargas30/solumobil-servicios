<div class="container mt-2">
    <div class="row text-right justify-content-center">
        <!-- <button type="button" id="btnTutoInitialReports" class="btn btn-default btn-sm mb-3 rounded-circle">
            <i class="fas fa-question-circle text-primary"></i>
        </button> -->
        <!-- <button type="button" id="btnSpeakReports" class="btn btn-default mb-3 ml-2 rounded-circle">
            <i class="fas fa-assistive-listening-systems text-primary"></i>
        </button> -->
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6 col-sm-12 item h-100">
                    <div class="card px-0 bg-gradient-orange card-custom item-card card-block">
                        <div class="p-3 d-block">
                            <h5 class="card-title text-dark card-title-custom mt-3 mb-3">
                                <?php
                                if (!empty($dailyGain)) {
                                    if (!is_null($dailyGain[0]->daily_gain))
                                        echo '<span class="text-white font-weight-bold">' . number_format($dailyGain[0]->daily_gain) . ' COP</span>';
                                    else {
                                        echo '<span class="text-white text-uppercase font-weight-bold border-white text-sm" title="Te invitamos a que te pases por los
                                diferentes módulos y generes información nueva">No hay ganancia diaria
                                </span>';
                                    }
                                } else {
                                    echo '<span class="text-white text-uppercase font-weight-bold border-white text-sm" title="Te invitamos a que te pases por los
                            diferentes módulos y generes información nueva">No hay ganancia diaria
                            </span>';
                                }
                                ?>
                            </h5>
                            <p class="card-text text-white">Ganancias de hoy</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 item">
                    <div class="card px-0 bg-gradient-green card-custom item-card card-block">
                        <div class="p-3 d-block">
                            <h5 class="card-title text-dark card-title-custom mt-3 mb-3">
                                <?php
                                if (!empty($weeklyGain)) {
                                    if (!is_null($weeklyGain[0]->weekly_gain))
                                        echo '<span class="text-white font-weight-bold">' . number_format($weeklyGain[0]->weekly_gain) . ' COP</span>';
                                    else {
                                        echo '<span class="text-white text-uppercase font-weight-bold border-white text-sm" title="Te invitamos a que te pases por los
                                diferentes módulos y generes información nueva">No hay ganancia semanal
                                </span>';
                                    }
                                } else {
                                    echo '<span class="text-white text-uppercase font-weight-bold border-white text-sm" title="Te invitamos a que te pases por los
                            diferentes módulos y generes información nueva">No hay ganancia semanal
                            </span>';
                                }
                                ?>

                            </h5>
                            <p class="card-text text-white">Ganancias de esta semana</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 item">
                    <div class="card px-0 card-custom bg-gradient-blue item-card card-block">
                        <div class="p-3 d-block">
                            <h5 class="card-title text-dark card-title-custom mt-3 mb-3 ">
                                <?php
                                if (!empty($conflictus_apparatus)) {
                                    if (!is_null($conflictus_apparatus[0]->modelo))
                                        echo '<span class="text-white font-weight-bold">' . $conflictus_apparatus[0]->modelo . '</span>';
                                    else {
                                        echo '<span class="text-white text-uppercase font-weight-bold border-white text-sm" title="Te invitamos a que te pases por los
                                diferentes módulos y generes información nueva">No hay dispositivo con más conflictos
                                </span>';
                                    }
                                } else {
                                    echo '<span class="text-white text-uppercase font-weight-bold border-white text-sm" title="Te invitamos a que te pases por los
                            diferentes módulos y generes información nueva">No hay dispositivo con más conflictos
                            </span>';
                                }
                                ?>
                            </h5>
                            <p class="card-text text-white">Dispositivo con mas conflictos</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 item">
                    <div class="card px-0 bg-gradient-maroon card-custom item-card card-block">
                        <div class="p-3 d-block ">
                            <h5 class="card-title text-dark card-title-custom mt-3 mb-3">
                                <?php
                                if (!empty($bestEmployee)) {
                                    if (
                                        !is_null($bestEmployee[0]->nombre_tecnico) &&
                                        !is_null($bestEmployee[0]->apellido_tecnico)
                                    )
                                        echo '<span class="text-white font-weight-bold">' . $bestEmployee[0]->nombre_tecnico . " " . $bestEmployee[0]->apellido_tecnico . '</span>';
                                    else {
                                        echo '<span class="text-white text-uppercase font-weight-bold border-white text-sm" title="Te invitamos a que te pases por los
                                diferentes módulos y generes información nueva">No hay técnico del mes
                                </span>';
                                    }
                                } else {
                                    echo '<span class="text-white text-uppercase font-weight-bold border-white text-sm" title="Te invitamos a que te pases por los
                            diferentes módulos y generes información nueva">No hay técnico del mes
                            </span>';
                                }
                                ?>
                            </h5>
                            <p class="card-text text-white">Técnico con mas servicios realizados en el mes</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="col-md-12 item">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>
    </div>
</div>



<script>
    let arrMonthlyGain = <?php echo json_encode($arrMonthlyGain); ?>;
    console.log(arrMonthlyGain);

    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
            ],
            datasets: [{
                label: `Ganancias Año ${new Date().getFullYear()}`,
                data: arrMonthlyGain,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>

<!--
<div class="container">
    <div class="row">
        <div class="col-12">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card my-2 card-custom item-card card-block">
                                    <div class="card-body">
                                        <h5 class="card-title  card-title-custom">Special title treatment</h5>
                                        <p class="card-text item-card-title">With supporting text below as a natural lead-in to additional content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-12 mb-5 mt-2 text-center">
                <a class="btn bg-gradient-red rounded-pill shadow border-0" href="#carouselExampleControls" role="button" data-slide="prev">
                    <i class="controls class-fade fas fa-caret-left"></i>
                </a>
                <a class="btn bg-gradient-red rounded-pill shadow border-0" href="#carouselExampleControls" role="button" data-slide="next">
                    <i class="controls class-active fas fa-caret-right"></i>
                </a>
            </div>
        </div>
    </div>
</div> -->

<script>
    // $("#carouselExampleControls").on("slide.bs.carousel", function(e) {
    //     var inner = document.querySelector(".carousel-inner");
    //     var controls = document.querySelectorAll(".controls");
    //     if (e.direction === "left") {
    //         controls[0].className = "controls class-active fas fa-caret-left";
    //     }
    //     if (e.direction === "right") {
    //         controls[1].className = "controls class-active fas fa-caret-right";
    //     }

    //     if (e.relatedTarget == inner.lastElementChild) {
    //         controls[1].className = "controls class-fade fas fa-caret-right";
    //     }
    //     if (e.relatedTarget == inner.firstElementChild) {
    //         controls[0].className = "controls class-fade fas fa-caret-left";
    //     }
    // });
</script>

<script>
    const talk = (texto = "Bienvenido") => speechSynthesis.speak(new SpeechSynthesisUtterance(texto));

    let btnTutoReports = ele("btnTutoInitialReports"),
        btnSpeakReports = ele('btnSpeakReports');

    function tutoReports() {
        btnTutoReports.classList.add("d-none");
        btnSpeakReports.classList.add("d-none");

        let username = "<?php echo $_SESSION['user']['technical']->nombre_tecnico ?>",
            userlastname = "<?php echo $_SESSION['user']['technical']->apellido_tecnico ?>";

        if (userlastname.trim() != '') {
            talk(`Bienvenido señor ${userlastname}`)
        } else if (username.trim() != '') {
            talk(`Bienvenido señor ${username}`)
        } else {
            talk(`Bienvenido señor`)
        }

        talk("En esta sección podra informarse de las ganacias diarias, semanales, dispositivo con mas conflictos y técnico del mes")

        setTimeout(() => {
            btnTutoReports.classList.remove("d-none");
            btnSpeakReports.classList.remove("d-none");
        }, 9700)
    }

    eve(btnTutoReports, tutoReports);


    // function talkReports() {

    //     btnTutoReports.classList.add("d-none");
    //     btnSpeakReports.classList.add("d-none");

    //     // Validar lo que tiene que hablar y que hable

    //     let dailyGain = "<?php //echo (!is_null($dailyGain[0]->daily_gain)) ? $dailyGain[0]->daily_gain : ''
                            ?>"
    //     let weeklyGain = "<?php //echo (!is_null($weeklyGain[0]->weekly_gain)) ? $weeklyGain[0]->weekly_gain : ''
                                ?>"
    //     let conflictus_apparatus = "<?php //echo (!is_null($conflictus_apparatus[0]->modelo)) ? $conflictus_apparatus[0]->modelo : ''
                                        ?>"
    //     let employeeName = "<?php //echo (!is_null($bestEmployee[0]->nombre_tecnico)) ? $bestEmployee[0]->nombre_tecnico : ''
                                ?>"
    //     let employeeLastname = "<?php //echo (!is_null($bestEmployee[0]->apellido_tecnico)) ? $bestEmployee[0]->apellido_tecnico : ''
                                    ?>"

    //     if (dailyGain.trim() != '') {
    //         talk(`Las ganancias de hoy s son de ${dailyGain} pesos`)
    //     } else {
    //         talk("No hay ganancias diarias s")
    //     }

    //     if (weeklyGain.trim() != '') {
    //         talk(`Las ganancias de esta semana son de ${weeklyGain} pesos`)
    //     } else {
    //         talk("No hay ganancias de esta semana s")
    //     }

    //     if (conflictus_apparatus.trim() != '') {
    //         talk(`El dispositivo con más conflictos es ${conflictus_apparatus}`)
    //     } else {
    //         talk("No hay dispositivo con más conflictos registrado")
    //     }

    //     if (employeeName.trim() != '') {
    //         talk(`El técnico del mes es ${employeeName} ${employeeLastname}`)
    //     } else {
    //         talk("No hay técnico del mes registrado")
    //     }

    //     setTimeout(() => {
    //         btnTutoReports.classList.remove("d-none");
    //         btnSpeakReports.classList.remove("d-none");
    //     }, 9700)

    // }

    // eve(btnSpeakReports, talkReports);
</script>