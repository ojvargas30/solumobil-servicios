<div class="jumbotron mt-3 pt-4 justify-content-center text-center">
    <h3 class="">Bienvenido! <span class="font-weight-bolder">
            <?php
            if (isset($_SESSION['user']['technical']->nombre_tecnico)) {
                echo $_SESSION['user']['technical']->nombre_tecnico;
            } elseif (isset($_SESSION['user']['client']->nombre_cliente)) {
                echo $_SESSION['user']['client']->nombre_cliente;
            } else {
                echo $_SESSION['user']['user']->correo;
            }
            ?>
        </span>
    </h3>
    <p class="lead">Dejanos instruirte para que puedas sacar el maximo provecho del software Solupanel<img src="https://img.icons8.com/emoji/48/000000/smirking-face.png" /></p>
    <hr class="my-4">
    <p class="text-sm">En el siguiente video instructivo super interactivo podras apreciar las diferentes funcionalidades que ofrecemos gratis para ti.</p>
    <!-- <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a> -->
    <!-- <div class="embed-responsive embed-responsive-16by9 mt-4">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
    </div> -->
    <div class="container text-center">
        <div class="row">
            <div class="col-12">
                <video autoplay loop id="myVideo" class="embed-responsive embed-responsive-16by9">
                    <source src="assets/img/videos/2.mp4" type="video/mp4">
                    <!-- <source src="assets/img/videos/2.ogg" type="video/ogg"> -->
                    Tu buscador no soporta videos HTML5
                </video>

                <div class="video-content">
                    <div class="row justify-content-center">
                        <h2 class="text-white">Usabilidad</h2>
                        <div class="col-6">
                            <p class="text-white text-left text-sm">Realizar solicitudes de servicio técnico a domicilio o por citá, cambiar servicios y personalizar tu perfil son algunas de las acciones que puedes hacer en nuestra plataforma.</p>
                        </div>
                        <div class="col-6">
                        </div>
                        <button id="myBtn" class="btn rounded btn-lg px-4 ml-2" onclick="myFunction()"><i class='fas fa-pause'></i></button>
                    </div>
                </div>
                <!-- <h1 class="display-2 text-white bg-dark"><strong>PROXIMAMENTE!</strong></h1> -->
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>