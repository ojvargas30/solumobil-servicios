<div class="row mb-5 mx-1 h-100">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gradient-green overflow-auto">
                    <div class="card-header">
                        <h3 class="card-title">Servicios finalízados</h3>

                        <div class="card-tools">
                            <!-- <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="widgets.html" data-source-selector="#card-refresh-content" data-load-on-init="false"><i class="fas fa-sync-alt"></i></button> -->
                            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <div class="card-body">
                        <div class="card p-1 px-2 carta-modo-osc">
                            <h5 class="shadow card-header fondo-titulo text-uppercase text-dark pt-3 text-left text-sm mb-2 mt-1">
                                <small>
                                    <span class="d-inline">
                                        Servicios Finalízados<img src="https://img.icons8.com/emoji/48/000000/smiling-face-with-sunglasses.png" />

                                    </span>
                                    <p class="text-dark d-inline text-sm"> con exito</p>
                                </small>
                            </h5>

                            <table class="shadow table table-white table-sm datatable">
                                <thead>
                                    <tr>
                                        <th class="text-dark">
                                            <small class="font-weight-bold">#<small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold">
                                                <i class="fas fa-user-circle"></i> Cliente
                                                <small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold">
                                                <i class="fas fa-shield-alt fa-xs"></i> Estado
                                                <small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold">
                                                <i class="fas fa-calendar-alt fa-xs"></i> Petición
                                                <small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold ">
                                                <i class="fas fa-dollar-sign fa-xs"></i>
                                                Total<small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold  float-left">
                                                <i class="fas fa-info-circle fa-xs "></i>
                                                Mas
                                                <small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold">
                                                <i class="fas fa-file-signature fa-xs "></i>
                                                Acciones<small>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $cont = 0; //echo count($fservices);
                                    ?>
                                    <?php foreach ($fservices as $fservice) : ?>
                                        <?php $cont++; ?>
                                        <tr class="shadow-sm text-dark" <?= (count($fservices) === $cont) ? "style='background:#c4f4f8'" : "" ?>>
                                            <td class="align-middle text-wrap  texto-negro text-left text-sm">
                                                <span class="d-block  ">
                                                    <?php echo $fservice->id_servicio_PK; ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-left text-sm texto-negrotext-dark">
                                                <span class="d-block">
                                                    <p class="card-text text-dark text-capitalize">
                                                        <?php echo $fservice->num_id_cli ?>
                                                    </p>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap texto-negrotext-dark">
                                                <span class="d-block">
                                                    <span class="card-title text-left text-sm float-none text-uppercase h5">

                                                        <?php switch ($fservice->id_estado_servicio_FK) {
                                                            case 1:
                                                        ?>
                                                                <span class="right badge badge-success"><?php echo $fservice->nombre_estado;
                                                                                                        break; ?></span>
                                                            <?php
                                                            case 2: ?>
                                                                <span class="right badge badge-info"><?php echo $fservice->nombre_estado;
                                                                                                        break; ?></span>
                                                            <?php
                                                            case 3: ?>
                                                                <span class="right badge badge-warning"><?php echo $fservice->nombre_estado;
                                                                                                        break; ?></span>
                                                            <?php
                                                            case 4: ?>
                                                                <span class="right badge badge-danger"><?php echo $fservice->nombre_estado;
                                                                                                        break; ?></span>

                                                        <?php } ?>
                                                    </span>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-left text-sm texto-negrotext-dark">
                                                <span class="d-block">
                                                    <span class="card-title text span">
                                                        <?php
                                                        //   FECHA, strtotime($fecha)));
                                                        $fecha = $fservice->fecha_peticion;
                                                        $fecha = ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
                                                        echo utf8_encode($fecha);
                                                        ?>
                                                        <span class="card-title text h5 float-none">
                                                            <em>
                                                                <!-- HORA -->
                                                                <?php

                                                                $time = new DateTime($fservice->hora_peticion);
                                                                echo $time->format('g:ia');

                                                                ?>
                                                            </em>
                                                        </span>
                                                    </span>
                                                </span>
                                            </td>

                                            <?php foreach ($details as $det) : ?>
                                                <?php if ($fservice->id_servicio_PK == $det->id_servicio_FK) { ?>

                                                    <td class="align-middle text-wrap text-left text-sm texto-negrotext-dark">
                                                        <span class="d-block">
                                                            <span class="card-title text span text-left text-sm float-none">
                                                                <?php echo number_format($det->costo_total); ?>
                                                            </span>
                                                        </span>
                                                    </td>
                                            <?php }
                                            endforeach ?>
                                            <td class="align-middle text-wrap text-left text-sm p-2 texto-negro  ">
                                                <a href="?c=service&m=listDetails&id_servicio_PK=
                                        <?php echo $fservice->id_servicio_PK ?>" class="btnDetail btn btn-info  btn-sm border-light" title="Mas info del servicio"><i class="fas fa-info-circle"></i>
                                                </a>
                                            </td>
                                            <!-- ----------------acciones------------------------- -->
                                            <td class="align-middle text-wrap text-left text-sm p-2 texto-negro">
                                                <?php
                                                if ($fservice->id_estado_servicio_FK <= 2) {
                                                ?>

                                                    <div class="dropdown-divider"></div>

                                                    <div class="dropdown">
                                                        <button class="btn btn-sm btn-dark dropdown-toggle button-inactive border-light" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-power-off fa-lg p-2"></i>
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item btnFinalizar" href="?c=service&m=updateServiceStatus&id_servicio_PK=<?php echo $fservice->id_servicio_PK ?>&id_estado_servicio_FK=3">
                                                                <i class="fas fa-eject fa-xs"></i>
                                                                Finalizar
                                                            </a>
                                                            <a class="dropdown-item btnCancelar" href="?c=service&m=updateServiceStatus&id_servicio_PK=<?php echo $fservice->id_servicio_PK ?>&id_estado_servicio_FK=4">
                                                                <i class="fas fa-window-close fa-xs"></i> Cancelar
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <p></p>
                                                <?php } ?>
                                                <?php if ($fservice->id_estado_servicio_FK <= 4 && $fservice->id_estado_servicio_FK > 2) { ?>

                                                    <div class="dropdown-divider"></div>
                                                    <div class="dropdown">
                                                        <button class="btn btn-success btn-sm dropdown-toggle button-list-active" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-plug fa-lg p-2"></i> Activar
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item btnActive" href="?c=service&m=updateServiceStatus&id_servicio_PK=<?php echo $fservice->id_servicio_PK ?>&id_estado_servicio_FK=1">
                                                                <i class="fas fa-screwdriver fa-xs"></i> En proceso
                                                            </a>
                                                            <a class="dropdown-item btnActive" href="?c=service&m=updateServiceStatus&id_servicio_PK=<?php echo $fservice->id_servicio_PK ?>&id_estado_servicio_FK=2">
                                                                <i class="fas fa-pause-circle fa-xs"></i> En espera
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <p></p>
                                                </span>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <span class="border-bottom p-2 text-warning d-none">
                                <?php echo "Fecha de última modificación del fichero fuente: '" . strftime('%A %d de %B del %Y', strtotime(date("F d Y H:i:s.", getlastmod()))) . "'"; ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-danger overflow-auto">
                    <div class="card-header">
                        <h3 class="card-title">Servicios cancelados</h3>

                        <div class="card-tools">
                            <!-- <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="widgets.html" data-source-selector="#card-refresh-content" data-load-on-init="false"><i class="fas fa-sync-alt"></i></button> -->
                            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="card p-1 px-2 carta-modo-osc">
                            <h5 class="shadow card-header fondo-titulo text-uppercase text-dark pt-3 text-left text-sm mb-2 mt-1">
                                <span class="d-inline">
                                    Servicios Cancelados <img src="https://img.icons8.com/emoji/48/000000/pensive-face.png" />
                                </span>
                                <small class="d-inline">
                                    <p class="text-secondary text-sm"></p>
                                </small>
                            </h5>
                            <table class="shadow table table-white table-sm datatable">
                                <thead class="title-medium text-dark ">
                                    <tr>
                                        <th class=" ">
                                            <small class="font-weight-bold  ">#<small>
                                        </th>
                                        <th scope="col" class="">
                                            <small class="font-weight-bold ">
                                                <i class="fas fa-user-circle"></i> Cliente
                                                <small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold ">
                                                <i class="fas fa-shield-alt fa-xs pr-0 pb-2 pl-1"></i> Estado
                                                <small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold ">
                                                <i class="fas fa-calendar-alt fa-xs"></i> Petición
                                                <small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold ">
                                                <i class="fas fa-dollar-sign fa-xs"></i>
                                                Total<small>
                                        </th>
                                        <th scope="col" class="text-dark">
                                            <small class="font-weight-bold  float-left pl-1 pr-3">
                                                <i class="fas fa-info-circle fa-xs pr-0 pb-2"></i>
                                                Más
                                                <small>
                                        </th>
                                        <th scope="col" class=" ">
                                            <small class="font-weight-bold  pr-4">
                                                <i class="fas fa-file-signature fa-xs pr-0 pl-2 pb-2"></i>
                                                Acciones<small>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <?php $cont = 0; //echo count($ifservices);
                                    ?>
                                    <?php foreach ($cservices as $cservice) : ?>
                                        <?php $cont++; ?>
                                        <tr class="shadow-sm text-dark" <?= (count($cservices) === $cont) ? "style='background:#f2f2f2'" : "" ?>>
                                            <td class="align-middle text-wrap  texto-negro text-left text-sm">
                                                <span class="d-block  ">
                                                    <?php echo $cservice->id_servicio_PK; ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-left text-sm texto-negrotext-dark">
                                                <span class="d-block">
                                                    <p class="card-text text-dark text-capitalize">
                                                        <em><?php echo $cservice->num_id_cli ?></em>
                                                    </p>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap texto-negrotext-dark">
                                                <span class="d-block">
                                                    <span class="card-title text-left text-sm float-none text-uppercase h5">

                                                        <?php switch ($cservice->id_estado_servicio_FK) {
                                                            case 1:
                                                        ?>
                                                                <span class="right badge badge-success"><?php echo $cservice->nombre_estado;
                                                                                                        break; ?></span>
                                                            <?php
                                                            case 2: ?>
                                                                <span class="right badge badge-info"><?php echo $cservice->nombre_estado;
                                                                                                        break; ?></span>
                                                            <?php
                                                            case 3: ?>
                                                                <span class="right badge badge-warning"><?php echo $cservice->nombre_estado;
                                                                                                        break; ?></span>
                                                            <?php
                                                            case 4: ?>
                                                                <span class="right badge badge-danger"><?php echo $cservice->nombre_estado;
                                                                                                        break; ?></span>
                                                        <?php } ?>
                                                    </span>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-left text-sm texto-negrotext-dark">
                                                <span class="d-block">
                                                    <span class="card-title text span">
                                                        <?php
                                                        //   FECHA
                                                        $fecha = $cservice->fecha_peticion;
                                                        $fecha = ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
                                                        echo utf8_encode($fecha);
                                                        ?>
                                                        <span class="card-title text h5 float-none">
                                                            <em>
                                                                <!-- HORA -->
                                                                <?php
                                                                $vars = array();
                                                                $time = new DateTime(parse_str($cservice->hora_peticion, $vars));
                                                                echo $time->format('g:ia');

                                                                ?>
                                                            </em>
                                                        </span>
                                                    </span>
                                                </span>
                                            </td>

                                            <?php foreach ($details as $det) : ?>
                                                <?php if ($cservice->id_servicio_PK == $det->id_servicio_FK) { ?>



                                                    <td class="align-middle text-wrap text-left text-sm texto-negrotext-dark">
                                                        <span class="d-block">
                                                            <span class="card-title text span text-left text-sm float-none">
                                                                <?php echo number_format($det->costo_total); ?>
                                                            </span>
                                                        </span>
                                                    </td>
                                            <?php }
                                            endforeach ?>
                                            <td class="align-middle text-wrap text-left text-sm p-2 texto-negro  ">
                                                <a href="?c=service&m=listDetails&id_servicio_PK=
                                    <?php echo $cservice->id_servicio_PK ?>" class="btn btn-info btn-xs" title="Mas info del servicio"><i class="fas fa-info-circle"></i>
                                                </a>
                                            </td>
                                            <!-- ----------------acciones------------------------- -->
                                            <td class="align-middle text-wrap text-left text-sm p-2 texto-negro">
                                                <?php
                                                if ($cservice->id_estado_servicio_FK <= 2) {
                                                ?>
                                                    <a href="?c=service&m=edit&id_servicio_PK=
                                <?php echo $cservice->id_servicio_PK ?>" class="btn btn-xs btn-warning buttonEditar border-light">
                                                        <i class="fas fa-pen-alt fa-lg p-2"></i>
                                                    </a>
                                                    <div class="dropdown-divider"></div>

                                                    <div class="dropdown">
                                                        <button class="btn btn-xs btn-dark dropdown-toggle button-inactive border-light" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-power-off fa-lg p-2"></i>
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item btnFinalizar" href="?c=service&m=updateServiceStatus&id_servicio_PK=<?php echo $cservice->id_servicio_PK ?>&id_estado_servicio_FK=3">
                                                                <i class="fas fa-eject fa-xs"></i>
                                                                Finalizar
                                                            </a>
                                                            <a class="dropdown-item btnCancelar" href="?c=service&m=updateServiceStatus&id_servicio_PK=<?php echo $cservice->id_servicio_PK ?>&id_estado_servicio_FK=4">
                                                                <i class="fas fa-window-close fa-xs"></i> Cancelar
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <p></p>
                                                <?php } ?>
                                                <?php if ($cservice->id_estado_servicio_FK <= 4 && $cservice->id_estado_servicio_FK > 2) { ?>
                                                    <div class="dropdown">
                                                        <button class="btn btn-success btn-sm dropdown-toggle button-list-active" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-plug fa-lg p-2"></i> Activar
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item btnActive" href="?c=service&m=updateServiceStatus&id_servicio_PK=<?php echo $cservice->id_servicio_PK ?>&id_estado_servicio_FK=1">
                                                                <i class="fas fa-screwdriver fa-xs"></i> En proceso
                                                            </a>
                                                            <a class="dropdown-item btnActive" href="?c=service&m=updateServiceStatus&id_servicio_PK=<?php echo $cservice->id_servicio_PK ?>&id_estado_servicio_FK=2">
                                                                <i class="fas fa-pause-circle fa-xs"></i> En espera
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <p></p>
                                                </span>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <span class="border-bottom p-2 text-warning d-none">
                                <?php echo "Fecha de última modificación del fichero fuente: '" . strftime('%A %d de %B del %Y', strtotime(date("F d Y H:i:s.", getlastmod()))) . "'"; ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>