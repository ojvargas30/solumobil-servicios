<div class="card mt-3">
    <div class="card-header bg-gradient-red text-white">
        <h2 class="card-title">Editar Servicio N° <?php echo isset($id_servicio_PK) ? $id_servicio_PK : '' ?></h2>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row d-none">
            <div class="col-sm-12">
                <div class="form-group">
                    <input type="hidden" class="d-none" id="id_servicio_PK" value="<?php echo $data[0]->id_servicio_PK ?>">
                    <input type="hidden" class="d-none" id="id_revision_servicio_PK" value="<?php echo $details[0]->id_revision_servicio_PK ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">

                <div class="form-group">
                    <label>Reparaciones</label>
                    <select id="scategory" class="custom-select selector2 controlBuscador" required="">
                        <option value="" aria-readonly="">Buscar categoría</option>
                        <?php
                        foreach ($scategories as $scategory) {

                        ?>
                            <option selected value="<?php echo $scategory->id_categoria_servicio_PK  ?>">
                                <?php echo '#' . $scategory->id_categoria_servicio_PK . ' -' ?>
                                <?php echo $scategory->nombre_cs ?>
                            </option>
                        <?php

                        }
                        ?>
                    </select>
                    <div class="col-md-12 mt-2 text-center justify-content-center">
                        <?php if (count($detailsCat) > 0) {
                            $arrCategoryService = [];
                            foreach ($detailsCat as $detCat) {
                                array_push(
                                    $arrCategoryService,
                                    [
                                        'id_categoria_servicio_PK'     => $detCat->id_categoria_servicio_FK,
                                        'nombre_cs'  => $detCat->nombre_cs
                                    ]
                                );
                            }
                        ?>

                            <script>
                                var arrScategories = <?php echo json_encode($arrCategoryService); ?>
                            </script>

                        <?php } else { ?>

                            <script>
                                var arrScategories = [];
                            </script>

                        <?php } ?>

                        <div class="form-group text-sm text-left pl-2 border-success border-bottom" id="list-scategories"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-12">Artefactos</label>
                    <select id="artifact" class="custom-select selector2 controlBuscador" required="">
                        <option value="" aria-readonly="">Seleccione...</option>
                        <?php foreach ($artifacts as $artifact) { ?>
                            <option selected value="<?php echo $artifact->id_artefacto_PK ?>">
                                <?php echo $artifact->modelo ?>
                            </option>
                        <?php } ?>
                    </select>
                    <div class="col-md-12 mt-2 text-center justify-content-center">
                        <?php if (count($detailsArt) > 0) {
                            $arrArtifactService = [];
                            foreach ($detailsArt as $detArt) {
                                array_push(
                                    $arrArtifactService,
                                    [
                                        'id_artefacto_PK'  => $detArt->id_artefacto_FK,
                                        'modelo'           => $detArt->modelo
                                    ]
                                );
                            }
                        ?>
                            <script>
                                var arrArtifacts = <?php echo json_encode($arrArtifactService); ?>
                            </script>

                        <?php } else { ?>

                            <script>
                                var arrArtifacts = [];
                            </script>

                        <?php } ?>

                        <div class="form-group text-sm text-left pl-2 border-success border-bottom" id="list-artifacts"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Técnico asignado</label>
                    <select id="technical" class="custom-select selector2 controlBuscador" required="">
                        <option selected value="0" aria-readonly="">Buscar técnico disponible
                        </option>
                        <?php
                        foreach ($technicians as $technical) {

                        ?>
                            <option value="<?php echo $technical->id_tecnico_PK  ?>">
                                <?php echo '#' . $technical->id_tecnico_PK . ' -' ?>
                                <?php echo $technical->nombre_tecnico . ' * ' ?>
                                <?php echo $technical->num_id_tec ?>
                            </option>
                        <?php

                        }
                        ?>
                    </select>
                    <div class="col-md-12 mt-2 text-center justify-content-center">
                        <?php if (count($detailsTech) > 0) {
                            $arrTechnicalService = [];
                            foreach ($detailsTech as $detTech) {
                                array_push(
                                    $arrTechnicalService,
                                    [
                                        'id_tecnico_PK'     => $detTech->id_tecnico_asignado_FK,
                                        'num_id_tec'        => $detTech->num_id_tec
                                    ]
                                );
                            }
                        ?>

                            <script>
                                var arrTechnicians = <?php echo json_encode($arrTechnicalService); ?>
                            </script>

                        <?php } else { ?>

                            <script>
                                var arrTechnicians = [];
                            </script>

                        <?php } ?>

                        <div class="form-group text-left pl-2 border-success border-bottom" id="list-technicians"></div>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-12">Estado</label>
                    <select id="id_estado_servicio_FK" class="custom-select selector2 controlBuscador" required="">
                        <option value="">Seleccione...</option>
                        <?php foreach ($statuses as $status) : ?>
                            <?php if ($status->id_estado_PK == $data[0]->id_estado_servicio_FK) { ?>
                                <option selected value="<?php echo $status->id_estado_PK ?>">
                                    <?php echo $status->nombre_estado ?>
                                </option>
                            <?php } else { ?>
                                <option value="<?php echo $status->id_estado_PK ?>">
                                    <?php echo $status->nombre_estado; ?>
                                </option>
                        <?php }
                        endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="col-12">Cliente</label>
                    <select id="id_cliente_FK" class="custom-select selector2 controlBuscador" required="">
                        <option value="">Buscar Cliente</option>
                        <?php foreach ($clients as $client) : ?>
                            <?php if ($client->id_cliente_PK == $data[0]->id_cliente_FK) { ?>
                                <option selected value="<?php echo $client->id_cliente_PK ?>">
                                    <?php echo $client->id_cliente_PK . " "; ?>
                                    <?php echo isset($client->correo) ? $client->correo : ''; ?>
                                    <?php echo isset($client->num_id_cli) ? $client->num_id_cli : ''; ?>
                                    <?php echo isset($client->nombre_cliente) ? $client->nombre_cliente : ''; ?>
                                </option>
                        <?php }
                        endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <!-- textarea -->
                <div class="form-group">
                    <label>Descripción Cliente</label>
                    <textarea class="form-control" rows="3" id="descripcion_cliente" placeholder="Descripción del cliente"><?php echo $details[0]->descripcion_cliente; ?></textarea>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Diagnóstico</label>
                    <textarea class="form-control" id="diagnostico" rows="3" placeholder="Diagnóstico del servicio" required><?php echo $details[0]->diagnostico ?></textarea>
                </div>
            </div>
        </div>

        <!-- <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Fecha de citación</label>
                    <input type="date" value="<?php //echo ($details[0]->fecha_encuentro != '') ? date('Y-m-d', strtotime($details[0]->fecha_encuentro)) : ''
                                                ?>" placeholder="Fecha de citación" id="fecha_encuentro" required="" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Hora de citación</label>
                    <input type="time" value="<?php //echo ($details[0]->hora_encuentro != '') ? $details[0]->hora_encuentro : ''
                                                ?>" placeholder="Hora de citación" id="hora_encuentro" required="" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Dirección citación</label>
                    <input type="text" value="<?php //echo ($details[0]->direccion_lugar != '') ? $details[0]->direccion_lugar : ''
                                                ?>" placeholder="Dirección de citación" id="direccion_lugar" required="" class="form-control">
                </div>
            </div>
        </div> -->

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Costo de revisión</label>
                    <input type="number" value="<?php echo $details[0]->costo_revision ?>" class="form-control" placeholder="Precio revisión" id="costo_revision">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Costo de domicilio</label>
                    <input type="number" value="<?php echo ($details[0]->costo_domicilio != '') ? $details[0]->costo_domicilio : '0' ?>" placeholder="Costo por domicilio" id="costo_domicilio" required="" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Abono</label>
                    <input type="number" value="<?php echo ($details[0]->abono != '') ? $details[0]->abono : '0' ?>" placeholder="Abono" id="abono" required="" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Costo por reparaciones</label>
                    <input type="number" value="<?php echo $details[0]->costo_total ?>" placeholder="Precio total" id="costo_total" required="" class="form-control">
                </div>
            </div>
            <!-- <div class="col-sm-3">
                <div class="form-group">
                    <label>Categorías de arreglo</label>
                    <div class="custom-control custom-checkbox">
                        <?php //if ($details[0]->is_software == false) {
                        ?>
                            <input class="custom-control-input" type="checkbox" id="is_software">
                        <?php //} else {
                        ?>
                            <input class="custom-control-input" type="checkbox" id="is_software" checked>
                        <?php //}
                        ?>
                        <label for="is_software" class="custom-control-label">Software</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <?php //if ($details[0]->is_hardware == false) {
                        ?>
                            <input class="custom-control-input" type="checkbox" id="is_hardware">
                        <?php //} else {
                        ?>
                            <input class="custom-control-input" type="checkbox" id="is_hardware" checked>
                        <?php //}
                        ?>
                        <label for="is_hardware" class="custom-control-label">Hardware</label>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-sm-8 col-md-8 pl-3">
                    <h6 class="text-left mt-1 font-weight-bold">Evidencia del servicio</h6>
                    <div class="form-group text-left">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-upload"></i></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" id="evidencia" class="custom-file-input" data-toggle="tooltip" data-placement="top" title="Puedes subir varios archivos" multiple lang="es" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label" for="evidencia">Seleccionar Archivo</label>
                            </div>
                            <legend class="text-muted politica text-center">Este campo es opcional</legend>
                            <div id="msgEvidence">

                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-4 justify-content-center text-center">
                    <div class="form-group">
                        <button id="update" type="submit" class="btn btn-outline-success mt-4">
                            <i class="fas fa-save fa-2x"></i> Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script>
    // Evidencia del servicio
    const evidenciaInput = document.getElementById('evidencia');
    const msgEvidence = document.getElementById('msgEvidence');
    let arrImages = [];

    evidenciaInput.addEventListener('change', async (e) => {
        let imgs = new FormData();

        Array.from(e.target.files).forEach((img, index) => {
            imgs.append(`img${index}`, img);
        })

        let options = {
            method: 'POST',
            body: imgs
        }

        fetch('?c=service&m=serviceEvidence', options)
            .then(res => {
                return res.json();
            })
            .then(res => {

                if (res.ok) swalSimple("Evidencia subida correctamente")
                else swalSimple("Error al subir la evidencia");

                arrImages = res.images;
            })
            .catch(e => console.log(e))
    })
</script>


<script src="assets/js/detalles/categoryService.js"></script>
<script src="assets/js/detalles/technicalService.js"></script>
<script src="assets/js/detalles/detailService.js"></script>
<script src="assets/js/detalles/artifactService.js"></script>