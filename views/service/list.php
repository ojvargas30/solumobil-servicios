<div class="row mx-1 h-100 carta-modo-osc shadow">
    <div class="col-md-12 carta-modo-osc ">
        <div class="row ">
            <div class="col-md-12 carta-modo-osc mb-2 bg-gradient-light">
                <h5 class="my-2 p-1">Reporte Estado Servicio Técnico <span class="text-success">de Hoy</span> </h5>
                <div class="row carta-modo-osc p-3 border-top mb-5">
                    <div class="col-md-3 border-top border-bottom rounded">
                        <div class="small-box carta-modo-osc bg-white shadow-sm">
                            <div class="inner">
                                <h3><?php echo $status_reports[0]->en_proceso; ?></h3>

                            </div>
                            <div class="icon">
                                <i class="fas fa-cogs callout mt-0 pt-0 callout-success">
                                    <p class="text-res text-dark">En proceso</p>
                                </i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 border-top border-bottom rounded ">
                        <div class="small-box carta-modo-osc bg-white shadow-sm">
                            <div class="inner">
                                <h3><?php echo $status_reports2[0]->en_espera; ?></h3>

                            </div>
                            <div class="icon">
                                <i class="fas fa-pause-circle callout mt-0 pt-0 callout-info">
                                    <p class="text-res text-dark">En espera</p>

                                </i>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 border-top border-bottom rounded ">
                        <div class="small-box carta-modo-osc bg-white shadow-sm">
                            <div class="inner">
                                <h3><?php echo $status_reports3[0]->finalizado; ?></h3>

                            </div>
                            <a href="?c=service&m=history">
                                <div class="icon">
                                    <i class="fas fa-hourglass-end callout mt-0 pt-0 callout-warning">
                                        <p class="text-res text-dark">Finalizados</p>

                                    </i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 border-top border-bottom rounded ">
                        <div class="small-box carta-modo-osc bg-white shadow-sm">
                            <div class="inner">
                                <h3><?php echo $status_reports4[0]->cancelado; ?></h3>

                            </div>
                            <a href="?c=service&m=history">
                                <div class="icon">
                                    <i class="fas fa-ban text-gradient-red callout mt-0 pt-0 callout-danger">
                                        <p class="text-res text-dark">Cancelados</p>

                                    </i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 carta-modo-osc">
        <div class="row carta-modo-osc">
            <div class="col-md-12 carta-modo-osc">
                <div class="card carta-modo-osc overflow-auto">
                    <div class="card-header bg-gradient-red carta-modo-osc align-middle d-block">
                        <span class="card-title h5 font-weight-bolder">
                            Servicio Técnico Activo
                        </span>

                        <div class="card-tools mb-0 pb-0">
                            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button> -->
                            <button class="btn btn-sm border-light shadow border-0 m-0 mb-0 mr-5 bg-gradient-green buttonAdd toastrDefaultInfo text-center" id="modalToggleTechAdmin">
                                <i class="fas fa-folder-plus"></i> Nuevo
                            </button>
                            <button type="button" class="btn btn-tool pr-0" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                            <button type="button" class="btn btn-tool pr-0" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                    </div>

                    <div class="">
                        <div class="p-1 px-2">
                            <table id="table" class="shadow table bg-white
                                        table-bordered table-active
                                         table-hover table-sm
                                         ">
                                <thead class=" text-light bg-gradient-red">
                                    <tr>
                                        <th class="head-hover">
                                            <small class="font-weight-bold"><small> #
                                        </th>
                                        <th class="head-hover">
                                            <small class="font-weight-bold"> Cliente
                                                <small>
                                        </th>
                                        <th class="head-hover">
                                            <small class="font-weight-bold">
                                                Estado
                                                <small>
                                        </th>
                                        <th class="head-hover" title="Fecha de solicitud">
                                            <small class="font-weight-bold">
                                                Solicitud
                                                <small>
                                        </th>
                                        <th class="head-hover">
                                            <small class="font-weight-bold">
                                                Problema<small>
                                        </th>

                                        <th class="head-hover">
                                            <small class="font-weight-bold">
                                                Diagnóstico<small>
                                        </th>

                                        <th class="head-hover">
                                            <small class="font-weight-bold">
                                                Dispositivo<small>
                                        </th>

                                        <th class="head-hover">
                                            <small class="font-weight-bold">
                                                Total<small>
                                        </th>
                                        <th class="head-hover">
                                            <small class="font-weight-bold">
                                                Acciones<small>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="pr-0" id="table-body">
                                    <?php
                                    $cont = 0;
                                    ?>
                                    <?php
                                    foreach ($services as $service) :
                                    ?>
                                        <?php
                                        $cont++;

                                        ?>

                                        <tr class="shadow-sm text-dark" <?= (count($services) === $cont) ? "style='background:#f2f2f2'" : "" ?>>
                                            <td class="align-middle text-wrap texto-negro text-center">
                                                <span class="d-block">
                                                    <?php echo "<span style='font-size:0.81rem !important;'>" . $service->id_servicio_PK . "</span>"; ?>

                                                    <?php
                                                    $flagEvidenceService = true;
                                                    if (count($evidence_service) != 0) {
                                                        if ($evidence_service[0]->url_foto_evidencia != null) {

                                                            $arrEvidences = [];
                                                            for ($i = 0; $i < count($evidence_service); $i++) {
                                                                if ($evidence_service[$i]->id_servicio_FK == $service->id_servicio_PK) {
                                                                    array_push($arrEvidences, $evidence_service[$i]->url_foto_evidencia);
                                                                }
                                                            }

                                                            for ($i = 0; $i < count($evidence_service); $i++) {
                                                                if ($evidence_service[$i]->id_servicio_FK == $service->id_servicio_PK) {

                                                                    // array_push($arrEvidences, trim($evidence_service[$i]->url_foto_evidencia));

                                                                    if ($flagEvidenceService) { ?>
                                                                        <ul class="list-inline mt-2 mr-1">
                                                                            <li class="">
                                                                                <a class="" onclick='modalEvidenceService(<?php echo $service->id_servicio_PK; ?>, <?php echo json_encode($arrEvidences); ?>)' data-toggle="modal" data-target="#evidenceService<?php echo $service->id_servicio_PK; ?>" href="#myGallery" data-slide-to="<?php echo $i ?>">
                                                                                    <div class="">
                                                                                        <?php
                                                                                        // $flagImg = true;
                                                                                        foreach ($evidence_service as $evidence) {
                                                                                            if ($evidence->id_servicio_FK == $service->id_servicio_PK) {
                                                                                                // if($flagImg){
                                                                                                // dd($evidence->url_foto_evidencia);
                                                                                                // echo (file_exists($evidence->url_foto_evidencia)) ?  'si' : $evidence->url_foto_evidencia
                                                                                                // if (file_exists($evidence->url_foto_evidencia)) {
                                                                                        ?>
                                                                                                <img class="img-thumbnail img-fluid" width="70" src="<?php echo $evidence->url_foto_evidencia; ?>">
                                                                                    </div>
                                                                            <?php
                                                                                                // }
                                                                                                break;
                                                                                                // $flagImg = false;}
                                                                                            }
                                                                                        } ?>
                                                                            <!-- <small class="text-muted politica3">Evidencia</small> -->
                                                                            <!-- Caption -->
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                    <?php
                                                                        $flagEvidenceService = false;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }  ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-center texto-negro tcliente">
                                                <span class="d-block text-sm" style="font-size: 0.75rem !important;">

                                                    <?php
                                                    echo '#' . $service->id_cliente_PK . ' ';
                                                    if ($service->correo != '') {
                                                        echo isset($service->correo) ? "<span class='politica font-weight-bolder'>" . $service->correo . "</span><div></div> " : '<div></div> ';
                                                    } else {
                                                        echo isset($service->num_id_cli) ? $service->num_id_cli . " <div></div> " : ' <div></div> ';
                                                    }
                                                    echo isset($service->nombre_cliente) ? $service->nombre_cliente : ' <div></div> ';
                                                    echo isset($service->apellido_cliente) ? $service->apellido_cliente . " <div></div> " : ' <div></div> ';
                                                    ?>

                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap texto-negro testado">
                                                <span class="d-block text-center float-none text-uppercase">

                                                    <?php switch ($service->id_estado_servicio_FK) {
                                                        case 1:
                                                    ?>
                                                            <span class="right badge bg-gradient-green text-center">
                                                                <?php echo $service->nombre_estado;
                                                                break; ?>
                                                            </span>
                                                        <?php
                                                        case 2: ?>
                                                            <span class="right badge bg-gradient-blue text-center">
                                                                <?php echo $service->nombre_estado;
                                                                break; ?>
                                                            </span>
                                                        <?php
                                                        case 3: ?>
                                                            <span class="right badge badge-warning text-center">
                                                                <?php echo $service->nombre_estado;
                                                                break; ?>
                                                            </span>
                                                        <?php
                                                        case 4: ?>
                                                            <span class="right badge bg-gradient-red text-center">
                                                                <?php echo $service->nombre_estado;
                                                                break; ?>
                                                            </span>

                                                    <?php } ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-left texto-negro tpeticion">
                                                <span class="d-block text-sm" style="font-size: 0.75rem !important;">
                                                    <?php
                                                        $fecha = $service->fecha_peticion;
                                                        $fecha = ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
                                                        echo utf8_encode($fecha);
                                                    ?>
                                                    <span class="float-none">
                                                        <span class="font-weight-bolder">
                                                            <?php
                                                            $vars = array();
                                                            $time = new DateTime(parse_str($service->hora_peticion, $vars));
                                                            echo $time->format('g:ia');
                                                            ?>
                                                        </span>
                                                    </span>
                                                </span>
                                            </td>

                                            <?php foreach ($details as $det) : ?>
                                                <?php if ($service->id_servicio_PK == $det->id_servicio_FK) { ?>
                                                    <!-- <td class="align-middle text-wrap text-left texto-negro ttotal">
                                                        <span class="d-block text-sm float-none" style="font-size: 0.75rem !important;">
                                                            <?php

                                                            // if (CURRENT_HOST != 'localhost') {
                                                            //echo ($det->fecha_encuentro != '') ? $fecha : ' <div></div>';
                                                            //} else {
                                                            //$fecha = ucfirst(strftime('%A %d de %B del %Y', strtotime($det->fecha_encuentro)));
                                                            //echo ($det->fecha_encuentro != '') ? $fecha : ' <div></div>';
                                                            //}
                                                            ?>

                                                            <span class="font-weight-bold">
                                                                <?php
                                                                //$hora = new DateTime($det->hora_encuentro);
                                                                //$time = $hora->format('g:ia');
                                                                //echo ($det->hora_encuentro != '') ? $time : ' <div></div>';
                                                                ?>
                                                            </span>
                                                            <?php
                                                            //echo " en <span class='font-weight-normal'>" . $det->direccion_lugar . "</span>"
                                                            ?>
                                                        </span>
                                                    </td> -->
                                                    <td class="align-middle text-wrap text-center texto-negro">
                                                        <span class="d-block text-sm float-none" style="font-size: 0.75rem !important;">
                                                            <?php
                                                            echo $det->descripcion_cliente;
                                                            ?>
                                                        </span>
                                                    </td>


                                                    <td class="align-middle text-wrap text-center texto-negro">
                                                        <span class="d-block text-sm float-none" style="font-size: 0.75rem !important;">
                                                            <?php
                                                            echo $det->diagnostico;
                                                            ?>
                                                        </span>
                                                    </td>

                                                    <td class="align-middle text-wrap text-center texto-negro">
                                                        <span class="d-block text-sm float-none" style="font-size: 0.75rem !important;">
                                                            <?php
                                                            echo $det->imei;
                                                            ?>
                                                        </span>
                                                    </td>

                                                    <td class="align-middle text-wrap text-center texto-negro ttotal">
                                                        <span class="d-block text-sm float-none" style="font-size: 0.75rem !important;">
                                                            <?php
                                                            if ($det->pagado == 2) echo '<span class="text-success">Pagado</span>';
                                                            else echo number_format($det->costo_total) . ' COP';
                                                            ?>
                                                        </span>
                                                    </td>

                                                    <?php
                                                    $arrCostsWithId = [];
                                                    array_push($arrCostsWithId, $det->id_revision_servicio_PK);
                                                    array_push($arrCostsWithId, $det->costo_revision);
                                                    array_push($arrCostsWithId, $det->costo_domicilio);
                                                    array_push($arrCostsWithId, $det->abono);
                                                    array_push($arrCostsWithId, $det->costo_total);
                                                    ?>

                                            <?php }
                                            endforeach ?>

                                            <!-- ----------------ACCIONES------------------------- -->
                                            <td class="align-middle text-wrap text-center texto-negro">
                                                <?php
                                                if ($service->id_estado_servicio_FK <= 2) {
                                                ?>
                                                    <!-- FINALIZAR SERVICIO -->
                                                    <button type="button" onclick="endService(<?php echo $service->id_servicio_PK ?>)" class="btn bg-gradient-success btn-xs btnsEndServiceAdmin">
                                                        <i class="fas fa-check"></i>
                                                    </button>

                                                    <!-- CANCELAR SERVICIO -->
                                                    <button type="button" onclick="cancelService(<?php echo $service->id_servicio_PK ?>)" class="btn my-1 bg-gradient-danger btn-xs btnsCancelServiceAdmin">
                                                        <i class="fas fa-window-close"></i>
                                                    </button>

                                                    <div>

                                                    </div>

                                                    <!-- EDITAR -->
                                                    <button type="button" class="btn bg-gradient-indigo btn-xs" onclick="editService(<?php echo $service->id_servicio_PK ?>)">
                                                        <i class="fas fa-pen-alt"></i>
                                                    </button>


                                                    <!-- Costear Srevicio -->
                                                    <button type="button" class="fas fa-dollar-sign fa-xs btn bg-gradient-olive btn-sm" onclick='modalCostingService(<?php echo json_encode($arrCostsWithId); ?>)' data-toggle="modal" data-target="#costingService<?php echo $arrCostsWithId[0]; ?>">

                                                    </button>

                                                    <!-- Ver mas -->
                                                    <button type="button" class="btn bg-gradient-blue btn-xs" onclick="viewMoreService(<?php echo $service->id_servicio_PK ?>)" title="Detalles">
                                                        <i class="fas fa-angle-double-right"></i>
                                                    </button>



                                                <?php } ?>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<div id="modalEvidenceServiceContainer">

</div>

<div id="modalCostingServiceContainer">

</div>


<script>
    // Evidencia del servicio
    function modalEvidenceService(idService, arrImages) {
        console.log(arrImages);
        let modal = `
        <div class="modal fade" id="evidenceService${idService}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-gradient-red">
                        <div class="pull-left">Evidencia del servicio</div>
                        <button type="button" class="close" data-dismiss="modal" title="Close">
                        <span class="fas fa-times"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <div id="myGallery" class="carousel slide" data-interval="false">
                        <div class="carousel-inner">
                    `;

        let flagImg = true;

        for (let i = 0; i < arrImages.length; i++) {
            modal += `
                    <div class="text-center my-2 rounded col-12 item ${(flagImg) ? 'active' : ''}">
                        <img class="shadow-lg" src="${arrImages[i]}" width="350" alt="evidence${i + 1}">
                        <div class="carousel-caption">
                            <h4 class="bg-dark rounded-pill" style="opacity: 0.25"><span class="" style="opacity: 10">Evidencia N°${i + 1}</span></h4>
                        </div>
                    </div>
                    `;

            flagImg = false;

        }

        modal += `
                    <a class="left carousel-control" href="#myGallery" role="button" data-slide="prev">
                        <i class="fas fa-chevron-left text-white"></i>
                    </a>
                    <a class="right carousel-control float-right mr-4" href="#myGallery" role="button" data-slide="next">
                        <i class="fas fa-chevron-right text-white"></i>
                    </a>
                </div>
            </div>
                            <div class="modal-footer">
                                <div class="pull-left"></div>
                                <button class="btn-sm close" type="button" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;

        let modalEvidenceService = ele('modalEvidenceServiceContainer');

        modalEvidenceService.innerHTML = modal;
    }

    const editCostService = (idService, arrCostsToEdit) => {
        if (idService != '') {
            let arrIdAndostsService = {
                arrCostsToEdit,
                idService
            };
            console.log(arrIdAndostsService);
            console.log(typeof arrIdAndostsService);

            let options = {
                    method: 'POST',
                    body: "costing=" + JSON.stringify(arrIdAndostsService),

                    headers: {
                        // multipart/form-data
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                },
                url = '?c=service&m=costingService';

            fetch(url, options)
                .then(res => {
                    if (res.ok) {
                        return res.json()
                    }
                })
                .then(result => {

                    console.log(result);

                    if (result == "well") {
                        swalSimple("Servicio Costeado")

                        setTimeout(() => {
                            location.href = "?c=service&m=index";
                        }, 2000)
                    } else {
                        swalError("Error, Datos ingresados invalidos")
                    }
                })
                .catch(e => {
                    swalError("Error, Datos ingresados invalidos")

                    console.error(e)
                });
        }
    }



    // Evidencia del servicio
    function modalCostingService(arrCostsWithId) {
        console.log(arrCostsWithId);

        let idService = arrCostsWithId[0];

        let modal = `
        <div class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog" id="costingService${idService}" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-gradient-red">
                        <div class="pull-left">Costear servicio N°${idService}</div>
                        <button type="button" class="close" data-dismiss="modal" title="Close">
                            <span class="fas fa-times"></span>
                        </button>
                    </div>
                    <div class="modal-body row justify-content-center">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label>Costo de revisión</label>
                            <input type="number" value="${(arrCostsWithId[1]) ? arrCostsWithId[1] : '0' }" class="form-control" placeholder="Costo por revisión" id="costo_revisionEdit">
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label>Costo de domicilio</label>
                            <input type="number" value="${(arrCostsWithId[2]) ? arrCostsWithId[2] : '0' }" placeholder="Costo por domicilio" id="costo_domicilioEdit" required="" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label>Abono</label>
                            <input type="number" value="${(arrCostsWithId[3]) ? arrCostsWithId[3] : '0' }" placeholder="Abono" id="abonoEdit" required="" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label>Costo por reparaciones</label>
                            <input type="number" value="${(arrCostsWithId[4]) ? arrCostsWithId[4] : '0' }" placeholder="Costo por reparaciones" id="costo_totalEdit" required="" class="form-control">
                        </div>
                    </div>

                    </div>
                    <div class="modal-footer row text-center d-block justify-content-center">
                        <div class="pull-left"></div>
                        <button class="btn bg-gradient-success" id="btnCostingService" type="button">Costear</button>
                    </div>
                </div>
            </div>
        </div>`;

        let arrCosts = {};

        let modalCostingService = ele('modalCostingServiceContainer');

        modalCostingService.innerHTML = modal;

        eve(ele('btnCostingService'), () => {
            arrCosts.costo_revision = ele('costo_revisionEdit').value;
            arrCosts.costo_domicilio = ele('costo_domicilioEdit').value;
            arrCosts.abono = ele('abonoEdit').value;
            arrCosts.costo_total = ele('costo_totalEdit').value;
            editCostService(idService, arrCosts)
        });
    }




    // <small>Photographs by <a href="https://placeimg.com" target="new">placeimg.com</a></small>
    // Toca Evitar csrf token
    function endService(idService) {
        console.log(`click`);
        if (confirm("¿Esta seguro de que finalizo el servicio?"))
            location.href = `?c=service&m=updateserviceStatus&id_servicio_PK=${idService}&id_estado_servicio_FK=3`;
    }

    function cancelService(idService) {
        console.log(`click`);
        if (confirm("¿Esta seguro de que el servicio se cancelo?"))
            location.href = `?c=service&m=updateserviceStatus&id_servicio_PK=${idService}&id_estado_servicio_FK=4`;
    }

    function editService(idService) {
        location.href = `?c=service&m=edit&id_servicio_PK=${idService}`;
    }

    function viewMoreService(idService) {
        location.href = `?c=service&m=listDetails&id_servicio_PK=${idService}`;
    }

    function datatable() {
        $('#table').DataTable({
            "order": [
                [0, "desc"]
            ],
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            buttons: [{
                    //Botón para PDF
                    extend: 'copy',
                    footer: true,
                    title: 'Copiar info de la tabla en el portapapéles',
                    // filename: 'SolumovilReport',
                    text: '<button class="btn btn-default shadow-sm btn-sm"><i class="fas fa-copy"></i></button>'
                },
                {
                    //Botón para Excel
                    extend: 'excel',
                    footer: true,
                    title: 'Archivo Excel',
                    filename: 'SolumovilExcelReport',
                    text: '<button class="btn bg-gradient-green btn-sm"><i class="fas fa-file-excel"></i></button>'
                },
                {
                    //Botón para PDF
                    extend: 'pdf',
                    footer: true,
                    title: 'Archivo PDF',
                    filename: 'SolumovilPDFReport',
                    text: '<button class="btn bg-gradient-red btn-sm"><i class="far fa-file-pdf"></i></button>'
                },
                {
                    //Imprimir información de la tabla
                    extend: 'print',
                    footer: true,
                    title: 'Imprimir información de la tabla',
                    text: '<button class="btn bg-gradient-blue btn-sm"><i class="fas fa-print"></i></button>'
                },
                {
                    //Visualizar determinados campos de la tabla
                    extend: 'colvis',
                    footer: true,
                    title: 'Visualizar determinados campos de la tabla',
                    text: '<button class="btn bg-gradient-orange btn-sm"><i class="fas fa-eye text-white"></i></button>'
                }
            ]
        }).buttons().container().appendTo('#table_wrapper .col-md-6:eq(0)');
    }

    $(document).ready(function() {
        datatable()
    });

    const verAuditoria = document.querySelectorAll(".verAuditoria");
    verAuditoria.forEach((p, i) => {
        p.addEventListener('click', (e) => {
            let o = e.target;
            if (o.value == "Audit") {
                o.classList.remove("btn-default");
                o.classList.add("btn-danger");
                o.classList.add("mt-1");
                o.value = "Ocultar";
                document.getElementsByClassName("noVerTrAuditoria")[i].classList.remove("d-none");
            } else {
                o.classList.remove("btn-danger");
                o.classList.remove("mt-1");
                o.classList.add("btn-default");
                o.value = "Audit";
                document.getElementsByClassName("noVerTrAuditoria")[i].classList.add("d-none");
            }
        });
    });
</script>