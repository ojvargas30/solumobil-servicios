<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <div class="text-left col-11 border-left">
                    <div class="row pl-2">
                        <h5 class="modal-title text-white col-10">
                            <strong>
                                SOLICITUD DE SERVICIO TÉCNICO
                            </strong>
                            <em class="ml-1 d-none" id="draftTitle">
                                <u>
                                    <small class="text-white text-sm" id="textDraft">Borrador</small>
                                </u>
                            </em>
                        </h5>
                        <h6 class="text-white col-10 text-sm">Los campos con <span class="text-white"> * </span> son
                            obligatorios</h6>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active text-danger" id="serviceTab" data-toggle="tab" href="#service-data" role="tab">Servicios</a>
                    <li>
                    <li class="nav-item">
                        <a class="nav-link text-danger" id="artifactTab" data-toggle="tab" href="#artifact-data" role="tab">Dispositivos</a>
                    <li>
                    <li class="nav-item">
                        <a class="nav-link text-danger" id="clientTab" data-toggle="tab" href="#client_data" role="tab">Cliente</a>
                    <li>
                    <!-- <li class="nav-item">
                        <a class="nav-link text-danger" id="techTab" data-toggle="tab" href="#tech_data" role="tab">Técnico</a>
                    <li> -->
                    <li class="nav-item">
                        <a class="nav-link text-danger" id="diagTab" data-toggle="tab" href="#diag_data" role="tab">Diagnóstico</a>
                    <li>
                    <!-- <li class="nav-item">
                        <a class="nav-link text-danger" id="citaTab" data-toggle="tab" href="#cita_data" role="tab">Entrega</a>
                    <li> -->
                    <li class="nav-item">
                        <a class="nav-link text-danger" id="costTab" data-toggle="tab" href="#cost_data" role="tab">Costo</a>
                    <li>
                        <!-- <li class="nav-item">
                        <a class="nav-link text-danger" id="evidenceTab" data-toggle="tab" href="#evidence_data" role="tab">Evidencia</a>
                    <li> -->
                </ul>

                <div class="tab-content mt-2">
                    <div class="tab-pane fade show active p-1 justify-content-center text-center" id="service-data" role="tabpanel">
                        <!-- <h5 class="m-1">Servicio</h5> -->
                        <div class="d-none">
                            <input id="id_servicio_PK" value="<?php echo $data[0]->id_servicio_PK ?>">
                            <input id="id_revision_servicio_PK" value="<?php echo $details[0]->id_revision_servicio_PK ?>">
                        </div>
                        <div class="row justify-content-center ml-2 pl-2">
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <h6 class="text-center mt-1 font-weight-bold">Servicio/s<span class="text-danger"> * </span></h6>
                                    <select id="scategory" autofocus style="width: 100%;" class="custom-select selector2 controlBuscador " required="">
                                        <option selected value="" aria-readonly="">Seleccione...</option>
                                        <?php foreach ($scategories as $scategory) {
                                            if ($scategory->id_categoria_servicio_PK != 1) { ?>
                                                <option value="<?php echo $scategory->id_categoria_servicio_PK  ?>">
                                                    <?php echo '#' . $scategory->id_categoria_servicio_PK . ' ';  ?>
                                                    <?php echo isset($scategory->nombre_cs) ? $scategory->nombre_cs : '' ?>
                                                </option>
                                        <?php }
                                        } ?>
                                    </select>
                                    <!-- <div class="col-md-2 mt-2">
                                        <a href="#" id="addScategory" class="btn btn-secondary rounded-circle border p-2"><i class="fas fa-plus fa-sm p-1"></i>
                                        </a>
                                    </div> -->
                                    <div class="col-md-12 mt-2 text-center justify-content-center">
                                        <?php if (isset($detailsCat) > 0) {
                                            $arrCategoryService = [];
                                            foreach ($detailsCat as $detCat) {
                                                array_push(
                                                    $arrCategoryService,
                                                    [
                                                        'id_categoria_servicio_PK'     => $detCat->id_categoria_servicio_FK,
                                                        'nombre_cs'  => $detCat->nombre_cs
                                                    ]
                                                );
                                            }
                                        ?>

                                            <script>
                                                var arrScategories = <?php echo json_encode($arrCategoryService); ?>
                                            </script>

                                        <?php } else { ?>

                                            <script>
                                                var arrScategories = [];
                                            </script>

                                        <?php } ?>

                                        <div class="form-group text-left pl-2 border-success border-bottom" id="list-scategories">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 justify-content-center d-flex align-items-center">
                                <a id="showSubFormService" class="btn bg-gradient-success text-center btn-sm shadow align-self-center text-center m-auto" href="#">Nuevo</a>
                                <div id="subFormService" class="subForm d-none mt-5">
                                    <div class="row mb-3 justify-content-center text-center">
                                        <div class="col-sm-12 form-group">
                                            <h6 class="text-sm d-block text-left">
                                                <span class="text-danger">*</span> Nombre
                                            </h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fas fa-wrench"></i></span>
                                                </div>
                                                <input id="nombre_cs" type="text" maxlength="200" class="form-control" placeholder="Nombre Servicio">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 form-group">
                                            <h6 class="text-sm d-block text-left">
                                                Precio
                                            </h6>
                                            <div class="input-group input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                                </div>
                                                <input id="precio_cs" type="number" max="9999999" class="form-control" placeholder="Precio Servicio" title="No muy frecuente">
                                            </div>
                                            <span class="text-muted politica3 d-block p-0 mb-3">Opcional</span>
                                        </div>
                                        <button id="submitSubFormService" type="button" class="btn bg-gradient-info btn-sm text-center">
                                            <i class="fas fa-save"></i> Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn bg-gradient-secondary mt-1 text-center" id="service-data-continue">Siguiente</button>
                    </div>
                    <div class="tab-pane fade p-1 justify-content-center text-center" id="artifact-data" role="tabpanel">
                        <div class="row justify-content-center ml-2 pl-2">
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <h6 class="text-center mt-1 font-weight-bold">Dispositivo<span class="text-danger"> * </span></h6>
                                    <select id="artifact" style="width: 100%;" class="custom-select selector2 controlBuscador" required="">
                                        <option selected value="" aria-readonly="" id="seleccioneDispositivo">Seleccione...</option>
                                        <?php foreach ($artifacts as $artifact) { ?>
                                            <option value="<?php echo $artifact->id_artefacto_PK ?>">
                                                <?php echo '#' . $artifact->id_artefacto_PK . ' ' ?>
                                                <?php echo $artifact->nombre_categoria_artefacto ?>
                                                <?php echo $artifact->modelo . ' -' ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <!-- <div class="col-md-2 mt-2">
                                        <a href="#" id="addArtifact" class="btn btn-secondary rounded-circle border p-2"><i class="fas fa-plus fa-sm p-1"></i>
                                        </a>
                                    </div> -->
                                    <div class="col-md-12 mt-2 text-center justify-content-center">
                                        <?php if (isset($detailsArt) > 0) {
                                            $arrArtifactService = [];
                                            foreach ($detailsArt as $detArt) {
                                                array_push(
                                                    $arrArtifactService,
                                                    [
                                                        'id_artefacto_PK'  => $detArt->id_artefacto_FK,
                                                        'modelo'           => $detArt->modelo
                                                    ]
                                                );
                                            }
                                        ?>
                                            <script>
                                                var arrArtifacts = <?php echo json_encode($arrArtifactService); ?>
                                            </script>

                                        <?php } else { ?>

                                            <script>
                                                var arrArtifacts = [];
                                            </script>

                                        <?php } ?>

                                        <div class="form-group text-left pl-2 border-success border-bottom" id="list-artifacts">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 justify-content-center d-flex align-items-center">
                                <div class="form-group">
                                    <div class="ghost">
                                        <a id="ghost-child-artifact" class="btn bg-gradient-success text-center btn-sm shadow align-self-center text-center m-auto" href="#">Nuevo</a>

                                        <div id="ghost-child-content-artifact">
                                            <div class="row mb-3 justify-content-center text-center">
                                                <div class="col-sm-8">
                                                    <label for="id_marca_artefacto_FK" class="text-sm text-muted col-12"><span class="text-danger">*</span>Marca <i class="fas fa-desktop"></i></label>
                                                    <select id="id_marca_artefacto_FK" style="width: 100%;" class="form-control-sm custom-select selector2 controlBuscador" required="">
                                                        <option selected value="" id="seleccione">Seleccione...</option>
                                                        <?php foreach ($brands as $brand) : ?>
                                                            <option value="<?php echo $brand->id_marca_artefacto_PK ?>">
                                                                <?php echo '#' . $brand->id_marca_artefacto_PK . ' '; ?>
                                                                <?php echo $brand->nombre_marca ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="ghost">
                                                        <a id="ghost-child-brand" class="btn btn-sm bg-gradient-success shadow mr-4 mt-3" href="#">Nueva</a>
                                                        <div id="ghost-child-content-brand">
                                                            <form>
                                                                <div class="input-group input-group-sm mb-3">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fas fa-desktop"></i></span>
                                                                    </div>
                                                                    <input id="nombre_marca" type="text" class="form-control" placeholder="Nombre de la marca" required>
                                                                    <div class="row justify-content-center">
                                                                        <button type="submit" id="submitGhostBrand" class="btn bg-gradient-success btn-sm text-center" title="Guardar marca">
                                                                            <i class="fas fa-save pl-1 text-center"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3 justify-content-center text-center">
                                                <div class="col-sm-12">
                                                    <label for="id_categoria_artefacto_FK" class="text-sm text-muted p-0 m-0 col-12"><span class="text-danger">*</span>Tipo <i class="fas fa-laptop-medical"></i></label>
                                                    <select id="id_categoria_artefacto_FK" style="width: 100%;" class="form-control-sm custom-select selector2 controlBuscador" required="">
                                                        <option selected value="" id="seleccione">Seleccione...</option>
                                                        <?php foreach ($types as $type) : ?>
                                                            <option value="<?php echo $type->id_categoria_artefacto_PK; ?>">
                                                                <?php echo '#' . $type->id_categoria_artefacto_PK . ' '; ?>
                                                                <?php echo $type->nombre_categoria_artefacto; ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-3 justify-content-center text-center">
                                                <div class="col-sm-12 d-flex">
                                                    <label for="modelo" class="text-sm text-muted"><span class="text-danger">*</span></label>
                                                    <div class="input-group input-group-sm mt-2">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fab fa-buromobelexperte"></i></span>
                                                        </div>
                                                        <input id="modelo" maxlength="30" type="text" class="form-control" placeholder="Modelo" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row justify-content-center">
                                                <input id="submitGhostArtifact" maxlength="100" type="submit" class="inserta text-center" value="Guardar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-12">
                                <div class="row mb-3 justify-content-center text-center">
                                    <div class="col-sm-6 d-flex">
                                        <label for="modelo" class="text-sm text-muted"></label>
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-fingerprint"></i></span>
                                            </div>
                                            <input id="imei" min="16" max="16" type="number" class="form-control form-control-sm vnum" title="Retirando la tapa de tu dispositivo o
                                                        posicionandote en configuración podrás obtener
                                                        imei de tu dispositivo" placeholder="Imei">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn bg-gradient-secondary text-center " id="artifact-data-back">Anterior</button>
                        <button class="btn bg-gradient-secondary text-center " id="artifact-data-continue">Siguiente</button>
                    </div>
                    <div class="tab-pane fade p-1 justify-content-center text-center" id="client_data" role="tabpanel">
                        <div class="row justify-content-center text-center">
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <h6 class="text-center mt-1 font-weight-bold">Cliente<span class="text-danger"> * </span></h6>
                                    <select id="id_cliente_FK" style="width: 100%;" class="custom-select selector2 controlBuscador" required="">
                                        <option selected value="" id="seleccioneC">Seleccione...</option>
                                        <?php foreach ($clients as $client) : ?>
                                            <option value="<?php echo $client->id_cliente_PK ?>">
                                                <?php echo '#' . $client->id_cliente_PK . " "; ?>
                                                <?php echo isset($client->nombre_cliente) ? $client->nombre_cliente : ' '; ?>
                                                <?php echo isset($client->num_id_cli) ? $client->num_id_cli : ' '; ?>
                                                <?php echo isset($client->correo) ? $client->correo : ' '; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2 justify-content-center d-flex align-items-center">
                                <div class="form-group">
                                    <div class="ghost">

                                        <a id="ghost-child" class="btnNew btn bg-gradient-success btn-sm shadow align-self-center text-center m-auto" href="#">
                                            Nuevo
                                        </a>

                                        <div id="ghost-child-content">
                                            <div class="row">
                                                <p class="col-12">El correo que ingreses recibira un enlace para registro y visualización de sus servicios en la plataforma</p>
                                                <div class="col-sm-6">
                                                    <div class="input-group input-group-sm mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text fas fa-signature text-danger"></span>
                                                        </div>
                                                        <input class="form-control form-control-sm" id="nombre_cliente" maxlength="100" type="text" placeholder="Nombre*" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="input-group input-group-sm mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text fas fa-file-signature text-danger"></span>
                                                        </div>
                                                        <input id="apellido_cliente" maxlength="100" class="form-control form-control-sm" type="text" placeholder="Apellido*" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="input-group input-group-sm mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text fas fa-envelope text-danger"></span>
                                                        </div>
                                                        <input id="correo" type="email" class="form-control form-control-sm" placeholder="Correo*" required>
                                                    </div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text fas fa-check-double text-danger"></span>
                                                        </div>
                                                        <input id="emailConfirm" type="email" minlength="4" maxlength="100" class="form-control form-control-sm" placeholder="Confirmar Correo*" required>
                                                    </div>
                                                </div>
                                                <span id="btnFillMoreClientData" class="mb-4 text-center text-sm col-12 text-a">Diligenciar mas</span>
                                            </div>

                                            <div id="boxFillMoreClientData" class="d-none">
                                                <div class="row mb-3 justify-content-center text-center">
                                                    <div class="col-sm-6">
                                                        <div class="input-group input-group-sm mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text fas fa-map-marker-alt"></span>
                                                            </div>
                                                            <input id="direccion_residencia" type="text" class="form-control form-control-sm" placeholder="Dirección" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="input-group input-group-sm mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text fas fa-phone"></span>
                                                            </div>
                                                            <input id="telefono" type="number" class="vnum form-control form-control-sm" placeholder="Teléfono" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row justify-content-center text-center">
                                                    <div class="col-sm-6">
                                                        <select id="tipo_doc_cli" class="form-control form-control-sm">
                                                            <option selected>Seleccione T.I...</option>
                                                            <option>CC Cédula de Ciudadanía</option>
                                                            <option>CE Cédula de Extranjería</option>
                                                            <option>PA Pasaporte</option>
                                                            <option>RC Registro Civil</option>
                                                            <option>TI Tarjeta de Identidad</option>
                                                            <option>Otro</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="input-group input-group-sm mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text fas fa-id-card"></span>
                                                            </div>
                                                            <input id="num_id_cli" type="number" class="vnum form-control form-control-sm" placeholder="No. de identificación" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3 justify-content-center text-center">
                                                    <div class="col-sm-10">
                                                        <label for="id_barrio_FK" class="text-sm text-muted col-12">Barrio</label>
                                                        <select id="id_barrio_FK" style="width: 100% !important;" class="form-control-sm custom-select selector2 controlBuscador" required="">
                                                            <option selected value="" id="seleccione">Seleccione...</option>
                                                            <?php foreach ($districts as $district) : ?>
                                                                <option value="<?php echo $district->id_barrio_PK ?>">
                                                                    <?php echo $district->nombre_barrio ?>
                                                                </option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="ghost d-flex align-items-center">
                                                            <a id="ghost-child-2" class="btnBarrio text-center mr-0 btn bg-gradient-success shadow" href="#">Nuevo</a>
                                                            <div id="ghost-child-content-2">
                                                                <form>
                                                                    <div class="input-group input-group-sm mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">Barrio</span>
                                                                        </div>
                                                                        <input id="nombre_barrio" type="text" class="form-control" placeholder="Nombre barrio" required>
                                                                        <div class="row justify-content-center">
                                                                            <button type="submit" id="submitGhost-2" class="btn bg-gradient-success btn-sm text-center" title="Guardar barrio">
                                                                                <i class="fas fa-save pl-1 text-center"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row justify-content-center">
                                                <input id="submitGhost" maxlength="100" type="submit" class="inserta text-center" value="Guardar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn bg-gradient-secondary text-center " id="client-data-back">Anterior</button>
                        <button class="btn bg-gradient-secondary text-center " id="client_data_continue">Siguiente</button>
                    </div>

                    <!-- <div class="tab-pane fade p-1 justify-content-center text-center" id="tech_data" role="tabpanel">
                        <div class="row justify-content-center ml-2 pl-2">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <h6 class="text-center mt-1 font-weight-bold">Asignar Técnico<span class="text-danger"> * </span></h6>
                                    <select id="technical" style="width: 100%;" class="custom-select selector2 controlBuscador" required="">
                                        <option selected value="" aria-readonly="">Seleccione...
                                        </option>
                                        <?php
                                        // foreach ($technicians as $technical) {
                                        ?>
                                            <option class="" value="<?php //echo $technical->id_tecnico_PK  ?>">
                                                <?php //echo isset($technical->id_tecnico_PK) ? $technical->id_tecnico_PK : '';
                                                ?>
                                                <?php

                                                // echo '#' . trim($technical->id_tecnico_PK) . ' ';
                                                // echo (trim($technical->nombre_tecnico))
                                                //     ? trim($technical->nombre_tecnico) . ' ' . trim($technical->apellido_tecnico)
                                                //     : trim($technical->correo) . ' ' . trim($technical->num_id_tec);
                                                ?>
                                            </option>
                                        <?php //} ?>
                                    </select>
                                     <div class="col-md-2 mt-2">
                                        <a href="#" id="addTechnical" class="btn btn-secondary rounded-circle border p-2"><i class="fas fa-plus fa-sm p-1"></i></a>
                                    </div>
                                   <div class="col-md-12 mt-2 text-center justify-content-center">
                                        <?php
                                        // if (isset($detailsTech) > 0) {
                                        //     $arrTechnicalService = [];
                                        //     foreach ($detailsTech as $detTech) {
                                        //         if (trim($detTech->num_id_tec) != '') {
                                        //             array_push(
                                        //                 $arrTechnicalService,
                                        //                 [
                                        //                     'id_tecnico_PK'     => $detTech->id_tecnico_asignado_FK,
                                        //                     'num_id_tec'        => $detTech->num_id_tec
                                        //                 ]
                                        //             );
                                        //         } else if (trim($detTech->num_id_tec) != '') {
                                        //             array_push(
                                        //                 $arrTechnicalService,
                                        //                 [
                                        //                     'id_tecnico_PK'     => $detTech->id_tecnico_asignado_FK,
                                        //                     'num_id_tec'        => null
                                        //                 ]
                                        //             );
                                        //         }
                                        //     }
                                        ?>

                                            <script>
                                                var arrTechnicians = <?php //echo json_encode($arrTechnicalService); ?>
                                            </script>

                                        <?php //} else { ?>

                                            <script>
                                                var arrTechnicians = [];
                                            </script>

                                        <?php //} ?>

                                        <div class="form-group text-left pl-2 border-success border-bottom" id="list-technicians">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-5">
                                <div class="form-group">
                                    <h6 class="text-center mt-1 font-weight-bold">Técnico que recibe<span class="text-danger"> * </span></h6>
                                    <select id="id_tecnico_FK" style="width: 100%;" class="custom-select selector2 controlBuscador" required="">
                                        <option selected value="0" aria-readonly="">Seleccione...
                                        </option>
                                        <?php
                                        //foreach ($technicians as $technical) {
                                        ?>
                                            <option class="" value="<?php //echo $technical->id_tecnico_PK  ?>">
                                                <?php

                                                //echo '#' . trim($technical->id_tecnico_PK) . ' ';
                                                //echo (trim($technical->nombre_tecnico))
                                                //    ? trim($technical->nombre_tecnico) . ' ' . trim($technical->apellido_tecnico)
                                                //    : trim($technical->correo) . ' ' . trim($technical->num_id_tec); ?>
                                            </option>
                                        <?php //} ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-2 d-flex">
                                <button id="btnGhostNewTech" onclick="alert('Proximamente en funcionamiento')" class="btn bg-gradient-success shadow align-self-center text-center m-auto">
                                    Nuevo
                                </button>
                            </div>


                        </div>
                        <button class="btn bg-gradient-secondary text-center " id="tech-data-back">Anterior</button>
                        <button class="btn bg-gradient-secondary text-center " id="tech_data_continue">Siguiente</button>
                    </div> -->

                    <div class="tab-pane fade p-1 justify-content-center text-center" id="diag_data" role="tabpanel">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 mt-1">
                                        <h6 class="text-center mt-1 font-weight-bold">¿Que sucedio con el dispositivo?</h6>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-sm" rows="2" id="descripcion_cliente" placeholder="Descripción del problema" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 justify-content-center">
                                        <h6 class="text-center mt-1 font-weight-bold">Diagnóstico<span class="text-danger"> * </span></h6>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-sm" rows="2" id="diagnostico" placeholder="Diagnóstico del técnico" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn bg-gradient-secondary text-center " id="diag-data-back">Anterior</button>
                        <button class="btn bg-gradient-secondary text-center " id="diag_data_continue">Siguiente</button>
                    </div>

                    <!-- <div class="tab-pane fade p-1 justify-content-center text-center" id="cita_data" role="tabpanel">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-sm-12">
                                <div class="row justify-content-center" id="face-to-faceForm">
                                    <h5 class="text-center col-sm-12 mt-1">Entrega presencial</h5>
                                    <div class="col-sm-12 col-md-12 mt-1">
                                        <h6 class="text-left mt-1 font-weight-bold">Lugar Entrega<span class="text-danger">*</span></h6>
                                        <div class="form-group text-left">
                                            <input type="text" placeholder="Dirección residencia del cliente" id="direccion_lugar" required="" class="form-control form-control-sm">
                                            <div class="custom-control custom-checkbox text-center justify-content-center d-block">
                                                <div class="col-md-5 col-sm-5 d-inline">
                                                    <input class="custom-control-input" id="localAddress" type="checkbox">
                                                    <label for="localAddress" class="p-1 bg-light custom-control-label h6 mr-3 font-weight-normal">En el punto de Engativá</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 justify-content-center">
                                        <h6 class="text-left mt-1 font-weight-bold">Fecha Entrega<span class="text-danger">*</span></h6>
                                        <div class="form-group text-left">
                                            <input type="date" id="fecha_encuentro" required="" class="form-control form-control-sm">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 justify-content-center">
                                        <h6 class="text-left mt-1 font-weight-bold">Hora Entrega<span class="text-danger">*</span></h6>
                                        <div class="form-group">
                                            <input type="time" id="hora_encuentro" required="" class="form-control form-control-sm">
                                        </div>
                                    </div>
                                    <a href="#" id="virtual" class="mb-4 text-center">Entrega virtual</a>
                                </div>

                                 Virtual Appointment
                                <div class="row d-none justify-content-center" id="virtualForm">
                                    <h5 class="text-center col-sm-12 mt-1">Entrega virtual</h5>
                                    <div class="col-sm-12 col-md-12 justify-content-center">
                                        <h6 class="text-left mt-1 font-weight-bold">Fecha / Hora<span class="text-danger">*</span></h6>
                                        <div class="form-group text-left">
                                            <input type="datetime-local" id="fecha_hora_cita_virtual" required="true" class="form-control form-control-sm">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 justify-content-center">
                                        <h6 class="text-left mt-1 font-weight-bold">Enlace Reunión Entrega<span class="text-danger">*</span></h6>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <textarea type="text" id="link" rows="1" readonly="readonly" class="form-control form-control-sm" required="true" maxlength="10000"></textarea>
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-outline-success px-2" id="newMeet" type="button">
                                                        <img src="assets/img/makeAService/meet.svg" width="30">
                                                    </button>
                                                </div>
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-outline-secondary px-2 rounded-right" id="copy" type="button">
                                                        <i class="fas fa-copy fa-sm"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" id="face-to-face" class="mb-4 text-center">Entrega presencial</a>
                                </div>
                            </div>
                        </div>
                        <button class="btn bg-gradient-secondary text-center " id="cita-data-back">Anterior</button>
                        <button class="btn bg-gradient-secondary text-center " id="cita_data_continue">Siguiente</button>
                    </div> -->

                    <div class="tab-pane fade p-1 justify-content-center text-center" id="cost_data" role="tabpanel">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-sm-12">
                                <h5>Costos(COP)</h5>
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <h6 class="text-left mt-1 font-weight-bold">Domicilio<span class="text-danger">*</span></h6>
                                        <div class="form-group text-left">
                                            <input type="number" value="00" class="form-control form-control-sm" placeholder="Costo domicilio" id="costo_domicilio">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 justify-content-center">
                                        <h6 class="text-left mt-1 font-weight-bold">Diagnóstico<span class="text-danger">*</span></h6>
                                        <div class="form-group text-left">
                                            <input type="number" value="10000" class="form-control form-control-sm" placeholder="Costo diagnóstico" id="costo_revision">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 justify-content-center">
                                        <h6 class="text-left mt-1 font-weight-bold">Abono<span class="text-danger">*</span></h6>
                                        <div class="form-group text-left">
                                            <input type="number" value="0" class="form-control form-control-sm" placeholder="Abono" id="abono">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 justify-content-center">
                                        <h6 class="text-left mt-1 font-weight-bold">Reparación/es<span class="text-danger">*</span></h6>
                                        <div class="form-group text-left">
                                            <input type="number" class="form-control form-control-sm" placeholder="Costo de reparación/es" id="costo_total">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 mb-1 col-md-12 justify-content-center d-none">
                                        <span class="text-left font-weight-bold text-sm text-muted d-inline">Tipo de arreglo:</span>
                                        <div class="custom-control custom-checkbox d-inline">
                                            <div class="col-md-5 col-sm-5 d-inline p-1 mr-3">
                                                <input class="custom-control-input" type="checkbox" id="is_software">
                                                <label for="is_software" class=" bg-light custom-control-label text-sm mr-3 font-weight-light">Software</label>
                                            </div>
                                            <div class="col-md-5 col-sm-5 d-inline p-1">
                                                <input class="custom-control-input" type="checkbox" id="is_hardware">
                                                <label for="is_hardware" class=" bg-light custom-control-label text-sm font-weight-light">Hardware</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <button class="btn bg-gradient-secondary text-center " id="cost-data-back">Anterior</button>
                        <button id="submitService" type="submit" class="btn bg-gradient-success shadow text-center">
                            <i class="fas fa-paper-plane"></i> Guardar
                        </button>
                        <!-- <button class="btn bg-gradient-secondary text-center" id="cost_data_continue">Siguiente</button> -->

                        <!-- <button id="submitService" type="submit" class="btn bg-gradient-success shadow mt-3 text-center"><i class="fas fa-paper-plane"></i>Guardar</button> -->
                    </div>

                    <!-- <div class="tab-pane fade p-1 justify-content-center text-center" id="evidence_data" role="tabpanel">
                        <div class="row justify-content-center">
                            <div class="col-md-12 col-sm-12">
                                <div class="row justify-content-center">
                                    <div class="col-sm-12 col-md-6">
                                        <h6 class="text-left mt-1 font-weight-bold">Evidencia del servicio</h6>
                                        <div class="form-group text-left">

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fas fa-upload"></i></span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" id="evidencia" class="custom-file-input" data-toggle="tooltip" data-placement="right" title="Puedes subir varios archivos" multiple lang="es" aria-describedby="inputGroupFileAddon01">
                                                    <label class="custom-file-label" for="evidencia">Seleccionar Archivo</label>
                                                </div>
                                                <legend class="text-muted politica text-center">Este campo es opcional</legend>
                                                <div id="msgEvidence">

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn bg-gradient-secondary text-center " id="evidence-data-back">Anterior</button>
                        <button id="submitService" type="submit" class="btn bg-gradient-success shadow text-center">
                            <i class="fas fa-paper-plane"></i> Guardar
                        </button>
                    </div> -->
                </div>

                <div class="progress mt-5">
                    <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 5%" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100">Paso 1 de 7</div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn bg-gradient-dark btn-lg fas fa-cog" data-toggle="tooltip" data-placement="bottom" title="Configuración"></button> -->
                <button type="button" class="btn bg-gradient-danger" data-dismiss="modal" data-toggle="tooltip" data-placement="bottom" title="Cancelar solicitud de servicio">Cancelar</button>
                <button type="button" class="btn bg-gradient-orange text-white" id="btnDraftNewService" data-toggle="tooltip" data-placement="bottom" title="Guardar borrador">Borrador</button>
                <button type="button" class="btn bg-gradient-primary" id="btnCleanDraftNewService" data-toggle="tooltip" data-placement="bottom" title="Limpiar borrador">
                    <i class="fas fa-paint-roller">

                    </i>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    // Llenar mas info del cliente
    let btnFillMoreClientData = document.getElementById('btnFillMoreClientData'),
        boxFillMoreClientData = document.getElementById('boxFillMoreClientData');

    if (btnFillMoreClientData && boxFillMoreClientData) {
        eve(btnFillMoreClientData, () => {
            boxFillMoreClientData.classList.toggle('d-none');
            if (btnFillMoreClientData.textContent == 'Diligenciar mas') {
                btnFillMoreClientData.textContent = 'Diligenciar menos'
            } else {
                btnFillMoreClientData.textContent = 'Diligenciar mas'
            }
        })
    }

    const keyPre = (e) => {
        let tabsElems = document.querySelectorAll('[data-toggle="tab"]')

        let izq = '',
            der = '';

        tabsElems.forEach((elem, index) => {

            if (elem.classList.contains('active')) {

                if (e) {
                    if (e.keyCode) {
                        if (e.keyCode == 37) {

                            if (tabsElems[index - 1]) izq = tabsElems[index - 1].id;
                        }

                        if (e.keyCode == 39) {

                            if (tabsElems[index + 1]) der = tabsElems[index + 1].id;
                        }
                    }
                }
            };
        });

        if (izq) {
            document.getElementById(izq).click()
        }

        if (der) {
            document.getElementById(der).click()
        }
    }

    eve(window, keyPre, 'keydown');

    // Cambiar de tabs con btns
    function changeTab(tabId, tabObjId) {
        document.getElementById(`${tabId}-data-back`).addEventListener('click', () => document.getElementById(`${tabObjId}Tab`).click())
    }

    +function(d) {
        changeTab('artifact', 'service')
        changeTab('client', 'artifact')
        // changeTab('tech', 'client')
        changeTab('diag', 'client')
        // changeTab('cita', 'diag')
        changeTab('cost', 'diag')
        // changeTab('evidence', 'cost')
    }(document);
</script>

<!-- DIRECCIÓN AUTO -->
<script>
    vnum(15);

    document.addEventListener('DOMContentLoaded', () => {
        $('#costo_domicilio').val('0')
        $('#costo_revision').val('10000')
        $('#abono').val('0')
        $('#diagnostico').val('Cambio de Repuesto/s')
    })

    // let localAddressCheck = document.getElementById('localAddress');
    // let input_address = document.getElementById('direccion_lugar');
    // localAddressCheck.addEventListener('change', () => {
    //     if (input_address.disabled == true) {
    //         input_address.disabled = false
    //         input_address.value = '';

    //         $('#fecha_encuentro').val('')
    //         $('#hora_encuentro').val('')
    //     } else {
    //         input_address.value = 'Carrera 116c # 66A 16 Villa Teresita Engativa';
    //         input_address.disabled = true

    //         // Fecha cita
    //         let fecha = new Date(); //Fecha actual
    //         let mes = fecha.getMonth() + 1; //obteniendo mes
    //         let dia = fecha.getDate() + 1; //obteniendo dia
    //         let ano = fecha.getFullYear(); //obteniendo año
    //         if (dia < 10)
    //             dia = '0' + dia; //agrega cero si el menor de 10
    //         if (mes < 10)
    //             mes = '0' + mes //agrega cero si el menor de 10
    //         document.getElementById('fecha_encuentro').value = ano + "-" + mes + "-" + dia;

    //         // Hora cita
    //         document.getElementById('hora_encuentro').value = "<?php //echo date('H:i') ?>";
    //     }


    // })
</script>
<script>
    // Evidencia del servicio
    // const evidenciaInput = document.getElementById('evidencia');
    // const msgEvidence = document.getElementById('msgEvidence');
    // let arrImages = [];

    // evidenciaInput.addEventListener('change', async (e) => {
    //     let imgs = new FormData();

    //     Array.from(e.target.files).forEach((img, index) => {
    //         imgs.append(`img${index}`, img);
    //     })

    //     let options = {
    //         method: 'POST',
    //         body: imgs
    //     }

    //     fetch('?c=service&m=serviceEvidence', options)
    //         .then(res => {
    //             return res.json();
    //         })
    //         .then(res => {

    //             if (res.ok) swalSimple("Evidencia subida correctamente")
    //             else swalSimple("Error al subir la evidencia");

    //             arrImages = res.images;
    //         })
    //         .catch(e => console.log(e))
    // })
</script>

<script src="assets/js/layouts/fetchPost.js"></script>
<script src="assets/js/general/service.js"></script>
<script src="assets/js/general/brand.js"></script>
<script src="assets/js/general/artifact.js"></script>
<script src="assets/js/layouts/swalAlerts.js"></script>
<script src="assets/js/layouts/getAndValidate.js"></script>
<script src="assets/js/detalles/technicalService.js"></script>
<script src="assets/js/layouts/fetchPost.js"></script>
<script src="assets/js/general/meet.js"></script>
<script src="assets/js/general/client.js"></script>
<script src="assets/js/general/district.js"></script>
<script src="assets/js/detalles/categoryService.js"></script>
<script src="assets/js/detalles/detailService.js"></script>
<script src="assets/js/detalles/artifactService.js"></script>

<!-- BORRADOR -->
<script>
    if (!ele('draftTitle').classList.contains('d-none')) ele('draftTitle').classList.add('d-none');

    // Usar web sockets
    setInterval(() => {
        saveDraftNewService()
        putDataDraftNewService()
    }, 1000)

    putDataDraftNewService()

    function saveDraftNewService() {
        let draftNewService = [{
            services: arrScategories,
            artifacts: arrArtifacts,
            // technicians: arrTechnicians,
            client: $("#id_cliente_FK").val(),
            technical: $("#id_tecnico_FK").val(),
            arrDetails: getDetailToDraft()
        }];

        ele('textDraft').textContent = "Guardando borrador"
        localStorage.setItem('draftNewService', JSON.stringify(draftNewService));
    }

    function putDataDraftNewService() {
        if (localStorage.getItem('draftNewService') !== null) {
            // Poner titulo de borrador arriba
            ele('draftTitle').classList.remove('d-none');
            ele('textDraft').textContent = "Borrador"

            let draftNS = JSON.parse(localStorage.getItem('draftNewService'));

            let arrDet = draftNS[0]['arrDetails'][0];

            // if (arrDet['is_software'] == true) ele('is_software').checked = true;
            // else ele('is_software').checked = false;

            // if (arrDet['is_hardware'] == true) ele('is_hardware').checked = true;
            // else ele('is_hardware').checked = false;

            ele('diagnostico').value = arrDet['diagnostico']
            ele('descripcion_cliente').value = arrDet['descripcion_cliente']
            ele('costo_revision').value = arrDet['costo_revision']
            ele('costo_domicilio').value = arrDet['costo_domicilio']
            ele('costo_total').value = arrDet['costo_total']
            ele('abono').value = arrDet['abono']
            ele('imei').value = arrDet['imei']
            // ele('direccion_lugar').value = (arrDet['direccion_lugar'] != undefined) ? arrDet['direccion_lugar'] : '';
            // ele('fecha_encuentro').value = (arrDet['fecha_encuentro'] != undefined) ? arrDet['fecha_encuentro'] : '';
            // ele('hora_encuentro').value = (arrDet['hora_encuentro'] != undefined) ? arrDet['hora_encuentro'] : '';
            // ele('fecha_hora_cita_virtual').value = (arrDet['fecha_hora_cita_virtual'] != undefined) ? arrDet['fecha_hora_cita_virtual'] : '';
            // ele('link').value = (arrDet['link'] != undefined) ? arrDet['link'] : '';

            // if (arrDet['link'] != undefined) {
            //     ele('link').value = arrDet['link']
            // } else {
            //     if (flagLinkMeet) {
            //         ele('newMeet').click()
            //         flagLinkMeet = false
            //     }
            //     ele('link').value = (arrDet['link'] != undefined) ? arrDet['link'] : '';
            // }

            // for (let i = 0; i < ele('id_tecnico_FK').options.length; i++) {
            //     if (ele('id_tecnico_FK').options[i].value == draftNS[0]['technical']) {
            //         ele('id_tecnico_FK').options[i].selected = "true"
            //     }
            // }

            for (let i = 0; i < ele('id_cliente_FK').options.length; i++) {
                if (ele('id_cliente_FK').options[i].value == draftNS[0]['client'])
                    ele('id_cliente_FK').options[i].selected = "true"
            }

            // console.log(draftNS[0]['artifacts']);
            arrArtifacts = draftNS[0]['artifacts'];
            showArtifacts()

            arrScategories = draftNS[0]['services'];
            showScategories()

            // arrTechnicians = draftNS[0]['technicians'];
            // showTechnicians()
        }
    }

    eve(ele("btnDraftNewService"), () => {
        swalSimple("Borrador guardado")
        saveDraftNewService()
        putDataDraftNewService()
    })

    eveFull(ele('diagnostico'), () => saveDraftNewService())
    eveFull(ele('descripcion_cliente'), () => saveDraftNewService())
    eveFull(ele('costo_revision'), () => saveDraftNewService())
    eveFull(ele('costo_domicilio'), () => saveDraftNewService())
    eveFull(ele('costo_total'), () => saveDraftNewService())
    eveFull(ele('abono'), () => saveDraftNewService())
    eveFull(ele('imei'), () => saveDraftNewService())
    // eveFull(ele('direccion_lugar'), () => saveDraftNewService())
    // eveFull(ele('fecha_encuentro'), () => saveDraftNewService())
    // eveFull(ele('fecha_hora_cita_virtual'), () => saveDraftNewService())
    // eveFull(ele('hora_encuentro'), () => saveDraftNewService())
    // eveFull(ele('link'), () => saveDraftNewService())
    // eveFull(ele('is_software'), () => saveDraftNewService())
    // eveFull(ele('is_hardware'), () => saveDraftNewService())
    // eveFull(ele('id_tecnico_FK'), () => saveDraftNewService())
    eveFull(ele('id_cliente_FK'), () => saveDraftNewService())
    eveFull(ele('scategory'), () => saveDraftNewService())
    eveFull(ele('artifact'), () => saveDraftNewService())
    // eveFull(ele('technical'), () => saveDraftNewService())

    eve(ele("btnCleanDraftNewService"), () => {
        swalSimple("Borrador limpio")

        arrArtifacts = [];
        showArtifacts()
        arrScategories = [];
        showScategories()
        // arrTechnicians = [];
        // showTechnicians()

        ele('diagnostico').value = '';
        ele('descripcion_cliente').value = '';
        ele('costo_revision').value = '';
        ele('costo_domicilio').value = '';
        ele('costo_total').value = '';
        ele('abono').value = '';
        ele('imei').value = '';
        // ele('direccion_lugar').value = '';
        // ele('fecha_encuentro').value = '';
        // ele('fecha_hora_cita_virtual').value = '';
        // ele('hora_encuentro').value = '';
        // ele('link').value = '';

        // ele('is_software').checked = false;
        // ele('is_hardware').checked = false;

        localStorage.clear();
    })
</script>