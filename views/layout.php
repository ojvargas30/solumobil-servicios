<!DOCTYPE html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" lang="es" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="Author" lang="es" content="Óscar Javier Vargas Diaz, oscarjaviervargas@hotmail.com">
    <meta name="DC.identifier" lang="es" content="">
    <!--Aqui va la pagina Solumobil.............................-->
    <META http-equiv="Expires" lang="es" content="0">
    <!--ESTA NOSE PARA QUE ES.-->
    <meta name="Keywords" lang="es" content="Engativa,Colombia, Bogota,
	servicio tecnico, localidad, Quitar Cuenta Google,
	reparacion, celulares, pantalla, dañada, puerto de carga, tablets, baratos,
	flasheo de celulares o tablets, cambio de pantalla, cambio de repuestos.">
    <META http-equiv="PICS-Label" content='
	(PICS-1.1 "http://www.gcf.org/v2.5"
	labels on "1994.11.05T08:15-0500"
	until "1995.12.31T23:59-0000"
	for "http://w3.org/PICS/Overview.html"
	ratings (suds 0.5 density 0 color/hue 1))
 '>
    <!--Esto es para ayudar a los padres y a las escuelas a controlar los lugares a los
   que pueden acceder los niños en Internet, también facilita otros usos para las etiquetas,
  incluyendo firmas de código, privacidad, y gestión de los derechos de la propiedad
  intelectual.-->
    <META name="copyright" content="&copy; 2020 Solumobil Company.">
    <meta name="Description" lang="es" content="Pagina de servicio profesional
  enfocada en el mantenimiento y reparacion de celulares en la ciudad de Bogota-Colombia.
  Servicio tecnico de moviles.">
    <META name="date" content="19:05:09, sábado 29, febrero 2020 -05">
    <meta name="generator" content="HTML-KIT 2.9" />
    <meta name="language" content="es" />
    <meta name="revisit-after" content="1 month" />
    <meta name="robots" content="index, follow" />
    <meta name="application-name" content="servicio tecnico web de reparacion de celulares" />
    <meta name="encoding" charset="utf-8" />
    <meta http-equiv=»X-UA-Compatible». />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="organization" content="Solumobil Company" />
    <meta name="revisit" content="7" />
    <noscript>
        <meta http-equiv="refresh" content="60; url=https://www.youtube.com/watch?v=XyW1XiNBsaQ">
    </noscript>

    <!----------------------------------------------------------------------------------->
    <!----------------------------------------------------------------------------------->
    <!--TERMINA AQUI --------------------------------------------------------------META-->
    <!----------------------------------------------------------------------------------->

    <!-- Theme style -->
    <link rel="stylesheet" href="assets/css/adminlte.css">

    <!-- normalize -->
    <link rel="stylesheet" href="assets/css/normalize.css">

    <!-- Favicon -->
    <link rel="Shortcut Icon" href="assets/img/1.ico" />

    <!-- SWAL -->
    <link rel="stylesheet" href="assets/plugins/swal/dist/sweetalert2.min.css">

    <!-- TOSTR -->
    <link rel="stylesheet" href="assets/plugins/toastr/toastr.min.css">

    <!-- SELECT CON BUSCADOR -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css">

    <!-- DATATABLE -->
    <!-- <link rel="stylesheet" href="assets/plugins/datatable/css/bootstrap.css"> -->
    <link rel="stylesheet" href="assets/plugins/datatable/datatables/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" href="assets/plugins/datatable/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="assets/plugins/datatable/datatables-buttons/css/buttons.bootstrap4.min.css">

    <!-- FONTAWESOME -->
    <link rel="stylesheet" href="assets/font/css/all.css">

    <!-- CHART.JS -->
    <link rel="stylesheet" href="assets/plugins/chart/chart.min.css">

    <!-- ANIMATE.CSS -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.compat.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.min.css"> -->
    <script data-ad-client="ca-pub-4415779182320006" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <title>Solumobil</title>
</head>

<body class="hold-transition sidebar-mini carta-modo-osc" id="body">
    <div class="paleta loader">
        <img src="assets/img/1.ico" alt="Solumobil" width="95" height="95">
    </div>
    <div class="wrapper carta-modo-osc" id="content">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-secondary" id="barsBtnDash" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="?c=dash&m=index">Inicio</a>
                </li>
                <li class="nav-item d-flex align-items-center">
                    <a class="btn bg-gradient-blue btn-sm" target="_blank" href="/inventario?c=main">INVENTARIO</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="btn  nav-link font-weight-bolder" target="_blank" href="https://www.imeicolombia.com.co/">
                        IMEI COLOMBIA
                    </a>
                </li>
                <li class="nav-item">
                    <a class="btn  nav-link font-weight-bolder" target="_blank" href="https://www.imei.info/">
                        IMEI INFO
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-secondary" href="?c=dash&m=using">
                        <i class="text-secondary fas fa-question-circle"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-secondary" id="closeSessionSort" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i class="fas fa-sort-down"></i></a>
                </li>
            </ul>
        </nav>
        <aside class="main-sidebar sidebar-light-primary elevation-2">
            <a href="?c=dash&m=index" class="brand-link">
                <img src="assets/img/logo_disminuido.png" alt="Solumobil" class="brand-image img-circle">
                <span class="brand-text font-weight-light">Solumobil</span>
            </a>
            <div class="sidebar">
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="
                        <?php
                        if (isset($_SESSION['user']['user']->foto)) {
                            if ($_SESSION['user']['user']->foto != null) echo $_SESSION['user']['user']->foto;
                        } else echo "assets/img/clients/sinfoto.png";

                        ?>" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">
                            <?php echo isset($_SESSION['user']['technical']->nombre_tecnico) ?
                                $_SESSION['user']['technical']->nombre_tecnico . ' ' .
                                $_SESSION['user']['technical']->apellido_tecnico : ''
                            ?>
                        </a>
                        <strong class="politica">
                            <a href="#" class="d-block">
                                <?php
                                if (isset($_SESSION['user']['user']->rol)) {
                                    if ($_SESSION['user']['user']->rol == "Tecnico Administrador") {
                                        echo substr_replace($_SESSION['user']['user']->rol, 'Técnico Administrador', 0, 21);
                                    }
                                }
                                ?>
                            </a>
                        </strong>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link text-secondary">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Servicio Técnico
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="?c=service&m=index" class="nav-link text-secondary">
                                        <i class="fas fa-concierge-bell nav-icon"></i>
                                        <p>Solicitudes</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?c=service&m=history" class="nav-link text-secondary">
                                        <i class="fas fa-history nav-icon"></i>
                                        <p>Historial</p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link text-secondary">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    Usuarios
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="?c=user&m=index" class="nav-link text-secondary">
                                        <i class="fas fa-users nav-icon"></i>
                                        <p>Todos</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?c=technical&m=index" class="nav-link text-secondary">
                                        <i class="fas fa-user-tie nav-icon"></i>
                                        <p>Técnicos</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?c=client&m=index" class="nav-link text-secondary">
                                        <i class="fas fa-user-friends nav-icon"></i>
                                        <p>Clientes</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?c=user&m=records" class="nav-link text-secondary">
                                        <i class="fas fa-universal-access nav-icon"></i>
                                        <p>Accesos</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="?c=statistic&m=index" class="nav-link text-secondary">
                                <i class="nav-icon fas fa-chart-pie"></i>
                                <p>
                                    Estadísticas
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link text-secondary">
                                <i class="nav-icon fas fa-grin-tongue-wink"></i>
                                <p>
                                    Juegos
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="https://solumemo.solumobil.com/" target="_blank" class="nav-link text-secondary">
                                        <i class="fas fa-puzzle-piece nav-icon"></i>
                                        <p>Solumemo</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </aside>

        <div class="content-wrapper">
            <!-- HEADER UBICACIÓN -->
            <div class="content-header mb-0 pb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- <h1 class="m-0 text-dark">Ubicación</h1> -->
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">

                                <?php

                                if ('/Soludomv1/?c=service&m=index' == $_SERVER['REQUEST_URI']) {
                                    echo '<li class="breadcrumb-item"><a href="#">';
                                    echo str_replace('&m=index', ' ', str_replace('/Soludomv1/?c=service&m=index', ' Servicios ', $_SERVER['REQUEST_URI']));
                                    echo '</a></li><li class="breadcrumb-item active">Solumobil</li>';
                                } elseif ('/Soludomv1/?c=service&m=history' == $_SERVER['REQUEST_URI']) {
                                    echo '<li class="breadcrumb-item"><a href="#">';
                                    echo str_replace('&m=history', ' ', str_replace('/Soludomv1/?c=service&m=history', ' Historial de Servicios ', $_SERVER['REQUEST_URI']));
                                    echo '</a></li><li class="breadcrumb-item active">Solumobil</li>';
                                } elseif ('/Soludomv1/?c=user&m=index' == $_SERVER['REQUEST_URI']) {
                                    echo '<li class="breadcrumb-item"><a href="#">';
                                    echo str_replace('&m=index', ' ', str_replace('/Soludomv1/?c=user&m=index', ' Todos ', $_SERVER['REQUEST_URI']));
                                    echo '</a></li><li class="breadcrumb-item active">Solumobil</li>';
                                } elseif ('/Soludomv1/?c=technical&m=index' == $_SERVER['REQUEST_URI']) {
                                    echo '<li class="breadcrumb-item"><a href="#">';
                                    echo str_replace('&m=index', ' ', str_replace('/Soludomv1/?c=technical&m=index', ' Técnicos ', $_SERVER['REQUEST_URI']));
                                    echo '</a></li><li class="breadcrumb-item active">Solumobil</li>';
                                } elseif ('/Soludomv1/?c=client&m=index' == $_SERVER['REQUEST_URI']) {
                                    echo '<li class="breadcrumb-item"><a href="#">';
                                    echo str_replace('&m=index', ' ', str_replace('/Soludomv1/?c=client&m=index', ' Clientes ', $_SERVER['REQUEST_URI']));
                                    echo '</a></li><li class="breadcrumb-item active">Solumobil</li>';
                                } elseif ('/Soludomv1/?c=user&m=records' == $_SERVER['REQUEST_URI']) {
                                    echo '<li class="breadcrumb-item"><a href="#">';
                                    echo str_replace('&m=records', ' ', str_replace('/Soludomv1/?c=user&m=records', ' Accesos ', $_SERVER['REQUEST_URI']));
                                    echo '</a></li><li class="breadcrumb-item active">Solumobil</li>';
                                }

                                ?>
                                <!-- <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?php //echo str_replace('&m=history', ' ', str_replace('/Soludomv1/?c=service&m=history', ' Historial de Servicios ', $_SERVER['REQUEST_URI']));
                                                                        ?></a></li>
                                <li class="breadcrumb-item active">Solumobil</li>
                            </ol>
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?php //echo str_replace('&m=index', ' ', str_replace('/Soludomv1/?c=user&m=index', ' Todos ', $_SERVER['REQUEST_URI']));
                                                                        ?></a></li>
                                <li class="breadcrumb-item active">Solumobil</li>
                            </ol>
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?php //echo str_replace('&m=index', ' ', str_replace('/Soludomv1/?c=technical&m=index', ' Técnicos ', $_SERVER['REQUEST_URI']));
                                                                        ?></a></li>
                                <li class="breadcrumb-item active">Solumobil</li>
                            </ol>
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?php //echo str_replace('&m=index', ' ', str_replace('/Soludomv1/?c=client&m=index', ' Clientes ', $_SERVER['REQUEST_URI']));
                                                                        ?></a></li>
                                <li class="breadcrumb-item active">Solumobil</li> -->
                            </ol>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <div class="content fondo pb-5">
                <div class="container-fluid mt-3">
                    <a id="back-to-top" href="#" class="btn btn-default btn-sm rounded-circle text-lightred back-to-top" role="button" aria-label="Scroll to top">
                        <i class="fas fa-chevron-up fa-sm"></i>
                    </a>
                    <aside class="control-sidebar control-sidebar-light">
                        <aside class="control-sidebar control-sidebar-light d-block">
                            <div class="p-3 control-sidebar-content">
                                <h5 class="">Opciones</h5>
                                <hr class="mb-2">
                                <div class="d-flex">
                                    <div class="d-flex flex-wrap mb-3">
                                        <h4 class="main_time reloj-font" id="time"></h4>
                                        <h5 class="mt-1 reloj-font" id="format"></h5>
                                    </div>
                                </div>
                                <!-- <h6 class="mt-2">Modo Semioscuro</h6>
                                <div class="d-flex"></div>
                                <div class="d-flex flex-wrap mb-3">
                                    <button class="switch" id="switch">
                                        <span>
                                            <li class="fas fa-sun"></li>
                                        </span>
                                        <span>
                                            <li class="fas fa-moon"></li>
                                        </span>
                                    </button>
                                </div> -->

                                <div class="my-5">
                                    <a id="exit" class="btn bg-gradient-red text-white cursor-pointer shadow">Cerrar sesion</a>
                                </div>
                            </div>
                        </aside>
                    </aside>


                    <noscript>
                        <div class="position-fixed col-4 justify-content-md-start bg-danger rounded ml-3">
                            <p class="text-light h6">Bienvenido al portal Solumobil</p>
                            <p class="text-light h6">La página que estás
                                viendo requiere para su funcionamiento el uso de JavaScript.
                                Si lo has deshabilitado intencionalmente, por favor vuelve a activarlo.</p>
                            <p class="text-light">Se te redigira a un tutorial en 60 segundos</p>
                        </div>
                    </noscript>


                    <!-- <script src="assets/js/jquery.min.js"></script> -->
                    <script src="assets/js/jquery-3.5.1.min.js"></script>
                    <script src="assets/js/bootstrap.bundle.min.js"></script>
                    <script src="assets/js/popper.min.js"></script>

                    <!-- GRAPHS -->
                    <script src="assets/plugins/chart/chart.js"></script>

                    <!-- SWEETALERT -->
                    <script src="assets/plugins/swal/dist/sweetalert2.all.min.js"></script>
                    <script src="assets/js/layouts/swals.js"></script>


                    <!-- OPTIONAL SCRIPTS -->
                    <script src="assets/js/dashboard3.js"></script>
                    <script src="assets/js/localStorage.js"></script>

                    <!-- SELECT2 CON BUSCADOR -->
                    <!-- <script src="assets/plugins/datatable/js/jquery-3.5.1.js"></script> -->
                    <script src="assets/plugins/select2/select2.min.js"></script>


                    <!-- TOASTR -->
                    <script src="assets/plugins/toastr/toastr.min.js"></script>

                    <!-- DATATABLE -->
                    <script src="assets/plugins/datatable/datatables/js/jquery.dataTables.min.js"></script>
                    <script src="assets/plugins/datatable/datatables/js/dataTables.bootstrap4.min.js"></script>

                    <script src="assets/plugins/datatable/datatables-responsive/js/dataTables.responsive.min.js"></script>
                    <script src="assets/plugins/datatable/datatables-buttons/js/dataTables.buttons.min.js"></script>
                    <script src="assets/plugins/datatable/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
                    <script src="assets/plugins/datatable/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
                    <script src="assets/plugins/datatable/jszip/jszip.min.js"></script>
                    <script src="assets/plugins/datatable/pdfmake/pdfmake.min.js"></script>
                    <script src="assets/plugins/datatable/pdfmake/vfs_fonts.js"></script>
                    <script src="assets/plugins/datatable/datatables-buttons/js/buttons.html5.min.js"></script>
                    <script src="assets/plugins/datatable/datatables-buttons/js/buttons.print.min.js"></script>
                    <script src="assets/plugins/datatable/datatables-buttons/js/buttons.colVis.min.js"></script>
                    <script src="assets/plugins/datatable/datatables-editor/dataTables.editor.min.js"></script>
                    <script src="assets/plugins/datatable/datatables-editor/dataTables.select.min.js"></script>

                    <!-- archivo en el cual se establece el statable por el id de la página -->
                    <script src="assets/plugins/datatable/datatables/js/call-datatable.js"></script>

                    <!-- TO FORM VALIDATE -->
                    <script src="assets/plugins/validator.js"></script>

                    <script src="assets/js/adminlte.js"></script>
                    <script src="assets/js/general/methods.js"></script>
                    <script src="assets/js/layouts/dashLayout.js"></script>
                    <script src="assets/js/layouts/swalAlerts.js"></script>
                    <script src="assets/js/layouts/fillArrays.js"></script>
                    <script>
                        let exit = ele("exit");
                        if (exit) {
                            exit.addEventListener('click', (e) => {
                                e.preventDefault();

                                let areyousure = confirm("¿Estas seguro de que quieres cerrar sesión?");
                                if (areyousure) location.href = "?c=login&m=destroySession"

                            })
                        }
                    </script>

                    <script>
                        $(document).ready(function() {
                            $('[data-toggle="tooltip"]').tooltip();
                        });
                    </script>