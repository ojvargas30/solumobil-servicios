<!-- Main content -->
<section class="content pt-3">

    <!-- Default box -->
    <div class="card card-solid">
        <h3 class="card-header fondo-titulo text-uppercase text-dark p-3 text-center mb-2 mt-1">
            <i class="fas fa-laugh-beam fa-lg"></i>
            Técnicos asignados al servicio <i class="fas fa-ruler"></i> N°
            <?php echo isset($data[0]->id_servicio_PK) ? $data[0]->id_servicio_PK : "<p></p>No se han definido técnicos en el servicio" ?>
        </h3>
        <div class="card-body pb-0">
            <div class="row d-flex align-items-stretch">
                <?php foreach ($data as $dat) :
                    if (
                        $dat->id_tecnico_PK == $dat->id_tecnico_asignado_FK &&
                        $dat->id_rol_PK == $dat->id_rol_FK &&
                        $dat->id_usuario_PK == $dat->id_usuario_FK &&
                        $dat->estado_usu == 1
                    ) { ?>
                        <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                            <div class="card bg-light">
                                <div class="card-header text-muted border-bottom-0">
                                    <?php echo ($dat->estado_rol == 1) ? $dat->nombre_rol : '' ?>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="row">
                                        <div class="col-7">
                                            <h2 class="lead"><b><?php echo $dat->nombre_tecnico . ' ' . $dat->apellido_tecnico; ?></b></h2>
                                            <p class="text-muted text-sm"><b>Acerca de: </b> <?php echo $dat->desc_tecnico; ?> </p>
                                            <ul class="ml-4 mb-0 fa-ul text-muted">
                                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Dirección: <?php echo $dat->direccion_residencia; ?></li>
                                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Teléfono : <a href="tel:<?php echo $dat->telefono; ?>"> <?php echo $dat->telefono; ?> </a></li>
                                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-paper-plane"></i></span> Correo : <?php echo $dat->correo; ?></li>
                                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-id-card-alt"></i></span> Identificación : <?php echo $dat->tipo_doc_tec . ' <b>' . $dat->num_id_tec . '</b>' ?></li>
                                            </ul>
                                        </div>
                                        <div class="col-5 text-center">
                                            <?php if ($dat->foto == null) { ?>
                                                <img src="assets/img/technicians/sin_foto.png" class="img-circle img-fluid" alt="Foto Técnico" />
                                            <?php } elseif ($dat->foto != null) { ?>
                                                <img src="<?php echo $dat->foto; ?>" class="img-circle img-fluid" alt="Foto Técnico">
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-right">
                                        <a href="" class="btn btn-sm bg-teal">
                                            <i class="fas fa-comments"></i>
                                        </a>
                                        <a href="" class="btn btn-sm btn-primary">
                                            <i class="fas fa-user"></i> Ver perfíl
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</section>
</div>
</div>
</div>