<div id="myModalLeft" data-backdrop="static" data-keyboard="false" class="modal fade modalNew" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-gradient-red">
                <div class="text-left col-11 border-left">
                    <div class="row pl-2">
                        <h5 class="modal-title ">
                            <strong>
                                GENERAR NUEVO TÉCNICO
                            </strong>
                        </h5>
                        <h6 class="text-sm" style="font-size: 13px !important;">Los campos en ROJO son obligatorios</h6>
                    </div>
                </div>
                <div class="col-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="card shadow">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-12">Nombre<label style="color: red;">*</label></label>
                                    <input type="text" placeholder="Nombre/s" maxlength="50" class="form-control form-control-sm" rows="3" id="nombre_tecnico" placeholder="">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-12">Apellido<label style="color: red;">*</label></label>
                                    <input type="text" placeholder="Apellido/s" maxlength="50" class="form-control form-control-sm" rows="3" id="apellido_tecnico" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-12">Tipo Documento</label>
                                    <select id="tipo_doc_tec" class="form-control form-control-sm mt-2" required="">
                                        <option selected value="" aria-readonly="">Seleccione...</option>
                                        <option value="">CC Cédula de Ciudadanía</option>
                                        <option value="">CE Cédula de Extranjería</option>
                                        <option value="">PA Pasaporte</option>
                                        <option value="">RC Registro Civil</option>
                                        <option value="">TI Tarjeta de Identidad</option>
                                        <!-- <option value="">Otro</option> -->
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-12">Número Documento<label class="text-danger">*</label></label>
                                    <input type="number" placeholder="No. de identificación" maxlength="12" class="form-control form-control-sm vnum" id="num_id_tec" placeholder="" required>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-12">Correo</label>
                                    <input id="correo" type="email" minlength="4" maxlength="100" class="form-control form-control-sm" placeholder="Correo electrónico" required>
                                    <p class="politica3 text-center mb-0 pb-0">Ej: test@gmail.com</p>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-12">Confirmar correo</label>
                                    <input id="emailConfirm" type="email" minlength="4" maxlength="100" class="form-control form-control-sm" placeholder="Confirmar Correo" required>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Contraseña</label>
                                    <input id="pass" type="password" maxlength="100" class="form-control form-control-sm" placeholder="Contraseña" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Confirmar contraseña</label>
                                    <input id="passConfirm" type="password" maxlength="100" class="form-control form-control-sm" placeholder="Confirmar Contraseña" required>
                                    <p class="politica3 text-center mb-0">La contraseña debe incluir números, mayúsculas, minúsculas y caracteres
                                        especiales como: * ? ! @ # $ / () { } = . , ; :</p>
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Rol</label>
                                    <select id="id_rol_FK" class="form-control form-control-sm" required="">
                                        <option selected value="" id="seleccione">Seleccione...</option>
                                        <?php foreach ($roles as $role) : ?>
                                            <option value="<?php echo $role->id_rol_PK ?>">
                                                <?php echo $role->nombre_rol ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-12">Teléfono</label>
                                    <input type="number" placeholder="Teléfono" maxlength="9" class="form-control form-control-sm vnum" rows="3" id="telefono" placeholder="">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-12">Dirección</label>
                                    <input class="form-control form-control-sm" placeholder="Dirección" rows="3" id="direccion_residencia" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Detalles</label>
                                    <textarea class="form-control form-control-sm" id="desc_tecnico" placeholder="Detalles"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 justify-content-center text-center">
                            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-sm btn-default shadow">
                                Cancelar
                            </button>

                            <button id="submitTech" type="submit" class="btn btn-sm ml-1 bg-gradient-success shadow">
                                Guardar
                            </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    vnum(12)
</script>
<!-- <script src="assets/js/layouts/swals.js"></script> -->
<script src="assets/js/general/technical.js"></script>