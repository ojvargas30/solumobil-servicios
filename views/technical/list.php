<div class="col-md-12 carta-modo-osc">
    <div class="row carta-modo-osc">
        <div class="col-md-12 carta-modo-osc">
            <div class="card carta-modo-osc overflow-auto">
                <div class="card-header carta-modo-osc align-middle d-block">
                    <span class="card-title text-sm">
                        <span class="carta-modo-osc h5 font-weight-bolder text-lightred">
                            Técnicos
                        </span>
                    </span>
                    <div class="card-tools mb-0 pb-0">
                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        <!-- <button class="btn btn-sm d-block m-0 mb-0 mt-3 ml-2 bg-gradient-red buttonAdd toastrDefaultInfo text-center" id="modalToggleTechAdmin">
                            <i class="fas fa-folder-plus fa-xs"></i> Nuevo Técnico
                        </button> -->
                        <a href="?c=technical&m=add" class="btn btn-sm d-block m-0 mb-0 mt-3 ml-2 bg-gradient-red buttonAdd toastrDefaultInfo mb-3 text-center mt-1 align-self-center" data-toggle="modal" data-target="#myModalLeft">
                            <i class="fas fa-folder-plus"></i> Nuevo
                        </a>
                    </div>
                </div>
                <div class="">
                    <div class="p-1 px-2">
                        <table class="shadow table listado bg-white border-dark border-left pr-0
                                        table-bordered table-active
                                         table-hover table-sm table-responsive-sm
                                         datatable">
                            <thead class="pr-0 text-light bg-gradient-red">
                                <tr>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"><small> #
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Nombre
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Correo
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Documento
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Contacto
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Domicilio
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Rol <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold">
                                            Estado
                                            <small>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="pr-0">
                                <?php $cont = 0;
                                ?>
                                <?php foreach ($technicians as $technical) : ?>
                                    <?php $cont++; ?>
                                    <tr class="shadow-sm text-dark" <?= (count($technicians) === $cont) ? "style='background:#f2f2f2'" : "" ?>>
                                        <td class="align-middle text-wrap texto-negro text-center">
                                            <span class="d-block">
                                                <?php echo $technical->id_tecnico_PK; ?>
                                            </span>
                                        </td>

                                        <td class="align-middle text-wrap text-center texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $technical->nombre_tecnico . '<div></div>' . $technical->apellido_tecnico; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-center texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $technical->tipo_doc_tec . '<div></div>' . $technical->num_id_tec; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-center texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $technical->correo; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-center texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $technical->telefono; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-center texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $technical->direccion_residencia; ?>
                                            </span>
                                        </td>

                                        <?php //echo $technical->desc_tecnico;
                                        ?>
                                        <td class="align-middle text-wrap texto-negro testado">
                                            <span class="d-block text-center float-none">
                                                <?php //echo $technical->nombre_rol;
                                                ?>
                                                <?php switch ($technical->id_rol_FK) {
                                                    case 1:
                                                ?>
                                                        <span class="right badge badge-success text-center">
                                                            <?php
                                                            echo "Técnico<div></div>";
                                                            echo "Administrador";
                                                            break;
                                                            ?>
                                                        </span>
                                                    <?php
                                                    case 2: ?>
                                                        <span class="right badge badge-info text-center">
                                                            <?php
                                                            echo 'Tecnico<div></div>';
                                                            echo "Empleado";
                                                            break;
                                                            ?>
                                                        </span>
                                                    <?php
                                                    case 3: ?>
                                                        <span class="right badge badge-warning text-center">
                                                            <?php
                                                            echo 'Cliente';
                                                            break;
                                                            ?>
                                                        </span>
                                                <?php } ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap texto-negro testado">
                                            <span class="d-block text-center float-none text-uppercase">

                                                <?php switch ($technical->estado_usu) {
                                                    case 1:
                                                ?>
                                                        <span class="right badge badge-success text-center">
                                                            <?php
                                                            echo "Activo";
                                                            break;
                                                            ?>
                                                        </span>
                                                    <?php
                                                    case 2: ?>
                                                        <span class="right badge badge-danger text-center">
                                                            <?php
                                                            echo "Inactivo";
                                                            break;
                                                            ?>
                                                        </span>
                                                <?php } ?>
                                            </span>
                                        </td>
                                    </tr>
                                <?php
                                endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<script>
    (function(d) {

        const verAuditoria = d.querySelectorAll(".verAuditoria");
        verAuditoria.forEach((p, i) => {
            p.addEventListener('click', (e) => {
                let o = e.target;
                if (o.value == "Audit") {
                    o.classList.remove("btn-default");
                    o.classList.add("btn-danger");
                    o.classList.add("mt-1");
                    o.value = "Ocultar";
                    d.getElementsByClassName("noVerTrAuditoria")[i].classList.remove("d-none");
                } else {
                    o.classList.remove("btn-danger");
                    o.classList.remove("mt-1");
                    o.classList.add("btn-default");
                    o.value = "Audit";
                    d.getElementsByClassName("noVerTrAuditoria")[i].classList.add("d-none");
                }
            });
        });

    })(document);
</script>