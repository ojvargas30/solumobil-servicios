<div class="card mt-3">
    <div class="card-header sectionEdit text-white">
        <h2 class="card-title">Actualización del Técnico N° <?php echo isset($id_tecnico_PK) ? $id_tecnico_PK : '' ?></h2>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row d-none">
            <div class="col-sm-12">
                <div class="form-group">
                    <input type="hidden" class="d-none" id="id_tecnico_PK" value="<?php echo $data[0]->id_tecnico_PK ?>">
                    <!-- <input type="hidden" class="d-none" id="id_revision_servicio_PK" value="<?php //echo $details[0]->id_revision_servicio_PK
                                                                                                    ?>"> -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
            <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre Técnico</label>
                            <input type="text" id="nombre_tecnico" class="form-control" value="<?php echo $data[0]->nombre_tecnico ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Apellido Técnico</label>
                            <input type="text" id="apellido_tecnico" class="form-control" value="<?php echo $data[0]->apellido_tecnico ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Tipo Documento del Técnico</label>
                    <select id="tipo_doc_tec" class="custom-select controlBuscador" required="">
                        <?php foreach ($technicians as $technical) { ?>
                            <option selected value="<?php echo $technical->id_tecnico_PK  ?>">
                                <?php echo $technical->tipo_doc_tec?>
                            </option>
                            <option>
                                <?php echo "T.I" ?>
                            </option>
                                <option>
                                <?php echo "C.C" ?>
                            </option>
                                <option>
                                <?php echo "Documento de Extranjería" ?>
                            </option>
                                <option>
                                <?php echo "Pasaporte" ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Número de Documento</label>
                            <input type="number" id="num_id_tec" class="form-control" value="<?php echo $data[0]->num_id_tec ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="number" id="telefono" class="form-control" value="<?php echo $data[0]->telefono ?>">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Dirección de Residencia</label>
                            <input type="text" id="direccion_residencia" class="form-control" value="<?php echo $data[0]->direccion_residencia ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Descripción del Técnico</label>
                            <input type="text" id="desc_tecnico" class="form-control" value="<?php echo $data[0]->desc_tecnico ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Foto del Técnico</label>
                            <input type="file" id="foto_tecnico" value="<?php echo $data[0]->foto_tecnico ?>" aria-rowspan="3">
                        </div>
                    </div>
                </div>

                <div class="col-sm-1 justify-content-center text-center">
                    <div class="form-group">
                        <button id="updateTech" type="submit" class="btn btn-outline-success mt-4">
                            <i class="fas fa-save fa-2x"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

    <script src="assets/js/general/technical.js"></script>
