<div id="myModalLeft" data-backdrop="static" data-keyboard="false" class="modal fade modalNew" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="text-left col-11 border-left">
                    <div class="row pl-2">
                        <h5 class="modal-title text-dark">
                            <strong>
                                GENERAR NUEVO CLIENTE
                            </strong>
                        </h5>
                        <h6 class="text-dark">Genere la información del cliente</h6>
                    </div>
                </div>
                <div class="col-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="card card-success mt-1 shadow">
                    <div class="card-header shadow">
                        <h2 class="card-title">Los campos en rojo son obligatorios</h2>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Barrio<label style="color: red;">*</label></label>
                                    <input type="text" maxlength="9" class="form-control" rows="3" id="id_barrio_FK" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label class="col-12">Tipo Documento del Cliente<label style="color: red;">*</label></label>
                                    <select id="tipo_doc_cli" class="custom-select" required="">
                                        <option selected value="" aria-readonly="">Seleccione...</option>
                                        <option value="">CC Cédula de Ciudadanía</option>
                                        <option value="">CE Cédula de Extranjería</option>
                                        <option value="">PA Pasaporte</option>
                                        <option value="">RC Registro Civil</option>
                                        <option value="">TI Tarjeta de Identidad</option>
                                        <option value="">Otro</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Nombre del Cliente<label style="color: red;">*</label><label style="color: red;">*</label></label>
                                    <input type="text" maxlength="50" class="form-control" rows="3" id="nombre_cliente" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Apellido del Cliente<label style="color: red;">*</label></label>
                                    <input id="apellido_cliente" type="text" minlength="4" maxlength="100" class="form-control" placeholder="" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Número de Documento<label style="color: red;">*</label></label>
                                    <input id="num_id_cli" minlength="8" type="number" class="form-control" placeholder="" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Teléfono<label style="color: red;">*</label></label>
                                    <input id="telefono" minlength="8" type="number" class="form-control" placeholder="" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Dirección de Residencia<label style="color: red;">*</label></label>
                                    <input class="form-control" rows="3" id="direccion_residencia" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-12">Foto del Técnico</label>
                                    <!-- <textarea class="form-control" rows="3" id="foto_tecnico" placeholder=""></textarea> -->
                                    <input type="file" class="for-control" id="foto_cliente" aria-rowspan="3">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 justify-content-center text-center">
                            <div class="form-group">
                                <button id="submitTech" type="submit" class="btn btn-outline-success shadow mt-5">
                                    <i class="fas fa-save fa-2x"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/general/technical.js"></script>
<script src="assets/js/ghost/clientGhost.js"></script>
<script src="assets/js/ghost/user.js"></script>
<script src="assets/js/ghost/district.js"></script>