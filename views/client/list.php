<div class="row mb-5 mx-1 h-100">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="card bg-gradient-white mt-4">
                <div class="card-header">
                    <h3 class="card-title">Clientes</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="card p-1 px-2 carta-modo-osc">
                        <h5 class="shadow card-header fondo-titulo text-uppercase text-dark pt-3 text-center mb-2 mt-1">
                            <span >
                                Clientes <img src="https://img.icons8.com/emoji/48/000000/star-struck.png" />
                            </span>
                            <small >
                                <p class="text-secondary text-sm"> Activos</p>
                            </small>
                        </h5>
                        <!-- <a href="?controller=client&method=add" class="btn bg-gradient-red buttonAdd toastrDefaultInfo mb-3 text-center mt-1 align-self-center" data-toggle="modal" data-target="#myModalLeft">
                            <i class="fas fa-user-plus fa-2x"></i>
                        </a> -->

                        <table class="shadow table listado bg-white border-dark border-left pr-0
                                        table-bordered table-active
                                         table-hover table-sm table-responsive-sm
                                         datatable">
                            <thead class="pr-0 text-light bg-gradient-red">
                                <tr>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"><small> #
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Nombre
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Correo
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Documento
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Contacto
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Domicilio
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Estado<small>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="pr-0">
                                <?php $cont = 0;
                                ?>
                                <?php foreach ($clients as $client) : ?>
                                    <?php $cont++; ?>
                                    <tr class="shadow-sm text-dark" <?= (count($clients) === $cont) ? "style='background:#f2f2f2'" : "" ?>>
                                        <td class="align-middle texto-negro text-sm text-center p-2">
                                            <span class="d-block text-center">
                                                <?php echo $client->id_cliente_PK; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-center texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php if ($client->foto == null) { ?>

                                                    <img src="assets/img/clihnicians/sin_foto.png" class="rounded-circle img-perfil" alt="Foto cliente" />
                                                    <div>

                                                    </div>

                                                <?php } elseif ($client->foto != null) { ?>

                                                    <img src="<?php echo $client->foto; ?>" class="rounded-circle img-perfil" alt="Foto cliente" />
                                                    <div>

                                                    </div>

                                                <?php } ?>
                                                <?php echo $client->nombre_cliente . '<div></div>' . $client->apellido_cliente; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-left texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $client->tipo_doc_cli . '<div></div>' . $client->num_id_cli; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-left texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $client->correo; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-left texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $client->telefono; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-left texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $client->direccion_residencia . '<div></div>' . $client->nombre_barrio; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap texto-negro testado">
                                            <span class="d-block text-center float-none text-uppercase">
                                                <?php switch ($client->estado_usu) {
                                                    case 1:
                                                ?>
                                                        <span class="right badge badge-success text-center">
                                                            <?php
                                                            echo "Activo";
                                                            break;
                                                            ?>
                                                        </span>
                                                    <?php
                                                    case 2: ?>
                                                        <span class="right badge badge-danger text-center">
                                                            <?php
                                                            echo "Inactivo";
                                                            break;
                                                            ?>
                                                        </span>
                                                <?php } ?>
                                            </span>
                                        </td>
                                    </tr>
                                <?php
                                endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script src="assets/js/mejoras/listJs.js"></script>