<div class="card mt-3">
    <div class="card-header sectionEdit text-white">
        <h2 class="card-title">Actualización del Técnico N° <?php echo isset($id_cliente_PK) ? $id_cliente_PK : '' ?></h2>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row d-none">
            <div class="col-sm-12">
                <div class="form-group">
                    <input type="hidden" class="d-none" id="id_cliente_PK" value="<?php echo $data[0]->id_cliente_PK ?>">
                    <!-- <input type="hidden" class="d-none" id="id_revision_servicio_PK" value="<?php //echo $details[0]->id_revision_servicio_PK
                                                                                                    ?>"> -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Barrio del Cliente</label>
                    <input type="text" id="nombre_barrio" class="form-control" value="<?php echo $data[0]->id_barrio_FK ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Tipo Documento</label>
                    <select id="tipo_doc_cli" class="custom-select controlBuscador" required="">
                        <?php foreach ($clients as $client) { ?>
                            <option selected value="<?php echo $client->id_cliente_PK  ?>">
                                <?php echo $client->tipo_doc_cli ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre Cliente</label>
                            <input type="text" id="nombre_cliente" class="form-control" value="<?php echo $data[0]->nombre_cliente ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Apellido Cliente</label>
                            <input type="text" id="apellido_cliente" class="form-control" value="<?php echo $data[0]->apellido_cliente ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Número de Documento</label>
                            <input type="number" id="num_id_cli" class="form-control" value="<?php echo $data[0]->num_id_cli ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="number" id="telefono" class="form-control" value="<?php echo $data[0]->telefono ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Dirección de Residencia</label>
                            <input type="text" id="direccion_residencia" class="form-control" value="<?php echo $data[0]->direccion_residencia ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Foto del Cliente</label>
                            <input type="file" id="foto_cliente" value="<?php //echo $data[0]->foto_cliente
                                                                        ?>" aria-rowspan="3">
                        </div>
                    </div>
                </div>

                <div class="col-sm-1 justify-content-center text-center">
                    <div class="form-group">
                        <button id="updateCli" type="submit" class="btn btn-outline-success mt-4">
                            <i class="fas fa-save fa-2x"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

    <script src="assets/js/general/technical.js"></script>