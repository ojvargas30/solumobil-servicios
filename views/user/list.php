<div class="row mb-5 mx-1 h-100 carta-modo-osc">
    <div class="col-md-12 carta-modo-osc">
        <div class="row carta-modo-osc">
            <div class="col-md-12 carta-modo-osc">
                <div class="card carta-modo-osc overflow-auto">
                    <div class="card-header carta-modo-osc align-middle d-block">
                        <span class="card-title text-sm">
                            <span class="carta-modo-osc h5 font-weight-bolder text-lightred">
                                Usuarios
                            </span>
                        </span>
                        <div class="card-tools mb-0 pb-0">
                            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="p-1 px-2">
                        <table class="shadow table listado bg-white pr-0
                                        table-bordered table-active
                                         table-hover table-sm table-responsive-sm
                                         datatable">
                            <thead class="pr-0 text-light bg-gradient-red">
                                <tr>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"><small> #
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold"> Correo
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold">
                                            Rol
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold">
                                            Estado
                                            <small>
                                    </th>
                                    <th scope="col" class="head-hover">
                                        <small class="font-weight-bold">
                                            Acciones<small>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="pr-0">
                                <?php $cont = 0;
                                ?>
                                <?php foreach ($users as $user) : ?>
                                    <?php $cont++; ?>
                                    <?php //if($user->id_usuario_PK != $_SESSION['user']['user']->id_usuario_PK){
                                    ?>

                                    <tr class="shadow-sm text-dark" <?= (count($users) === $cont) ? "style='background:#f2f2f2'" : "" ?>>
                                        <td class="align-middle text-wrap texto-negro text-center">
                                            <span class="d-block">
                                                <?php echo $user->id_usuario_PK; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap text-center texto-negro tcliente">
                                            <span class="d-block text-sm">
                                                <?php echo $user->correo; ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap texto-negro testado">
                                            <span class="d-block text-center float-none">
                                                <?php //echo $user->nombre_rol;
                                                ?>
                                                <?php switch ($user->id_rol_FK) {
                                                    case 1:
                                                ?>
                                                        <span class="right badge badge-success text-center">
                                                            <?php
                                                            echo "Técnico<div></div>";
                                                            echo "Administrador";
                                                            break;
                                                            ?>
                                                        </span>
                                                    <?php
                                                    case 2: ?>
                                                        <span class="right badge badge-info text-center">
                                                            <?php
                                                            echo 'Tecnico<div></div>';
                                                            echo "Empleado";
                                                            break;
                                                            ?>
                                                        </span>
                                                    <?php
                                                    case 3: ?>
                                                        <span class="right badge badge-warning text-center">
                                                            <?php
                                                            echo 'Cliente';
                                                            break;
                                                            ?>
                                                        </span>
                                                <?php } ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap texto-negro testado">
                                            <span class="d-block text-center float-none text-uppercase">

                                                <?php switch ($user->estado_usu) {
                                                    case 1:
                                                ?>
                                                        <span class="right badge badge-success text-center">
                                                            <?php
                                                            echo "Activo";
                                                            break;
                                                            ?>
                                                        </span>
                                                    <?php
                                                    case 2: ?>
                                                        <span class="right badge badge-danger text-center">
                                                            <?php
                                                            echo "Inactivo";
                                                            break;
                                                            ?>
                                                        </span>
                                                <?php } ?>
                                            </span>
                                        </td>
                                        <td class="align-middle text-wrap d-block text-sm text-center texto-negro tcliente">
                                            <?php if ($user->estado_usu == 1) { ?>
                                                <a href="?controller=user&method=updateUserStatus&id_usuario_PK=<?php echo $user->id_usuario_PK ?>&estado_usu=2">
                                                    <i class="fas fa-toggle-on fa-2x text-success"></i>
                                                </a>
                                            <?php } elseif ($user->estado_usu == 2) { ?>
                                                <a href="?controller=user&method=updateUserStatus&id_usuario_PK=<?php echo $user->id_usuario_PK ?>&estado_usu=1">
                                                    <i class="fas fa-toggle-off fa-2x text-danger"></i>
                                                </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php //}
                                endforeach; ?>
                            </tbody>
                        </table>
                        <h6 class="border-bottom p-2 text-muted politica">
                            <?php echo "Fecha de última modificación del fichero fuente: '" . strftime('%A %d de %B del %Y', strtotime(date("F d Y H:i:s.", getlastmod()))) . "'"; ?>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>