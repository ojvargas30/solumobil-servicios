<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" lang="es" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="Author" lang="es" content="Óscar Javier Vargas Diaz, oscarjaviervargas@hotmail.com">
    <meta name="DC.identifier" lang="es" content="">
    <!--Aqui va la pagina Solumovil.............................-->
    <META http-equiv="Expires" lang="es" content="0">
    <!--ESTA NOSE PARA QUE ES.-->
    <meta name="Keywords" lang="es" content="Engativa,Colombia, Bogota,
	servicio tecnico, localidad, Quitar Cuenta Google,
	reparacion, celulares, pantalla, dañada, puerto de carga, tablets, baratos,
	flasheo de celulares o tablets, cambio de pantalla, cambio de repuestos.">
    <META http-equiv="PICS-Label" content='(PICS-1.1 "https://www.gcf.org/v2.5"labels on "1994.11.05T08:15-0500"until "1995.12.31T23:59-0000"
	for "http://w3.org/PICS/Overview.html"ratings (suds 0.5 density 0 color/hue 1))'>
    <META name="copyright" content="&copy; 2020 Solumovil Company.">
    <meta name="Description" lang="es" content="Pagina de servicio profesional
	enfocada en el mantenimiento y reparacion de celulares en la ciudad de Bogota-Colombia.
	Servicio tecnico de moviles.">
    <META name="date" content="19:05:09, sábado 29, febrero 2020 -05">
    <meta name="generator" content="HTML-KIT 2.9" />
    <meta name="language" content="es" />
    <meta name="revisit-after" content="1 month" />
    <meta name="robots" content="index, follow" />
    <meta name="application-name" content="servicio tecnico web de reparacion de celulares" />
    <meta name="encoding" charset="utf-8" />
    <meta http-equiv=»X-UA-Compatible». />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="organization" content="Solumovil Company" />
    <meta name="revisit" content="7" />
    <noscript>
        <meta http-equiv="refresh" content="30; url=https://www.youtube.com/watch?v=XyW1XiNBsaQ">
    </noscript>

    <!----------------------------------------------------------------------------------->
    <!--TERMINA AQUI --------------------------------------------------------------META-->
    <script src="assets/js/general/methods.js"></script>

    <!-- Style -->
    <link rel="stylesheet" type="text/css" href="assets/css/adminlte.css">
    <link rel="stylesheet" type="text/css" href="assets/css/login.css">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/normalize.css">

    <!-- FONTAWESOME -->
    <link rel="stylesheet" href="assets/font/css/all.css">

    <!-- Favicon -->
    <link rel="Shortcut Icon" href="assets/img/1.ico" />

    <!-- SWAL -->
    <link rel="stylesheet" href="assets/plugins/swal/dist/sweetalert2.min.css">

    <link rel="stylesheet" href="assets/plugins/toastr/toastr.min.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Solumovil | Complete data</title>
</head>

<body>
    <noscript>
        <div class="position-fixed col-4 justify-content-md-start bg-danger rounded ml-3">
            <p class="text-light h6">Bienvenido al portal Solumovil</p>
            <p class="text-light h6">La página que estás
                viendo requiere para su funcionamiento el uso de JavaScript.
                Si lo has deshabilitado intencionalmente, por favor vuelve a activarlo.</p>
            <p class="text-light">Se te redigira a un tutorial en 30 segundos</p>
        </div>
    </noscript>

    <!-- Jquery -->
    <!-- <script src="assets/js/jquery-1.11.2.min.js"></script> -->
    <!-- <script src="assets/js/jquery.min.css"></script> -->

    <!-- SWEETALERT -->
    <!-- <script src="assets/plugins/swal/dist/sweetalert2.all.min.js"></script> -->

    <!-- Bootstrap JS-->

    <h1 class="text-center col-12 my-2">Completando tus datos</h1>

    <div id="formVertical" class="">
        <h3>Identificación</h3>
        <section>
            <div class="container h-100 d-flex align-items-center">
                <div class="row justify-content-center">
                    <div class="col-md-6 col-sm-12 col-lg-6">
                        <div class="row justify-content-center text-center">
                            <div class="form-group col-12">
                                <div class="form-group">
                                    <label for="nombre_cliente">Nombre</label>
                                    <input type="text" class="form-control vnum placeholder-left" placeholder="Ingrese su nombre" id="nombre_cliente" value="<?php echo isset($validateUser[0]->nombre_cliente) && $validateUser[0]->nombre_cliente != null ? $validateUser[0]->nombre_cliente : '' ?>">
                                </div>
                                <div class="form-group">
                                    <label for="apellido_cliente">Apellido</label>
                                    <input type="text" class="form-control vnum placeholder-left" placeholder="Ingrese su apellido" id="apellido_cliente" value="<?php echo isset($validateUser[0]->apellido_cliente) && $validateUser[0]->apellido_cliente != null ? $validateUser[0]->apellido_cliente : '' ?>">
                                </div>

                                <label for="tipo_doc_cli">Tipo de identificación</label>
                                <select class="custom-select form-control" id="tipo_doc_cli">
                                    <?php
                                    $arrIdTypes =
                                        [
                                            'RC - Registro Civil',
                                            'TI - Tarjeta de identidad',
                                            'CC - Cédula de ciudadanía',
                                            'CE - Cédula de extranjería',
                                            'PA - Pasaporte',
                                            'MS - Menor sin identificación',
                                            'AS - Adulto sin identidad'
                                        ];

                                    if ($validateData[0]->tipo_doc_cli != null) {

                                        foreach ($arrIdTypes as $idType) {
                                            if ($idType == $validateData[0]->tipo_doc_cli) {
                                    ?>
                                                <option selected>
                                                    <?php echo $idType; ?>
                                                </option>

                                            <?php } else { ?>

                                                <option>
                                                    <?php echo $idType; ?>
                                                </option>

                                        <?php
                                            }
                                        }
                                    } else {
                                        ?>

                                        <option value="0">Seleccione...</option>

                                        <?php
                                        foreach ($arrIdTypes as $idType) {
                                        ?>
                                            <option value="<?php echo $idType; ?>"><?php echo $idType; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-12">
                                <label for="num_id_cli">Número de identificación</label>
                                <input type="number" class="form-control vnum placeholder-left" placeholder="Ingrese número de identificación" id="num_id_cli" value="<?php echo isset($validateUser[0]->num_id_cli) && $validateUser[0]->num_id_cli != null ? $validateUser[0]->num_id_cli : '' ?>">
                                <legend class="text-muted text-sm">El número de identificación debe estar entre 8 y 12
                                    digitos</legend>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section>
        <h3>Domicilio</h3>
        <section>
            <div class="container h-100 d-flex align-items-center">
                <div class="row mb-3 justify-content-center text-center">
                    <div class="col-sm-12">
                        <label for="id_barrio_FK" class="col-12">Barrio</label>
                        <div class="d-flex form-group">
                            <select id="id_barrio_FK" class="form-control-sm d-inline custom-select selector2 controlBuscador" required="">
                                <option selected value="0" id="seleccione">Seleccione...</option>
                                <?php foreach ($districts as $district) : ?>
                                    <option value="<?php echo $district->id_barrio_PK ?>">
                                        <?php echo $district->nombre_barrio ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                            <button type="button" id="showBarrioSubForm" class="btn btn-default btn-sm d-inline border-0 shadow mx-1">Otro</button>
                            <div class="input-group subBarrioForm input-group-sm" id="subBarrioForm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Barrio</span>
                                </div>
                                <input id="nombre_barrio" type="text" class="form-control" placeholder="Ingrese nombre del barrio" required>
                                <button type="button" id="saveBarrio" class="btn btn-success btn-sm text-center" title="Guardar barrio">
                                    <i class="fas fa-save text-center"></i>
                                </button>

                                <script>
                                    // Asunto del barrio
                                    let btnNewBarrio = ele("saveBarrio");

                                    const send = (e) => {
                                        e.preventDefault();
                                        let barrio = ele("nombre_barrio");
                                        if (barrio.value.trim() != '') {
                                            let data = [
                                                barrio.value
                                            ];

                                            let options = {
                                                    method: 'POST',
                                                    body: "data=" + JSON.stringify(data),

                                                    headers: {
                                                        "Content-Type": "application/x-www-form-urlencoded"
                                                    }
                                                },
                                                url = "?c=district&m=saveDistrictProfile";

                                            fetch(url, options)
                                                .then(response => {
                                                    if (response.ok) return response.text()
                                                })
                                                .then(result => {
                                                    result = JSON.parse(result)
                                                    console.log(result);
                                                    // console.log(result);
                                                    console.log(result[0].id_barrio_PK);

                                                    if (result.length != 0) {

                                                        let select = document.getElementById("id_barrio_FK"),
                                                            opti = document.createElement("option");
                                                        opti.value = result[0].id_barrio_PK;
                                                        opti.textContent = result[0].nombre_barrio;
                                                        select.appendChild(opti);

                                                        // Seleccion
                                                        document.getElementById("seleccione").removeAttribute("selected");
                                                        opti.setAttribute("selected", "true");

                                                        //Auto clic
                                                        let clicBtnNew = document.getElementById('showBarrioSubForm');
                                                        clicBtnNew.click();

                                                    } else alert("Error, intentalo mas tarde")
                                                })
                                                .catch(e => console.log((e)));
                                        } else alert("Ingresa el nombre de tu barrio");

                                    }

                                    eve(btnNewBarrio, send);
                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="direccion_residencia">Dirección</label>
                            <input type="text" class="form-control placeholder-left" placeholder="Ingrese dirección de residencia" id="direccion_residencia" value="<?php echo isset($validateUser[0]->direccion_residencia) && $validateUser[0]->direccion_residencia != null ? $validateUser[0]->direccion_residencia : '' ?>">
                        </div>
                    </div>
                </div>
            </div>

            <script>
                ele('showBarrioSubForm').addEventListener('click', () => ele("subBarrioForm").classList.toggle("subBarrioForm"))
            </script>
        </section>

        <h3>Contacto</h3>
        <section>
            <div class="row justify-content-center text-center h-100 d-flex align-items-center">
                <div class="col-6">
                    <label for="telefono">Teléfono</label>
                    <input type="number" class="form-control vnum placeholder-left" placeholder="Ingrese su teléfono de contacto" id="telefono" value="<?php echo isset($validateUser[0]->telefono) && $validateUser[0]->telefono != null ? $validateUser[0]->telefono : '' ?>">
                    <legend class="text-muted text-sm">El teléfono debe estar entre 7 y 16 digitos</legend>
                </div>
            </div>
        </section>

        <script>
            function vnum(maxle) {
                let input = document.querySelectorAll('.vnum');
                if (input) {
                    input.forEach(e => {
                        e.addEventListener('input', (e) => {
                            if (e.target.value.length > maxle) e.target.value = e.target.value.slice(0, maxle);
                        })
                    });
                }
            }
            vnum(16);
        </script>
    </div>

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
    <script src="assets/js/popper.min.js"></script>

    <script>
        $("#formVertical").steps({
            labels: {
                current: "current step:",
                pagination: "Pagination",
                finish: "Finalizar",
                next: "Siguiente",
                previous: "Anterior",
                loading: "Cargando ..."
            },
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: "vertical",
            onFinishing: (event, currentIndex) => {

                if (
                    ele('nombre_cliente').value != '' &&
                    ele('apellido_cliente').value != '' &&
                    ele("tipo_doc_cli").value != "0" &&
                    ele('num_id_cli').value != '' &&
                    ele("id_barrio_FK").value != "0" &&
                    ele('direccion_residencia').value != '' &&
                    ele('telefono').value != ''
                ) {
                    if (!ele('telefono').value.length < 7 || !ele('telefono').value.length > 16) {
                        if (!ele('num_id_cli').value.length < 8 || !ele('num_id_cli').value.length > 12) {
                            let data = [
                                id_cliente_PK = "<?php echo $validateData[0]->id_cliente_PK; ?>",
                                nombre_cliente = ele('nombre_cliente').value,
                                apellido_cliente = ele('apellido_cliente').value,
                                tipo_doc_cli = ele("tipo_doc_cli").value,
                                num_id_cli = ele('num_id_cli').value,
                                id_barrio_FK = ele("id_barrio_FK").value,
                                direccion_residencia = ele('direccion_residencia').value,
                                telefono = ele('telefono').value
                            ];

                            console.log(data);
                            let options = {
                                    method: 'POST',
                                    body: "data=" + JSON.stringify(data),

                                    headers: {
                                        "Content-Type": "application/x-www-form-urlencoded"
                                    }
                                },
                                url = "?c=client&m=update";

                            fetch(url, options)
                                .then(response => {
                                    if (response.ok) return response.text()
                                })
                                .then(result => {
                                    console.log(result);
                                    if (result.length != 0) location.href = "?c=dash&m=index";
                                    else if (result == '"bien"') location.href = "?c=dash&m=index"
                                    else alert("Error, intentalo mas tarde")
                                })
                                .catch(e => reject(e));
                        } else alert("El número de identificación debe estar entre 8 y 12 digitos")
                    } else alert("El teléfono debe estar entre 7 y 16 digitos")
                } else alert("Por favor completa todos los campos del formulario")
            }
        });
    </script>


</body>

</html>