<div class="row mb-5 mx-1 h-100 carta-modo-osc">
    <div class="col-md-12 carta-modo-osc">
        <div class="row carta-modo-osc">
            <div class="col-md-12 carta-modo-osc">
                <div class="card carta-modo-osc overflow-auto">
                    <div class="card-header carta-modo-osc align-middle d-block">
                        <span class="card-title text-sm">
                            <span class="carta-modo-osc h5 font-weight-bolder text-lightred">
                                Historial de accesos de usuarios
                            </span>
                        </span>
                        <div class="card-tools mb-0 pb-0">
                            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="">
                        <div class="p-1 px-2">
                            <table class="shadow table listado bg-white border-dark border-left pr-0
                                        table-bordered table-active
                                         table-hover table-sm table-responsive
                                         datatable">
                                <thead class="pr-0 text-light bg-gradient-red">
                                    <tr>
                                        <th scope="col" class="head-hover">
                                            <small class="font-weight-bold"><small> #
                                        </th>
                                        <th scope="col" class="head-hover">
                                            <small class="font-weight-bold"> Nombre
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover">
                                            <small class="font-weight-bold">
                                                Rol
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover">
                                            <small class="font-weight-bold">
                                                Identidad
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover">
                                            <small class="font-weight-bold" title="Fecha de encuentro definida">
                                                Último Acceso
                                                <small>
                                        </th>
                                        <th scope="col" class="head-hover">
                                            <small class="font-weight-bold">
                                                Total de accesos<small>
                                        </th>
                                        <th scope="col" class="head-hover">
                                            <small class="font-weight-bold">
                                                Dispositivo<small>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="pr-0">
                                    <?php $cont = 0;
                                    // dd($records);
                                    ?>
                                    <?php foreach ($records as $record) : ?>
                                        <?php $cont++; ?>

                                        <tr class="shadow-sm text-dark" <?= (count($records) === $cont) ? "style='background:#f2f2f2'" : "" ?>>
                                            <td class="align-middle text-wrap texto-negro text-center">
                                                <span class="d-block">
                                                    <?php echo $record->id_record_PK; ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-center texto-negro tcliente">
                                                <span class="d-block text-sm">
                                                    <?php echo $record->username; ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap texto-negro testado">
                                                <span class="d-block text-center float-none text-uppercase">

                                                    <?php switch ($record->userrole) {
                                                        case 1:
                                                    ?>
                                                            <span class="right badge badge-success text-center">
                                                                <?php
                                                                echo "Técnico<div></div>";
                                                                echo "Administrador";
                                                                break;
                                                                ?>
                                                            </span>
                                                        <?php
                                                        case 2: ?>
                                                            <span class="right badge badge-info text-center">
                                                                <?php
                                                                echo "Técnico<div></div>";
                                                                echo "Empleado";
                                                                break;
                                                                ?>
                                                            </span>
                                                        <?php
                                                        case 3: ?>
                                                            <span class="right badge badge-warning text-center">
                                                                <?php
                                                                echo "Cliente";
                                                                break;
                                                                ?>
                                                            </span>
                                                    <?php } ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-center texto-negro tcliente">
                                                <span class="d-block text-sm">
                                                    <?php echo $record->useridentify; ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-left texto-negro tpeticion">
                                                <span class="d-block text-sm">
                                                    <?php
                                                    $fecha = $record->accessdate;
                                                    $fecha = ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
                                                    echo utf8_encode($fecha);
                                                    ?>
                                                    <span class="float-none">
                                                        <span class="font-weight-bolder">
                                                            <?php
                                                            $vars = array();
                                                            $time = new DateTime(parse_str($record->accessdate, $vars));
                                                            echo $time->format('g:ia');
                                                            ?>
                                                        </span>
                                                    </span>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-center texto-negro tcliente">
                                                <span class="d-block text-sm">
                                                    <?php echo $record->totalaccess; ?>
                                                </span>
                                            </td>
                                            <td class="align-middle text-wrap text-center texto-negro tcliente">
                                                <span class="d-block text-sm">
                                                    <?php echo $record->device; ?>
                                                </span>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <h6 class="border-bottom p-2 text-muted politica">
                                <?php echo "Fecha de última modificación del fichero fuente: '" . strftime('%A %d de %B del %Y', strtotime(date("F d Y H:i:s.", getlastmod()))) . "'"; ?>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>